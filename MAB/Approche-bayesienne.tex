\documentclass[11pt]{beamer}

\usepackage{pgf}
\usepackage{tikz}

\usepackage[pdf]{pstricks}
\usepackage{auto-pst-pdf}
\usepackage{pst-node,pst-text,pst-3d,pst-tree}	

%\usepackage{multimedia}
%\usepackage{marvosym}
%\usepackage{booktabs}

%\usepackage{wasysym}

\mode<presentation> {
  \usetheme{Boadilla}

  %\setbeamercovered{transparent}  
}

\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}

\usepackage[french]{babel}

\usepackage{times}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc}

%\usepackage{caption}
\usepackage{mathtools}

%To prevent autospace for http address
\NoAutoSpaceBeforeFDP

%Titre
\title[Approche bayesienne en bioinformatique] 
{Approche bayesienne en bioinformatique}

\author[Thomas Faraut] {Thomas Faraut, Simon de Givry et Thomas Schiex}

%coordonées
\institute[INRA Toulouse] 
{
  Laboratoire de génétique cellulaire\\
  Laboratoire MIAT \\
  INRA Toulouse
}

\date[17 octobre 2013] 
{17 octobre 2013 / M2R informatique}

%-----------------------------
%Définition de couleurs
%-----------------------------

\definecolor{darkblue}{RGB}{0,0,50} % bleu nuit
\definecolor{MyBlue}{rgb}{0.3,0.3,.8}

\setbeamercolor{sequence}{fg=black,bg=lgreen}%
\setbeamercolor{alerted text}{fg=blue}

%Redéfinition de la part page
\setbeamertemplate{part page}
{
  \begin{centering}
    {\usebeamerfont{part name}\usebeamercolor[fg]{part name}}
    \vskip1em\par
    \begin{beamercolorbox}[sep=16pt,center]{part title}
      \usebeamerfont{part title}\insertpart\par
    \end{beamercolorbox}
  \end{centering}
}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Approche bayesienne}

\part{Aproche fréquentiste {\em vs} bayesienne}
\frame{\partpage}

%---------------------------------------------------------
\begin{frame}
  \frametitle{Approche fréquentiste}

Les modèles probabilistes que nous avons présentés on été utilisés essentiellement pour obtenir des estimations ponctuelles dans une démarche fréquentiste:

\begin{itemize}
\item Les paramètres des modèles probabilistes sont estimés à l'aide de méthodes tels que le maximum de vraisemblance
\item L'incertitude associée à ces estimations peut éventuellement être mesurée (comportement aléatoire de l'estimateur vis à vis du processus d'échantillonnage)  
\end{itemize}

\end{frame}

%---------------------------------------------------------
\begin{frame}
  \frametitle{Exemples d'application}

\begin{itemize}
\item {\bf cartographie}: la carte optimale (on obtient également des estimations des paramètres de nuisances)
\item {\bf alignement}: l'alignement optimal (la solution au problème d'alignement)
\item {\bf segmentation}: décomposition d'une séquence d'ADN en fonction de la composition en nucléotides
\end{itemize}

\end{frame}

%---------------------------------------------------------
\begin{frame}
  \frametitle{exemple : la cartographie génétique}
\small

Déterminer l'ordre des marqueurs $(M_1,M_2,\ldots,M_n)$ à partir des données de ségrégation.

{\em Modèle probabiliste}
\begin{itemize}
\item $\pi$ = $(M_{j_1},M_{j_2},\ldots,M_{j_n})$ 
\item fréquences de recombinaison $(\theta_{j_1},\ldots,\theta_{j_{n-1}})$
\end{itemize}
{\em Données}
\begin{itemize}
\item les allèles des individus $I_i$ aux marqueurs $M_j$, $X_i^j$ et les recombinaisons associées $n_{j_k}$ 
\end{itemize}
La vraisemblance des observations s'écrit 

$$L(\pi, \theta ; X) = \prod_{k=1}^{n} \theta_{j_k}^{n_r^{j_k}} (1 - \theta_{j_k})^{n-n_r^{j_k}}$$

et l'on s'intéresse à l'ordre qui maximise la vraisemblance

$$\hat\pi = \mathop{argmax}_{\pi}( L(\pi, \theta ; X) )$$

\end{frame}

%---------------------------------------------------------
\begin{frame}
  \frametitle{Approche bayesienne : principe}

\begin{itemize}
\item Toutes les quantités (paramètres $\theta$, données $Y_{obs}$) sont caractérisées par des distributions de probabilité. On associe aux paramètres $\theta$ des lois de probabilité {\em a priori}
$$\pi(\theta)$$
\item Modèle probabiliste complet: probabilité conjointe d'observer les données et les paramètres associés
$$P( Y_{obs}, \theta )$$
\item On synthétise l'information associée aux paramètres (apportée par les données) par des lois de probabilités {\em a posteriori}
$$P( \theta | Y_{obs} )$$

\end{itemize}

\end{frame}

%---------------------------------------------------------
\begin{frame}
  \frametitle{Approche bayesienne : principe}

A l'aide de la règle de Bayes

\[ P( Y_{obs}, \theta ) =  P(  Y_{obs} | \theta )  \pi(\theta)  \]

{\small
{\hspace{10mm} }$P( Y_{obs} | \theta )$ est la vraisemblance également noté $L( \theta; Y_{obs})$ }
\vspace{2mm}

La démarche bayesienne consiste à calculer 

\begin{spreadlines}{1em}
\begin{align*}
 P( \theta | Y_{obs} ) &= \frac{ P(  Y_{obs} | \theta )  \pi(\theta)}{ P(  Y_{obs} )} \\
                       &=  \frac{ P(  Y_{obs} | \theta )  \pi(\theta)}{ \int_{\theta} P(  Y_{obs} | \theta )  \pi(\theta) d\theta} \\
                       &\propto  P(  Y_{obs} | \theta )  \pi(\theta)
\end{align*}
\end{spreadlines}
 
\end{frame}

%---------------------------------------------------------
\begin{frame}
  \frametitle{Approche bayesienne : principe}

En présence de plusieurs paramètres $\theta=(\theta_1,\theta_2)$ on peut souhaiter connaître la distribution a posteriori du seul paramètre $\theta_1$ :

\begin{spreadlines}{1em}
\begin{align*}
 P( \theta_1 | Y_{obs} ) &=  \frac{ \int_{\theta_2} P(  Y_{obs} | \theta )  \pi(\theta_1,\theta_2) d\theta_2}{ \int_{\theta_1} \int_{\theta_2} P(  Y_{obs} | \theta )  \pi(\theta) d\theta_2d\theta_1} \\                      
\end{align*}
\end{spreadlines}
 
\end{frame}


%---------------------------------------------------------
\begin{frame}
  \frametitle{exemple : Séquence dans un alphabet de deux lettres}
\small

On considère une séquence de $n$ tirages, avec remplacement, dans une urne contenant deux types de lettres $A$ et $T$, on s'intéresse à la fréquence,  $\theta$, de la lettre $A$. La vraisemblance des observations s'écrit

$$ L( \theta; Y_{obs}) = P( Y_{obs} | \theta ) = \theta^{n_A}(1-\theta)^{n_T}$$

\begin{center}
\input{include/boite1_alleles.tex}
\end{center}

\end{frame}

\begin{frame}
  \frametitle{exemple : Séquence}

Distribution a priori de $\theta$ : loi bêta de paramètres $\alpha$ et $\beta$

\begin{align*}
 \pi(\theta) &= \frac{\Gamma(\alpha+\beta)}{\Gamma(\alpha)\Gamma(\beta)} \theta^{\alpha-1}(1-\theta)^{\beta-1} 
\shortintertext{avec}
\Gamma(x) &= \int_0^{\infty} t^{x-1}e^{-t} dt 
\end{align*}
on a en particulier
$$\int_0^1  \theta^{\alpha-1}(1-\theta)^{\beta-1} = \frac{\Gamma(\alpha)\Gamma(\beta)}{\Gamma(\alpha+\beta)}$$

\end{frame}

\begin{frame}
  \frametitle{La loi bêta $B(\alpha,\beta)$}

\begin{center}
\includegraphics[width=0.6\textwidth]{800px-Beta_distribution_pdf}
\end{center}

\end{frame}

\begin{frame}
  \frametitle{Exemple : séquence}
\small

La probabilité conjointe des données et du paramètre $\theta$ s'écrit

\begin{align*}
P(Y_{obs}, \theta ) &=  P(  Y_{obs} | \theta )  \pi(\theta)  \\
                    &= \frac{\Gamma(\alpha+\beta)}{\Gamma(\alpha)\Gamma(\beta)} \theta^{n_A+\alpha-1}(1-\theta)^{n_T+\beta-1}
\intertext{la loi marginale}
P(Y_{obs})  &= \frac{\Gamma(\alpha+\beta)}{\Gamma(\alpha)\Gamma(\beta)}  \int_0^1 \theta^{n_A+\alpha-1}(1-\theta)^{n_T+\beta-1} d\theta_1 \\     
            &= \frac{\Gamma(\alpha+\beta)}{\Gamma(\alpha)\Gamma(\beta)}  \frac{\Gamma(n_A+\alpha)\Gamma(n_T+\beta)}{\Gamma(n+\alpha+\beta)}
\intertext{et la distribution a posteriori}
P(\theta | Y_{obs})  &= \frac{P(  Y_{obs}, \theta )}{P(Y_{obs})} = \frac{\Gamma(n+\alpha+\beta)}{\Gamma(n_A+\alpha)\Gamma(n_T+\beta)}\theta^{n_A+\alpha-1}(1-\theta)^{n_T+\beta-1}
\end{align*} 

\end{frame}

\begin{frame}
  \frametitle{exemple : Séquence dans un alphabet de deux lettres}
\small
Il est facile de généraliser à un alphabet de $D$ lettres, où chaque lettre a une probailité $\theta_d$ pour $d=1,\ldots,D$.

La loi marginale s'écrit alors
\begin{align*}
P(Y_{obs}) &=  \frac{\Gamma(\sum_d\alpha_d)}{\prod_d\Gamma( n_d + \alpha_d)} \frac{\prod_d\Gamma(n_d+\alpha_d)}{\Gamma( n +\sum_d\alpha_d)}   \\
\intertext{où $\alpha_d$ sont les paramètres de la loi {\em a priori} (une loi de Dirichlet) et $n_d$ les comptages. La distribution a posteriori s'écrit}
P( \theta | Y_{obs}) &=  \frac{\Gamma( \sum_d (n +\sum_d\alpha_d))}{\prod_d\Gamma(n_d+\alpha_d)} \prod_d \theta_d^{n_d +\alpha_d +1}
\end{align*}

\end{frame}

\begin{frame}
  \frametitle{exemple : Séquence avec deux boites}
\small

On considère maintenant qu'il y a deux boites et que l'on commence par tirer les $K$ premières lettres dans une boite 
et les $n-K$ suivantes dans l'autre. La vraimsenblance des observations s'écrit
$$ L( \theta_1,\theta_2; Y_{obs}, K = k) = \theta_1^{n^1_A}(1-\theta_1)^{n^1_T}\theta_2^{n^2_A}(1-\theta_2)^{n^2_T}g(k)$$


\begin{center}
\begin{tabular}{cc}
\input{include/boite2_alleles.tex} & \input{include/boite1_alleles.tex} \\
\end{tabular}
\end{center}

\end{frame}

\begin{frame}
    \frametitle{exemple : Séquence avec deux boites}
\small
La loi de probabilité jointe s'écrit
\begin{align*}
P(\theta_1, \theta_2, Y_{obs}, K=k) &= L( \theta_1,\theta_2; Y_{obs}, K = k) \pi( \theta_1, \theta_2) g(k) \\
                                    &= g(k) \prod_{i=1}^2 \frac{\Gamma(\alpha_i+\beta_i)}{\Gamma(\alpha_i)\Gamma(\beta_i)} \theta_i^{n_A^i+\alpha_i-1}(1-\theta_i)^{n_T^i+\beta_i-1}
                                    \intertext{et l'on peut obtenir la loi marginale du point de rupture}
P( K=k, Y_{obs} ) &= g(k) \int \int P(\theta_1, \theta_2, Y_{obs}, K=k) d\theta_1 d\theta_2\\
                  &= g(k) \prod_{i=1}^2 \frac{\Gamma(\alpha_i+\beta_i)}{\Gamma(\alpha_i)\Gamma(\beta_i)} \frac{\Gamma(n_A^i\alpha_i)\Gamma(n_T^i\beta_i)}{\Gamma(n_A^i\alpha_i+n_T^i\beta_i)} 
\shortintertext{en sommant sur les points de rupture possibles}
P(Y_{obs}) &= \sum_k  P( K=k, Y_{obs} )  
 \shortintertext{et}
P( K=k | Y_{obs} ) &=  \frac{P( K=k, Y_{obs} )}{P(Y_{obs})}
\end{align*}

\end{frame}

\begin{frame}
    \frametitle{exemple : Séquence avec deux boites}
\small

Approche par échantillonage de Gibbs
\begin{itemize}
\item Fixer $K=k$ et $\theta_2$, tirer $\theta_1$ dans sa distribuition postéireure conditionnelle 
$$P(\theta_1 | K=k, \theta_2, Y_{obs} ) = P(\theta_1 | K=k, Y_{obs} ) = Beta( \theta_1 ; n_A^1 + \alpha_1,  n_T^1 + \beta_1 )$$
\item Fixer $K=k$ et $\theta_1$, tirer $\theta_2$ dans sa distribuition postéireure conditionnelle 
$$P(\theta_2 | K=k, \theta_1, Y_{obs} ) = P(\theta_2 | K=k, Y_{obs} ) = Beta( \theta_2 ; n_A^2 + \alpha_2,  n_T^2 + \beta_2 )$$
\item Finalement, avec $\theta_1$ et $\theta_2$ fixés tirer $K$ dans
$$P(K=k |\theta_1 \theta_2, Y_{obs} ) \propto \theta_1^{n^1_A}(1-\theta_1)^{n^1_T}\theta_2^{n^2_A}(1-\theta_2)^{n^2_T}g(k)$$
\end{itemize}
On calcule le terme de gauche pour toutes les valeurs de $k=0,\ldots,n$ et l'on normalise par la somme.

\end{frame}

\begin{frame}
    \frametitle{exemple : Séquence avec deux boites}
\small

\begin{center}
\includegraphics[width=0.8\textwidth]{Fig/Bioinformatics-1999-Liu-Figure6}
\end{center}

\end{frame}

\end{document}
