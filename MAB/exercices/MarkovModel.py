# This code is largely inspired by the MarkovModel module of the Biopython distribution 
# see http://biopython.org/DIST/docs/api/Bio.HMM-module.html
#

"""Deal with representations of Markov Models.
"""

from __future__ import division

# standard modules
import copy
import math
import random
import numpy as np
import operator

from Bio.Seq import MutableSeq
from collections import defaultdict

from termcolor import colored

def _gen_random_array(n):
    """ Return an array of n random numbers, where the elements of the array sum
    to 1.0"""
    randArray = [random.random() for i in range(n)]
    total = sum(randArray)
    normalizedRandArray = [x/total for x in randArray]

    return normalizedRandArray


def _calculate_emissions(emission_probs):
    """Calculate which symbols can be emitted in each state
    """
    # loop over all of the state-symbol duples, mapping states to
    # lists of emitted symbols
    emissions = dict()
    for state, symbol in emission_probs:
        try:
            emissions[state].append(symbol)
        except KeyError:
            emissions[state] = [symbol]

    return emissions


def _calculate_from_transitions(trans_probs):
    """Calculate which 'from transitions' are allowed for each state

    This looks through all of the trans_probs, and uses this dictionary
    to determine allowed transitions. It converts this information into
    a dictionary, whose keys are source states and whose values are
    lists of destination states reachable from the source state via a
    transition.
    """
    transitions = dict()
    for from_state, to_state in trans_probs:
        try:
            transitions[from_state].append(to_state)
        except KeyError:
            transitions[from_state] = [to_state]

    return transitions


def _calculate_to_transitions(trans_probs):
    """Calculate which 'to transitions' are allowed for each state

    This looks through all of the trans_probs, and uses this dictionary
    to determine allowed transitions. It converts this information into
    a dictionary, whose keys are destination states and whose values are
    lists of source states from which the destination is reachable via a
    transition.
    """
    transitions = dict()
    for from_state, to_state in trans_probs:
        try:
            transitions[to_state].append(from_state)
        except KeyError:
            transitions[to_state] = [from_state]

    return transitions




class MarkovModelBuilder(object):
    """Interface to build up a Markov Model.

    This class is designed to try to separate the task of specifying the
    Markov Model from the actual model itself. This is in hopes of making
    the actual Markov Model classes smaller.

    So, this builder class should be used to create Markov models instead
    of trying to initiate a Markov Model directly.
    """
    # the default pseudo counts to use
    DEFAULT_PSEUDO = 1

    def __init__(self, state_alphabet, emission_alphabet):
        """Initialize a builder to create Markov Models.

        Arguments:

        o state_alphabet -- An alphabet containing all of the letters that
        can appear in the states

        o emission_alphabet -- An alphabet containing all of the letters for
        states that can be emitted by the HMM.
        """
        self._state_alphabet = state_alphabet
        self._emission_alphabet = emission_alphabet

        # probabilities for the initial state, initialized by calling
        # set_initial_probabilities (required)
        self.initial_prob = {}

        # the probabilities for transitions and emissions
        # by default we have no transitions and all possible emissions
        self.transition_prob = {}
        self.emission_prob = self._all_blank(state_alphabet,
                                             emission_alphabet)

        # the default pseudocounts for transition and emission counting
        self.transition_pseudo = {}
        self.emission_pseudo = self._all_pseudo(state_alphabet,
                                                emission_alphabet)

    def _all_blank(self, first_alphabet, second_alphabet):
        """Return a dictionary with all counts set to zero.

        This uses the letters in the first and second alphabet to create
        a dictionary with keys of two tuples organized as
        (letter of first alphabet, letter of second alphabet). The values
        are all set to 0.
        """
        all_blank = {}
        for first_state in first_alphabet.letters:
            for second_state in second_alphabet.letters:
                all_blank[(first_state, second_state)] = 0

        return all_blank

    def _all_pseudo(self, first_alphabet, second_alphabet):
        """Return a dictionary with all counts set to a default value.

        This takes the letters in first alphabet and second alphabet and
        creates a dictionary with keys of two tuples organized as:
        (letter of first alphabet, letter of second alphabet). The values
        are all set to the value of the class attribute DEFAULT_PSEUDO.
        """
        all_counts = {}
        for first_state in first_alphabet.letters:
            for second_state in second_alphabet.letters:
                all_counts[(first_state, second_state)] = self.DEFAULT_PSEUDO

        return all_counts

    def get_markov_model(self):
        """Return the markov model corresponding with the current parameters.

        Each markov model returned by a call to this function is unique
        (ie. they don't influence each other).
        """

        # user must set initial probabilities
        if not self.initial_prob:
            raise Exception("set_initial_probabilities must be called to " +
                            "fully initialize the Markov model")

        initial_prob = copy.deepcopy(self.initial_prob)
        transition_prob = copy.deepcopy(self.transition_prob)
        emission_prob = copy.deepcopy(self.emission_prob)
        transition_pseudo = copy.deepcopy(self.transition_pseudo)
        emission_pseudo = copy.deepcopy(self.emission_pseudo)

        state_alphabet = self._state_alphabet
        emission_alphabet = self._emission_alphabet

        return HiddenMarkovModel(state_alphabet, emission_alphabet, initial_prob, transition_prob, emission_prob,
                                 transition_pseudo, emission_pseudo)

    def set_initial_probabilities(self, initial_prob):
        """Set initial state probabilities.

        initial_prob is a dictionary mapping states to probabilities.
        Suppose, for example, that the state alphabet is ['A', 'B']. Call
        set_initial_prob({'A': 1}) to guarantee that the initial
        state will be 'A'. Call set_initial_prob({'A': 0.5, 'B': 0.5})
        to make each initial state equally probable.

        This method must now be called in order to use the Markov model
        because the calculation of initial probabilities has changed
        incompatibly; the previous calculation was incorrect.

        If initial probabilities are set for all states, then they should add up
        to 1. Otherwise the sum should be <= 1. The residual probability is
        divided up evenly between all the states for which the initial
        probability has not been set. For example, calling
        set_initial_prob({}) results in P('A') = 0.5 and P('B') = 0.5,
        for the above example.
        """
        self.initial_prob = copy.copy(initial_prob)

        # ensure that all referenced states are valid
        for state in initial_prob:
            assert state in self._state_alphabet.letters, \
                   "State %s was not found in the sequence alphabet" % state

        # distribute the residual probability, if any
        num_states_not_set =\
            len(self._state_alphabet.letters) - len(self.initial_prob)
        if num_states_not_set < 0:
            raise Exception("Initial probabilities can't exceed # of states")
        prob_sum = sum(self.initial_prob.values())
        if prob_sum > 1.0:
            raise Exception("Total initial probability cannot exceed 1.0")
        if num_states_not_set > 0:
            prob = (1.0 - prob_sum) / num_states_not_set
            for state in self._state_alphabet.letters:
                if not state in self.initial_prob:
                    self.initial_prob[state] = prob

    def set_equal_probabilities(self):
        """Reset all probabilities to be an average value.

        Resets the values of all initial probabilities and all allowed
        transitions and all allowed emissions to be equal to 1 divided by the
        number of possible elements.

        This is useful if you just want to initialize a Markov Model to
        starting values (ie. if you have no prior notions of what the
        probabilities should be -- or if you are just feeling too lazy
        to calculate them :-).

        Warning 1 -- this will reset all currently set probabilities.

        Warning 2 -- This just sets all probabilities for transitions and
        emissions to total up to 1, so it doesn't ensure that the sum of
        each set of transitions adds up to 1.
        """

        # set initial state probabilities
        new_initial_prob = float(1) / float(len(self.transition_prob))
        for state in self._state_alphabet.letters:
            self.initial_prob[state] = new_initial_prob

        # set the transitions
        new_trans_prob = float(1) / float(len(self.transition_prob))
        for key in self.transition_prob:
            self.transition_prob[key] = new_trans_prob

        # set the emissions
        new_emission_prob = float(1) / float(len(self.emission_prob))
        for key in self.emission_prob:
            self.emission_prob[key] = new_emission_prob

    def set_random_initial_probabilities(self):
        """Set all initial state probabilities to a randomly generated distribution.
        Returns the dictionary containing the initial probabilities.
        """
        initial_freqs = _gen_random_array(len(self._state_alphabet.letters))
        for state in self._state_alphabet.letters:
            self.initial_prob[state] = initial_freqs.pop()

        return self.initial_prob

    def set_random_transition_probabilities(self):
        """Set all allowed transition probabilities to a randomly generated distribution.
        Returns the dictionary containing the transition probabilities.
        """

        if not self.transition_prob:
            raise Exception("No transitions have been allowed yet. " +
                            "Allow some or all transitions by calling " +
                            "allow_transition or allow_all_transitions first.")

        transitions_from = _calculate_from_transitions(self.transition_prob)
        for from_state in transitions_from:
            freqs = _gen_random_array(len(transitions_from[from_state]))
            for to_state in transitions_from[from_state]:
                self.transition_prob[(from_state, to_state)] = freqs.pop()

        return self.transition_prob

    def set_random_emission_probabilities(self):
        """Set all allowed emission probabilities to a randomly generated
        distribution.  Returns the dictionary containing the emission
        probabilities.
        """

        if not self.emission_prob:
            raise Exception("No emissions have been allowed yet. " +
                            "Allow some or all emissions.")

        emissions = _calculate_emissions(self.emission_prob)
        for state in emissions:
            freqs = _gen_random_array(len(emissions[state]))
            for symbol in emissions[state]:
                self.emission_prob[(state, symbol)] = freqs.pop()

        return self.emission_prob

    def set_random_probabilities(self):
        """Set all probabilities to randomly generated numbers.

        Resets probabilities of all initial states, transitions, and
        emissions to random values.
        """
        self.set_random_initial_probabilities()
        self.set_random_transition_probabilities()
        self.set_random_emission_probabilities()

    # --- functions to deal with the transitions in the sequence

    def allow_all_transitions(self):
        """A convenience function to create transitions between all states.

        By default all transitions within the alphabet are disallowed; this
        is a way to change this to allow all possible transitions.
        """

        # first get all probabilities and pseudo counts set
        # to the default values
        all_probs = self._all_blank(self._state_alphabet,
                                    self._state_alphabet)

        all_pseudo = self._all_pseudo(self._state_alphabet,
                                      self._state_alphabet)

        # now set any probabilities and pseudo counts that
        # were previously set
        for set_key in self.transition_prob:
            all_probs[set_key] = self.transition_prob[set_key]

        for set_key in self.transition_pseudo:
            all_pseudo[set_key] = self.transition_pseudo[set_key]

        # finally reinitialize the transition probs and pseudo counts
        self.transition_prob = all_probs
        self.transition_pseudo = all_pseudo

    def allow_transition(self, from_state, to_state, probability = None,
                         pseudocount = None):
        """Set a transition as being possible between the two states.

        probability and pseudocount are optional arguments
        specifying the probabilities and pseudo counts for the transition.
        If these are not supplied, then the values are set to the
        default values.

        Raises:
        KeyError -- if the two states already have an allowed transition.
        """
        # check the sanity of adding these states
        for state in [from_state, to_state]:
            assert state in self._state_alphabet.letters, \
                   "State %s was not found in the sequence alphabet" % state

        # ensure that the states are not already set
        if (from_state, to_state) not in self.transition_prob and \
           (from_state, to_state) not in self.transition_pseudo:
            # set the initial probability
            if probability is None:
                probability = 0
            self.transition_prob[(from_state, to_state)] = probability

            # set the initial pseudocounts
            if pseudocount is None:
                pseudcount = self.DEFAULT_PSEUDO
            self.transition_pseudo[(from_state, to_state)] = pseudocount
        else:
            raise KeyError("Transition from %s to %s is already allowed."
                           % (from_state, to_state))

    def destroy_transition(self, from_state, to_state):
        """Restrict transitions between the two states.

        Raises:
        KeyError if the transition is not currently allowed.
        """
        try:
            del self.transition_prob[(from_state, to_state)]
            del self.transition_pseudo[(from_state, to_state)]
        except KeyError:
            raise KeyError("Transition from %s to %s is already disallowed."
                           % (from_state, to_state))

    def set_transition_score(self, from_state, to_state, probability):
        """Set the probability of a transition between two states.

        Raises:
        KeyError if the transition is not allowed.
        """
        if (from_state, to_state) in self.transition_prob:
            self.transition_prob[(from_state, to_state)] = probability
        else:
            raise KeyError("Transition from %s to %s is not allowed."
                           % (from_state, to_state))

    def set_transition_pseudocount(self, from_state, to_state, count):
        """Set the default pseudocount for a transition.

        To avoid computational problems, it is helpful to be able to
        set a 'default' pseudocount to start with for estimating
        transition and emission probabilities (see p62 in Durbin et al
        for more discussion on this. By default, all transitions have
        a pseudocount of 1.

        Raises:
        KeyError if the transition is not allowed.
        """
        if (from_state, to_state) in self.transition_pseudo:
            self.transition_pseudo[(from_state, to_state)] = count
        else:
            raise KeyError("Transition from %s to %s is not allowed."
                           % (from_state, to_state))

    # --- functions to deal with emissions from the sequence

    def set_emission_score(self, seq_state, emission_state, probability):
        """Set the probability of a emission from a particular state.

        Raises:
        KeyError if the emission from the given state is not allowed.
        """
        if (seq_state, emission_state) in self.emission_prob:
            self.emission_prob[(seq_state, emission_state)] = probability
        else:
            raise KeyError("Emission of %s from %s is not allowed."
                           % (emission_state, seq_state))

    def set_emission_pseudocount(self, seq_state, emission_state, count):
        """Set the default pseudocount for an emission.

        To avoid computational problems, it is helpful to be able to
        set a 'default' pseudocount to start with for estimating
        transition and emission probabilities (see p62 in Durbin et al
        for more discussion on this. By default, all emissions have
        a pseudocount of 1.

        Raises:
        KeyError if the emission from the given state is not allowed.
        """
        if (seq_state, emission_state) in self.emission_pseudo:
            self.emission_pseudo[(seq_state, emission_state)] = count
        else:
            raise KeyError("Emission of %s from %s is not allowed."
                           % (emission_state, seq_state))

def random_parameter_gen(state_alphabet, emission_alphabet):
    """ generate a set of random parameters for the Hidden Markov model

      This is done using the functionnalities of MarkovModelBuilder

      Arguments:

      o state_alphabet -- The alphabet of the possible state sequences
        that can be generated.

      o emission_alphabet -- The alphabet of the possible emitted symbols
    
      Returns a random parameter with initial_prob, transition_prob, emission_prob
    
    """

    mm_builder = MarkovModelBuilder(state_alphabet,
                                                     emission_alphabet)

    # constructing a new (random) markov model (initial set of parameters)
    mm_builder.allow_all_transitions()
    mm_builder.set_random_probabilities()

    model = mm_builder.get_markov_model()

    parameter= { "initial"    : model.initial_prob,
                 "transition" : model.transition_prob,
                 "emission" : model.emission_prob }

    return parameter

def print_emission_prob(emission_prob):
    for key, p in iter(sorted(emission_prob.iteritems())):
        state, letter = key
        print " {:<1} -> {:<1} {:.3f}".format(state,letter,p),
    print

def print_transition_prob(transition_prob):
    for key, p in iter(sorted(transition_prob.iteritems())):
        state, letter = key
        print " {:<1} -> {:<1} {:.3f}".format(state,letter,p),
    print

def print_initial_prob(initial_prob):
    for s, p in iter(sorted(initial_prob.iteritems())):
        print " {:<1}  {:.3f}".format(s,p),
    print

def print_parameters(parameter):
    print "Initial :"
    print_initial_prob(parameter["initial"])
    print "Transition :"
    print_transition_prob(parameter["transition"])
    print "Emission :"
    print_emission_prob(parameter["emission"])

def printshort_parameters(parameter):
    print_transition_prob(parameter["transition"])
    print_emission_prob(parameter["emission"])

def printsimple_parameters(emission_prob,transition_prob):
    for key, p in iter(sorted(emission_prob.iteritems())):
        state, letter = key
        print " {:<1}->{:<1} {:.3f}".format(state,letter,p),
    print "     ",
    for key, p in iter(sorted(transition_prob.iteritems())):
        state, letter = key
        print " {:<1}->{:<1} {:.3f}".format(state,letter,p),
    print


class HiddenMarkovModel(object):
    """Represent a hidden markov model that can be used for state estimation.
    """
    def __init__(self, state_alphabet, emission_alphabet, initial_prob, transition_prob, emission_prob,
                 transition_pseudo, emission_pseudo):
        """Initialize a Markov Model.

        Note: You should use the MarkovModelBuilder class instead of
        initiating this class directly.

        Arguments:

        o state_alphabet -- the different states
        
        o emission_alphabet -- the observation alphabet

        o initial_prob - A dictionary of initial probabilities for all states.

        o transition_prob -- A dictionary of transition probabilities for all
        possible transitions in the sequence.

        o emission_prob -- A dictionary of emission probabilities for all
        possible emissions from the sequence states.

        o transition_pseudo -- Pseudo-counts to be used for the transitions,
        when counting for purposes of estimating transition probabilities.

        o emission_pseudo -- Pseudo-counts to be used for the emissions,
        when counting for purposes of estimating emission probabilities.
        """

        self.initial_prob = initial_prob

        self._transition_pseudo = transition_pseudo
        self._emission_pseudo = emission_pseudo

        self.transition_prob = transition_prob
        self.emission_prob = emission_prob
        
        self.state_alphabet = state_alphabet
        self.emission_alphabet = emission_alphabet

        # a dictionary of the possible transitions from each state
        # each key is a source state, mapped to a list of the destination states
        # that are reachable from the source state via a transition
        self._transitions_from = \
           _calculate_from_transitions(self.transition_prob)

        # a dictionary of the possible transitions to each state
        # each key is a destination state, mapped to a list of source states
        # from which the destination is reachable via a transition
        self._transitions_to = \
           _calculate_to_transitions(self.transition_prob)

    def get_blank_transitions(self):
        """Get the default transitions for the model.

        Returns a dictionary of all of the default transitions between any
        two letters in the sequence alphabet. The dictionary is structured
        with keys as (letter1, letter2) and values as the starting number
        of transitions.
        """
        return self._transition_pseudo

    def get_blank_emissions(self):
        """Get the starting default emmissions for each sequence.

        This returns a dictionary of the default emmissions for each
        letter. The dictionary is structured with keys as
        (seq_letter, emmission_letter) and values as the starting number
        of emmissions.
        """
        return self._emission_pseudo

    def transitions_from(self, state_letter):
        """Get all destination states to which there are transitions from the
        state_letter source state.

        This returns all letters which the given state_letter can transition
        to. An empty list is returned if state_letter has no outgoing
        transitions.
        """
        if state_letter in self._transitions_from:
            return self._transitions_from[state_letter]
        else:
            return []

    def transitions_to(self, state_letter):
        """Get all source states from which there are transitions to the
        state_letter destination state.

        This returns all letters which the given state_letter is reachable
        from. An empty list is returned if state_letter is unreachable.
        """
        if state_letter in self._transitions_to:
            return self._transitions_to[state_letter]
        else:
            return []

    def get_param(self):
        """ returns the model parameters
              o initial_prob
              o translition_prob
              o emission_prob
        """
        estim_param = { "initial"    : self.initial_prob,
                        "transition" : self.transition_prob,
                        "emission"   : self.emission_prob }
        
        return  estim_param
		 
    def generate(self, n):
        """Generate a sequence of observation according to the HiddenMarkov model

          Arguments :
          
          o n : the size of the sequence to generate
          
          Returns the generated sequence and the associated states

        """
        state_letters = self.state_alphabet.letters
        emission_letters = self.emission_alphabet.letters

        # transition probabilities
        #   stored in an array associated to each state, probs are ordered according to state_letters
        transition_prob = {}
        for state in state_letters:
            transition_prob[state] = []
            for next_state in state_letters:
                transition_prob[state].append(self.transition_prob[(state, next_state)])

        # emission probabilities
        #   stored in an array associated to each state, probs are ordered according to emission_letters
        emission_prob = {}
        for state in state_letters:
            emission_prob[state] = []
            for letter in emission_letters:
                emission_prob[state].append(self.emission_prob[(state, letter)])

        # initial probabilities
        #   array with a proba associated to each state
        initial_prob = []
        for state in state_letters:
            initial_prob.append(self.initial_prob[state])

        sequence = []
        states   = []
        # the sequence of observations of size n
        for i in range(0,n):
            if i == 0 :
                trans_prob = initial_prob
            else :
                trans_prob = transition_prob[prev_state]
            # now generating the current state and the associated letter in turn
            cur_state = np.random.choice(state_letters, 1, p = trans_prob)[0]
            letter = np.random.choice(emission_letters,1,p = emission_prob[cur_state])[0]
            sequence.append(letter)
            states.append(cur_state)
            prev_state = cur_state

        return sequence, states

    def Forward(self, sequence):
        """Calculate the forward probabilities

        This implements the Forward algorithm

          Arguments:

          o sequence -- a sequence of observations

          Returns the forward probailities alpha_t(u) and the likelihood

        """

        alpha = {}
        normfactor    = {}
        state_letters = self.state_alphabet.letters

        L = len(sequence)
        # --- recursion
        # loop over the sequence (i = 0 .. L-1)
        for i in range(0, L):
            # loop over all of the possible i-th states
            for cur_state in state_letters:
                # P( o_{i} | u)
                emission_part = self.emission_prob[(cur_state, sequence[i])]

                if i == 0:
                    # for the first state, use the initial probability rather
                    # than looking back to previous states
                    prob = self.initial_prob[cur_state]
                else:
                    # loop over all possible (i-1)-th previous states
                    possible_state_probs = {}
                    for prev_state in self.transitions_to(cur_state):
                        # p(v -> u)
                        trans_part = self.transition_prob[(prev_state, cur_state)]

                        # alpha_{i-1}(v)
                        forward_part = alpha[(prev_state, i - 1)]
                        cur_prob = forward_part * trans_part

                        possible_state_probs[prev_state] = cur_prob

                    # calculate the forward probability using the sum
                    prob = sum(possible_state_probs.values())

                # temp alpha_{i}(u)
                alpha[(cur_state,i)] = (emission_part  * prob)

        # calculate the probability of the full sequence (likelihood) : sum of the alpha_N
        # loop over all states
        likelihood = 0
        for state in state_letters:
            likelihood += alpha[(state,L-1)]

        return alpha, likelihood

    def Backward(self, sequence):
        """Calculate the backward probabilities

        This implements the Backward algorithm

          Arguments:

          o sequence -- a sequence of observations
          
          Returns the backward probabilities beta_t(u) and the likelihood

        """

        beta = {}
        state_letters = self.state_alphabet.letters

        L = len(sequence)
        # --- initialization for T = L-1
        for state in state_letters:
            beta[(state,L-1)] = 1

        # --- recursion
        # loop over the sequence (T-1 to 1, i = L-2 .. 0)
        for i in range(L-2,-1,-1):
            # loop over all of the possible i-th states in the state path
            for cur_state in state_letters:
                # loop over all possible (i-1)-th previous states
                possible_state_probs = {}
                for next_state in self.transitions_from(cur_state):
                    # p(u -> v)
                    trans_part = self.transition_prob[(cur_state,next_state)]
                    # beta_{i+1}(v)
                    backward_part = beta[(next_state, i + 1)]
                    # p( o_{i+1} | v)
                    emission_part = self.emission_prob[(next_state, sequence[i+1])]

                    cur_prob = backward_part * trans_part * emission_part

                    possible_state_probs[next_state] = cur_prob

                # calculate the backward probability using the sum
                beta[(cur_state,i)] = sum(possible_state_probs.values())

        likelihood = 0
        for state in state_letters:
            likelihood += self.initial_prob[state]*beta[(state,0)]

        return beta, likelihood

    def Baumwelch(self, sequence, init_param ):
        """Parameter estimation via the baum-welch algorithm.

        This implements the Baum-Welch algorithm

          Arguments:

          o sequence  -- a sequence of observations

          o init_param -- the intial value of parameters theta_0
          
          Returns the maximum likelihood parameters and the associated likelihood

       """

        state_letters = self.state_alphabet.letters
        emission_letters = self.emission_alphabet.letters

        self.initial_prob    = init_param["initial"]
        self.transition_prob = init_param["transition"]
        self.emission_prob   = init_param["emission"]
        
        L=len(sequence)
        
        # a first forward for the current likelihood
        alpha, likelihood =  self.Forward(sequence)
        k=0
        prev_likelihood = 0
        tol = 0.001
        while ( (k==0 or math.log(likelihood) - math.log(prev_likelihood)) > tol and k<10 ):

            ## Phase E
            # backward-forward
            alpha, likelihood =  self.Forward(sequence)
            beta, l  =  self.Backward(sequence)
            prev_likelihood = likelihood

            # computing the xsi
            Xi = defaultdict(list)
            Gamma = defaultdict(list)
            Gamma_o = defaultdict(list)
            for i in range(0,L-1):
                for from_state in state_letters:
                    # the transition parameters
                    gamma = 0
                    for to_state in state_letters:
                        xi = alpha[(from_state,i)]*self.emission_prob[(to_state,sequence[i+1])]*self.transition_prob[(from_state, to_state)]*beta[(to_state,i+1)]
                        Xi[(from_state, to_state)].append(xi)
                        gamma += xi
                    Gamma[from_state].append(gamma)
                    # the emission parameters
                    Gamma_o[(from_state,sequence[i])].append(gamma)
            
            # A last step for Gamma[L-1]
            for to_state in state_letters:
                gamma = 0
                for from_state in state_letters:
                    gamma += Xi[(from_state, to_state)][L-2]
                Gamma[to_state].append(gamma)   
                Gamma_o[(to_state,sequence[L-1])].append(gamma)
            
            
            ## Phase M
            # The updates
            for from_state in state_letters:
                for to_state in state_letters:
                    self.transition_prob[(from_state, to_state)] = sum(Xi[(from_state, to_state)])/sum(Gamma[from_state][:-1])
                for letter in emission_letters:
                    self.emission_prob[(from_state,letter)] = sum(Gamma_o[from_state,letter])/sum(Gamma[from_state])
                self.initial_prob[from_state] = Gamma[from_state][0]/likelihood  
            
            # a last forward for the likelihood with the updated parameters
            alpha, likelihood =  self.Forward(sequence)
            #print "  loglikelihood {:<4} ".format(math.log(likelihood))   
            k+=1

        
        estim_param = { "initial"    : self.initial_prob, 
                        "transition" : self.transition_prob,
                        "emission"   : self.emission_prob }
        
        return  estim_param, likelihood   


    def Estimation(self, sequence, num_init):
        """Unsupervised parameter estimation using Baum-Welch

        This run several Baum-Welch pass using different starting values

          Arguments:

          o sequence -- a sequence of observations

          Returns the maximum likelihood parameters (max among the different Baum-Welch runs)

        """
       
        estimations = []
        k = 1
        while ( k <= num_init):       
		      
            #print "Estimation {:<4}".format(k)	  
		      	 
            #initial value
            theta_0 = random_parameter_gen(self.state_alphabet, self.emission_alphabet)
            param, likelihood = self.Baumwelch( sequence, theta_0 )
            
            estimations.append({ "param":param, "likelihood" : likelihood })
            print "Estimation {:<4} loglikelihood {:<4}".format(k,math.log(likelihood))
            k += 1  
			    
        maxlikeli_parameters = max(estimations,key=operator.itemgetter("likelihood"))
        
        return maxlikeli_parameters['param'], maxlikeli_parameters["likelihood"]

    def forward(self, sequence):
        """Calculate the forward probabilities and normalization factors

        This implements the Forward algorithm with scalling

          Arguments:

          o sequence -- a sequence of observations
        
          Returns the forward normalized probailities \hat(alpha)_t(u), the normalization factors c_t and the loglikelihood

        """

        alpha = {}
        normfactor    = {}
        state_letters = self.state_alphabet.letters

        L = len(sequence)
        # --- recursion
        # loop over the sequence (i = 0 .. L-1)
        for i in range(0, L):
            temp_probs = {}
            # loop over all of the possible i-th states
            for cur_state in state_letters:
                # P( o_{i} | u)
                emission_part = self.emission_prob[(cur_state, sequence[i])]

                if i == 0:
                    # for the first state, use the initial probability
                    prob = self.initial_prob[cur_state]
                else:
                    # loop over all possible (i-1)-th previous states
                    possible_state_probs = {}
                    for prev_state in self.transitions_to(cur_state):
                        # p(v -> u)
                        trans_part = self.transition_prob[(prev_state, cur_state)]

                        # alpha_{i-1}(v)
                        forward_part = alpha[(prev_state, i - 1)]
                        cur_prob = forward_part * trans_part

                        possible_state_probs[prev_state] = cur_prob

                    # calculate the forward probability using the sum
                    prob = sum(possible_state_probs.values())

                # temp alpha_{i}(u)
                temp_probs[cur_state] = (emission_part  * prob)

            # now rescaling using the normalizing factor
            normfactor[i] = sum(temp_probs.values())
            for state in state_letters:
                alpha[(state, i)] = temp_probs[state]/normfactor[i]

        #adding a single value at the end (normfactor[L]=1)
        normfactor[L] = 1  
        # calculate the probability of the full sequence is given by the sum of the log normalizing factors
        loglikeli = 0
        for i in range(0, L):
            loglikeli += math.log(normfactor[i])

        return alpha, normfactor, loglikeli

    def backward(self, sequence, normfactors):
        """Calculate the backward probabilities

        This implements the Backward algorithm with scalling

          Arguments:

          o sequence -- a sequence of observations
        
          o normfactors computed by the forward algorithm (c_t)
        
          Returns the backward normalized probailities \hat(beta)_t(u)

        """

        beta = {}
        state_letters = self.state_alphabet.letters

        L = len(sequence)
        # --- initialization for T = L-1
        for state in state_letters:
            beta[(state,L-1)] = 1

        # --- recursion
        # loop over the sequence (T-1 to 1, i = L-2 .. 0)
        for i in range(L-2,-1,-1):
            # loop over all of the possible i-th states in the state path
            for cur_state in state_letters:
                # loop over all possible (i-1)-th previous states
                possible_state_probs = {}
                for next_state in self.transitions_from(cur_state):
                    # p(u -> v)
                    trans_part = self.transition_prob[(cur_state, next_state)]
                    # beta_{i+1}(v)
                    backward_part = beta[(next_state, i + 1)]
                    # p( o_{i+1} | v)
                    emission_part = self.emission_prob[(next_state, sequence[i+1])]

                    cur_prob = backward_part * trans_part * emission_part

                    possible_state_probs[next_state] = cur_prob

                # calculate the backward probability using the sum
                beta[(cur_state,i)] = sum(possible_state_probs.values())/normfactors[i+1]

        return beta

    def baumwelch(self, sequence, init_param ):
        """Parameter estimation via the baum-welch algorithm.

        This implements the Baum-Welch algorithm

          Arguments:

          o sequence -- a sequence of observations
        
         Returns the maximum likelihood parameters and the associated loglikelihood

       """

        state_letters = self.state_alphabet.letters
        emission_letters = self.emission_alphabet.letters

        self.initial_prob    = init_param["initial"]
        self.transition_prob = init_param["transition"]
        self.emission_prob   = init_param["emission"]

        # a first forward for the current likelihood
        alpha, norm, loglikelihood =  self.forward(sequence)
        k=0
        prev_loglikelihood = -100000
        tol = 0.001
        L = len(sequence)
        while ( (loglikelihood - prev_loglikelihood)>tol and k<1000):
            ## Phase E          
            # backward-forward
            alpha, norm, loglikelihood =  self.forward(sequence)
            beta =  self.backward(sequence, norm)
            prev_loglikelihood = loglikelihood

            # computing the xsi
            Xi = defaultdict(list)
            Gamma = defaultdict(list)
            Gamma_o = defaultdict(list)
            for i in range(0,L-1):
                for from_state in state_letters:
                    # the transition parameters
                    gamma = 0
                    for to_state in state_letters:
                        xi = alpha[(from_state,i)]*self.emission_prob[(to_state,sequence[i+1])]*self.transition_prob[(from_state, to_state)]*beta[(to_state,i+1)]/norm[i+1]
                        Xi[(from_state, to_state)].append(xi)
                        gamma += xi
                    Gamma[from_state].append(gamma)
                   
                    # the emission parameters
                    Gamma_o[(from_state,sequence[i])].append(gamma)

            # A last step for Gamma[L-1]
            for to_state in state_letters:
                gamma = 0
                for from_state in state_letters:
                    gamma += Xi[(from_state, to_state)][L-2]
                Gamma[to_state].append(gamma)   
                Gamma_o[(to_state,sequence[L-1])].append(gamma)
           
            ## Phase M
            # The updates
            # a last backward-forward
            for from_state in state_letters:
                for to_state in state_letters:
                    self.transition_prob[(from_state, to_state)] = sum(Xi[(from_state, to_state)])/sum(Gamma[from_state][:-1])
                for letter in emission_letters:
                    self.emission_prob[(from_state,letter)] = sum(Gamma_o[from_state,letter])/sum(Gamma[from_state])
                self.initial_prob[from_state] = Gamma[from_state][0]

            # a last forward for the loglikelihood with the updated parameters
            alpha, norm, loglikelihood =  self.forward(sequence)
            #print "loglikelihood {:<4}".format(loglikelihood)          
            k+=1
            
            #Printing the current hidden state path
            #map_states = self.map_states(sequence,Gamma)
            #print (''.join(map_states)+"          "),
            #printsimple_parameters(self.emission_prob,self.transition_prob)
           
        estim_param = { "initial"    : self.initial_prob,
                        "transition" : self.transition_prob,
                        "emission"   : self.emission_prob }
        
        return  estim_param, loglikelihood, Gamma        

    def map_decode(self, sequence):
        """Calculate the most probable state path using Maximum A Posteriori Decoding

        Arguments:

        o gamma -- P( S_t = u | X, theta)

        """        
        state_letters = self.state_alphabet.letters
        
        # a forward backward to compute the proba of each state at each step
        params = self.get_param()
        params, loglikelihood, gamma = self.baumwelch(sequence, params)
        
        L = len(sequence)
        # --- recursion
        # loop over the sequence (i = 0 .. L-1)
        states   = []
        for i in range(0, L):
            probs = {}
            for state in state_letters:
				    probs[state] = gamma[state][i]
            map_state = max(probs.iteritems(), key=operator.itemgetter(1))[0]
            states.append(map_state)
        return states
        

    def map_states(self, sequence, gamma):
        """Calculate the most probable state path using Maximum A Posteriori Decoding

        Arguments:

        o gamma -- P( S_t = u | X, theta)

        """        
        state_letters = self.state_alphabet.letters
        
        L = len(sequence)
        # --- recursion
        # loop over the sequence (i = 0 .. L-1)
        states   = []
        for i in range(0, L):
            probs = {}
            for state in state_letters:
				    probs[state] = gamma[state][i]
            map_state = max(probs.iteritems(), key=operator.itemgetter(1))[0]
            states.append(map_state)
        return states    
        
        
        
    def estimation(self, sequence, num_init):
        """Unsupervised parameter estimation using Baum-Welch

        This run several Baum-Welch using different starting values

        Arguments:

        o sequence -- a sequence of observations

        """
       
        estimations = []
        k = 1
        while ( k <= num_init ):       
            
            #print "Estimation {:<4}".format(k)
            
            #initial value
            theta_0 = random_parameter_gen(self.state_alphabet, self.emission_alphabet)
            param, loglikelihood, gamma = self.baumwelch(sequence, theta_0)
                       
            estimations.append({ "param":param, "loglikelihood" : loglikelihood })
            print "Estimation {:<4} loglikelihood {:<4}".format(k,loglikelihood)
            k += 1  
			    
        maxlikeli_parameters = max(estimations,key=operator.itemgetter("loglikelihood"))
        
        return maxlikeli_parameters['param'], maxlikeli_parameters["loglikelihood"]       

    def estimationVerbose(self, sequence, truestates, num_init):
        """Unsupervised parameter estimation using Baum-Welch

        This run several Baum-Welch using different starting values

        Arguments:

        o sequence -- a sequence of observations

        """
       
        estimations = []
        k = 1
        while ( k <= num_init ):       
                       
            #initial value
            print "Starting point"
            theta_0 = random_parameter_gen(self.state_alphabet, self.emission_alphabet)
            printshort_parameters(theta_0)
            print colored((''.join(sequence)),'yellow')
            print colored((''.join(truestates)),'green')
            
            param, loglikelihood, gamma = self.baumwelch(sequence, theta_0)
            
            print colored((''.join(truestates)),'green')
            print colored((''.join(sequence)),'yellow')
            printshort_parameters(param)
            raw_input("Press Enter to continue.")
            
            estimations.append({ "param":param, "loglikelihood" : loglikelihood })
            print "Estimation {:<4} loglikelihood {:<4}".format(k,loglikelihood)
            k += 1  
			    
        maxlikeli_parameters = max(estimations,key=operator.itemgetter("loglikelihood"))
        
        return maxlikeli_parameters['param'], maxlikeli_parameters["loglikelihood"]       



    def viterbi(self, sequence, state_alphabet):
        """Calculate the most probable state path using the Viterbi algorithm.

        This implements the Viterbi algorithm (see pgs 55-57 in Durbin et
        al for a full explanation -- this is where I took my implementation
        ideas from), to allow decoding of the state path, given a sequence
        of emissions.

        Arguments:

        o sequence -- A Seq object with the emission sequence that we
        want to decode.

        """

        # calculate logarithms of the initial, transition, and emission probs
        log_initial = self._log_transform(self.initial_prob)
        log_trans = self._log_transform(self.transition_prob)
        log_emission = self._log_transform(self.emission_prob)

        viterbi_probs = {}
        pred_state_seq = {}
        state_letters = self.state_alphabet.letters

        # --- recursion
        # loop over the training squence (i = 1 .. L)
        # NOTE: My index numbers are one less than what is given in Durbin
        # et al, since we are indexing the sequence going from 0 to
        # (Length - 1) not 1 to Length, like in Durbin et al.
        for i in range(0, len(sequence)):
            # loop over all of the possible i-th states in the state path
            for cur_state in state_letters:
                # e_{l}(x_{i})
                emission_part = log_emission[(cur_state, sequence[i])]

                max_prob = 0
                if i == 0:
                    # for the first state, use the initial probability rather
                    # than looking back to previous states
                    max_prob = log_initial[cur_state]
                else:
                    # loop over all possible (i-1)-th previous states
                    possible_state_probs = {}
                    for prev_state in self.transitions_to(cur_state):
                        # a_{kl}
                        trans_part = log_trans[(prev_state, cur_state)]

                        # v_{k}(i - 1)
                        viterbi_part = viterbi_probs[(prev_state, i - 1)]
                        cur_prob = viterbi_part + trans_part

                        possible_state_probs[prev_state] = cur_prob

                    # calculate the viterbi probability using the max
                    max_prob = max(possible_state_probs.values())

                # v_{k}(i)
                viterbi_probs[(cur_state, i)] = (emission_part + max_prob)

                if i > 0:
                    # get the most likely prev_state leading to cur_state
                    for state in possible_state_probs:
                        if possible_state_probs[state] == max_prob:
                            pred_state_seq[(i - 1, cur_state)] = state
                            break

        # --- termination
        # calculate the probability of the state path
        # loop over all states
        all_probs = {}
        for state in state_letters:
            # v_{k}(L)
            all_probs[state] = viterbi_probs[(state, len(sequence) - 1)]

        state_path_prob = max(all_probs.values())

        # find the last pointer we need to trace back from
        last_state = ''
        for state in all_probs:
            if all_probs[state] == state_path_prob:
                last_state = state

        assert last_state != '', "Didn't find the last state to trace from!"

        # --- traceback
        traceback_seq = MutableSeq('', state_alphabet)

        loop_seq = list(range(1, len(sequence)))
        loop_seq.reverse()

        # last_state is the last state in the most probable state sequence.
        # Compute that sequence by walking backwards in time. From the i-th
        # state in the sequence, find the (i-1)-th state as the most
        # probable state preceding the i-th state.
        state = last_state
        traceback_seq.append(state)
        for i in loop_seq:
            state = pred_state_seq[(i - 1, state)]
            traceback_seq.append(state)

        # put the traceback sequence in the proper orientation
        traceback_seq.reverse()

        return traceback_seq.toseq(), state_path_prob

    def _log_transform(self, probability):
        """Return log transform of the given probability dictionary.

        When calculating the Viterbi equation, add logs of probabilities rather
        than multiplying probabilities, to avoid underflow errors. This method
        returns a new dictionary with the same keys as the given dictionary
        and log-transformed values.
        """
        log_prob = copy.copy(probability)
        try:
            neg_inf = float("-inf")
        except ValueError:
            #On Python 2.5 or older that was handled in C code,
            #and failed on Windows XP 32bit
            neg_inf = - 1E400
        for key in log_prob:
            prob = log_prob[key]
            if prob > 0:
                log_prob[key] = math.log(log_prob[key])
            else:
                log_prob[key] = neg_inf

        return log_prob
