#!/usr/bin/env python
"""Test the MarkovModel module

"""
# standard modules
import math

from MarkovModel import *
from Bio import SeqIO

# create some simple alphabets
class NumberAlphabet():
    """Numbers as the states of the model.
    """
    letters = ['I', 'O']

class LetterAlphabet():
    """Letters as the emissions of the model.
    """
    letters = ['A', 'B' ]

# Try a simple HMM model
if __name__ == "__main__":
	
	mm_builder =  MarkovModelBuilder(NumberAlphabet(),LetterAlphabet())
   
   # set initial probabilities                                             
	prob_initial = [0.8, 0.2]
	mm_builder.set_initial_probabilities(
	                                     {'I': prob_initial[0], 'O': prob_initial[1]})  
   
   # set transition probabilities
	prob_transition = [[0.9, 0.1], [0.2, 0.8]]
	mm_builder.allow_transition('I', 'I', prob_transition[0][0])
	mm_builder.allow_transition('I', 'O', prob_transition[0][1])
	mm_builder.allow_transition('O', 'I', prob_transition[1][0])
	mm_builder.allow_transition('O', 'O', prob_transition[1][1])


	# set emission probabilities
	prob_emission = [ [0.8, 0.2 ], [ 0.2, 0.8]]
	mm_builder.set_emission_score('I', 'A', prob_emission[0][0])
	mm_builder.set_emission_score('I', 'B', prob_emission[0][1])
	
	mm_builder.set_emission_score('O', 'A', prob_emission[1][0])
	mm_builder.set_emission_score('O', 'B', prob_emission[1][1])
	
	# retrieve an HMM model with the corresponding parameters	
	model = mm_builder.get_markov_model()
	
	print("Parameters of the markov model")
	params = model.get_param()
	print_parameters(params)
		
	# generate a sequence of observations from this model
	sequence, states = model.generate(180)
	#print colored((''.join(sequence)),'yellow')
	#print colored((''.join(states)),'green')
	print	
							
	# estimate the model parameter from the generated sequence
	num_init = 10
	#est_params, loglikelihood = model.Estimation(sequence,num_init)
	est_params, loglikelihood = model.estimation(sequence,num_init)
	#est_params, loglikelihood = model.estimationVerbose(sequence,states,num_init)
	
	print "Final Loglikelihood {:<4}".format(loglikelihood)
	print_parameters(est_params)
	
	# Viterbi path
	viterbi_states, prob_path = model.viterbi(sequence, NumberAlphabet())
	
	# MAP path
	map_states = model.map_decode(sequence)
		
	print colored((''.join(sequence)),'yellow')
	print colored((''.join(states)+"    true"),'green')	
	print colored((''.join(viterbi_states))+"    viterbi",'white')	
	print colored((''.join(map_states)+"    MAP"),'cyan')	
		
