In order to run these examples, you will need (a pre-release of)
AIMMS 3.13 with CP functionality.

Please contact info@aimms.com to request a trial version or a 
free full academic license.
