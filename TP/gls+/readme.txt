For information on GLS+, please check out my thesis or our IJCAI paper on my website www.cs.ubc.ca/~hutter

This is just a research version of the code and the help. I'm in the process of making it all nice, but for now, just do the following:

First you need to compile GLS+ on your system. For this purpose, run "make all" on UNIX.
Everything should also work on Windows, but instead of CPU time we currently only measure user time on Windows.

Under UNIX, "make all" produces an executable called gls+
When calling the algorithm, you need to specify the input network as follows:
gls+ -i <Bayes net in .simple format>

The network has to be in my simple input format, but I'll 
provide a converter from the Bayes net interchange format to this format.
Soon (maybe next week), I'll be treating MRFs and general factor graphs, so stay tuned.

For now, don't bother with all the possible parameters, 
I've only used the standard parameters for quite a while now.
The only parameter that's important to know about is the 
preprocessing bound pB, i.e. how long to carry out variable elimination.

The default for this is 10^4=10000, but for random networks 
the preprocessing actually tends to take long and may
even make things worse, so we set pB=0.

So, this is what you would call:

gls+ -i <structured instance in .simple format>
gls+ -pB 0 -i <random instance in .simple format>

I hope the output is self-explanatory.
If you have questions please email me.