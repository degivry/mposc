#include "global.h"
#include "my_set.h"
#include "ProbabilityTable.h"

class AssignmentManager {
public:
	bool outputLM;
	int numRun;
	double overallBestLogProb;
	double runBestLogProb;
	double bestTime;
	double runBestTime;
	double optimalLogMPEValue;
	double worstSolutionCostFound;
	double avgSolutionCostFound;

	int numRunsWithOptimumFound;
	int bestRun;
	int numRunBestAssignments;
	int numGlobalBestAssignments;
	ProbabilityTable** localProbTables;
	int* tmpAssignment;

	int numVars;
	int** runBestAssignments;
	int** globalBestAssignments;
	//globalBestAssignments[i][var]=value <=> in the i'th 
								                //best explanation found at all, variable var is set to val

	AssignmentManager(){
		overallBestLogProb = -DOUBLE_BIG;
		runBestAssignments = new int*[MAX_NUM_OF_OPTIMAL_CONFIGURATIONS];
		globalBestAssignments = new int*[MAX_NUM_OF_OPTIMAL_CONFIGURATIONS];
		numRun = 0;
		numRunsWithOptimumFound = 0;
		worstSolutionCostFound = 0;
		avgSolutionCostFound = 0;
	}

	void setProbTables(ProbabilityTable** probTables){
		localProbTables = probTables;	
	} 

	void setNumberOfVariables(int newNumberOfVariables, bool newOutputLM){
		numVars = newNumberOfVariables;
		outputLM = newOutputLM;
		for(int i=0; i<MAX_NUM_OF_OPTIMAL_CONFIGURATIONS; i++){
			runBestAssignments[i] = new int[newNumberOfVariables];
			globalBestAssignments[i] = new int[newNumberOfVariables];
		}
		tmpAssignment = new int[newNumberOfVariables];
	}

	double get_log_score();
	double get_log_score(int* values);
	void endRun();
	bool updateIfNewBest(double log_prob);
	void outputRunLMs(FILE* outfile);
	void outputGlobalLMs(FILE* outfile);
	void outputAssignment(FILE* outfile, int* assignment);
	void outputCurrentAssignment(FILE* outfile);

	void newRun(){
		numRun++;
		runBestLogProb = -DOUBLE_BIG;
		numRunBestAssignments = 0; 
	}
	
	bool foundOptimalThisRun(){
		return runBestLogProb > optimalLogMPEValue - EPS;
	}

	void outputResult(FILE* outfile){
		fprintf(outfile, "%lf %lf\n", runBestLogProb, runBestTime);
	}

	void copyAssignment(int* newValues){
		for(int var=0; var<numVars; var++) newValues[var] = variables[var]->value;	
	}

	bool sameAssignment(const int* values){
		for(int var=0; var<numVars; var++){
			if( values[var] != variables[var]->value ) return false;	
		}
		return true;
	}

	int hammingDistFromLastLM(){
		int result = 0;
		for(int var=0; var<numVars; var++){
			if(variables[var]->value != variables[var]->lastILSValue) result++;
		}
		return result;
	}

	bool differentFromLastILSSolution(){
		return hammingDistFromLastLM() != 0;
	}
};