#include "ProbabilityTable.h"
#include "global.h"

class Bucket {
public:
	ProbabilityTable** bucketPTs;
	int* partition; // partition[ptNum] = k <=> probability table ptNum is in partition k
	int numPartitions; // numPartitions is the highest number k for partition
	int numSubPTs;
	int* subPTs; // subPTs[ptNum1]=ptNum2 <=> ptNum1 is a subPT of ptNum
	int num;
	int numPTs;
	int numBucketVars;
	double bucketWeight;
	int* bucketVars;
	int* varsInPartition; // temporary

	Bucket();
	void defaultInitialization();
	void add(ProbabilityTable* pProbTable);
	void outputBucket();
	ProbabilityTable* processPartition(int partitionNumber);
	int constructPartititons(int ib, double weightBound);
	int getBestAssignmentForBucket(const int* mpeAssignmentSoFar);
};