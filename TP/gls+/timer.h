#ifdef WIN32
	#define NT 1 
#endif

#ifdef NT
	#include <time.h>
#else
	#include <sys/resource.h>
#endif

#ifndef CLK_TCK
	#define CLK_TCK 60
#endif


void start_timer();
double elapsed_seconds();
