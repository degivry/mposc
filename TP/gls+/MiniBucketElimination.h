#include "Bucket.h"
#include "Variable.h"
class MiniBucketElimination{
public:
	void initOnce();
	void initRun(bool newOrder);
	void createBucketPT(ProbabilityTable* pProbTable);
	void outputBuckets();
	double solve(int ib, double weight, int* mbAssignment);
	void preprocess(double weightBoundToStopExecution, int *outNumVars, int** leftVarIndices, Variable ***outVars, int *outNumPots, ProbabilityTable ***outPots);
	int inducedWidth;
	double maxTakenWeight;

	int iBound;
	double weightBound;

	int numOfBuckets;
	Bucket* buckets;
	int* order;
	int* optimalVars;
	Variable **mbVariables;

	void createOrder(int numFakeEvidenceVars, int* fakeEvidenceVars, int* outInducedWidth, double* outInducedWeight);
private:
	double process();
};
