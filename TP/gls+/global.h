#ifndef _GLOBAL
#define _GLOBAL

class ProbabilityTable; // just such that we know the name;
class Variable; // just such that we know the name;
class AssignmentManager; // just such that we know the name;
/************************************/
/* Compilation flags                */
/************************************/

#ifdef WIN32
	#define NT 1 
#endif

/************************************/
/* Standard includes                */
/************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <assert.h>


/************************************/
/* Internal constants               */
/************************************/

#define TRUE 1
#define FALSE 0
#define NOVALUE -1

#define BIG 100000000
#define DOUBLE_BIG 1e100
#define LINE_LEN 1000
#define MAX_VARNAME_LENGTH 10000
#define MAX_SIZE_MARKOV_BLANKET 10000 
#define MAX_NUM_OF_OPTIMAL_CONFIGURATIONS 1000 // The Pigs instance is insance w.r.t. this !
//=== An assingment is only better than another if it is at least EPS better
//=== DON'T SET THIS TOO LOW !!!
//=== With 1e-7, there are already problems with numerical instabilities for
//=== larger problems and long runtimes. The caching makes this effect worse !
const double EPS = 1e-5; 

const double log_zero = -BIG/10000;
const int ALGO_GN = 0;
const int ALGO_GLS = 1;
const int ALGO_ILS = 2;
const int ALGO_HYBRID = 3;
const int ALGO_TABU = 4;
const int ALGO_MB = -1;

const int INIT_RANDOM = 0;
const int INIT_MB = 1;

const int CACHING_NONE = 0;
const int CACHING_INDICES = 1;
const int CACHING_SCORE = 2;
const int CACHING_GOOD_VARS = 3;

const int PERTUBATION_RANDOM_VARS_RANDOM_FLIP = 0;
const int PERTUBATION_RANDOM_VARS_SAMPLED_FLIP = 1;
const int PERTUBATION_RANDOM_POTS_RANDOM_INDEX = 2;
const int PERTUBATION_RANDOM_POTS_SAMPLED_INDEX = 3;
const int PERTUBATION_SAMPLED_POTS_RANDOM_INDEX = 4;
const int PERTUBATION_SAMPLED_POTS_SAMPLED_INDEX = 5;
const int PERTUBATION_MB = 6;

const int ACC_RW = 0;
const int ACC_BETTER = 1;
const int ACC_RESTART = 2;
const int ACC_BETTER_RW = 3;
const int ACC_BEST_WORSENING = 4;
const int ACC_RW_AFTER_N = 5;
const int ACC_RW_AFTER_N2 = 6;

#define MAX(A,B) ((A) > (B) ? (A) : (B))
#define MIN(A,B) ((A) < (B) ? (A) : (B))

struct instantiation{
	int var;
	int value;
};

extern double log_prob;
extern int num_vars;
extern int num_pots;
extern bool* isgoodvar;
extern int glsReal;
extern int algo;
extern double glsPenaltyMultFactor;

extern ProbabilityTable** probTables;
extern Variable** variables;
extern AssignmentManager assignmentManager;

#endif
