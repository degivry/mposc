#include "Bucket.h"
#include "my_set.h"
#include "Variable.h"

void Bucket::defaultInitialization(){
	int i;
	numPTs = 0;
	numBucketVars = 0;
	bucketWeight = 1;
	numSubPTs = 0;
	numPartitions = 0;
	for(i=0; i<num_pots; i++){
		subPTs[i] = -1;
		partition[i] = -1;
	}
}

Bucket::Bucket(){
	bucketPTs = new ProbabilityTable*[num_pots]; // this is an upper bound on the num of probTables
	partition = new int[num_pots];
	subPTs = new int[num_pots]; // upper bound as well
	bucketVars = new int[num_vars]; // this is an upper bound.
	varsInPartition = new int[num_vars];
}

void Bucket::add(ProbabilityTable* pProbTable){
	bucketPTs[numPTs++] = pProbTable;

	for(int i=0; i<pProbTable->numPTVars; i++){
		if( !contains(bucketVars, numBucketVars, pProbTable->ptVars[i])){
			bucketVars[numBucketVars++] = pProbTable->ptVars[i];
			bucketWeight *= variables[pProbTable->ptVars[i]]->domSize;
		}
	}
}

void Bucket::outputBucket(){
	printf("Bucket[%d]: ", num);
	for(int i=0; i<numPTs; i++){
		bucketPTs[i]->outputVars();
	}
	printf("\n");
}

ProbabilityTable* Bucket::processPartition(int partitionNumber){
	int i;
	//	printf("In process, numBucketVars = %d\n", numBucketVars);
	int numVarsInPartition = 0;
	int numMatchingPTs = 0;

	for(i=0; i<numPTs; i++){
		if(partition[i] == partitionNumber){
			addAllToFrom(varsInPartition, &numVarsInPartition, bucketPTs[i]->ptVars, bucketPTs[i]->numPTVars);
			if(subPTs[i] == -1) numMatchingPTs++;
		}
	}
	ProbabilityTable* result = new ProbabilityTable(numVarsInPartition, varsInPartition);

/* 
	printf("Processing bucket[%d] with %d PTs, weight %d and %d vars: ",num, numPTs, bucketWeight, numBucketVars );
	output(numBucketVars, bucketVars);
	printf("\n");
*/
	for(i=0; i<numPTs; i++){
		if(partition[i] == partitionNumber){
			result->multiplyBy(bucketPTs[i]);
		}
	}
//	printf("result.numEntries = %d\n",result->numEntries);
/*	printf("numMatchingPTs = %d\n",numMatchingPTs);
	assert(numMatchingPTs >= 1);*/
	return result;
}

int Bucket::getBestAssignmentForBucket(const int* mpeAssignmentSoFar){
/*
	printf("\n  Best so far: ");
	output(num_vars, mpeAssignmentSoFar);
	printf("\n");
*/
	if(variables[num]->fakeEvidenceForMB) return variables[num]->value;
	int domSize = variables[num]->domSize;
	double* wholeMarginal = new double[domSize];
	double* singleMarginal = new double[domSize];
	int i,j;
	for(i=0; i<domSize; i++) wholeMarginal[i]=0;
	for(i=0; i<numPTs; i++){
		if(bucketPTs[i]->numPTVars != 0){ // PTs w/o vars are constants.
			bucketPTs[i]->getMarginal(mpeAssignmentSoFar, num, singleMarginal);
			for(j=0; j<domSize; j++) wholeMarginal[j] += singleMarginal[j];
		}
	}
//	printf("  Whole Marginals: ");
//	output(domSize, wholeMarginal);
	double best = -BIG;
	int result = -1;
	for(int val=0; val<domSize; val++) {
		if(wholeMarginal[val] > best){
			best = wholeMarginal[val];
			result = val;
		}
	}
	assert(result != -1);
	delete[] wholeMarginal;
	delete[] singleMarginal;
//	printf("\n  Best index: %d\n",result);
	for(i=0; i<numPTs; i++) delete bucketPTs[i];

	return result;
}

int Bucket::constructPartititons(int ib, double weightBound){
	int i;
//=== Identify subsumptions.
	bool iSubsumesJ, jSubsumesI;
	for(i=0; i<numPTs; i++){
		for(int j=i+1; j<numPTs; j++){
			jSubsumesI = subset(bucketPTs[i]->numPTVars, bucketPTs[i]->ptVars, bucketPTs[j]->numPTVars, bucketPTs[j]->ptVars);
			iSubsumesJ = subset(bucketPTs[j]->numPTVars, bucketPTs[j]->ptVars, bucketPTs[i]->numPTVars, bucketPTs[i]->ptVars);
			if(jSubsumesI) subPTs[i] = j; // also if they are equal
			else{
				if(iSubsumesJ) subPTs[j] = i;
			}
		}
	}

//	outputBucket();
/*	
	printf("subPTs: ");
	output(numPTs, subPTs);
	printf("\n");
*/
//	printf("numPartitions=%d\n",numPartitions);

//=== Construct partitions.
	int numVarsInPartition = 0;
//	printf("constructing partition for bucket %d\n",num);
	for(i=0; i<numPTs; i++){
		if(subPTs[i] == -1){ // not subsumed
			addAllToFrom(varsInPartition, &numVarsInPartition, bucketPTs[i]->ptVars, bucketPTs[i]->numPTVars);
			if (numVarsInPartition > ib || sizeOfVariableSet(numVarsInPartition, varsInPartition) > weightBound){
			//=== Start next partition.
			//=== (unless there are only the new vars in the partition. In that case,
			//=== it was empty before. But at least one PT has to be in the partition.)
				if(numVarsInPartition != bucketPTs[i]->numPTVars) numPartitions++; 
				numVarsInPartition = bucketPTs[i]->numPTVars;
				copy_from_to(bucketPTs[i]->ptVars, varsInPartition, numVarsInPartition);
			}
			partition[i] = numPartitions;
		}
	}

	numPartitions++;
	
//	outputBucket();

//=== Iteratively assign partitions to subsumed PTs until no more subsumed ones left.
	bool changed = true;
	while(changed){
		changed = false;
		for(i=0; i<numPTs; i++){
			if(partition[i] == -1){ // still unassigned
				if(partition[subPTs[i]] == -1) continue;
				partition[i] = partition[subPTs[i]];
				changed = true;
			}
		}
	}

	for(i=0; i<numPTs; i++) assert(partition[i] != -1);

/*
	printf("partitions: ");
	output(numPTs, partition);
	printf("\n");
*/
	return numPartitions;

}