#include "AssignmentManager.h"

void AssignmentManager::outputCurrentAssignment(FILE* outfile){
	copyAssignment(tmpAssignment);
	outputAssignment(outfile, tmpAssignment);
}

void AssignmentManager::outputAssignment(FILE* outfile, int* assignment){
	fprintf(outfile, "assignment %lf[", get_log_score(assignment));
	for(int i=0; i<numVars-1; i++){
		fprintf(outfile, "%d ", assignment[i]);
	}
	fprintf(outfile, "%d]\n", assignment[numVars-1]);
}

void AssignmentManager::endRun(){
	if(runBestLogProb >= optimalLogMPEValue - EPS){
		numRunsWithOptimumFound++;
	}
	if( runBestLogProb < worstSolutionCostFound ) worstSolutionCostFound = runBestLogProb;
	avgSolutionCostFound += runBestLogProb;

	//=== Gather all best solutions.
	if(runBestLogProb > overallBestLogProb - EPS){  
		int i;
		if(runBestLogProb > overallBestLogProb + EPS){  // new better.
			overallBestLogProb = runBestLogProb;
			bestTime = runBestTime;
			bestRun = numRun;
			numGlobalBestAssignments = numRunBestAssignments;
			for(i=0; i<numRunBestAssignments; i++){
				copy_from_to(runBestAssignments[i], globalBestAssignments[i], numVars);
			}
		} else {   // (new?) equally good.
			for(i=0; i<numRunBestAssignments;i++){
				for(int j=0; j<numGlobalBestAssignments; j++){
					if(same_array(runBestAssignments[i], globalBestAssignments[j],numVars)) break;
					if( j==numGlobalBestAssignments-1){
						copy_from_to(runBestAssignments[i],globalBestAssignments[numGlobalBestAssignments++], numVars);
						if( numGlobalBestAssignments >= MAX_NUM_OF_OPTIMAL_CONFIGURATIONS){
							fprintf(stderr, "Too many best overall assignments (%d). Did not allocate as much memory.\nExiting.", numGlobalBestAssignments);
							assert(false);
						}
						break;
					}
				}
			}
		}
	}
}

bool AssignmentManager::updateIfNewBest(double log_prob){
	bool gotBetter = false;
	//	printf("LOG %.5lf\n",log_prob);
	if(log_prob > runBestLogProb - EPS){
		if(log_prob > runBestLogProb + EPS){  // new better.
			gotBetter = true;
			runBestLogProb = log_prob;
			copyAssignment(runBestAssignments[0]);
			numRunBestAssignments = 1;

			bool debug = false;
			if(debug){
				assert(fabs(get_log_score() - get_log_score(runBestAssignments[0]))<EPS);
				if(fabs(get_log_score(runBestAssignments[0]) - runBestLogProb) > EPS){
					fprintf(stdout, "Log probability %lf of best run assignment does not match %lf\n", get_log_score(runBestAssignments[0]), runBestLogProb);
					fprintf(stderr, "Log probability %lf of best run assignment does not match %lf\n", get_log_score(runBestAssignments[0]),  runBestLogProb);
					assert(false);
				}
			}
		} else {  // (new?) equally good.
			if(!outputLM) return gotBetter;
			for(int i=0; i<numRunBestAssignments;i++){
				if(sameAssignment(runBestAssignments[i])) break;
				if( i==numRunBestAssignments-1){
					copyAssignment(runBestAssignments[numRunBestAssignments++]);
					if( numRunBestAssignments >= MAX_NUM_OF_OPTIMAL_CONFIGURATIONS){
						fprintf(stderr, "Too many best run assignments (%d). Did not allocate as much memory.\nExiting.", numRunBestAssignments);
						assert(false);
					}
					break;
				}
			}
		}
	}
	return gotBetter;
}

void AssignmentManager::outputRunLMs(FILE* outfile){
	fprintf(outfile, "\nOptimal MPE found: %s\n\n", foundOptimalThisRun() ? "yes" : "no"); 
	if( outputLM )
		fprintf(outfile, "  numbest %d\n", numRunBestAssignments);
	for(int i=0; i<numRunBestAssignments; i++){
		outputAssignment(outfile, runBestAssignments[i]);

		if(fabs(get_log_score(runBestAssignments[i]) - runBestLogProb) > EPS){
			fprintf(outfile, "Log probability %lf of run assignment %d does not match %lf\n", get_log_score(runBestAssignments[i]), i, runBestLogProb);
			fprintf(stderr, "Log probability %lf of run assignment %d does not match %lf\n", get_log_score(runBestAssignments[i]), i, runBestLogProb);
			assert(false);
		}
	}
}

double AssignmentManager::get_log_score(int* values){
	double result = 0;
	int pot,j;

//==== Compute temporary potential indices and 
//==== return the sum of the log_probs at these indices.
	int tmp_pot_ind;
	long factor;
	for(pot=0; pot<num_pots; pot++){
		tmp_pot_ind = 0;
		factor = 1;
		for(j=localProbTables[pot]->numPTVars-1; j>=0; j--){
			tmp_pot_ind += factor*values[localProbTables[pot]->ptVars[j]];
			factor *= variables[localProbTables[pot]->ptVars[j]]->domSize;
		}
		result += localProbTables[pot]->logCPT[tmp_pot_ind];
		localProbTables[pot]->index = tmp_pot_ind; // this is done to be able to use pot_index in the pertubation!
	}
	return result;
}

void AssignmentManager::outputGlobalLMs(FILE* outfile){
	fprintf(outfile, "\n================== STATS ================\n");
	fprintf(outfile, "Worst solution quality found: %lf\n", worstSolutionCostFound);
	fprintf(outfile, "Average solution quality found: %lf\n", avgSolutionCostFound / numRun);
	fprintf(outfile, "Best solution quality found: %lf\n", overallBestLogProb);
	fprintf(outfile, "%d/%d runs found optimal MPE.\n", numRunsWithOptimumFound, numRun); 
	fprintf(outfile, "=========================================\n\n");

	if( outputLM ){
		fprintf(outfile, "Found %d assignments with same best logprob %lf\n", numGlobalBestAssignments, overallBestLogProb);
	} else {
		fprintf(outfile, "Found assignment with best logprob %lf\n", overallBestLogProb);
	}

	int i;
	for(i=0; i<numGlobalBestAssignments; i++){
		for(int j=i+1; j<numGlobalBestAssignments; j++){
			if(same_array(globalBestAssignments[i], globalBestAssignments[j],num_vars)){
				fprintf(outfile, "Assignments %d and %d are equal! Error, exiting.\n", i,j);		
				fprintf(stderr, "Assignments %d and %d are equal! Error, exiting.\n", i,j);
				assert(false);
			};
		}

		if(fabs(get_log_score(globalBestAssignments[i]) - overallBestLogProb) > EPS){
			fprintf(outfile, "Log probability %lf of assignment %d does not match %lf\n", get_log_score(globalBestAssignments[i]), i, overallBestLogProb);
			fprintf(stderr, "Log probability %lf of assignment %d does not match %lf\n", get_log_score(globalBestAssignments[i]), i, overallBestLogProb);
			assert(false);
		}
	
		for(i=0; i<numGlobalBestAssignments; i++){
			outputAssignment(outfile, globalBestAssignments[i]);
		}
	}
}

double AssignmentManager::get_log_score(){
	for(int var=0; var<numVars; var++) tmpAssignment[var] = variables[var]->value;
	return get_log_score(tmpAssignment);
}