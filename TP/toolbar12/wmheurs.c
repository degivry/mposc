/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmheurs.c
  $Id: wmheurs.c,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"
#include "wmauxiliar.h"
#include "wmtypes.h"
#include "wmheurs.h"

#define LC_CHECK 0 // set it to "1" if you wanna do checks to verify the consistency

extern int pruneTimes;

/*************************** HEURISTICS FOR VARIABLE SELECTION *********************************/
func_t HeurVarFuncWM[7];

/* PRECOMPUTED JEROSLOWS */

int emptyFunction1(problem *p,int i,int j)
{
	// Nothing to do
	// Used when Jeroslow is not used
	
	return TRUE;
}

int emptyFunction2(problem *p,int i)
{
	// Nothing to do
	// Used when Jeroslow is not used

	return TRUE;	
}

int nopassBinaryJeroslowExecute(problem *p,int i,int j)
{
	
	p->jeroslows[i]-= (p->BinaryCosts[i][j][FALSE][FALSE] +p->BinaryCosts[i][j][FALSE][TRUE] +p->BinaryCosts[i][j][TRUE][FALSE] + p->BinaryCosts[i][j][TRUE][TRUE])*p->pows[p->maximumArity-2];
		
	p->jeroslows[j]-= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];
      
	return TRUE;
}

int passBinaryJeroslowExecute(problem *p,int i,int j)
{	
	p->jeroslows[i]+= (p->BinaryCosts[i][j][FALSE][FALSE] +p->BinaryCosts[i][j][FALSE][TRUE] +p->BinaryCosts[i][j][TRUE][FALSE] + p->BinaryCosts[i][j][TRUE][TRUE])*p->pows[p->maximumArity-2];
	
	p->jeroslows[j]+= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];
	
	return TRUE;
}

int nopassJeroslowDeactivateExecute(problem *p,int i)
{
	int j;
	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars))
	{
		j=getActualReferenceVar(p->notAssignedVars)->indexToVariable;
		p->jeroslows[j]-= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];
		getNextLD(p->notAssignedVars);
	}
	return TRUE;
}

int passJeroslowDeactivateExecute(problem *p,int i)
{	
	int j;
	
	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars))
	{
		j=getActualReferenceVar(p->notAssignedVars)->indexToVariable;
		p->jeroslows[j]+= (p->BinaryCosts[j][i][FALSE][FALSE] +p->BinaryCosts[j][i][FALSE][TRUE] +p->BinaryCosts[j][i][TRUE][FALSE] + p->BinaryCosts[j][i][TRUE][TRUE])*p->pows[p->maximumArity-2];
		getNextLD(p->notAssignedVars);
	}
	return TRUE;
}



func_t nopassBinaryJeroslow[7] = {emptyFunction1,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute,nopassBinaryJeroslowExecute};

func_t passBinaryJeroslow[7] = {emptyFunction1,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute,passBinaryJeroslowExecute};

func_t nopassJeroslowDeactivate[7] = {emptyFunction2,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute,nopassJeroslowDeactivateExecute};

func_t passJeroslowDeactivate[7] = {emptyFunction2,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute,passJeroslowDeactivateExecute};

int iniVarJeroslow(int i, problem *p)
{
	// initialize variable 'i' with jeroslow values
	int res;
	ReferenceToClause * rc;
	clause * c;
	NodeLD * lAux; // to have a second iterator.
	int j;
	res=0;

	// NON BINARY COSTS
	setFirstLD(p->variables[i].listLiterals);
	while (!endListLD(p->variables[i].listLiterals))
	{
		rc=getActualReference(p->variables[i].listLiterals);
		c=&(p->clauses[rc->indexToClause]);
		if(c->elim==FALSE && c->literalsWithoutAssign>2) // Only consider this clause if it is not eliminate previously (more than two literals)
			res+= (c->weight ) * p->pows[p->maximumArity-c->literalsWithoutAssign];
		getNextLD(p->variables[i].listLiterals);
	}

	setFirstLD(p->variables[i].listNoLiterals);
	while (!endListLD(p->variables[i].listNoLiterals))
	{
		rc=getActualReference(p->variables[i].listNoLiterals);
		c=&(p->clauses[rc->indexToClause]);
		if(c->elim==FALSE && c->literalsWithoutAssign>2) // Only consider this clause if it is not eliminate previously (more than two literals)
			res+= (c->weight ) * p->pows[p->maximumArity-c->literalsWithoutAssign];
		getNextLD(p->variables[i].listNoLiterals);
	}

	// BINARY COSTS
	lAux=p->notAssignedVars->first;

	while(lAux!=NULL)
	{
		j=((ReferenceToVariable *)(lAux->elem))->indexToVariable;
		res+= (p->BinaryCosts[i][j][FALSE][FALSE] +p->BinaryCosts[i][j][FALSE][TRUE] +p->BinaryCosts[i][j][TRUE][FALSE] + p->BinaryCosts[i][j][TRUE][TRUE])*p->pows[p->maximumArity-2];
		lAux=lAux->next;
	}

	return res;	
}


void initializeJeroslow(problem *p)
{
	// Two sided-Jeroslow Heuristic to choice the next variable to consider	
	// Before entering the branch and bound, we must initilize the array where we will
	// compute incremental jeroslow

	ReferenceToVariable * rv;
       
	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars))
	{
	        rv=getActualReferenceVar(p->notAssignedVars);
	        p->jeroslows[rv->indexToVariable]+= iniVarJeroslow(rv->indexToVariable,p);
		getNextLD(p->notAssignedVars);
	}
}

int WM_Jeroslow_like(problem *p)
{
	ReferenceToVariable * rv;
	int jAct,j;
	int max;
	// returns the first variable with a domain of only one value
	// else the variable with higher jeroslow       
	NodeLD * rvMax = NULL;

	setFirstLD(p->notAssignedVars);
	j=0;
	max=0;
	
	while (!endListLD(p->notAssignedVars))	
	{
		rv=getActualReferenceVar(p->notAssignedVars);
		if (p->ArrayOfDecision[rv->indexToVariable].not_considered_values==1)
			return (rv->indexToVariable);
		jAct=p->jeroslows[rv->indexToVariable]+(p->UnaryCosts[rv->indexToVariable][FALSE]+ p->UnaryCosts[rv->indexToVariable][TRUE]) * p->pows[p->maximumArity-1];
		
		if (jAct>=j)
		{	
			j=jAct;
			max=rv->indexToVariable;
			rvMax=p->notAssignedVars->actual;
		}
		getNextLD(p->notAssignedVars);
	}
	p->notAssignedVars->actual = rvMax;
	return max;
}





int WM_sort_var_lex(problem *p)
{
	// Returns the minimum variable of the list of not assigned variables.
	// Example: 23,34,56,7,12,2,12,11 -> It will select "2".
       
        int jAct,j;
	NodeLD * rvMax = NULL;
	
	setFirstLD(p->notAssignedVars);
	j=p->totalVariables;
	while (!endListLD(p->notAssignedVars))
	{
		jAct=getActualReferenceVar(p->notAssignedVars)->indexToVariable;
		if (jAct<=j)
		{
			j=jAct;
			rvMax=p->notAssignedVars->actual;
		}
		getNextLD(p->notAssignedVars);
	}
	p->notAssignedVars->actual = rvMax;
	return j;
}

func_t HeurVarFuncWM[] = {WM_sort_var_lex,WM_Jeroslow_like,WM_Jeroslow_like,WM_Jeroslow_like,WM_Jeroslow_like,WM_Jeroslow_like,WM_Jeroslow_like};

/*************************** HEURISTICS FOR VALUE SELECTION *********************************/

int WM_sort_val_lex(problem *p,restoreStruct *r)
{
	// Returns the first value to consider. In this case
	// always false is the first, and True the second.
	
	return FALSE;
}

int WM_min_unary_cost(problem *p,restoreStruct *r)
{
	// Returns the first value to consider. It will be the value with minimum unary cost.
	
	if(p->UnaryCosts[r->var][FALSE]<=p->UnaryCosts[r->var][TRUE]) return FALSE;
	return TRUE;
}

func_t HeurValFuncWM[2] = {WM_sort_val_lex,WM_min_unary_cost};

/*************************** LOCAL CONSISTENCY HEURISTICS *********************************/

/* FUNCTIONS TO MANTAIN NODE CONSISTENCY */

void initQandR(problem * p, restoreStruct * r)
{
	// Precondition: Any variable has been assigned yet.
	// set all the variables to the Q and R stack
	setFirstLD(p->notAssignedVars);
	while (endListLD(p->notAssignedVars)!=TRUE)
	{
		pushStack(p->Q,getActualReferenceVar(p->notAssignedVars)->indexToVariable);
		pushStack(p->R,getActualReferenceVar(p->notAssignedVars)->indexToVariable);
		getNextLD(p->notAssignedVars);
	}	
}

int checkNC(problem * p, int i)
{
	int tot=0;
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if((p->LB+p->UnaryCosts[i][FALSE])>=p->UB)	
		{
			printf("\nNode inconsistent for X%d=FALSE",i);
			return FALSE;
		}
		if(p->UnaryCosts[i][FALSE]==BOTTOM) tot++;	
	}
	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if((p->LB+p->UnaryCosts[i][TRUE])>=p->UB)	
		{
			printf("\nNode inconsistent for X%d=TRUE",i);
			return FALSE;
		}
		if(p->UnaryCosts[i][TRUE]==BOTTOM) tot++;
	}

	if(tot>0)
		return TRUE;	
	else
	{
		printf("\nNode inconsistent for X%d",i);
		return FALSE;
	}
}



int checkNodeConsistency(problem * p)
{
	ReferenceToVariable * rv;

	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars))	
	{		
		rv=getActualReferenceVar(p->notAssignedVars);
		if(!checkNC(p,rv->indexToVariable)) return FALSE;
		getNextLD(p->notAssignedVars);
	}
	return TRUE;
}

int projectUnary(problem *p, restoreStruct * r,int i)
{	
	// pass costs from Ci to Lower bound
	int min;

	if(p->UnaryCosts[i][p->SupU[i]]>BOTTOM)
	{
		if(p->ArrayOfDecision[i].not_considered_values == 2)
		{
			min=minV(p->UnaryCosts[i][FALSE],p->UnaryCosts[i][TRUE]);
			if (min>BOTTOM)
			{
				p->UnaryCosts[i][FALSE]-= min;
				p->UnaryCosts[i][TRUE]-= min;
				p->LB=p->LB+min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,min));	
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,min));
				if(p->UnaryCosts[i][FALSE]==BOTTOM) p->SupU[i]=FALSE;
				else p->SupU[i]=TRUE;

				return TRUE;
			}
			
		}	
		else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
		{
			min=p->UnaryCosts[i][FALSE];
			if (min>BOTTOM)
			{
				p->UnaryCosts[i][FALSE]-= min;
				p->LB=p->LB+min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,min));
				p->SupU[i]=FALSE;

				return TRUE;
			}
		}

		else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)	
		{
			
			min=p->UnaryCosts[i][TRUE];
			if (min>BOTTOM)
			{
				p->UnaryCosts[i][TRUE]-= min;
				p->LB=p->LB+min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,min));
				p->SupU[i]=TRUE;

				return TRUE;
			}
		}
	}

	return FALSE;	
}



int binaryToUnaryAndNodeConsistency(problem *p,restoreStruct *r)
{
	ReferenceToVariable * rv;

	// Create all the lists for restoration of local consistency
	createListRAux(&r->pruneList);
	createListRAux(&r->NCList);
	createListRAux(&r->ACList);

	r->NClimit=NO_VALUE;
	(*nopassJeroslowDeactivate[HeurVar])(p,r->var);
	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars))
	{
		rv=getActualReferenceVar(p->notAssignedVars);
		

		if(p->UnaryCosts[rv->indexToVariable][FALSE]==BOTTOM && (p->UnaryCosts[rv->indexToVariable][FALSE]+p->BinaryCosts[r->var][rv->indexToVariable][r->val][FALSE])>BOTTOM)
		{
			pushStack(p->R,rv->indexToVariable);
		}

		if(p->UnaryCosts[rv->indexToVariable][TRUE]==BOTTOM && (p->UnaryCosts[rv->indexToVariable][TRUE]+p->BinaryCosts[r->var][rv->indexToVariable][r->val][TRUE])>BOTTOM)
		{
			pushStack(p->R,rv->indexToVariable);
		}

		// We pass binary costs to unary
		p->UnaryCosts[rv->indexToVariable][FALSE]+=p->BinaryCosts[r->var][rv->indexToVariable][r->val][FALSE];
		p->UnaryCosts[rv->indexToVariable][TRUE]+=p->BinaryCosts[r->var][rv->indexToVariable][r->val][TRUE];
		projectUnary(p,r,rv->indexToVariable);
		

		if(p->LB>=p->UB)			
		{
			r->NClimit=rv->indexToVariable;
			return FALSE;
		}

		getNextLD(p->notAssignedVars);
	}
	return TRUE;
}



int restoreBinaryToUnaryLimited(problem *p,restoreStruct *r)
{
	ReferenceToVariable * rv;
	int end=FALSE;
	
	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars) && !end)
	{
		rv=getActualReferenceVar(p->notAssignedVars);
		p->UnaryCosts[rv->indexToVariable][FALSE]-=p->BinaryCosts[r->var][rv->indexToVariable][r->val][FALSE];
		p->UnaryCosts[rv->indexToVariable][TRUE]-=p->BinaryCosts[r->var][rv->indexToVariable][r->val][TRUE];
		if(r->NClimit==rv->indexToVariable) end=TRUE;
		getNextLD(p->notAssignedVars);
	}

	(*passJeroslowDeactivate[HeurVar])(p,r->var);
	
	return TRUE;	
}



int executeNodeConsistency(problem *p, restoreStruct * r)
{
	// NC: Node Consistency
	ReferenceToVariable * rv;

	// Create all the lists for restoration of local consistency
	createListRAux(&r->pruneList);
	createListRAux(&r->NCList);
	createListRAux(&r->ACList);
       
	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars))
	{
		rv=getActualReferenceVar(p->notAssignedVars);
		projectUnary(p,r,rv->indexToVariable);
		if(p->LB>=p->UB) return FALSE;
		getNextLD(p->notAssignedVars);
	}
	
#if (LC_CHECK==1) 
	checkNodeConsistency(p);
#endif
	return TRUE;
}



void restoreNodeConsistency(problem *p, restoreStruct * r)
{
	NodeR *n;
	setFirstLR(&r->NCList);

	while(!endListLR(&r->NCList))	
	{
		n=extractActualR(&r->NCList);
		p->UnaryCosts[n->val1][n->val2]+= n->val3;
		insertElement(&p->restoreList,n);
	}
}


/* FUNCTIONS TO MANTAIN AC CONSISTENCY (AC3)*/


int pruneVar(problem *p, restoreStruct *r,int var)
{
	// Prune values from a variable. Returns FALSE if all values are pruned (do backtracking at the BB).
	// FALSE
	
	if(p->ArrayOfDecision[var].usedValue[FALSE]==FALSE)
	{
		if ((p->UnaryCosts[var][FALSE] + p->LB)>=p->UB)
		{
			insertElement(&r->pruneList,getNewNode2(&p->restoreList,var,FALSE));
			p->ArrayOfDecision[var].usedValue[FALSE]=TRUE;
			p->ArrayOfDecision[var].not_considered_values--;
			pushStack(p->Q,var);
			pruneTimes++;
		}
	}	

	// TRUE
	if(p->ArrayOfDecision[var].usedValue[TRUE]==FALSE)
	{
		if ((p->UnaryCosts[var][TRUE] + p->LB)>=p->UB)
		{
			insertElement(&r->pruneList,getNewNode2(&p->restoreList,var,TRUE));
			p->ArrayOfDecision[var].usedValue[TRUE]=TRUE;
			p->ArrayOfDecision[var].not_considered_values--;
			pushStack(p->Q,var);
			pruneTimes++;
		}
	}

	if(p->ArrayOfDecision[var].not_considered_values==0)
	{
		return FALSE;
	}

	return TRUE;
}



int orP(int a,int b)
{
	
	if((a==TRUE) || (b==TRUE)) return TRUE;
	
	else return FALSE;
	
}

int findSupportsAC3(problem *p,restoreStruct * r, int i, int j)
{

	// Find supports: Cij binary -> Ci unary + Project Ci
	int min;
	int flag=FALSE;
	
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			min=minV(p->BinaryCosts[i][j][FALSE][FALSE],p->BinaryCosts[i][j][FALSE][TRUE]);
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][TRUE]-=min;
				p->BinaryCosts[i][j][FALSE][FALSE]-=min;
				p->BinaryCosts[j][i][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,min));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,min));
				
				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==TRUE)
		{
			min=p->BinaryCosts[i][j][FALSE][FALSE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,min));

				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE && p->ArrayOfDecision[j].usedValue[FALSE]==TRUE)
		{
			min=p->BinaryCosts[i][j][FALSE][TRUE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][FALSE][TRUE]-= min;
				p->BinaryCosts[j][i][TRUE][FALSE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);

				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,min));

				if(p->UnaryCosts[i][FALSE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][FALSE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,FALSE,-min));
				flag=TRUE;
			}
		}
	}

	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			min=minV(p->BinaryCosts[i][j][TRUE][FALSE],p->BinaryCosts[i][j][TRUE][TRUE]);
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][TRUE]-= min;
				p->BinaryCosts[i][j][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][TRUE][TRUE]-= min;
				p->BinaryCosts[j][i][FALSE][TRUE]-= min;
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,min));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,min));
				
				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==TRUE)
		{
			min=p->BinaryCosts[i][j][TRUE][FALSE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][FALSE]-= min;
				p->BinaryCosts[j][i][FALSE][TRUE]-= min;
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,min));
				
				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE  && p->ArrayOfDecision[j].usedValue[FALSE]==TRUE)
		{
			min=p->BinaryCosts[i][j][TRUE][TRUE];
			if(min>BOTTOM)
			{
				(*nopassBinaryJeroslow[HeurVar])(p,i,j);
				p->BinaryCosts[i][j][TRUE][TRUE]-= min;
				p->BinaryCosts[j][i][TRUE][TRUE]-= min;
				(*passBinaryJeroslow[HeurVar])(p,i,j);
				setIsBinaryPair(p,i,j,isBinaryPair(p,i,j));
				insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,min));

				if(p->UnaryCosts[i][TRUE]==BOTTOM)
				{
					pushStack(p->R,i); // this variable must be checked by DAC
				}
				p->UnaryCosts[i][TRUE]+= min;
				insertElement(&r->NCList,getNewNode3(&p->restoreList,i,TRUE,-min));
				flag=TRUE;
			}
		}
	}

	return orP(flag,projectUnary(p,r,i));
}

int existSupport(problem * p,int i,int j)
{
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{	
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][FALSE][FALSE]!=0 && p->BinaryCosts[i][j][FALSE][TRUE]!=0)
				
				return FALSE;
		}
		
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][FALSE][FALSE]!=0) return FALSE;
		}
			
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
				
			if(p->BinaryCosts[i][j][FALSE][TRUE]!=0) return FALSE;
		}		
	}
	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][TRUE][FALSE]!=0 && p->BinaryCosts[i][j][TRUE][TRUE]!=0)
				
			  return FALSE;
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{
			
			if(p->BinaryCosts[i][j][TRUE][FALSE]!=0) return FALSE;
                }
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{

			if(p->BinaryCosts[i][j][TRUE][TRUE]!=0) return FALSE;
		}	
	}
	
	return TRUE;
}

void showAC3(problem * p, int i, int j)
{
	
	printf("\ni=FALSE: %d",p->ArrayOfDecision[i].usedValue[FALSE]);
	
	printf("\ni=TRUE: %d",p->ArrayOfDecision[i].usedValue[TRUE]);
	
	printf("\nj=FALSE: %d",p->ArrayOfDecision[j].usedValue[FALSE]);
	
	printf("\nj=TRUE: %d",p->ArrayOfDecision[j].usedValue[TRUE]);
	
	printf("\ni=FALSE -> j =FALSE :%d",p->BinaryCosts[i][j][FALSE][FALSE]);
	
	printf("\ni=FALSE -> j =TRUE :%d",p->BinaryCosts[i][j][FALSE][TRUE]);
	
	printf("\ni=TRUE -> j =FALSE :%d",p->BinaryCosts[i][j][TRUE][FALSE]);
	
	printf("\ni=TRUE -> j =TRUE :%d",p->BinaryCosts[i][j][TRUE][TRUE]);	
}



int checkAC3(problem * p, restoreStruct * r)
{
	int i,j;
	NodeLD * lAux; // to have a second iterator.
	setFirstLD(p->notAssignedVars);
       
	while(!endListLD(p->notAssignedVars))	
	{
		j=getActualReferenceVar(p->notAssignedVars)->indexToVariable;
		if(!checkNC(p,j)) return FALSE;
		lAux=p->notAssignedVars->first;

		while(lAux!=NULL)			
		{
			i=((ReferenceToVariable *)(lAux->elem))->indexToVariable;
			if (existSupport(p,i,j)==FALSE)
			{
				printf("\nNot exists support between %d and %d",i,j);
				showAC3(p,i,j);
			}
			lAux=lAux->next;
		}
		getNextLD(p->notAssignedVars);
	}
	return TRUE;
}

int executeAC3(problem * p, restoreStruct * r)
{
	// AC: Arc consistency computed with the well-known AC3 algorithm
	int flag;
	int i,j;
	ReferenceToVariable *rv;
	
	while(p->Q->n_elem>0)
	{
		flag=FALSE;
		j=popStack(p->Q);
		
		setFirstLD(p->notAssignedVars);

		while(!endListLD(p->notAssignedVars))
		{
			i=getActualReferenceVar(p->notAssignedVars)->indexToVariable;
			if(p->isBinary[i][j])
			{
			flag = orP(flag,findSupportsAC3(p,r,i,j));
			if(p->LB>=p->UB) return FALSE;
			}
			getNextLD(p->notAssignedVars);
		} // end while not assigned vars

		if(flag)
		{
			// Prune new values with the new low-bound
			setFirstLD(p->notAssignedVars);
			while (endListLD(p->notAssignedVars)==FALSE)
			{
				rv=(ReferenceToVariable *) getActualReferenceVar(p->notAssignedVars);
				if (pruneVar(p,r,rv->indexToVariable)==FALSE) return FALSE;
				getNextLD(p->notAssignedVars);;
			}
		} // flag = TRUE
	} // end while Q

#if (LC_CHECK == 1)

	if (LcLevel==LC_AC)
		
		checkAC3(p,r);
	
#endif
	return TRUE;
	
}

void restoreAC3(problem *p, restoreStruct * r)
{
	NodeR *n;

	setFirstLR(&r->ACList);
	while(!endListLR(&r->ACList))
	{
		n=extractActualR(&r->ACList);

		(*nopassBinaryJeroslow[HeurVar])(p,n->val1,n->val2);
		p->BinaryCosts[n->val1][n->val2][n->val3][n->val4]+= n->val5;
		p->BinaryCosts[n->val2][n->val1][n->val4][n->val3]+= n->val5;
		setIsBinaryPair(p,n->val1,n->val2,isBinaryPair(p,n->val1,n->val2));
		(*passBinaryJeroslow[HeurVar])(p,n->val1,n->val2);

		insertElement(&p->restoreList,n);
	}
}


/* FUNCTIONS TO MANTAIN DAC CONSISTENCY */

void calculateDACValues(problem *p,int i,int j,int *eT,int *eF)
{
	int pT=0,pF=0;

	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE && p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pT=minV((p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE]),(p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE]));
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
			pT=p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE];
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pT=(p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE]);
	}
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE && p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pF=minV((p->BinaryCosts[i][j][FALSE][TRUE]+p->UnaryCosts[j][TRUE]),(p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE]));
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
			pF=p->BinaryCosts[i][j][FALSE][TRUE]+p->UnaryCosts[j][TRUE];
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
			pF=(p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE]);
	}
	if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE && p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eT=maxV(pT-p->BinaryCosts[i][j][TRUE][TRUE],pF-p->BinaryCosts[i][j][FALSE][TRUE]);
		else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
			*eT=pT-p->BinaryCosts[i][j][TRUE][TRUE];
		else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eT=pF-p->BinaryCosts[i][j][FALSE][TRUE];
	}
	else *eT=0;
	if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
	{
		if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE && p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eF=maxV(pT-p->BinaryCosts[i][j][TRUE][FALSE],pF-p->BinaryCosts[i][j][FALSE][FALSE]);
		else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
			*eF=pT-p->BinaryCosts[i][j][TRUE][FALSE];
		else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
			*eF=pF-p->BinaryCosts[i][j][FALSE][FALSE];
	}
	else *eF=0;
	
}

void executeExtend(problem *p,restoreStruct * r,int i,int j)
{
	// Find extensions: Cj unary -> Cij binary
	int k1,k2;
	if(p->UnaryCosts[j][FALSE]!=0 || p->UnaryCosts[j][TRUE]!=0)
	{

		calculateDACValues(p,i,j,&k2,&k1);

		if(k1>0)
		{
			p->UnaryCosts[j][FALSE]=p->UnaryCosts[j][FALSE]-k1;
			insertElement(&r->NCList,getNewNode3(&p->restoreList,j,FALSE,k1));
			(*nopassBinaryJeroslow[HeurVar])(p,i,j);
			p->BinaryCosts[i][j][FALSE][FALSE]+= k1;
			p->BinaryCosts[i][j][TRUE][FALSE]+= k1;
			p->BinaryCosts[j][i][FALSE][FALSE]+= k1;
			p->BinaryCosts[j][i][FALSE][TRUE]+= k1;
			(*passBinaryJeroslow[HeurVar])(p,i,j);

			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,FALSE,-k1));
			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,FALSE,-k1));
		}
	
		if(k2>0)			
		{
			p->UnaryCosts[j][TRUE]=p->UnaryCosts[j][TRUE]-k2;
			insertElement(&r->NCList,getNewNode3(&p->restoreList,j,TRUE,k2));

			(*nopassBinaryJeroslow[HeurVar])(p,i,j);
			p->BinaryCosts[i][j][FALSE][TRUE]+= k2;
			p->BinaryCosts[i][j][TRUE][TRUE]+= k2;
			p->BinaryCosts[j][i][TRUE][FALSE]+= k2;		
			p->BinaryCosts[j][i][TRUE][TRUE]+= k2;
			(*passBinaryJeroslow[HeurVar])(p,i,j);
			
			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,TRUE,TRUE,-k2));
			insertElement(&r->ACList,getNewNode5(&p->restoreList,i,j,FALSE,TRUE,-k2));
		}
	}	
}


int findFullSupports(problem *p,restoreStruct * r,int i,int j)
{
	// Find extensions: Cj unary -> Cij binary
	executeExtend(p,r,i,j);
	
	// Find supports: Cij binary -> Ci unary + Project Ci
	return findSupportsAC3(p,r,i,j);
}



void showDAC(problem * p, int i, int j)
{
	
	printf("\ni=FALSE: %d W:%d",p->ArrayOfDecision[i].usedValue[FALSE],p->UnaryCosts[i][FALSE]);
	
	printf("\ni=TRUE: %d W:%d",p->ArrayOfDecision[i].usedValue[TRUE],p->UnaryCosts[i][TRUE]);
	
	printf("\nj=FALSE: %d W:%d",p->ArrayOfDecision[j].usedValue[FALSE],p->UnaryCosts[j][FALSE]);
	
	printf("\nj=TRUE: %d W:%d",p->ArrayOfDecision[j].usedValue[TRUE],p->UnaryCosts[j][TRUE]);
	
	printf("\ni=FALSE -> j =FALSE :%d",p->BinaryCosts[i][j][FALSE][FALSE]);
	
	printf("\ni=FALSE -> j =TRUE :%d",p->BinaryCosts[i][j][FALSE][TRUE]);
	
	printf("\ni=TRUE -> j =FALSE :%d",p->BinaryCosts[i][j][TRUE][FALSE]);
	
	printf("\ni=TRUE -> j =TRUE :%d",p->BinaryCosts[i][j][TRUE][TRUE]);
}

int existFullSupport(problem * p,int i,int j)
{
	if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)	
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			if((p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE])!=0 && (p->BinaryCosts[i][j][FALSE][TRUE] +p->UnaryCosts[j][TRUE])!=0)
				
				return FALSE;			
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{
			
			if((p->BinaryCosts[i][j][FALSE][FALSE]+p->UnaryCosts[j][FALSE])!=0) return FALSE;
			
		}
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{		
				if((p->BinaryCosts[i][j][FALSE][TRUE]+p->UnaryCosts[j][TRUE])!=0) return FALSE;
		}		
	}
	
	if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE && p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
			if((p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE])!=0 && (p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE])!=0)
				
				return FALSE;
		}
		else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
		{	
			if((p->BinaryCosts[i][j][TRUE][FALSE]+p->UnaryCosts[j][FALSE])!=0) return FALSE;
		}	
		else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
		{
				
		        if((p->BinaryCosts[i][j][TRUE][TRUE]+p->UnaryCosts[j][TRUE])!=0) return FALSE;
		}		
	}
	
	return TRUE;
}


int checkDAC(problem *p)
{
	
	int i,j;
	
	for(j=0;j<p->totalVariables;j++)
	{
		
		if(p->Result[j]==NO_VALUE) // only not assigned variables
		{
			if(!checkNC(p,j)) return FALSE;
			
			for(i=0;i<p->totalVariables;i++)
			{
				if(p->Result[i]==NO_VALUE) // only not assigned variables
				{
					if(p->order[i]<p->order[j]) // To mantain an order						
					{
						if (existFullSupport(p,i,j)==FALSE)
						{
							printf("\nNot exists full support between %d and %d",i,j);
							showDAC(p,i,j);							
						}
					}
					
				}
				
			}
			
		}
		
	}
	return TRUE;
}



int executeDAC(problem *p,restoreStruct * r)
{
	// DAC: Directed Arc-Consistency
	int j,i,k,flag=FALSE;
	ReferenceToVariable *rv;
	
	while(p->R->n_elem>0)
	{
		
		j=popStackMaxDom(p->R);
		
		if (pruneVar(p,r,j)==FALSE) return FALSE;
		
		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[i][j])
			{
				flag=orP(flag,findFullSupports(p,r,i,j));
				if(p->LB>=p->UB) return FALSE;
			}
		} // end for	
	} // end while R
	
	if(flag)
	{
		// Prune new values with the new low-bound
		setFirstLD(p->notAssignedVars);
		
		while (!endListLD(p->notAssignedVars))
		{
			rv=(ReferenceToVariable *) getActualReferenceVar(p->notAssignedVars);
			if (pruneVar(p,r,rv->indexToVariable)==FALSE) return FALSE;
			getNextLD(p->notAssignedVars);
		}
	} // flag = TRUE
	
#if (LC_CHECK==1)
	
	if (LcLevel==LC_DAC)
		
		checkDAC(p);
	
#endif
	
	return TRUE;
}


/* FUNCTIONS TO MANTAIN FDAC CONSISTENCY */



int executeFDAC(problem *p,restoreStruct *r)
{

	// FDAC: Full Directed arc consistency

	int stop;
	stop=FALSE;

	while((p->Q->n_elem>0 || p->R->n_elem>0) && !stop)
	{

		stop= !executeAC3(p,r) || !executeDAC(p,r);

	}

#if(LC_CHECK==1)

	if (LcLevel==LC_FDAC && !stop)
	{
		checkDAC(p);
		checkAC3(p,r);
	}

#endif

	return(!stop);

}

/* FUNCTIONS TO MANTAIN EDAC CONSISTENCY */

int calculateEpsilon(problem *p,int j,int k,int val)
{
	int res=0;
	if(p->ArrayOfDecision[k].not_considered_values==2)
	{
		res=minV((p->UnaryCosts[k][FALSE]+p->BinaryCosts[j][k][val][FALSE]),(p->UnaryCosts[k][TRUE]+p->BinaryCosts[j][k][val][TRUE]));
	}
	else if(p->ArrayOfDecision[k].usedValue[FALSE]==FALSE)
	{
		res=p->UnaryCosts[k][FALSE]+p->BinaryCosts[j][k][val][FALSE];
	}
	else if(p->ArrayOfDecision[k].usedValue[TRUE]==FALSE)
	{
		res=p->UnaryCosts[k][TRUE]+p->BinaryCosts[j][k][val][TRUE];
	}

	return res;
}


int condAdd(problem *p,int i)
{
	if(p->ArrayOfDecision[i].not_considered_values==2)
	{
		if(p->UnaryCosts[i][FALSE]+p->UnaryCosts[i][TRUE]>BOTTOM) return TRUE;
	}
	else if(p->ArrayOfDecision[i].usedValue[FALSE]==FALSE)
	{
		if(p->UnaryCosts[i][FALSE]>BOTTOM) return TRUE;
	}
	else if(p->ArrayOfDecision[i].usedValue[TRUE]==FALSE)
	{
		if(p->UnaryCosts[i][TRUE]>BOTTOM) return TRUE;
	}

	return FALSE;
}

void autoSetCondAdd(problem *p,int i)
{
	p->tmpArr[i]=condAdd(p,i);
}

void initConds(problem *p)
{
	int k;
	for(k=0;k<p->totalVariables;k++)
	{
		if(p->Result[k]==NO_VALUE)
		{
			autoSetCondAdd(p,k);
		}
	}
}

int execEAC(problem *p)
{
	initConds(p);
	if((p->UB-p->LB)==1) return FALSE;
	return TRUE;
}


int considerVar(problem *p,int i)
{
	return p->tmpArr[i];
}


int existsEpsilonSupport(problem *p,restoreStruct *r,int j)
{
	int k,epsilonF=0,epsilonT=0;

	if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE) epsilonF+=p->UnaryCosts[j][FALSE];
	else epsilonF=-1;

	if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE) epsilonT+=p->UnaryCosts[j][TRUE];
	else epsilonT=-1;

	for(k=0;k<p->totalVariables;k++)
	{
		if(p->Result[k]==NO_VALUE)
		{
			if(p->order[j]<p->order[k])
			{
				if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE) epsilonF+=calculateEpsilon(p,j,k,FALSE);
				if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE) epsilonT+=calculateEpsilon(p,j,k,TRUE);
			}
		}
	}

	if(p->ArrayOfDecision[j].not_considered_values==2)
	{
		if(epsilonF>0 && epsilonT>0)
		{
			printf("\nX%d:=FALSE (%d-%d)  X%d:=TRUE (%d-%d)\n",j,p->UnaryCosts[j][FALSE],epsilonF,j,p->UnaryCosts[j][TRUE],epsilonT);
			return FALSE;
		}
	}
	else
	{
		if(epsilonF==-1 && epsilonT>BOTTOM)
		{
			printf("X%d:=TRUE (%d)",j,epsilonT);
			return FALSE;
		}
		else if(epsilonF>BOTTOM && epsilonT==-1)
		{
			printf("X%d:=FALSE (%d)",j,epsilonF);
			return FALSE;
		}
	}


	return TRUE;
}

int checkEAC(problem *p,restoreStruct *r,int b)
{
	if(execEAC(p)==TRUE)
	{
		setFirstLD(p->notAssignedVars);
		while (!endListLD(p->notAssignedVars))
		{
			existsEpsilonSupport(p,r,getActualReferenceVar(p->notAssignedVars)->indexToVariable);
			getNextLD(p->notAssignedVars);
		}
	}

	return TRUE;
}

int moveCosts(problem *p,restoreStruct *r,int j,int *flag)
{

	int i,k;

	for(k=p->order[j]-1;k>=0;k--)
	{
		i=p->order2[k];
		if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
		{
				*flag=orP(*flag,findFullSupports(p,r,j,i));
				if(p->LB>=p->UB) return FALSE;
				autoSetCondAdd(p,i);
		}
	}


	autoSetCondAdd(p,j);

	return TRUE;

}



int existentialSupport(problem *p,restoreStruct *r,int j,int *flag)
{
	int i,k,epsilonF=0,epsilonT=0;

	if(p->ArrayOfDecision[j].not_considered_values==2)
	{
		epsilonF+=p->UnaryCosts[j][FALSE];

		epsilonT+=p->UnaryCosts[j][TRUE];

		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
			{
					epsilonF+=calculateEpsilon(p,j,k,FALSE);
					epsilonT+=calculateEpsilon(p,j,k,TRUE);
			}
		}

		if(epsilonF>BOTTOM && epsilonT>BOTTOM) moveCosts(p,r,j,flag);

	}

	else if(p->ArrayOfDecision[j].usedValue[FALSE]==FALSE)
	{
		epsilonF+=p->UnaryCosts[j][FALSE];

		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
			{
					epsilonF+=calculateEpsilon(p,j,k,FALSE);
			}
		}


		if(epsilonF>BOTTOM) moveCosts(p,r,j,flag);
	}

	else if(p->ArrayOfDecision[j].usedValue[TRUE]==FALSE)
	{
		epsilonT+=p->UnaryCosts[j][TRUE];

		for(k=p->order[j]-1;k>=0;k--)
		{
			i=p->order2[k];
			if(p->Result[i]==NO_VALUE && p->isBinary[j][i] && considerVar(p,i))
			{
					epsilonT+=calculateEpsilon(p,j,k,TRUE);
			}
		}


		if(epsilonT>BOTTOM) moveCosts(p,r,j,flag);
	}

	return TRUE;
}


int executeEAC(problem *p,restoreStruct *r)
{
	int i,flag;
	flag=FALSE;
	for(i=0;i<p->totalVariables;i++)
	{
		if(p->Result[i]==NO_VALUE)
		{
			if(!existentialSupport(p,r,i,&flag)) return FALSE;
		}
	}

	if(flag)
	{
		// Prune new values with the new low-bound
		for(i=0;i<p->totalVariables;i++)
		{
			if(p->Result[i]==NO_VALUE)
			{
				if (pruneVar(p,r,i)==FALSE) return FALSE;
			}
		}
	} // flag = TRUE


	return TRUE;
}


int executeEDAC(problem *p,restoreStruct *r)
{
	int stop;
	stop=FALSE;

	if(!stop && execEAC(p)==TRUE) stop=!executeEAC(p,r);

	do
	{
		if(!stop) stop=!executeDAC(p,r);
		if(!stop) stop=!executeAC3(p,r);
		if(!stop && execEAC(p)==TRUE) stop=!executeEAC(p,r);
	}
	while((p->Q->n_elem>0 || p->R->n_elem>0) && !stop);


#if(LC_CHECK==1)

	if (LcLevel==LC_EDAC && !stop)
	{
		checkNodeConsistency(p);
		checkDAC(p);
		checkAC3(p,r);
		checkEAC(p,r,TRUE);
	}

#endif
	return(!stop);
}


/* LOCAL CONSISTENCY ENFORCE */

int WM_NC(problem *p,restoreStruct *r)
{
#if (LC_CHECK==1)
	checkNodeConsistency(p);
#endif
	return TRUE;
}

int WM_AC3(problem *p,restoreStruct *r)
{
	return executeAC3(p,r);
}


int WM_DAC(problem *p,restoreStruct *r)
{
	return executeDAC(p,r);
}


int WM_FDAC(problem *p,restoreStruct *r)
{
	return executeFDAC(p,r);
}

int WM_EDAC(problem *p,restoreStruct *r)
{
	return executeEDAC(p,r);
}


func_t LcFuncWM[5] = {WM_NC,WM_AC3,WM_DAC,WM_FDAC,WM_EDAC};
