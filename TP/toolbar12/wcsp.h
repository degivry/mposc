/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wcsp.h
  $Id: wcsp.h,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
--------------------------------------------------------------------- */
#ifndef WCSP_H
#define WCSP_H
#include <limits.h>
#include <math.h>

/* general header for all file.c */
/* declares global variables, macros and external functions */

#if defined(VALDOUBLE)
#include <float.h>
typedef double cost_t;
#define BOTTOM 0
#define MAXCOST DBL_MAX*DBL_MAX /* yields infinity w/o any warning nor compilation problem */
#define READCOST(s,a,b) strtod(s,a) 
#define SCANCOST(f,a) if (fscanf(f,"%lf",a) != 1)  {perror("Error in cost format!"); abort();}
#define FPRINTCOST(f,a) fprintf(f,"%lf",a)
#define PRINTCOST(a) printf("%lf",a)
#elif defined(VALFLOAT)
#include <float.h>
typedef float cost_t;
#define BOTTOM 0
#define MAXCOST FLT_MAX*FLT_MAX /* yields infinity w/o any warning nor compilation problem */
#define READCOST(s,a,b) (float)strtod(s,a) 
#define SCANCOST(f,a) if (fscanf(f,"%f",a) != 1)  {perror("Error in cost format!"); abort();}
#define FPRINTCOST(f,a) fprintf(f,"%f",a)
#define PRINTCOST(a) printf("%f",a)
#else
typedef int cost_t;
#define BOTTOM 0
#define MAXCOST INT_MAX
#define READCOST(s,a,b) strtol(s,a,b)
#define SCANCOST(f,a) if (fscanf(f,"%d",a) != 1)  {perror("Error in cost format!"); abort();}
#define FPRINTCOST(f,a) fprintf(f,"%d",a)
#define PRINTCOST(a) printf("%d",a)
#endif /* VALUATION */

#define MIN(X,Y) ((X) > (Y) ? (Y) : (X))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))

extern void *mycalloc(int nobj, size_t size);
extern void *mymalloc(int nobj, size_t size);
extern void allocateWCSP();
extern void certificateCheck(int *assig);
extern void allocateSol();
extern void freeWCSP();
extern void freeSol();

#define myfscanf1(f,s,e1) if (fscanf(f,s,e1) != 1) {perror("Error in file format! 1"); abort();}
#define myfscanf2(f,s,e1,e2) if (fscanf(f,s,e1,e2) != 2) {perror("Error in file format! 2"); abort();}
#define myfscanf3(f,s,e1,e2,e3) if (fscanf(f,s,e1,e2,e3) != 3) {perror("Error in file format! 3"); abort();}
#define myfscanf4(f,s,e1,e2,e3,e4) if (fscanf(f,s,e1,e2,e3,e4) != 4) {perror("Error in file format! 4"); abort();}
#define myfscanf5(f,s,e1,e2,e3,e4,e5) if (fscanf(f,s,e1,e2,e3,e4,e5) != 5) {perror("Error in file format! 5"); abort();}

typedef enum bool {FALSE = 0, TRUE = 1} bool_t;

/* total number of variables */
extern int NVar;
/* maximum number of values per domain */
extern int NValues;
/* total number of n-ary constraints (with n>2) */
extern int NConstr;
/* maximum arity of n-ary constraints */
extern int MaxArity;

/* global upper bound */
extern cost_t Top;
/* global lower bound */
extern cost_t Bottom;

/* number of nodes */
extern unsigned long NNodes;
extern unsigned long SingletonNNodes;
/* number of checks */
extern int NChecks;
/* best solution found so far */
extern int *BestSol;
/* number of solutions better than Top */
extern int AllSolutions; /* flag: TRUE = search for all solutions, FALSE = search optimum */
extern int NumberOfSolutions;


typedef enum LcLevels { LC_NC = 0, LC_AC = 1, LC_DAC =2, LC_FDAC = 3, LC_EDAC = 4,LC_MAXLEVEL = 5} LC_t;
/* level of local consistency
	  0 : NC, 1 : AC, 2 : DAC, 3 : FDAC*/
extern LC_t LcLevel;

extern int preProject;

typedef enum HeurVars { VAR_NONE =0, VAR_DEGREE = 1, VAR_RANDOM_STATIC = 2,
			VAR_RANDOM_DYNAMIC = 3, VAR_MIN_DOM = 4, VAR_MIN_DOMBYDEG = 5,
			VAR_JEROSLOW_LIKE = 6, VAR_JEROSLOW_LIKE2 = 7, VAR_SINGLETON = 8,
			VAR_MAXHEUR = 9} HeuristicVar_t;
/* main variable ordering heuristic
	  0 : lexico/nothing, 1 : max-degree (static), 2 : random (static), 3 : random (dynamic),
	  4 : min-domain (dynamic), 5 : min-domain/degree (dynamic),
	  6 : JerowsLow-like (dynamic), 7 : JerowsLow-like ties broken randomly (dynamic),
          8 : Pseudo-costs (dynamic) */
extern HeuristicVar_t HeurVar;

/* Possible Value ordering */
typedef enum HeurVals { VAL_NONE = 0, VAL_MINUNARYCOUNT = 1, VAL_MINUNARYCOUNTRND = 2, VAL_MAXHEUR = 3} HeuristicVal_t;

extern HeuristicVal_t HeurVal;

typedef enum HeurTreedecs { MAX_CARD = 0, MIN_FILL = 1, MIN_WEIGHT = 2, MIN_WIDTH = 3, KOSTER = 4, VAR_MAXHEUR_TREEDEC  } HeuristicTreedec_t;

extern HeuristicTreedec_t HeurTreedec;

extern bool_t TreedecMode; /* If TRUE, compute a tree-dec ordering instead of solving
			     the problem... */
extern bool_t ExportMode; /* If TRUE, export the tree-dec to a graphic format (if TreedecMode is FALSE,
			     don't export anything */
extern bool_t BEMode; /* If TRUE, BE or MB is used to solve the problem*/

typedef enum FileFormats { FORMAT_WCSP = 0, FORMAT_CNF = 1, FORMAT_WCNF = 2, FORMAT_BAYESNET = 3,
			   FORMAT_MAX = 4} FileFormats_t;
extern int FileFormat;

extern int Verbose;
extern int TimeLimit;
extern unsigned long NodeLimit;
extern int Seed;
extern int ElimLevel;


typedef enum OptionsEnum { OPTIONS_DOMINANCE_PRE = 1, OPTIONS_SINGLETON_PRE = 2, OPTIONS_GLOBALINVCONSISTENCY_PRE = 4, OPTIONS_DOMINANCE_BB = 8, OPTIONS_SINGLETON_BB = 16 } OptionsEnum_t;
extern int Options;
extern int RestrictedSingletonNbVar;
extern int max_sat_rules;


extern char *Filename;
// the finame of a certificate to check.
extern char *CertificateName;
extern char *SaveFileName;

extern int Width; //Maximum width allowed for (mini)bucket elimination
extern short *EliminationOrder;

/* scopes of nary constraints */
extern int **NaryConstraints;
/* get the arity of the nary constraint i */
#define ARITY(i) NaryConstraints[i][0]
/* get the address of the first variable in the scope of the nary constraint i */
#define SCOPE(i) &NaryConstraints[i][1]
/* get the k variable in the scope of the nary constraint i */
#define SCOPEONE(i,k) NaryConstraints[i][1 + k]

/* cost tables of nary constraints */
extern cost_t **NaryCosts;
/* get the address of the cost array of the nary constraint i */
#define COSTS(i) NaryCosts[i]

/* static mean costs of nary constraints */
extern double *MeanCosts;
/* return the mean cost (sum of costs divided by the product of domain sizes) of nary constraint i */
#define WEIGHTCONSTRAINT(i) MeanCosts[i]

/* lists of nary constraint numbers where each variable appears (pointers to VARSTACK memory) */
extern int **Variables;
/* get the number of nary constraints where the variable i appears */
#define VARNCONSTR(i) Variables[i][0]
/* get the address of the first nary constraints where the variable i appears */
#define VARCONSTR(i) &Variables[i][1]
/* get the nary constraint number in k position where the variable i appears */
#define VARCONSTRONE(i,k) Variables[i][1 + k]

/* domain size for each variable */
extern int *DomainSize;
/* get the domain size of the variable i */
#define DOMSIZE(i) DomainSize[i]

/* unary costs for any variable assignment */
extern cost_t *UnaryConstraints;
/* get the cost of the assignment of the variable i to the value a */
#define UNARYCOST(i,a) UnaryConstraints[i * NValues + a]

/* stack of binary constraint costs */
extern cost_t *BinaryConstraints;
/* current free binary constraint stack pointer */
extern cost_t *BinaryFree;
/* end of stack of binary constraint costs */
extern cost_t *BinaryFreeLimit;

/* binary constraint graph (NULL (no binary constraint) or pointer to BinaryConstraints) */
extern cost_t **Graph;
/* return false (zero) if there is no binary constraint between variables i and j
   and return true (pointer to the binary cost table) if there is a binary constraint */
#define ISBINARY(i,j) Graph[i * NVar + j]

/* create a binary constraint between i and j */
extern void createBinaryConstraint(int i, int j);

/* destroy a binary constraint between i and j */
extern void destroyBinaryConstraint(int i, int j);

/* return the cost of assignment i := a and j := b */
extern cost_t binaryCost(int i, int j, int a, int b);

/* return the cost of assignment i := a and j := b */
/* Warning! Assume there is a binary constraint AND i < j */
#define BINARYCOST(i,j,a,b) ISBINARY(i,j)[a * DOMSIZE(j) + b]

/* set the cost of assignment i := a and j := b (i canbe > j) */
extern void setBinaryCost(int i, int j, int a, int b, cost_t v);

/* set the cost v of assignment i := a and j := b */
/* Warning! Assume there is a binary constraint and i < j */
/* equivalent to BINARYCOST(i,j,a,b) = v */
#define SETBINARYCOST(i,j,a,b,v) (ISBINARY(i,j)[a * DOMSIZE(j) + b] = v)

/* return the cartesian product of domain sizes involved in the scope of the nary constraint i */
extern int cartesianProduct(int i);

/* return true if variable i is in the scope of constraint c */
extern int inScope(int i, int c);

/* return the cost of a complete assignment for the nary constraint i */
extern cost_t cost(int i, int *completeAssignment);

/* set the cost of a tuple for the nary constraint i */
extern void setCost(int i, int *completeAssignment, cost_t v);

extern void Process_Options(int argc, char **argv);
extern void SeedInit (int seed);

extern void readWCSP(FILE* file);
extern void saveWCSP(FILE* file);
extern void ErgoRead(FILE* file);

/* a special function which is called if the time limit is reached */
extern void timeOut();
extern void timer(int t);
extern void timerStop();
extern double cpuTime();

extern void nodeOut();

#define CostU(i, a) (UNARYCOST(i,a)-DeltaU[i])
#define CostB(i, a, j, b) ((i<j)?(BINARYCOST(i,j,a,b)-DeltaB[i][j][a]-DeltaB[j][i][b]):(BINARYCOST(j,i,b,a)-DeltaB[i][j][a]-DeltaB[j][i][b]))
#endif
