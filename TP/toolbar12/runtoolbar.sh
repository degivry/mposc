#!/bin/sh

# usage: runtoolbar.sh WCSP/benchs
 
# executes toolbar on every problem in wcsp format found in directory and its subdirectory

echo "#file initial_upperbound optimum nodes time(in seconds)"
for e in `find $1 -regex ".*[.]wcsp" -print | sort` ; do
  runtoolbar1.sh $e
done
