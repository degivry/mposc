#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"
#include "tbelim.h"

extern void BB();

int small_degree(int *d,int *var, int *var2, int *var3)
{
  int i,j,k,d2;

  *var=-1; *var2=-1; *var3=-1;
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && degree[i]<=ElimLevel)
    {
      d2=degree[i];
      k=TRUE;
      for(j=0;j<VARNCONSTR(i);j++)
	{
	  if(currentArity[VARCONSTRONE(i,j)]>2){k=FALSE; j=NConstr;}
	}
      if(k)
	{
	  *var=i; 
	  if(d2>0)
	    {
	      for(j=0;j<NVar;j++)
		{if(assignments[j]==-1 && ISBINARY(i,j)) {*var2=j;j=NVar;}}
	    }
	  if(d2>1)
	    {
	      for(j=*var2+1;j<NVar;j++)
		{if(assignments[j]==-1 && ISBINARY(i,j)) {*var3=j;j=NVar;}}
	    }
	  *d=d2;
	  return TRUE;
	}
    }
  return FALSE;
}



int elim1(int var, int j)
{
  int val, val2,stop,flag,k;
  cost_t temp;
  
  stop=FALSE;
  flag=FALSE;
  List_reset_traversal(&feasible_values[j]);
  while(!List_end_traversal(&feasible_values[j]))
    {
      val2=List_consult1(&feasible_values[j]);
      List_next(&feasible_values[j]);
      temp=MAXCOST;
      List_reset_traversal(&feasible_values[var]);
      while(!List_end_traversal(&feasible_values[var]))
	{
	  val=List_consult1(&feasible_values[var]);
	  List_next(&feasible_values[var]);
	  if(CostB(var,val,j,val2)+CostU(var,val)<temp)
	    {temp=CostB(var,val,j,val2)+CostU(var,val);}
	}
      if(CostU(j,val2)== BOTTOM && temp>BOTTOM) flag=TRUE;
      if(temp>BOTTOM)
	{
	  UNARYCOST(j,val2)+=temp;
	  List_insert3pc(&l2[depth],j,val2,0,temp); /* records the change for context restoration */
	}
    }
  if(flag)
  {
	  stream_push(Rdac,j);
	  stream_push(S,j);
  }
  stop=!findSupportU(j,&flag);
  for(k=0;k<NVar;k++)
    {
      if(stop) k=NVar;
      else{
	if(assignments[k]==-1 && prune_variable(k,&stop))
	  {stream_push(Q,k);}
      }
    }
  return(!stop);
}


int elim2(int v, int v1, int v2)
{
  int val,a,b,sa, sb, mod,addedBinary;
  cost_t inc;
  int flag1,flag2;

  //printf("elim2 %i %i\n",num_future,NNodes);

  mod=FALSE; addedBinary=FALSE;
  if(!ISBINARY(v1,v2))
    {createBinaryConstraint(v1,v2);addedBinary=TRUE;}
  flag1=FALSE; flag2=FALSE;
  List_reset_traversal(&feasible_values[v1]);
  while(!List_end_traversal(&feasible_values[v1]))
    {
      a=List_consult1(&feasible_values[v1]);
      List_next(&feasible_values[v1]);
      sa=SupB[v1][v2][a];
      if(addedBinary && (current_domains[v2][sa]!=-1 ||
			 CostU(v2,sa)))flag1=TRUE;
      List_reset_traversal(&feasible_values[v2]);
      while(!List_end_traversal(&feasible_values[v2]))
	{
	  b=List_consult1(&feasible_values[v2]);
	  List_next(&feasible_values[v2]);
	  sb=SupB[v2][v1][b];
	  if(addedBinary && (current_domains[v1][sb]!=-1 ||
			     CostU(v1,sb)))flag2=TRUE;
	  inc=MAXCOST;

	  List_reset_traversal(&feasible_values[v]);
	  while(!List_end_traversal(&feasible_values[v]))
	    {
	      val=List_consult1(&feasible_values[v]);
	      List_next(&feasible_values[v]);
	      if(CostB(v,val,v1,a)+CostB(v,val,v2,b)+CostU(v,val)<inc)
		inc=CostB(v,val,v1,a)+CostB(v,val,v2,b)+CostU(v,val);
	    }
	  if(inc>BOTTOM)
	    {
	      mod=TRUE;

	      if(v1<v2) BINARYCOST(v1,v2,a,b)+=inc;
	      else BINARYCOST(v2,v1,b,a)+=inc;

	      List_insert4(&l6[depth],v1,v2,a,b);List_next(&l6[depth]);
	      List_insert3pc(&l6[depth],0,0,0,inc);List_next(&l6[depth]);
	      if(sa==b)flag1=TRUE;
	      if(sb==a)flag2=TRUE;
	    }
	}
    }
  if(mod)
    {
      if(flag1) stream_push(Q,v2);
      if(flag2) stream_push(Q,v1);
      if(flag1 && order[v1]<order[v2])stream_push(Rdac,v2);
      if(flag2 && order[v2]<order[v1])stream_push(Rdac,v1);
      if(addedBinary)
	{
	  degree[v1]++; degree[v2]++;
	  List_insert4(&l5[depth],v1,v2,0,0);
	}
    }
  else
    {
      if(addedBinary) destroyBinaryConstraint(v1,v2);
    }

  return TRUE;
}

void elim(int l, int i, int i2, int i3)
{
  num_future--;
  depth++;
  assignments[i]=NValues+1;
  if(l>0) degree[i2]--;
  if(l>1) degree[i3]--;
  List_create(&l1[depth]); List_create(&l2[depth]);
  List_create(&l3[depth]); List_create(&l4[depth]);
  List_create(&l5[depth]); List_create(&l6[depth]);
  stream_ini(Q); /* initialize the stream */
  stream_ini(Rdac); /* initialize the stream */

  if(l==0 || (l==1 && elim1(i,i2) &&  (*LcFunc[LcLevel])()) ||
      (l==2 && elim2(i,i2,i3) &&  (*LcFunc[LcLevel])()))
    {
#if (AC_CHECK == 1)
	      (*CheckACFunc[LcLevel])();
#endif 
	      BB();
    }
  restore_context();
  List_dispose_memory(&l1[depth]); List_dispose_memory(&l2[depth]);
  List_dispose_memory(&l3[depth]); List_dispose_memory(&l4[depth]);
  List_dispose_memory(&l5[depth]); List_dispose_memory(&l6[depth]);
  assignments[i]=-1;
  num_future++;
  depth--;
}

int elimRecursive(int l, int i, int i2, int i3)
{
  int res = FALSE;
  num_future--;
  assignments[i]=NValues+1;
  if(l>0) degree[i2]--; 
  if(l>1) degree[i3]--;

  if(l==0 || (l==1 && elim1(i,i2) &&  (*LcFunc[LcLevel])()) ||
      (l==2 && elim2(i,i2,i3) &&  (*LcFunc[LcLevel])()))
    {
#if (AC_CHECK == 1)
	      (*CheckACFunc[LcLevel])();
#endif 
	      if (small_degree(&l,&i,&i2,&i3)) res = elimRecursive(l,i,i2,i3);
	      else res = TRUE;
		
    }
  return res;
}

int elimAtRoot()
{
  int l,i,i2,i3;
  int res = TRUE;

  depth++;
  List_create(&l1[depth]); List_create(&l2[depth]); 
  List_create(&l3[depth]); List_create(&l4[depth]);
  List_create(&l5[depth]); List_create(&l6[depth]);
  stream_ini(Q); /* initialize the stream */
  stream_ini(Rdac); /* initialize the stream */
  stream_ini(S); /* initialize the stream */

  if (small_degree(&l,&i,&i2,&i3)) res = elimRecursive(l,i,i2,i3);

  List_dispose_memory(&l1[depth]); List_dispose_memory(&l2[depth]);
  List_dispose_memory(&l3[depth]); List_dispose_memory(&l4[depth]);
  List_dispose_memory(&l5[depth]); List_dispose_memory(&l6[depth]);
  depth--;
  return res;
}
