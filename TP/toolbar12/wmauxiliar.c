/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmauxiliar.c
  $Id: wmauxiliar.c,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include "wmauxiliar.h"



extern double cpuTime();



static double StartTime;



void start_timers()

{

  StartTime = cpuTime();

}



unsigned long elapsed_time( )

{

  return (unsigned long) ((cpuTime() - StartTime));

}



/* AUXILIAR FUNCTIONS */



int minV(int a,int b)

{

	if(a<b) return a;

	else return b;

}



int maxV(int a,int b)

{

	if(a>b) return a;

	else return b;

}





int myabs(int val)

{

	// returns the absolute value

	if (val<0) return (-val);

	else return (val);

}



void error(int v)

{

	// show an error and abort.

	if (v==INS_MEM)

		printf("\nInsuficient memory...\n");

	else if (v==LIST_ERROR)

		printf("\nList error...\n");

	else if (v==FILE_NOT_EXISTS)

		printf("\nData file not exists...\n");

	else if (v==FILE_CREATING_ERROR)

		printf("\nData file creation error...\n");

	else if (v==ERROR_READING_PROBLEM)

		printf("\nIncorrect data file format...\n");



	printf("\nAborting program...\n");

	exit(-1);

}

