/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: getopt.c
  $Id: getopt.c,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include "wcsp.h"

/* --------------------------------------------------------------------
// Usage
// -------------------------------------------------------------------- */
void Usage ()
{
  fprintf(stderr,"\nPossible flags:\n");
  fprintf(stderr,"   -fFileFormat (0=WCSP, 1=CNF, 2=WCNF)\n");
  fprintf(stderr,"   -lLocalConsistency (0=NC, 1=AC, 2=DAC, 3=FDAC, 4=EDAC)\n");
  fprintf(stderr,"   -S[Number]: (restricted) singleton consistency (with optional number of selected variables for singleton testing)\n");
  fprintf(stderr,"   -d: Koster's dominance rule\n");
  fprintf(stderr,"   -oNumber: preprocessing techniques (only at the root node)\n");
  fprintf(stderr,"       0 : none\n");
  fprintf(stderr,"       1 : Koster's dominance rule\n");
  fprintf(stderr,"       2 : singleton consistency\n");
  fprintf(stderr,"       3 : Koster's dominance rule and singleton consistency\n");
  fprintf(stderr,"   -uUpperBound\n");
  fprintf(stderr,"   -a: count the number of solutions whose cost is strictly less than UpperBound\n");
  fprintf(stderr,"   -cCertificate\n");
  fprintf(stderr,"   -pMaxArity: project nary on binary for small arity\n");
  fprintf(stderr,"   -VNumber: value ordering heuristics\n");
  fprintf(stderr,"       0 : none (file order)\n");
  fprintf(stderr,"       1 : min unary cost (ties broken lexic.)\n");
  fprintf(stderr,"       2 : min unary cost (ties broken randomly)\n");
  fprintf(stderr,"   -HNumber: variable ordering heuristics (ties are broken lexicographically\n");
  fprintf(stderr,"       0 : none (file order)\n");
  fprintf(stderr,"       1 : max degree\n");
  fprintf(stderr,"       2 : random static\n");
  fprintf(stderr,"       3 : random dynamic\n");
  fprintf(stderr,"       4 : min domain (ties broken lexic.)\n");
  fprintf(stderr,"       5 : min domain by degree (ties broken lexic.)\n");
  fprintf(stderr,"       6 : 2-sided Jeroslow like (ties broken lexic.)\n");
  fprintf(stderr,"       7 : 2-sided Jeroslow like (ties broken randomly)\n");
  fprintf(stderr,"   -TNumber: compute a cluster tree decomposition\n");
  fprintf(stderr,"       0 : max cardinality\n");
  fprintf(stderr,"       1 : min fill\n");
  /* fprintf(stderr,"       2 : min degree\n"); */
  fprintf(stderr,"       3 : min width\n");
  fprintf(stderr,"       4 : Koster\n");
  fprintf(stderr,"   -g: export the tree-decomposition and the graph to two postscript files tree.eps and graph.eps\n");
  fprintf(stderr,"   -qFileName: save problem in wcsp format after preprocessing is done and quit.\n");
  fprintf(stderr,"   -tTimeLimit (in seconds, 0 means infinity)\n");
  fprintf(stderr,"   -nNodeLimit\n");
  fprintf(stderr,"   -eElimLevel (<=2)\n");
  fprintf(stderr,"   -sNumber: seed for random generator\n");
  fprintf(stderr,"   -v[Number]: verbose (with optional verbosity level)\n");
  fprintf(stderr,"   -i: bucket elimination resolution. The ordering of the variables is the one from -T option, or by default, the lexicographical one.\n");
  fprintf(stderr,"   -wNumber: the resulting function of each mini-bucket is not bigger than 2^Number. By default, Number=maximum_domain^w*. The feasible limit is Number=32.\n");
  fprintf(stderr,"   -m: Apply max-sat rules in cnf files.\n");
  fprintf(stderr,"   -h: this help\n");
  fprintf(stderr,"   \nIf no filename is present, standard input is used.\n");
  fprintf(stderr,"   \nIf DEFAULTS: -f0 -l4 -o0 -V1 -H5 -t0 -n%lu -e-1 -s0 -p2\n", ULONG_MAX);

}

void uselessOptionsCNF()
{
	int boolAux=FALSE;
  boolAux=FALSE;
  if(FileFormat==FORMAT_CNF || FileFormat==FORMAT_WCNF)
  {
		// Not available options for WMAXSAT version
	  if(HeurVar>6)
	  {
	  	HeurVar=6;
	  }
	  if(ElimLevel!=-1)
	  {
		  boolAux=TRUE;
		  printf("\n- 'e': Not available option for CNF version.");
	  }
	  if(Options!=0)
	  {
		  boolAux=TRUE;
		  printf("\n- Options 'S', 'o' and 'd' are not available in CNF version.");
	  }
	  if(AllSolutions==TRUE)
	  {
		  boolAux=TRUE;
		  printf("\n- 'a': Not available option for CNF version.");
	  }
	  if(TreedecMode==TRUE || ExportMode==TRUE)
	  {
		  boolAux=TRUE;
		  printf("\n- Options 'T' and 'g' are not available in CNF version.");
	  }
	  if (boolAux==TRUE) printf("\n\n");
  }

}

/* --------------------------------------------------------------------
//  Process command-line options and set corresponding global switches
//  and parameters.
// -------------------------------------------------------------------- */
void  Process_Options  (int argc, char ** argv) {
  int  i, errflag =0;  

  for  (i = 1;  i < argc;  i++) {

    switch  (argv[i][0]) {
    case  '-' :
      switch  (argv[i][1]) {

	/* check certificate */
      case 'c':
	CertificateName = argv[i]+2;
	break;
		  
	/* save problem in wcsp format after preprocessing and exit */
      case 'q':
	SaveFileName = argv[i]+2;
	break;

	/* u : upperbound */
      case  'u' :
	Top = MIN(Top,READCOST(argv[i]+2,NULL,0));
	if (errno || Top < 0) errflag++;
	break;

	/* v : verbose */
      case 'v' :
	Verbose = strtol(argv[i]+2,NULL,0);
	if (errno || Verbose <= 0) Verbose = 1;
	break;

	/* l : level of local consistency */
      case 'l' :
	LcLevel = strtol(argv[i]+2,NULL,0);
	if (errno || LcLevel < 0 || LcLevel >= LC_MAXLEVEL) errflag++;
	break;

	/* i : (mini)bucket elimination */
      case 'i' :
	//if (argv[i]+2!="") Width = strtol(argv[i]+2,NULL,0);
	//if (errno || Width < 0) errflag++;
	BEMode = TRUE;
	break;

	/* w : maximum width 2^w allowed for (mini)bucket elimination */
      case 'w' :
	Width = strtol(argv[i]+2,NULL,0);
	if (errno || Width < 0) errflag++;
	break;

	/* p : preproject nary constraints on binary */
      case 'p' :
	preProject = strtol(argv[i]+2,NULL,0);
	if (errno || preProject < 2) errflag++;
	break;

	/* V : value ordering heuristics */
      case 'V' :
	HeurVal = strtol(argv[i]+2,NULL,0);
	if (errno || HeurVal < 0 || HeurVal >= VAL_MAXHEUR) errflag++;
	break;

	/* h : help */
      case 'h' :
	Usage();
	exit(0);

	/* H : primary variable ordering heuristics */
      case 'H' :
	HeurVar = strtol(argv[i]+2,NULL,0);
	if (errno || HeurVar < 0 || HeurVar >= VAR_MAXHEUR) errflag++;
	break;

	/* T : computing a tree-decomposition */
      case 'T' :
	TreedecMode = TRUE;
	HeurTreedec = strtol(argv[i]+2,NULL,0);
	if (errno || HeurTreedec < 0 || HeurTreedec >= VAR_MAXHEUR_TREEDEC) errflag++;
	break;
      case 'g' :
	ExportMode = TRUE;
	break;

	/* t : time limit for search */
      case 't':
	TimeLimit = strtol(argv[i]+2,NULL,0);
	if (errno || TimeLimit <0) errflag++;
	break;

	/* n : node limit for search */
      case 'n' :
	NodeLimit = strtoul(argv[i]+2,NULL,0);
	if (errno) errflag++;
	break;

	/* f: file format */
      case 'f':
	FileFormat = strtol(argv[i]+2,NULL,0);
	if (errno || FileFormat <0 || FileFormat >= FORMAT_MAX) errflag++;
	break;

	/* s : seed for random generator */
      case 's' :
	Seed = strtol(argv[i]+2,NULL,0);
	if (errno) errflag++;
	break;

      case 'e' :
	ElimLevel = strtol(argv[i]+2,NULL,0);
	if (errno || ElimLevel >= 3) errflag++;
	break;

	/* o : miscelaneous options */
      case 'o' :
	Options |= strtol(argv[i]+2,NULL,0); /* can only add options but not remove them */
	if (errno) errflag++;
	break;

	/* d : Koster's dominance rule */
      case 'd' :
	Options |= OPTIONS_DOMINANCE_BB;
	break;

	/* a : find all solutions */
      case 'a' :
	AllSolutions = TRUE;
	break;

	/* S : (restricted) singleton consistency */
      case 'S' :
	Options |= OPTIONS_SINGLETON_BB;
	RestrictedSingletonNbVar = strtol(argv[i]+2,NULL,0);
	if (RestrictedSingletonNbVar == 0) RestrictedSingletonNbVar = INT_MAX;
	if (errno || RestrictedSingletonNbVar < 0) errflag++;
	break;

	/* m : Apply max-sat rules */
      case 'm' :
	max_sat_rules=TRUE;
	break;

      default :
	errflag++;
	break;
      }
      break;
    default :
      if (Filename == NULL) Filename = argv[i];
      else errflag++;
    }

    if (errflag) {
      fprintf(stderr, "Unrecognized option or wrong argument: %s\n", argv[i]);
      break;
    }
  }

  if (errflag) {
    Usage();
    exit(0);
  }

  // show error messages if the user specify undefined options for CNF or WCNF formats.
  uselessOptionsCNF();

	/* by default, no options */
  if (AllSolutions) ElimLevel = -1; /* not compatible with variable elimination */
  if (Verbose) {
    printf("ToolBar version 2.2, an open source solver for Weighted CSP and Max-SAT.\n");
    printf("Verbose = %d\nLcLevel = %d\nHeurVar = %d\n",
	   Verbose,LcLevel,HeurVar);
    printf("HeurVal = %d\nTimeLimit = %d\nNodeLimit = %lu\nSeed = %d\n",
	   HeurVal,TimeLimit,NodeLimit,Seed);
    printf("FileFormat = %d\nFilename = %s\n",FileFormat,Filename);
    if (Options & OPTIONS_DOMINANCE_BB) printf("Koster's dominance rule\n");
    else if (Options & OPTIONS_DOMINANCE_PRE) printf("Koster's dominance rule (preprocessing only)\n");
    if (Options & OPTIONS_SINGLETON_BB) {
      printf("Singleton consistency");
      if (RestrictedSingletonNbVar < INT_MAX) 
	printf(" (at most %d selected variables)",RestrictedSingletonNbVar);
      printf("\n");
    } else if (Options & OPTIONS_SINGLETON_PRE) printf("Singleton consistency (preprocessing only)\n");

  }
}
