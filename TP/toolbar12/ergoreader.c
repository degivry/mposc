/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: ergoreader.c
  $Id: ergoreader.c,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "wcsp.h"

 /* #define VERBOSE 1 */

 /* obsolete but remaining for memory purposes
    we do not want any cost larger than half the MAXCOST or else a
    wrap may occur. Let's fix the top value at half MAXCOST, we will
    fix the resolution so that the number of variables multiplied by
    the log min probability is strictly smaller than this cost. This
    guarantees that the worst theoretical non forbidden assignment
    costs less than a forbidden one.

    We have:
    
    - Top = MAXCOST/2

    - -NVar*NORMFACTOR*log10(minproba) < Top. Let's choose
        NORMFACTOR = Top/(NVar*log10(minproba))-1
 */

int ** Scopes;
double NormFactor;
#define MINPROB 0.01

/* --------------------------------------------------------------
// Cost conversion routines: proba to costs back and forth
//  -------------------------------------------------------------*/
cost_t Prob2Cost(double p) 
{
//  if (p && (p<MINPROB))
//    fprintf(stderr,"Warning, probability below the min. probability level found! \n");

  return (cost_t)fabs((p ? -log10(p)*NormFactor : Top));
}

double Cost2LogLike(cost_t c) 
{
  return -(double)c/NormFactor;
}

double Cost2Proba(cost_t c) 
{
  return pow(10, -(double)c/NormFactor);
}
/* --------------------------------------------------------------
// Comments skip: we assume that if a scanf fails, this is caused by
// comments which we try to skip (find end of comments)
//  -------------------------------------------------------------*/
void SkipComments(FILE* handle)
{
  int c = -1;

  while (1) 
    if ((getc(handle) == '*') && ((c = getc(handle)) == '/'))
      break;
    else if (c >=0) {ungetc(c,handle); c = -1;};
}
/* --------------------------------------------------------------
// A scanf that tries to skip comments
// -------------------------------------------------------------*/
int Sfscanfd(FILE* handle,int *val)
{
  int res = fscanf(handle,"%d",val);
  if (res) return res;
  SkipComments(handle);
  return fscanf(handle,"%d",val);
}
/* --------------------------------------------------------------
// A scanf that tries to skip comments
// -------------------------------------------------------------*/
int Sfscanff(FILE* handle,double *val)
{
  int res = fscanf(handle,"%lf",val);
  if (res) return res;
  SkipComments(handle);
  return fscanf(handle,"%lf",val);
}
/*--------------------------------------------------------------------
// next_tuple (tuple): enumerates tuples of values for parent variables 
// --------------------------------------------------------------------*/
void next_tuple(int ctr, int *tuple)
{                                  
  int i,end;
  
  /* one but last variable index */
  i = Scopes[ctr][0]-2;
  end = FALSE;

  while ((i >= 0) && !(end)) {
    tuple[i]++;
    if (tuple[i] < DomainSize[Scopes[ctr][i+1]]) end = TRUE;
    else {
      tuple[i] = 0;
      i--;
    }
  }
}
/*--------------------------------------------------------------------
//  comp_offset (ctr, tuple): to be used with above function
//------------------------------------------------------------------*/
int comp_offset(int ctr, int *tuple)
{                                       
  int i,res;
  int offset=0;

  res = 1;  
  for (i=Scopes[ctr][0]-1; i>=0; i--) {
    offset = offset+(tuple[i]*res);
    res *= DomainSize[Scopes[ctr][i+1]];
  }
  return offset;
}
/* --------------------------------------------------------------
// Fill a unary domain constraint from a file
// ------------------------------------------------------------*/
void FillUnary(FILE* handle,int nv,int nb)
{		
  double prob;
  int i;

  assert(nb == DomainSize[nv]);
  
  free(Scopes[nv]);
  Scopes[nv]= NULL;
  for (i=0; i < nb; i++) {
    Sfscanff(handle,&prob);
    UNARYCOST(nv,i) = Prob2Cost(prob);
  }
}
/* --------------------------------------------------------------
// Fill a unary domain constraint from a CPT
// ------------------------------------------------------------*/
void FillUnaryCPT(int nv, double *CPT)
{		
  int i;
  
  for (i=0; i < DomainSize[nv]; i++) {
    UNARYCOST(nv,i) = Prob2Cost(CPT[i]);
  }
  free(CPT);
  free(Scopes[nv]);
  Scopes[nv]= NULL;
}
/* --------------------------------------------------------------
// Fill a binary constraint from a file handle
// ------------------------------------------------------------*/
void FillBinary(FILE* handle,int nc,int nb)
{
  int v1 = Scopes[nc][1];
  int v2 = Scopes[nc][2];
  int d1 = DomainSize[v1];
  int d2 = DomainSize[v2];
  int i,j,swap = (v2<v1);
  double prob;

  free(Scopes[nc]);
  Scopes[nc] = NULL;

  assert(nb == (d1*d2));

  ISBINARY(v1,v2) = ISBINARY (v2,v1) = mymalloc(nb,sizeof(cost_t));
  for (i=0; i<d1; i++) 
    for (j=0; j<d2; j++) {
      Sfscanff(handle,&prob);
      if (swap)	ISBINARY(v1,v2)[d1*j+i] = Prob2Cost(prob);
      else ISBINARY(v1,v2)[d2*i+j] = Prob2Cost(prob);
    }
}
/* --------------------------------------------------------------
// Fill a binary constraint from a CPT
// ------------------------------------------------------------*/
void FillBinaryCPT(int nc,double *CPT)
{
  int v1 = Scopes[nc][1];
  int v2 = Scopes[nc][2];
  int d1 = DomainSize[v1];
  int d2 = DomainSize[v2];
  int i,j,swap = (v2<v1);
  double prob;

  free(Scopes[nc]);
  Scopes[nc] = NULL;

  ISBINARY(v1,v2) = ISBINARY (v2,v1) = mymalloc(d1*d2,sizeof(cost_t));
  for (i=0; i<d1; i++) 
    for (j=0; j<d2; j++) {
      prob = CPT[d1*j+i];
      if (swap)	ISBINARY(v1,v2)[d1*j+i] = Prob2Cost(prob);
      else ISBINARY(v1,v2)[d2*i+j] = Prob2Cost(prob);
    }
  free(CPT);
}
/* --------------------------------------------------------------
// Fill an nary constraint from a file handle
// ------------------------------------------------------------*/
void FillNary(FILE* handle,int nc,int nb,int nbary)
{
  int i;
  int Size = 1;
  double prob;

  for(i=1; i<= Scopes[nc][0]; i++) 
    Size*= DomainSize[Scopes[nc][i]];

  assert(nb == Size);
  Scopes[nc] = NULL;
  
  COSTS(nbary) = mymalloc(nb,sizeof(cost_t));
  Size = INT_MAX;
  for (i=0; i<nb; i++) {
    Sfscanff(handle,&prob);
    COSTS(nbary)[i] = Prob2Cost(prob);
    Size = MIN(Size,COSTS(nbary)[i]);
    WEIGHTCONSTRAINT(nbary) += COSTS(nbary)[i];
  }
  WEIGHTCONSTRAINT(nbary) /= nb;
  /*
    if (Size) fprintf(stderr,"Opportunity to project %d from nary (size %d) to 0-ary in ErgoReader\n",Size,nb);
  */
}
/* --------------------------------------------------------------
// Fill an nary constraint from a CPT
// ------------------------------------------------------------*/
void FillNaryCPT(int nc,int nbary, double *CPT)
{
  int i;
  int Size = 1;
  int Min = INT_MAX;

  for(i=1; i<= Scopes[nc][0]; i++) 
    Size*= DomainSize[Scopes[nc][i]];

  Scopes[nc] = NULL;
  
  COSTS(nbary) = mymalloc(Size,sizeof(cost_t));
  for (i=0; i<Size; i++) {
    COSTS(nbary)[i] = Prob2Cost(CPT[i]);
    Min = MIN(Min,COSTS(nbary)[i]);
    WEIGHTCONSTRAINT(nbary) += COSTS(nbary)[i];
  }
  WEIGHTCONSTRAINT(nbary) /= Size;
  /*
  if (Min) fprintf(stderr,"Opportunity to project %d from nary (size %d) to 0-ary in ErgoReader\n",Min,Size);
  */
}
/*--------------------------------------------------------------------
//  NOR2table: build the CPT for a Noisy OR/MAX function
//------------------------------------------------------------------*/
double* NOR2table(int ctr, double *leak, double ***c)
{
  int i,j,k,offset;
  double *CPT;
  int *tuple;
  int CPSize = 1;
  double prob;
  double SumValProb;      
  int TargetVindex = Scopes[ctr][0];
  int TargetV = Scopes[ctr][TargetVindex];

  tuple = mymalloc(Scopes[ctr][0],sizeof(int));

  /* compute the number of tuples to consider (parent variables) */
  for (i=1; i < TargetVindex; i++) 
    CPSize *= DomainSize[Scopes[ctr][i]];

  CPT = mymalloc(CPSize*DomainSize[TargetV],sizeof(double));

  /* initialize the values in the tuple */
  for (i=0; i < TargetVindex; i++) tuple[i] = 0;
  
  for (i=0; i < CPSize; i++) {
    SumValProb = 0;

    for (j=0; j < DomainSize[TargetV]-1; j++) {
      tuple[TargetVindex-1] = j;
      prob = leak[j];
    
      offset = comp_offset(ctr,tuple);
      /* compute coefficient for the parents */
      for (k = 0; k < TargetVindex-1; k++) {
	if (tuple[k] > 0) {
	  prob *= c[k][tuple[k]-1][j];
	}
      }
      CPT[offset] = prob;
      SumValProb += prob;
    }

    /* the last value in the domain of the target */
    if  (SumValProb <= 1.0) {
      tuple[TargetVindex-1] = j;
      offset = comp_offset(ctr,tuple);
      CPT[offset] = 1-SumValProb;
    }
    /* we need to normalize */
    else  {
      tuple[TargetVindex-1] = j;
      offset = comp_offset(ctr,tuple);
      CPT[offset] = 0.1;
      
      for (j = 0; j < DomainSize[TargetV]-1; j++) {
	tuple[TargetVindex-1] = j;
	offset = comp_offset(ctr,tuple);
	CPT[offset] *= (0.9/SumValProb);
      }
    }
    next_tuple(ctr,tuple);
  }

  free(tuple);
  return CPT;
}
/*--------------------------------------------------------------------
//  read_nor (): builds up a NOR cpt from the standard input in ERGO
//  format.
//------------------------------------------------------------------*/
double* read_nor(FILE* handle, int ctrindex)
{
  int i,j,k;
     double *CPT;
     //double leak[DomainSize[ctrindex]-1];
     //double **c[Scopes[ctrindex][0]-1];
	 double *leak;
	 double ***c;
	 leak=mymalloc(DomainSize[ctrindex]-1,sizeof(double));
	 c=mymalloc(Scopes[ctrindex][0]-1,sizeof(double));

     /* get leak parameters */
     for (i=0 ; i < DomainSize[ctrindex]-1; i++) {
       Sfscanff(handle,&leak[i]);
     }

     /* get link coefficients (see Kozlov and Singh, UAI96)
      or (Pradhan, Provan, Middleton and Henrion, UAI94) */
     for (i=1; i<Scopes[ctrindex][0]; i++) {
       c[i-1] = mymalloc(DomainSize[Scopes[ctrindex][i]]-1,sizeof(double *));
       for (j=0; j<DomainSize[Scopes[ctrindex][i]]-1; j++) {
	 c[i-1][j] = mymalloc(DomainSize[ctrindex]-1,sizeof(double));
	 for (k=0; k <DomainSize[ctrindex]-1; k++) {
	   Sfscanff(handle,&c[i-1][j][k]);
	 }
       }
     }
     
     CPT = NOR2table(ctrindex,leak,c);
     
     /* avoid leakage */
     for (i=1; i<Scopes[ctrindex][0]; i++) {
       for (j=0; j<DomainSize[Scopes[ctrindex][i]]-1; j++) 
	 free(c[i-1][j]);
       free(c[i-1]);
     }

	 free(leak);
	 free(c);

     return CPT;
}
/* --------------------------------------------------------------
// Read a BN from the standard input in ERGO format.
// -------------------------------------------------------------*/
void ErgoRead(FILE* handle)
{
  int i,j;
  int parents,temp,cells;
  int *Degrees;
  double *CPT;

  /* number of variables */
  Sfscanfd(handle,&NVar);
#ifdef VERBOSE 
  printf("Reading %d variables\n",NVar);
#endif
  
  Top = MIN(Top,33554432); /* 2^25 for floats */
  NormFactor = 1;//(-(double)Top/(NVar*log10(MINPROB)))-1.0;

  /* allocate DS */
  Variables = mymalloc(NVar,sizeof(int *));
  DomainSize = mymalloc(NVar,sizeof(int));
  Graph = mycalloc(NVar*NVar,sizeof(cost_t *));
  Degrees = mycalloc(NVar,sizeof(int));

  /* read domain sizes and and compute max */
  NValues = 0;
  for(i=0 ; i<NVar; i++) {
    Sfscanfd(handle,&DomainSize[i]);
    NValues = MAX(NValues,DomainSize[i]);
#ifdef VERBOSE
    printf("Reading v%d, domaine size %d\n",i,DomainSize[i]);
#endif
  }
  UnaryConstraints = mycalloc(NVar*NValues,sizeof(cost_t));

  /* we have as many constraints as variables */
  Scopes = mymalloc(NVar,sizeof(int *));
  
  /* Read constraints (CPT) scopes and compute nary degrees */
  MaxArity = 0;
  NConstr = 0;
  for (i=0 ; i<NVar; i++) {
    /* number of parents */
    Sfscanfd(handle,&parents);
    Scopes[i] = mymalloc((parents+2),sizeof(int));
    Scopes[i][0] = parents+1;
    MaxArity = MAX(MaxArity,parents+1);

    for (j=1; j <= parents ; j++) {
      Sfscanfd(handle,&temp);
      Scopes[i][j]=temp-1;
    }
    /* Variable i is also involved in the constraint */
    Scopes[i][parents+1] = i;

    if (parents > 1) {
      NConstr++;
      for (j=1; j<=parents+1 ; j++) 
	Degrees[Scopes[i][j]]++;
    }
  }
  NaryConstraints = mymalloc(NConstr,sizeof(void *));
  NaryCosts = mymalloc(NConstr,sizeof(void *));
  MeanCosts = mycalloc(NConstr, sizeof(double));

  /* Fill in Variables array */
  for (i=0 ; i<NVar; i++) {
    Variables[i] = mymalloc(1+Degrees[i],sizeof(int));
    Variables[i][0] = 0;
  }
  for (i=NVar-1 ; i>=0; i--) {
    if (Scopes[i][0] > 2) {
      NConstr--;
      NaryConstraints[NConstr] = Scopes[i];
      for (j=1; j<= Scopes[i][0]; j++) 
	Variables[Scopes[i][j]][++Variables[Scopes[i][j]][0]] = NConstr;
      
    }
  }
  free(Degrees);
    
  /* Read tuples */
  for (i=0 ; i<NVar; i++) {
    
    Sfscanfd(handle,&cells);

#ifdef VERBOSE
      printf("Reading c%d, arity %d, %d cells\n",i,Scopes[i][0],cells);
#endif
    if (cells) 
      switch (Scopes[i][0]) {
      case 1: FillUnary(handle,i,cells); break;
      case 2: FillBinary(handle,i,cells); break;
      default:FillNary(handle,i,cells,NConstr++);
      }
    else {
      CPT = read_nor(handle,i);
      switch (Scopes[i][0]) {
      case 1: FillUnaryCPT(i,CPT); break;
      case 2: FillBinaryCPT(i,CPT); break;
      default:FillNaryCPT(i,NConstr++,CPT);
      }
    }
      
  }
  free(Scopes);
}




