/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tbmain.c
  $Id: tbmain.c,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "tbsystem.h"
#include "wcsp.h"
#include "wmaxsat.h"
#include "solver.h"
#include "preproject.h"
#include "ergoreader.h"
#include "treedec.h"
#include "be.h"

static double SolvingTime;
char * CertificateName;

/* function called when time limit is out */
void timeOut() {
  int i;

  SolvingTime = cpuTime() - SolvingTime;
  if (BEMode) {
	  printf("BE time out after %g seconds.\n",SolvingTime);
	  return;
  }
  if(FileFormat==FORMAT_CNF || FileFormat==FORMAT_WCNF) saveResultFileX(SolvingTime);
  if (BestSol[0] != -1) {
    printf("Best bound: ");
    PRINTCOST(Top);
    printf(" in %lu nodes and %g seconds.\n",  NNodes, SolvingTime);
  } else {
    if (AllSolutions) {
      printf("%d solution(s) found after %lu nodes and %g seconds.\n", NumberOfSolutions, NNodes, SolvingTime);
    } else {
      printf("No solution found after %lu nodes and %g seconds.\n", NNodes, SolvingTime);
    }
  }
  if (FileFormat == FORMAT_BAYESNET)
    printf("Loglikelihood %lf (p = %le)\n", Cost2LogLike(Top),Cost2Proba(Top));
  if (Verbose && BestSol[0] != -1) {
    printf("Best solution:");
    for (i=0; i<NVar; i++) {
      printf(" %d", BestSol[i]);
    }
    printf("\n");
  }
} /* no need to call the exit functions, it is done in tbsystem.c */

/* function called when node limit is out */
void nodeOut()
{
  if(Verbose)
	printf("\nNode limit expired... Aborting...\n");
  timeOut();
  exit(0);
}

int main(int argc,char ** argv) {

  int i;

  FILE* fileHandle;

  Process_Options(argc,argv);
  SeedInit(Seed);

  if (Filename) {
    fileHandle = fopen(Filename, "r");
    if(fileHandle == NULL) {
      perror("Error: bad filename!"); 
      abort();
    }
  } else fileHandle = stdin;

  switch (FileFormat) {
  case FORMAT_WCSP : readWCSP(fileHandle); allocateWCSP(); break;
  case FORMAT_CNF : readMAXSATandWMAXSATProblems(fileHandle, TRUE); break;
  case FORMAT_WCNF : readMAXSATandWMAXSATProblems(fileHandle, FALSE); break;
  case FORMAT_BAYESNET : ErgoRead(fileHandle); allocateWCSP(); break;
  }
  fclose(fileHandle);

  /* test WEIGHTCONSTRAINT 
     for (i=0; i < NConstr; i++) {
     printf("%d %g\n", i, WEIGHTCONSTRAINT(i));
     } */

  /************************************************/
  /* If we choose to compute a tree decomposition */
  /************************************************/

  if (TreedecMode)
    {
      computeTreedec ();
      if (!BEMode) return (0);
    }

  if (BEMode) {
	solveBE();
	return (0);
  }

  allocateSol();

  if (Verbose) {
    printf("Top = ");
    PRINTCOST(Top);
    printf("\n");
  }

  if (TimeLimit) {
    timer(TimeLimit);
  }

  SolvingTime = cpuTime();
  
  if (preProject >2) Project2Binary(preProject, 0);

  /* checking certificate */
  if (CertificateName && !AllSolutions) {
    fileHandle = fopen(CertificateName, "r");
    if(fileHandle == NULL) {
      perror("Error: bad certificate filename!"); 
      abort();
    }
    for (i=0; i<NVar; i++) {fscanf(fileHandle,"%d",&(BestSol[i]));}
    printf("Certificate: ");
    for (i=0; i<NVar; i++) { printf(" %d", BestSol[i]);}
    printf("\n");
    certificateCheck(BestSol);
    return 0;
  }

  #ifdef UNIX
	signal(SIGINT,  timeout);
  #endif

  switch (FileFormat) {
  case FORMAT_WCSP : solve(); break;
  case FORMAT_CNF : solveMAXSATandWMAXSATProblems(); break;
  case FORMAT_WCNF : solveMAXSATandWMAXSATProblems(); break;
  case FORMAT_BAYESNET : solve(); break;
  }

  SolvingTime = cpuTime() - SolvingTime;

  if (TimeLimit) {
    timerStop();
  }

  if (AllSolutions) {
    printf("%d solution(s)", NumberOfSolutions);
  } else {
    if (BestSol[0] != -1) {
      printf("Optimum: ");
    } else {
      printf("Lower bound: ");
    }
    PRINTCOST(Top);
  }
  printf(" in %lu nodes and %g seconds", NNodes, SolvingTime);
  if ((Options & OPTIONS_SINGLETON_PRE) || (Options & OPTIONS_SINGLETON_BB)) 
    printf(" (singleton: %lu nodes)", SingletonNNodes);
  printf(".\n");
    
  if (FileFormat == FORMAT_BAYESNET)
    printf("Loglikelihood %lf (p = %le)\n", Cost2LogLike(Top),Cost2Proba(Top));

  if (Verbose && BestSol[0] != -1) {
    printf("Optimal solution:");
    for (i=0; i<NVar; i++) {
      printf(" %d", BestSol[i]);
    }
    printf("\n");
  }

  switch (FileFormat) {
  case FORMAT_WCSP : freeWCSP(); break;
  case FORMAT_CNF : break; /* already done in wmaxsat.c */
  case FORMAT_WCNF : break;
  case FORMAT_BAYESNET : freeWCSP(); break;
  }
  freeSol();

  return 0;
}
