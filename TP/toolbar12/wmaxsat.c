/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmaxsat.c
  $Id: wmaxsat.c,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors:
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"
#include "wmauxiliar.h"
#include "wmtypes.h"
#include "wmheurs.h"
#include "wmaxsat.h"

int pruneTimes;

int rep0=0;

static	problem Problem;


/* GIVE AND TAKE INFO FROM TOOLBAR */

void GiveParameters(problem *p)
{

    // Input

  NVar=p->totalVariables;
  NValues=2;  // FALSE / TRUE
  NConstr=p->totalClauses;
  MaxArity=p->maximumArity;
}


void GiveResults(problem *p)

{

  int i;
	
  // Output
  Top=p->UB;
  for(i=0;i<p->totalVariables;i++)
  BestSol[i]=p->bestResult[i];
}


/* FUNCTIONS FOR VERBOSE */


void saveResultFile(problem *p)
{
	// save results in two files

	FILE *f,*f2;
	int i;

	f=fopen("res.txt","a");

	if (LcLevel==LC_NC)

		f2=fopen("resNC.txt","a");

	else if (LcLevel==LC_AC)

		f2=fopen("resAC.txt","a");

	else if (LcLevel==LC_DAC)

		f2=fopen("resDAC.txt","a");

	else if (LcLevel==LC_FDAC)

		f2=fopen("resFDAC.txt","a");

	else // LcLevel==LC_EDAC

		f2=fopen("resEDAC.txt","a");


	if(f!=NULL && f2!=NULL)
	{
		fprintf(f,"\n\n**********************************************\n\n");

		if (LcLevel==LC_NC)

			fprintf(f,"\nProgram: NC");

		else if (LcLevel==LC_AC)

			fprintf(f,"\nProgram: AC");

		else if (LcLevel==LC_DAC)

			fprintf(f,"\nProgram: DAC");

		else if (LcLevel==LC_FDAC)

			fprintf(f,"\nProgram: FDAC");

		else // LcLevel==LC_EDAC

			fprintf(f,"\nProgram: EDAC");


		fprintf(f,"\nSource: %s",p->fileName);

		fprintf(f,"\nPrune Times: %d",pruneTimes);

		fprintf(f,"\nRecursive calls: %lu",NNodes);

		fprintf(f,"\nTotal time: %ld",p->totTime);

		fprintf(f,"\nBest Result found UB:%d UL:%d",p->UB,p->LB);

		fprintf(f,"\nArray of Best results:\n");

		for(i=0;i<p->totalVariables;i++)
		{

			fprintf(f," (X%d=%d) ",i+1,p->bestResult[i]);

		}

		fprintf(f2,"%s;%d;%d;%d;\n",Filename,NNodes,p->totTime,p->UB);

		fclose(f);

		fclose(f2);
	}
	else
	{

		printf("\nError saving results...\n");
	}

}

extern void saveResultFileX(double lim)
{
	Problem.totTime=lim;
	GiveResults(&Problem);
	if(Verbose) saveResultFile(&Problem);
}

void nodeLimitStop()
{
	GiveResults(&Problem);
	nodeOut();
}


/* FUNCTIONS TO MANAGE BINARY AND UNARY COSTS AND INITILIALIZE FUNCTIONS */


void binaryToUnary(problem *p,restoreStruct *r)
{

	// Not used function. Now we use "binaryToUnaryAndNodeConsistency" to improve performance
	ReferenceToVariable * rv;

	(*nopassJeroslowDeactivate[HeurVar])(p,r->var);

	setFirstLD(p->notAssignedVars);

	while (!endListLD(p->notAssignedVars))
	{

		rv=getActualReferenceVar(p->notAssignedVars);

		// We add this nodes to R queue for DAC 

		if(p->UnaryCosts[rv->indexToVariable][FALSE]==BOTTOM && (p->UnaryCosts[rv->indexToVariable][FALSE]+p->BinaryCosts[r->var][rv->indexToVariable][r->val][FALSE])>BOTTOM)
		{
			pushStack(p->R,rv->indexToVariable);
		}

		if(p->UnaryCosts[rv->indexToVariable][TRUE]==BOTTOM && (p->UnaryCosts[rv->indexToVariable][TRUE]+p->BinaryCosts[r->var][rv->indexToVariable][r->val][TRUE])>BOTTOM)
		{
			pushStack(p->R,rv->indexToVariable);
		}

		// We pass binary costs to unary

		p->UnaryCosts[rv->indexToVariable][FALSE]+=p->BinaryCosts[r->var][rv->indexToVariable][r->val][FALSE];

		p->UnaryCosts[rv->indexToVariable][TRUE]+=p->BinaryCosts[r->var][rv->indexToVariable][r->val][TRUE];

		getNextLD(p->notAssignedVars);

	}
}



void restoreBinaryToUnary(problem *p,restoreStruct *r)
{
	ReferenceToVariable * rv;

	setFirstLD(p->notAssignedVars);

	while (!endListLD(p->notAssignedVars))
	{

		rv=getActualReferenceVar(p->notAssignedVars);

		p->UnaryCosts[rv->indexToVariable][FALSE]-=p->BinaryCosts[r->var][rv->indexToVariable][r->val][FALSE];

		p->UnaryCosts[rv->indexToVariable][TRUE]-=p->BinaryCosts[r->var][rv->indexToVariable][r->val][TRUE];

		getNextLD(p->notAssignedVars);
	}

	(*passJeroslowDeactivate[HeurVar])(p,r->var);
}



void iniUnaryAndBinaryCosts(problem *p)

{

	int pos;
	clause * c;
	literal *lit;
	literal *lit1,*lit2;
	// Before calling Branch and Bound, we must set the tables of unary and binary costs with initial values.
	int i;

	for(i=0;i<p->totalClauses;i++)
	{
		if(p->clauses[i].literalsWithoutAssign==1)
		{
			// Sum costs of initial 1-literal clauses to the table of Unary costs

			c=&(p->clauses[i]);

			c->elim = PREELIM;

			c->terminator=NULL;

			lit=findFirstVariableWithoutAssign(p,i,&pos);

			c->termLit.boolValue=!lit->boolValue;

			c->termLit.idVar=lit->idVar;

			p->UnaryCosts[c->termLit.idVar][c->termLit.boolValue]+=c->weight;
		}
		else if(p->clauses[i].literalsWithoutAssign==2)
		{

			// Sum costs of initial 2-literals clauses to the table of Binary costs

			c=&(p->clauses[i]);

			c->elim = PREELIM2;

			c->terminator=NULL;

			lit1=findFirstVariableWithoutAssign(p,i,&pos);

			lit2=findSecondVariableWithoutAssign(p,i,&pos);

			c->termLit.boolValue=!lit1->boolValue;

			c->termLit.idVar=lit1->idVar;

			c->termLit2.boolValue=!lit2->boolValue;

			c->termLit2.idVar=lit2->idVar;

			p->BinaryCosts[c->termLit.idVar][c->termLit2.idVar][c->termLit.boolValue][c->termLit2.boolValue]+=c->weight;

			p->BinaryCosts[c->termLit2.idVar][c->termLit.idVar][c->termLit2.boolValue][c->termLit.boolValue]+=c->weight;
		}
	}
}



void undoUnaryAndBinaryCosts(problem *p)
{
	clause * c;

	int i;

	for(i=0;i<p->totalClauses;i++)
	{
		if(p->clauses[i].elim == PREELIM2)
		{

			c=&(p->clauses[i]);

			c->elim = FALSE;

			c->terminator=NULL;

			p->BinaryCosts[c->termLit.idVar][c->termLit2.idVar][c->termLit.boolValue][c->termLit2.boolValue]-= c->weight;

			p->BinaryCosts[c->termLit2.idVar][c->termLit.idVar][c->termLit2.boolValue][c->termLit.boolValue]-= c->weight;

		}
		else if(p->clauses[i].elim == PREELIM)
		{

			c=&(p->clauses[i]);

			c->elim = FALSE;

			c->terminator=NULL;

			p->UnaryCosts[c->termLit.idVar][c->termLit.boolValue]-= c->weight;
		}
	}
}

/* MAIN FUNCTIONS */

int prune(problem *p, restoreStruct * r)
{
	// This functions makes the prune of values from not assigned variables, and execute the local consistency

	ReferenceToVariable * rv;

	setFirstLD(p->notAssignedVars);
	while (!endListLD(p->notAssignedVars))
	{

		rv=getActualReferenceVar(p->notAssignedVars);
		// FALSE
		if(p->ArrayOfDecision[rv->indexToVariable].usedValue[FALSE]==FALSE)
		{
			if ((p->UnaryCosts[rv->indexToVariable][FALSE] + p->LB)>=p->UB)
			{

				insertElement(&r->pruneList,getNewNode2(&p->restoreList,rv->indexToVariable,FALSE));

				p->ArrayOfDecision[rv->indexToVariable].usedValue[FALSE]=TRUE;

				p->ArrayOfDecision[rv->indexToVariable].not_considered_values--;

				pushStack(p->Q,rv->indexToVariable);

				pruneTimes++;
			}
		}

		// TRUE
		if(p->ArrayOfDecision[rv->indexToVariable].usedValue[TRUE]==FALSE)
		{
			if ((p->UnaryCosts[rv->indexToVariable][TRUE] + p->LB)>=p->UB)
			{
				insertElement(&r->pruneList,getNewNode2(&p->restoreList,rv->indexToVariable,TRUE));

				p->ArrayOfDecision[rv->indexToVariable].usedValue[TRUE]=TRUE;

				p->ArrayOfDecision[rv->indexToVariable].not_considered_values--;

				pushStack(p->Q,rv->indexToVariable);

				pruneTimes++;
			}
		}

		if(p->ArrayOfDecision[rv->indexToVariable].not_considered_values==0)
		{
			return FALSE;
		}
		getNextLD(p->notAssignedVars);

	}
	return (*LcFuncWM[LcLevel])(p,r);
}

int fastNothing(problem *p,int i,int j)
{
	return TRUE;
}


int LAFDAC(problem *p,int i,int j)
{
	if(i<j)
	{
		if(!existFullSupport(p,i,j))
		{
			pushStack(p->R,j);
		}
		if (!existSupport(p,j,i))
		{
			pushStack(p->Q,i);
		}
	}
	else
	{
		if(!existFullSupport(p,j,i))
		{
			pushStack(p->R,i);
		}
		if (!existSupport(p,i,j))
		{
			pushStack(p->Q,j);
		}
	}
	return TRUE;
}


int LADAC(problem *p,int i,int j)
{
	if(i<j)
	{
		if(!existFullSupport(p,i,j))
		{
			pushStack(p->R,j);
		}
	}
	else
	{
		if(!existFullSupport(p,j,i))
		{
			pushStack(p->R,i);
		}
	}

	return TRUE;
}

int LAAC(problem *p,int i,int j)
{
	if (!existSupport(p,i,j))
	{
		pushStack(p->Q,j);
	}
	if (!existSupport(p,j,i))
	{
		pushStack(p->Q,i);
	}
	return TRUE;
}

func_t LAStacks[] = {fastNothing,LAAC,LADAC,LAFDAC,LAFDAC};

int update(problem * p,restoreStruct * r)
{
	int pos;

	ReferenceToClause * rc;

	clause * c;

	ListD * satistiedCla;

	ListD * notSatistiedCla;

	literal *lit1,*lit2;


	// This function returns FALSE if it does an early-prune, else return TRUE

	// save the actual LB

	r->actualLB=p->LB;

	// sum unary costs of actual variable to the lower bound

	p->LB=p->LB+p->UnaryCosts[r->var][r->val];


	if (p->LB>=p->UB)
	{
		p->LB=r->actualLB;

		p->Result[r->var]=NO_VALUE;

		return FALSE;
	}

	if(binaryToUnaryAndNodeConsistency(p,r)==FALSE)
	{
		p->LB=r->actualLB;

		p->Result[r->var]=NO_VALUE;

		restoreNodeConsistency(p,r);
		restoreBinaryToUnaryLimited(p,r);

		return FALSE;
	}


	if (r->val==TRUE)
	{

		satistiedCla = p->variables[r->var].listLiterals;
		notSatistiedCla = p->variables[r->var].listNoLiterals;
	}
	else
	{
		satistiedCla = p->variables[r->var].listNoLiterals;
		notSatistiedCla = p->variables[r->var].listLiterals;
	}

	// First, we eliminate satisfied clauses
	createListRAux(&r->satList);

	setFirstLD(satistiedCla);

	while (!endListLD(satistiedCla))
	{

		rc=getActualReference(satistiedCla);

		c=&(p->clauses[rc->indexToClause]);

		if (c->elim==FALSE)
		{
			c->elim = TRUE;

			insertElement(&r->satList,getNewNode1(&p->restoreList,rc->indexToClause));

			// actualize incremental jeroslow

			nopassJeroslow(p,rc->indexToClause);
		}
		getNextLD(satistiedCla);
	}



	// Now, we eliminate binary insatisfied clauses and

	// sum them weights to the table of binary costs

	setFirstLD(notSatistiedCla);

	while (!endListLD(notSatistiedCla))
	{
		rc=getActualReference(notSatistiedCla);

		c=&(p->clauses[rc->indexToClause]);

		if(c->elim==FALSE)
		{
			// actualize incremental jeroslow

			nopassJeroslow(p,rc->indexToClause);

			c->literalsWithoutAssign--;			

			if (c->literalsWithoutAssign==2)
			{

				// Pass costs to Binary costs table

				lit1=findFirstVariableWithoutAssign(p,rc->indexToClause,&pos);

				lit2=findSecondVariableWithoutAssign(p,rc->indexToClause,&pos);

				c->elim = PREELIM2;

				c->terminator=notSatistiedCla->actual;

				c->termLit.boolValue=!lit1->boolValue;

				c->termLit.idVar=lit1->idVar;

				c->termLit2.boolValue=!lit2->boolValue;

				c->termLit2.idVar=lit2->idVar;

				(*nopassBinaryJeroslow[HeurVar])(p,c->termLit.idVar,c->termLit2.idVar);

				p->BinaryCosts[c->termLit.idVar][c->termLit2.idVar][c->termLit.boolValue][c->termLit2.boolValue]+= c->weight;

				p->BinaryCosts[c->termLit2.idVar][c->termLit.idVar][c->termLit2.boolValue][c->termLit.boolValue]+= c->weight;

				setIsBinaryPair(p,c->termLit.idVar,c->termLit2.idVar,isBinaryPair(p,c->termLit.idVar,c->termLit2.idVar));

				(*passBinaryJeroslow[HeurVar])(p,c->termLit.idVar,c->termLit2.idVar);


				(*LAStacks[LcLevel])(p,c->termLit.idVar,c->termLit2.idVar);

			}

			else passJeroslow(p,rc->indexToClause);
		}
		getNextLD(notSatistiedCla);
	}
	return TRUE;
}



void restorePrunedValues(problem * p, restoreStruct * r)
{
	NodeR *n;

	setFirstLR(&r->pruneList);

	while(!endListLR(&r->pruneList))
	{

		n=extractActualR(&r->pruneList);

		if (n->val2==FALSE)

			p->ArrayOfDecision[n->val1].usedValue[FALSE]=FALSE;

		else

			p->ArrayOfDecision[n->val1].usedValue[TRUE]=FALSE;


		p->ArrayOfDecision[n->val1].not_considered_values++;

		insertElement(&p->restoreList,n);
	}
}



void restoreLocalConsistency(problem * p, restoreStruct * r)
{

	restoreNodeConsistency(p,r);

	restoreAC3(p,r);

}



void restoreState(problem * p, restoreStruct * r)
{
	NodeR *n;

	clause * c;

	ReferenceToClause * rc;

	ListD * satistiedCla;

	ListD * notSatistiedCla;

	//  Precondition: Only call this function if update did not prune

	// Restoration of Low Bound value

	p->LB=r->actualLB;



	/* Array of results */

	p->Result[r->var]=NO_VALUE;


	// restore local consistency
	restoreLocalConsistency(p,r);

	// restore pruned values
	restorePrunedValues(p,r);

	// restore unary and binary values
	restoreBinaryToUnary(p,r);


	/* First, undo "update" actions */

	if (r->val==TRUE)
	{
		satistiedCla = p->variables[r->var].listLiterals;
		notSatistiedCla = p->variables[r->var].listNoLiterals;
	}
	else
	{
		satistiedCla = p->variables[r->var].listNoLiterals;
		notSatistiedCla = p->variables[r->var].listLiterals;
	}

	setFirstLR(&r->satList);

	while(!endListLR(&r->satList))
	{

		n=extractActualR(&r->satList);

		p->clauses[n->val1].elim=FALSE;

		//c->terminator=NULL;

		insertElement(&p->restoreList,n);

		// actualize incremental Jeroslow
		passJeroslow(p,n->val1);
	}


	// For not unary unsatisfied formulas, we must 

	// reload the literal,

	// and we must reload each unary unsatisfied clause

	setFirstLD(notSatistiedCla);

	while (!endListLD(notSatistiedCla))
	{
		rc=getActualReference(notSatistiedCla);

		c=&(p->clauses[rc->indexToClause]);

		if (c->literalsWithoutAssign==2 && c->elim == PREELIM2 && (c->terminator==notSatistiedCla->actual))
		{

			// Early Elimintation restoration

			c->elim = FALSE;

			c->terminator=NULL;

			(*nopassBinaryJeroslow[HeurVar])(p,c->termLit.idVar,c->termLit2.idVar);

			p->BinaryCosts[c->termLit.idVar][c->termLit2.idVar][c->termLit.boolValue][c->termLit2.boolValue]-= c->weight;

			p->BinaryCosts[c->termLit2.idVar][c->termLit.idVar][c->termLit2.boolValue][c->termLit.boolValue]-= c->weight;

			setIsBinaryPair(p,c->termLit.idVar,c->termLit2.idVar,isBinaryPair(p,c->termLit.idVar,c->termLit2.idVar));

			(*passBinaryJeroslow[HeurVar])(p,c->termLit.idVar,c->termLit2.idVar);

			c->literalsWithoutAssign++;

			// actualize incremental jeroslow

			passJeroslow(p,rc->indexToClause);
		}
		else if(c->elim == FALSE)
		{
			// actualize incremental jeroslow

			nopassJeroslow(p,rc->indexToClause);

			c->literalsWithoutAssign++;

			// actualize incremental jeroslow

			passJeroslow(p,rc->indexToClause);
		}

		getNextLD(notSatistiedCla);

	}
}


void clearStacks(problem *p)
{
	clearStackValues(p->R);
	clearStackValues(p->Q);
}


void WM_BB(problem *p)
{
	// Main Branch and bound.

	restoreStruct r;

	int v1,v2,c1,c2;

	if(isEmptyLD(p->notAssignedVars)==TRUE)
	{
		// it has found a best solution

		// No more variables to consider

		p->UB=p->LB;

		// The best solution is in p->Result

		saveBestResult(p);

		GiveResults(p);

	    if (Verbose)
		{
			printf("New Top: "); PRINTCOST(Top); printf(" (%lu nodes, %.2lf secs)\n",NNodes,cpuTime());
	    }

	}
	else
	{
		// Selects the next variable to consider
		r.var=(*HeurVarFuncWM[HeurVar])(p);


		// Extracts the variable from list
		r.xAct=extractActual(p->notAssignedVars);
	

		// selects first the value -TRUE/FALSE- with minimum node cost.
		if(!(*HeurValFuncWM[HeurVal])(p,&r))
		{
			v1=FALSE;

			v2=TRUE;

			c1=p->ArrayOfDecision[r.var].usedValue[FALSE];

			c2=p->ArrayOfDecision[r.var].usedValue[TRUE];
		}
		else
		{
			v1=TRUE;

			v2=FALSE;

			c1=p->ArrayOfDecision[r.var].usedValue[TRUE];

			c2=p->ArrayOfDecision[r.var].usedValue[FALSE];
		}

		// Assign First Value to Xi

		r.val=v1;

		if(c1==FALSE)
		{

			NNodes++;
			depth++;

			if (NNodes >= NodeLimit) nodeLimitStop();

			p->Result[r.var]=r.val;

			clearStacks(p);

			if(update(p,&r)==TRUE)
			{
				if (prune(p,&r))
				{
					WM_BB(p);
				}

				restoreState(p,&r);
			}
			depth--;
		}

		// Assign Second Value to Xi

		r.val=v2;

		if(c2==FALSE)
		{

			NNodes++;
			depth++;

			if (NNodes >= NodeLimit) nodeLimitStop();

			p->Result[r.var]=r.val;

			clearStacks(p);

			if(update(p,&r))
			{
				if (prune(p,&r)==TRUE)
				{
					WM_BB(p);
				}
				restoreState(p,&r);
			}
			depth--;
		}

		// reinserts variable to the end of list
		insertExistentNode(p->notAssignedVars,r.xAct);
	} // end else
}

void NRES_A2_B1(problem *p,restoreStruct *r)
{
	// Neighborhoud resolution for 3-literals instances.
	int i,g,l,cox,coinc=0;
	ListD * posCla;
	ListD * negCla;
	ReferenceToClause * rc,*rc2;
	literal l1,l2;

	for(i=0;i<p->totalVariables;i++)
	{
		if(p->Result[i]==-1)
		{
			posCla = p->variables[i].listLiterals;

			setFirstLD(posCla);

			while(!endListLD(posCla))
			{
				rc=getActualReference(posCla);

				if(p->clauses[rc->indexToClause].literalsWithoutAssign==3 && p->clauses[rc->indexToClause].elim==FALSE)
				{
					negCla = p->variables[i].listNoLiterals;

					setFirstLD(negCla);

					while(!endListLD(negCla))
					{
						rc2=getActualReference(negCla);

						if(p->clauses[rc2->indexToClause].literalsWithoutAssign==3  && p->clauses[rc2->indexToClause].elim==FALSE && p->clauses[rc->indexToClause].elim==FALSE)
						{
							cox=compClauses(p,rc->indexToClause,rc2->indexToClause,i,&l1,&l2);

							if(cox>=2)
							{
								coinc++;			
								if(p->clauses[rc2->indexToClause].weight>=p->clauses[rc->indexToClause].weight)
								{
									g=rc2->indexToClause;
									l=rc->indexToClause;
								}
								else
								{
									g=rc->indexToClause;
									l=rc2->indexToClause;
								}

								if(l1.boolValue==FALSE) l1.boolValue=TRUE;
								else l1.boolValue=FALSE;

								if(l2.boolValue==FALSE) l2.boolValue=TRUE;
								else l2.boolValue=FALSE;

								p->clauses[l].elim=TRUE;
								p->clauses[g].weight=p->clauses[g].weight-p->clauses[l].weight;
								if(p->clauses[g].weight==0)
								{
									p->clauses[g].elim=TRUE;
								}

								(*nopassBinaryJeroslow[HeurVar])(p,l1.idVar,l2.idVar);

								p->BinaryCosts[l1.idVar][l2.idVar][l1.boolValue][l2.boolValue]+=p->clauses[l].weight;

								p->BinaryCosts[l2.idVar][l1.idVar][l2.boolValue][l1.boolValue]+=p->clauses[l].weight;

								(*passBinaryJeroslow[HeurVar])(p,l1.idVar,l2.idVar);
							}
						}
						getNextLD(negCla);
					}
				}
				getNextLD(posCla);
			}
		}
	}
	printf("NRES_2A_1B applied %d times\n",coinc);
}


void execute_max_sat_rules(problem *p,restoreStruct *r)
{
	if(max_sat_rules==TRUE) NRES_A2_B1(p,r);
}

void solveProblem(problem *p)
{
	restoreStruct r;

	// set all variables to initial values

	start_timers();

	pruneTimes=0;

	NNodes=0;


	//initialialize and execute branch and bound
	iniUnaryAndBinaryCosts(p);

	initializeJeroslow(p);

	executeNodeConsistency(p,&r);

	// Simplify the input problem
	execute_max_sat_rules(p,&r);


	initQandR(p,&r);

	// call the branch and bound after pass the local consistency test

	if((*LcFuncWM[LcLevel])(p,&r))
	{
		updateIsBinaryTable(p);
		WM_BB(p);
	}

	// restore consistency node and eliminate from memory

	restoreLocalConsistency(p,&r);

	undoUnaryAndBinaryCosts(p);


	// extract and show results from branch and bound

	p->totTime=elapsed_time();

	if(Verbose)
	  {
	    if (LcLevel==LC_NC)

	      printf("\nAlgorithm: NC");

	    else if (LcLevel==LC_AC)

	      printf("\nAlgorithm: AC");

	    else if (LcLevel==LC_DAC)

	      printf("\nAlgorithm: DAC");

	    else if (LcLevel==LC_FDAC)

	    printf("\nAlgorithm: FDAC");

	    printf("\nPrune Times: %d",pruneTimes);

	    printf("\nRecursive calls: %lu",NNodes);

	    printf("\nMax-Arity:%d",p->maximumArity);

	    printf("\nTotal cost:%d",p->UB);

	    printf("\nTotal time:%ld\n",p->totTime);

	    saveResultFile(p);
	  }
}


void readMAXSATandWMAXSATProblems(FILE *f,int isCnf)
{
  // load problem
  readProblem(f,&Problem,isCnf);

  // Give information from problem to Simon
  GiveParameters(&Problem);
}

void solveMAXSATandWMAXSATProblems()
{
  // solve problem
  solveProblem(&Problem);

  // save results for Simon code
  GiveResults(&Problem);

  // clear problem
  clearProblem(&Problem);
}
