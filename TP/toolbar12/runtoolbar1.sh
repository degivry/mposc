#!/bin/sh

# usage: runtoolbar1.sh problem.wcsp
# returns: problem initial_upperbound optimum(or "-" if unknown) nodes time(in seconds)

timelimit=600

dir=`dirname $1`
base=`basename $1 .wcsp`
ubfile="${dir}/${base}.ub" 
ub=`awk 'NR==1{printf("%d",$5);exit;}' $1`
if [[ -e $ubfile ]] ; then
ub2=`awk 'NF>=1{printf("%d",$1);exit;}' $ubfile`
if (($ub2 < $ub)) ; then ub=$ub2 ; fi
fi
printf "${dir}/$base $ub "
toolbar -t${timelimit} -u${ub} $1 | awk 'BEGIN{opt="-";nodes=0;time=0} /^Lower bound/{opt=$3; nodes=$5; time=$8;} /^Optimum/{opt=$2; nodes=$4; time=$7;} /^Best bound/{nodes=$5;time=$8;} /^No solution found/{nodes=$5;time=$8;} END{print opt,nodes,time;}'
