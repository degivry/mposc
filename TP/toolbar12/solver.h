/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: solver.h
  $Id: solver.h,v 1.1.1.1 2007-11-21 09:26:15 degivry Exp $

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#ifndef SOLVER_H
#define SOLVER_H

#include "tblist.h"
#include "tbstream.h"

typedef int (*func_t)();
extern func_t LcFunc[];
extern func_t HeurVarFunc[];
extern func_t UpdateHeurVarFunc[];

typedef void (*func2_t)(int);
extern func2_t HeurValFunc[];

typedef void (*func3_t)();
extern func3_t HeurVar2Func[];
extern func3_t CheckACFunc[];

typedef int (*func4_t)(int *, int *);
extern func4_t CmpHeurVarFunc[];

extern int *currentArity;

extern int *assignments;
       /* Assignments to past variables.
	    * assignments[i]=a  if i is currently assigned with value "a"
	    * assignments[i]=-1 if i is a future (unassigned) variable
	    */
extern double *H;
extern int *HeurValRND;
extern int **current_domains;
       /* current_domains[i][a]=j  if (i,a) was pruned when j was assigned
	    * current_domains[i][a]=-1 if (i,a) is feasible
	    */
extern List   *feasible_values;
extern List   *feasible_values2;
       /* feasible_values[i] is the list of feasible values of variable i
	    * It duplicates information with current_domains[][]
	    */
extern int num_future;            /* number of future variables */
extern int depth;

extern int *degree; /* For each future variable "i", degree[i] keeps the
                    number of future variables with which "i" is connected*/
/* The following two variables keep the static ordering of variables which is
used by DAC and FDAC*/
extern int *order; /* order[i]=k "i is the k-th variable"*/
extern int *order2; /* order2[i]=k "k is the i-th variable"*/

/*The following variable keeps the static variable ordering which is used for
  variable selection (if a static heuristic is selected) */
extern int *order3;

/*extern int *future_var;*/ /* future_var[0..num_future-1] contains the set of future
                        variables. I use this ordering in the DAC and FDAC algorithms*/


extern cost_t *DeltaU, ***DeltaB; /* Compensation costs*/
extern int *SupU, ***SupB;/*Supports as in AC2001*/


extern int  **SupU2, **degree2;
/* extern int  **future_var2;*/
extern int **fut_var_r2;
extern List *l1, /* list of pruned values (i,j)*/
	*l2, /* list of unary increments (i,a,cost)*/
	*l3, /* list of projections (i,j,a,cost),
	             binary constraint (i,j) has been projected over unary (i,a) constraint*/

        *l4, /* list of supports (a,i,j,b)*/
	*l5, /* list of binary constraints (i,j)*/
	*l6; /* list of binary increments (i,j,a,b,cost)*/

extern int **orderCopy;

extern void restore_context();
extern void sort_val(int var);
extern int selec_var();
extern int findSupportU(int j, int *flag);
extern int findSupportNext(int i, int j, int *flag1, int *flag3);
extern int findSupportFirst(int i, int j, int *flag1, int *flag3);
extern int AC();
extern int AC2();
extern int findFullSupport(int i, int j,int *flag1,int *flag3);
extern int findFullSupportEpsilon(int i, int j,int *flag1,int *flag3,int *flag4);
extern int ACplus();
extern int DAC();
extern int FDAC();
extern void solve();
/* extern cost_t CostU(int i, int a); */
/* extern cost_t CostB(int i, int a, int j, int b); */
extern int prune_variable(int h, int *stop);
extern void sort_fut_var();
extern void save_parameters1();
extern void restore_parameters1();
extern int look_ahead(int var, int val);
extern int projectB(int i);
extern void nextDirectionWCSP();


#endif
