
# usage: runtoolbar.sh ../benchs | awk -f restoolbar.awk | sort

# for benchmarking, summary of runtoolbar.sh output

# for each directory, returns:
# directory mean_initial_upperbound mean_optimum mean_nodes mean_time(in seconds) number_of_problems_completely_solved number_of_problems

#  {
#      print $0;
#  }

!/^[#]/ {
    dir = $1;
#    sub("/[^/]*$","",dir);
    n = split(dir,path,"/");
    name = path[1];
    for (i=2; i<=n; i++) {
	d = name ":";
	dirs[d]++;
	ub[d] += $2;
	if ($3 != "-") {
	    nbopt[d]++;
	    opt[d] += $3;
	}
	nodes[d] += $4;
	time[d] += $5;
	name = name "/" path[i];
    }
}

END {
    for (d in dirs) {
	n=dirs[d];
	if (d in nbopt) {
	    printf("%s %.2f %.2f %.2f %.2f %d %d\n",d,ub[d]/n,opt[d]/nbopt[d],nodes[d]/n,time[d]/n,nbopt[d],n);
	} else {
	    printf("%s %.2f %.2f %.2f %.2f %d %d\n",d,ub[d]/n,"-",nodes[d]/n,time[d]/n,0,n);
	}
    }
}
