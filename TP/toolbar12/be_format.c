#include <errno.h>
#include <math.h>
#include "be.h"
#include "wcsp.h"

#define MAXNARYCONSTR 65000

bucket *Bucket=NULL;
bucket *Result=NULL;
bucket *MB=NULL;

short *estruct_a_fich=NULL; 
short *fich_a_estruct=NULL; 

float log2i(int pot2)
{
	return (log(pot2)/log(2));
/*	short i;

	i=0;
	while ((pot2&1)==0)
	{
		pot2=pot2>>1;
		i++;
	}

	return i;*/
}


void decreaseOrder (short *vector, int N)
{
	// From N ... 0
	short i,j,aux;

	for (i=0;i<N;i++)
	{
		for (j=i+1;j<N;j++)
		{
			if (vector[i]<vector[j])
			{
				aux=vector[i];
				vector[i]=vector[j];
				vector[j]=aux;
			}
		}
	}
}

void* freeFunction (nodofunc *nodo) {

	if (nodo->despl_var!=NULL) free (nodo->despl_var);
	if (nodo->costs!=NULL) free (nodo->costs);
	if (nodo->vars!=NULL) free (nodo->vars);
	free (nodo);
	return NULL;
}

void freeBucket (bucket *b) {
	nodofunc *nodo1,*nodo2;

	nodo1=b->primera_func_original;
	b->primera_func_original=NULL;
	while (nodo1!=NULL)
	{
		nodo2=nodo1;
		nodo1=nodo1->sig_func;
		freeFunction (nodo2);
	}

	nodo1=b->primera_func_anadida;
	b->primera_func_anadida=NULL;
	while (nodo1!=NULL)
	{
		nodo2=nodo1;
		nodo1=nodo1->sig_func;
		freeFunction (nodo2);
	}
	b->num_funcs=0;
}

void varOrdering ()
{
	short i;

	fich_a_estruct=mycalloc(NVar,sizeof(short));

	if (EliminationOrder==NULL) {
		EliminationOrder=mymalloc(NVar, sizeof(short));
			for (i=0;i<NVar;i++) EliminationOrder[i]=i;
	}

	estruct_a_fich=EliminationOrder;

	for (i=0;i<NVar;i++)
	{
		fich_a_estruct[estruct_a_fich[i]]=i;
	}

}

bool_t nextAssignment (int *completeAssignment, int *variables, short arity) {
	short i;

	i=arity-1;
	while (i>=0 && completeAssignment[i]== DOMSIZE(variables[i])-1 ) {
		completeAssignment[i]=0;
		i--;
	}

	if (i>=0) {
		completeAssignment[i]++;
		return FALSE;
	} else return TRUE;
}


void initBucket (bucket *b) {
	b->num_funcs=0;
	b->primera_func_original=NULL;
	b->ultima_func_original=NULL;
	b->primera_func_anadida=NULL;
}


void initStructures () {
	short num_var, a, j, k, m;
	int i, nbc, pos, original_pos;
	nodofunc *nodo;
	int *corresp, *completeAssignment, *copy;
	cost_t def_val;
	bool_t naryconstr[MAXNARYCONSTR], overflow;
	
	Bucket=mycalloc(NVar,sizeof(bucket));
	for (i=0;i<NVar;i++) initBucket (&Bucket[i]);

	Result =mycalloc(1,sizeof(bucket));
	initBucket (Result);

	MB =mycalloc(1,sizeof(bucket));
	initBucket(MB);

	for (i=0; i<MAXNARYCONSTR; i++) 
		naryconstr[i]=FALSE;

	varOrdering(); // order the vars following the order from the tree descomposition

	for (i=0; i<NVar; i++) {
	//Unary restriccions
		nodo=mymalloc(1,sizeof(nodofunc));
		nodo->aridad=1;
		nodo->vars=mycalloc(1,sizeof(short));
		num_var = fich_a_estruct[i];
		nodo->vars[0]=num_var;
		nodo->despl_var=mycalloc(1,sizeof(int));
		nodo->despl_var[0]=1;
		nodo->ntuples=DOMSIZE(i);

		nodo->costs = mycalloc (DOMSIZE(i),sizeof(cost_t));
	
		//Maximum cost: if it's 0 (or BOTTOM), the unary constraint is not added
		def_val=BOTTOM;
		for (a=0; a<DOMSIZE(i); a++) {
			nodo->costs[a] = UNARYCOST(i,a);
			def_val = MAX(def_val, nodo->costs[a]);
		}

		nodo->sig_func=NULL;
		if (def_val!=BOTTOM) {
			if (Bucket[num_var].primera_func_original==NULL) {
				Bucket[num_var].primera_func_original=nodo;
				Bucket[num_var].ultima_func_original=nodo;
			} else {
				nodo->sig_func = Bucket[num_var].primera_func_original;
				Bucket[num_var].primera_func_original=nodo;
			}
			Bucket[num_var].num_funcs++;
		} else free(nodo);
		
	//Nary constraints: retrieved from first var to eliminate (i.e., NVar-1, ..., 0)
		for (j=0; j<VARNCONSTR(estruct_a_fich[NVar-i-1]); j++) {
			nbc=VARCONSTRONE(estruct_a_fich[NVar-i-1],j);
			if (!naryconstr[nbc]) {
				nodo=mymalloc(1,sizeof(nodofunc));
				nodo->aridad=ARITY(nbc);
				nodo->vars=mymalloc(nodo->aridad,sizeof(short));

				for (k=0;k<nodo->aridad;k++) nodo->vars[k]=fich_a_estruct[SCOPEONE(nbc,k)];
		
				decreaseOrder (nodo->vars,nodo->aridad); // decrease order of elimination

				corresp = mycalloc (nodo->aridad,sizeof(int));

				for (k=0;k<nodo->aridad;k++)
				{
					m=0;
					while (fich_a_estruct[SCOPEONE(nbc,k)]!=nodo->vars[m])
					{
						m++;
					}
					corresp[k]=m;
				}
				nodo->despl_var=mycalloc(nodo->aridad,sizeof(int));

				nodo->despl_var[0]=1;
				nodo->ntuples=DOMSIZE( estruct_a_fich[nodo->vars[0]] );
				for (k=1;k<nodo->aridad;k++)
				{
					nodo->despl_var[k]=nodo->despl_var[k-1]*DOMSIZE( estruct_a_fich[nodo->vars[k-1]] );//1<<suma_bits;
					nodo->ntuples*=DOMSIZE(estruct_a_fich[nodo->vars[k]] );//num_bits;
				}
				nodo->costs=mycalloc(nodo->ntuples,sizeof(cost_t));

				completeAssignment = mycalloc (nodo->aridad,sizeof(int));
				original_pos=0;
				overflow=FALSE;

				while (!overflow) {
				
					pos=0;
					for (m=0;m<nodo->aridad;m++)
					{
						pos=pos+completeAssignment[m]*(nodo->despl_var[corresp[m]]);
					}
					nodo->costs[pos] = COSTS(nbc)[original_pos];
					original_pos++;
					overflow=nextAssignment (completeAssignment, SCOPE(nbc), nodo->aridad);

				}

				nodo->sig_func=NULL;
				if (Bucket[nodo->vars[0]].primera_func_original==NULL) { 
					Bucket[nodo->vars[0]].primera_func_original=nodo;
					Bucket[nodo->vars[0]].ultima_func_original=nodo;
				} else {
					Bucket[nodo->vars[0]].ultima_func_original->sig_func=nodo;
					Bucket[nodo->vars[0]].ultima_func_original=nodo;
				}
				Bucket[nodo->vars[0]].num_funcs++;
		
				free(completeAssignment);
				free(corresp);
				naryconstr[nbc]=TRUE;
			} //else, that constraint has been already processed in a previous variable
		}

	//Binary constraints
		for (j=i+1; j<NVar; j++) {
			if (ISBINARY(i,j)) {
				nodo=mymalloc(1,sizeof(nodofunc));
				nodo->aridad=2;
				nodo->vars=mymalloc(nodo->aridad,sizeof(short));

				if (fich_a_estruct[i]>fich_a_estruct[j]) {
					nodo->vars[0]=fich_a_estruct[i];
					nodo->vars[1]=fich_a_estruct[j];
				} else {
					nodo->vars[0]=fich_a_estruct[j];
					nodo->vars[1]=fich_a_estruct[i];
				}

				nodo->despl_var=mycalloc(nodo->aridad,sizeof(int));
				nodo->despl_var[0]=1;
				nodo->despl_var[1]=DOMSIZE( estruct_a_fich[nodo->vars[0]]);
				nodo->ntuples=DOMSIZE( estruct_a_fich[nodo->vars[0]])*DOMSIZE( estruct_a_fich[nodo->vars[1]]);

				nodo->costs=mycalloc(nodo->ntuples,sizeof(cost_t));
				
				pos=0;
				for (k=0; k<DOMSIZE( estruct_a_fich[nodo->vars[1]] ); k++) {
					for (m=0; m<DOMSIZE( estruct_a_fich[nodo->vars[0]] ); m++) {
						nodo->costs[pos]=(fich_a_estruct[i]>fich_a_estruct[j]?binaryCost(i,j,m,k):binaryCost(i,j,k,m));
						pos++;
					}
				}

				nodo->sig_func=NULL;
				if (Bucket[nodo->vars[0]].primera_func_original==NULL) { 
					Bucket[nodo->vars[0]].primera_func_original=nodo;
					Bucket[nodo->vars[0]].ultima_func_original=nodo;
				} else {
					Bucket[nodo->vars[0]].ultima_func_original->sig_func=nodo;
					Bucket[nodo->vars[0]].ultima_func_original=nodo;
				}
				Bucket[nodo->vars[0]].num_funcs++;

				destroyBinaryConstraint(i,j);
			} //else, there is no binary constraint for variables i-j or j-i

		}
	}

	//Change the domain size of the variables taking into account the new order
	copy = mymalloc(NVar,sizeof(int));
	for (i=0;i<NVar;i++) copy[i]=DOMSIZE(estruct_a_fich[i]);
	for (i=0;i<NVar;i++) DOMSIZE(i)=copy[i];
	free(copy);
}

