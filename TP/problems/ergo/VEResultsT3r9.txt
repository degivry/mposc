alarm
Bucket Elimination: Optimum 3086726818 with maximal memory usage of 2^6 in 0 sec and 37 buckets processed exactly.
Loglikelihood -1.340548 (p = 4.565113e-02)
barley
Bucket Elimination: Optimum 43809507040 with maximal memory usage of 2^23 in 2.22 sec and 48 buckets processed exactly.
Loglikelihood -19.026227 (p = 9.413970e-20)
carpo
Bucket Elimination: Optimum -6917529017078758256 with maximal memory usage of 2^5 in 0 sec and 61 buckets processed exactly.
Loglikelihood 3004244682.025053 (p = inf)
Optimum: 7062298920 in 76 nodes and 0 seconds.
Loglikelihood -3.067117 (p = 8.568061e-04)
cpcs179
Bucket Elimination: Optimum 4975569898 with maximal memory usage of 2^14 in 0.02 sec and 179 buckets processed exactly.
Loglikelihood -2.160863 (p = 6.904583e-03)
cpcs360b
Bucket Elimination: Optimum 15350150749 with maximal memory usage of 2^20 in 1.88 sec and 360 buckets processed exactly.
Loglikelihood -6.666486 (p = 2.155332e-07)
cpcs422b
Bucket Elimination: Optimum 5238423325 with maximal memory usage of 2^22 in 8.76 sec and 422 buckets processed exactly.
Loglikelihood -2.275018 (p = 5.308620e-03)
cpcs54
Bucket Elimination: Optimum 24702629988 with maximal memory usage of 2^13 in 0 sec and 54 buckets processed exactly.
Loglikelihood -10.728216 (p = 1.869752e-11)
diabetes
Bucket Elimination: Optimum 83894596295 with maximal memory usage of 2^15 in 0.27 sec and 413 buckets processed exactly.
Loglikelihood -36.434960 (p = 3.673159e-37)
hailfinder
Bucket Elimination: Optimum 27265764037 with maximal memory usage of 2^9 in 0 sec and 56 buckets processed exactly.
Loglikelihood -11.841371 (p = 1.440884e-12)
insurance
Bucket Elimination: Optimum 6125933345 with maximal memory usage of 2^13 in 0 sec and 27 buckets processed exactly.
Loglikelihood -2.660459 (p = 2.185450e-03)
link
Bucket Elimination: Optimum 181867256856 with maximal memory usage of 2^22 in 3.38 sec and 724 buckets processed exactly.
Loglikelihood -78.983946 (p = 1.037657e-79)
mildew
Bucket Elimination: Optimum 22379060189 with maximal memory usage of 2^19 in 0.4 sec and 35 buckets processed exactly.
Loglikelihood -9.719102 (p = 1.909403e-10)
munin1
Bucket Elimination: Optimum 16639986792 with maximal memory usage of 2^26 in 13.85 sec and 189 buckets processed exactly.
Loglikelihood -7.226654 (p = 5.933973e-08)
munin2
Bucket Elimination: Optimum -9223371955457915512 with maximal memory usage of 2^17 in 0.16 sec and 1003 buckets processed exactly.
Loglikelihood 4005659546.799408 (p = inf)
Optimum: 83028356366 in 155183 nodes and 20.66 seconds.
Loglikelihood -36.058757 (p = 8.734599e-37)
munin3
Bucket Elimination: Optimum 76960456367 with maximal memory usage of 2^17 in 0.17 sec and 1044 buckets processed exactly.
Loglikelihood -33.423502 (p = 3.771364e-34)
munin4
Bucket Elimination: Optimum 84284065507 with maximal memory usage of 2^19 in 1.46 sec and 1041 buckets processed exactly.
Loglikelihood -36.604105 (p = 2.488258e-37)
pigs
Bucket Elimination: Optimum 201012682200 with maximal memory usage of 2^16 in 0.06 sec and 441 buckets processed exactly.
Loglikelihood -87.298699 (p = 5.026912e-88)
simple
Bucket Elimination: Optimum 2079441540 with maximal memory usage of 2^2 in 0 sec and 3 buckets processed exactly.
Loglikelihood -0.903090 (p = 1.250000e-01)
water
Bucket Elimination: Optimum 8086419257 with maximal memory usage of 2^19 in 0.19 sec and 32 buckets processed exactly.
Loglikelihood -3.511887 (p = 3.076895e-04)
