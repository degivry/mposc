/********** car_sequencing.lsp **********/

/* Reads instance data. */
function input() {

  usage = "\nUsage: localsolver car_sequencing.lsp "
    + "inFileName=inputFile solFileName=outputFile [lsTimeLimit=timeLimit]\n";

  if (inFileName == nil) error(usage);
  if (solFileName == nil) error(usage);

  inFile = openRead(inFileName);
  nbPositions = readInt(inFile);
  nbOptions = readInt(inFile);
  nbClasses = readInt(inFile);

  ratioNums[1..nbOptions] = readInt(inFile);
  ratioDenoms[1..nbOptions] = readInt(inFile);

  for [c in 1..nbClasses] {
    readInt(inFile); // Note: index of class is read but not used
    nbCars[c] = readInt(inFile);
    options[c][1..nbOptions] = readInt(inFile);
  }
}

/* Declares the optimization model. */
function model() {	
  // 0-1 decisions: 
  // cp[c][p] = 1 if class c is at position p, and 0 otherwise
  cp[1..nbClasses][1..nbPositions] <- bool();

  // constraints: 
  // for each class c, no more than nbCars[c] assigned to positions
  for [c in 1..nbClasses] 
    constraint sum[p in 1..nbPositions](cp[c][p]) == nbCars[c];

  // constraints: one car assigned to each position p
  for [p in 1..nbPositions] 
    constraint sum[c in 1..nbClasses](cp[c][p]) == 1;

  // expressions: 
  // op[o][p] = 1 if option o appears at position p, and 0 otherwise
  op[o in 1..nbOptions][p in 1..nbPositions] <- 
    or[c in 1..nbClasses : options[c][o]](cp[c][p]);

  // expressions: compute the number of cars in each window
  nbCarsWindows[o in 1..nbOptions][p in 1..nbPositions-ratioDenoms[o]+1] 
    <- sum[k in 1..ratioDenoms[o]](op[o][p+k-1]);

  // expressions: compute the number of violations in each window
  nbViolationsWindows[o in 1..nbOptions][p in 1..nbPositions-ratioDenoms[o]+1] 
    <- max(nbCarsWindows[o][p]-ratioNums[o], 0);

  // objective: minimize the sum of violations for all options and all windows
  obj <- sum[o in 1..nbOptions]
    [p in 1..nbPositions-ratioDenoms[o]+1](nbViolationsWindows[o][p]);
  minimize obj;	
}

/* Parameterizes the solver. */
function param() {
  if (lsTimeLimit == nil) lsTimeLimit = 10;
  lsTimeBetweenDisplays = 3;
  lsNbThreads = 1;
  lsAnnealingLevel = 0;
}

/* Writes the solution in a file following the following format: 
 * - 1st line: value of the objective;
 * - 2nd line: for each position p, index of class at positions p. */
function output() {
  solFile = openWrite(solFileName);
  println(solFile, getValue(obj));
  for [p in 1..nbPositions][c in 1..nbClasses : getValue(cp[c][p])] 
    print(solFile, c-1, " ");
  println(solFile);
}

