

Numberjack - A python constraint programming platform -
Numberjack is free software released under the LGPL. UCC-4C�2009
http://numberjack.ucc.ie/home

Examples of combinatorial problems in Numberjack under linux (plus experiments advices)

- Bibd.py
- CostasArray.py
- GolombRuler.py       (Mistral/SCIP)
- JobshopSimple.py
- JobshopDichotomy.py  (Mistral: dichotomic search)
- Langford.py         
- MagicSquare.py
- MagicWater.py
- NQueens.py           (Mistral:Lex/MinDomainMinVal/MinDomainMaxDegree, Mistral/SCIP)
- NQueensBinary.py     (Mistral: binaries/AllDifferent)
- Quasigroup.py
- SendMoreMoney.py
- SportScheduling.py   (Baseball League Tournament: MinDomainMaxDegree/Impact, Mistral/SCIP)
- Sudoku.py            (AllDifferent)
- Tsccd.py
- Warehouse.py         (SCIP capacited/uncapacited - comment last sum)
- WarehouseToulbar2.py (Toulbar2 capacited/uncapacited -comment line Gcc)

Documentation available here: http://numberjack.ucc.ie/doxy/index.html

Numberjack includes a MIP solver (SCIP), two SAT solvers (MiniSat) and (Walksat), a CP solver (Mistral), a WCSP solver (Toulbar2 - preliminary integrated version)
