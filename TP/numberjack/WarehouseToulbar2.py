#! /usr/bin/env python

from Numberjack import *


def solve(param):
    data = WareHouseParser(param['data'])
    lib = __import__(param['solver'])
    top = 10000000
#    top = 827559
#    top = 333

    WareHouseOpen = VarArray(data.NumberOfWarehouses)
    
    ShopSupplied = VarArray(data.NumberOfShops, data.NumberOfWarehouses)
    
    warehouseCosts = []
    transpCosts = []
    # Cost of running warehouses
    for i in range(data.NumberOfWarehouses):
        warehouseCosts.append(PostUnary(WareHouseOpen[i],[0,data.WareHouseCosts[i]]));
    # Cost of shops using warehouses
    for i in range(data.NumberOfShops):
        transpCosts.append(PostUnary(ShopSupplied[i],[data.SupplyCost[i][j] for j in range(data.NumberOfWarehouses)]))
    
    model = Model(
        # objective function
        warehouseCosts,
        transpCosts,

        # Channel from store opening to store supply matrix
        # (ShopSupplied[s]==w) implies (WareHouseOpen[w]==1)
        [PostBinary(ShopSupplied[s], WareHouseOpen[w], [int(i==w)*int(j==0)*top for i in range(data.NumberOfWarehouses) for j in range(2)]) for s in range(data.NumberOfShops) for w in range(data.NumberOfWarehouses)],

        # Make sure that each warehouse does not exceed it's supply capacity
#        Gcc(ShopSupplied, dict([[i,(0,cap)] for (i, cap) in zip(range(data.NumberOfWarehouses), data.Capacity)])) # decomposed version in multiple-among
        Gcc(ShopSupplied, dict([[i,(0,cap)] for (i, cap) in zip(range(data.NumberOfWarehouses), data.Capacity)]), "s", "var", str(10000000)) # flow-based soft GCC
    )

#    print model

    solver = lib.Solver(model)
    solver.setVerbosity(param['verbose'])
    solver.setOption('updateUb',str(top))
#    solver.setOption("dumpWCSP",1)
    solver.setOption('debug',1)
#    solver.setOption('nbDecisionVars',(data.NumberOfWarehouses + data.NumberOfShops))
    solver.setOption('showSolutions',1)
#    solver.setOption('lds',2)
#    solver.setOption('restart',1000)
    solver.solve()
    return "\nSolved! "
  
class WareHouseParser:
    
    def __init__(self, file):
        lines = open(file).readlines() 
        self.NumberOfWarehouses = int(lines[0][4:-2])
        self.NumberOfShops = int(lines[1][4:-2])
        self.FixedCost = int(lines[2][6:-2])
        self.WareHouseCosts = [self.FixedCost for val in range(self.NumberOfWarehouses)]
        self.SupplyCost = eval("["+
                               " ".join((map((lambda a: a[:-1]+","),
                                                lines[4:-1]))) +
                               "]")
        # There was a fixed capacity for all the warehouse problems
        self.Capacity = [4 for val in range(self.NumberOfWarehouses)]
        
    def get(self, name):
        if hasattr(self, name):
            return getattr(self, name)
        print name + " \t No Such Data!"
        return None
    

#default = {'solver':'Toulbar2', 'data':'data/cap131.dat.txt', 'verbose':0}
default = {'solver':'Toulbar2', 'data':'data/warehouse1.txt', 'verbose':0}

if __name__ == '__main__':
    param = input(default) 
    print solve(param)
