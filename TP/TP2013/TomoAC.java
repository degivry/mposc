import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.Solver;
import choco.kernel.solver.constraints.integer.extension.TuplesTest;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.Choco;

public class TomoAC {
    
    static int nbCol = 10;
    static int nbLig = 10;
    static int[][] colBlocs = new int[][]{
	new int[]{2, 3},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{2, 3},
	new int[]{3, 4},
	new int[]{8},
	new int[]{9},
    };
    
    static int[][] ligBlocs = new int[][]{
	new int[]{3, 6},
	new int[]{1, 4},
	new int[]{1, 1, 3},
	new int[]{2},
	new int[]{3, 3},
	new int[]{1, 4},
	new int[]{2, 5},
	new int[]{2, 5},
	new int[]{1, 1},
	new int[]{3},
    };
    
    public static void main(String[] args) 
    {
	Model m = new CPModel();
	IntegerVariable[][] vars = 
	    Choco.makeIntVarArray("case", nbCol, nbLig, 0, 1);
	
	for (int i = 0; i < ligBlocs.length; i++) 
	    {
		IntegerVariable[] ligne = new IntegerVariable[nbCol];
		for (int j = 0; j < ligne.length; j++)
		    {
			ligne[j] = vars[j][i];
		    }
		int[] blocs = ligBlocs[i];
		postConstraint(m, ligne, blocs);
	    }
	
	for (int i = 0; i < colBlocs.length; i++) 
	    {
		IntegerVariable[] colonne = new IntegerVariable[nbLig];
		for (int j = 0; j < colonne.length; j++) 
		    {
			colonne[j] = vars[i][j];
		    }
		int[] blocs = colBlocs[i];
		postConstraint(m, colonne, blocs);
	    }
	
	Solver s = new CPSolver();
	s.read(m);
	
	s.solve();
	s.printRuntimeStatistics();
	for (int i = 0; i < nbLig; i++) 
	    { 
		for (int j = 0; j < nbCol; j++) 
		    {
			IntegerVariable v = vars[j][i];
			System.out.print(s.getVar(v).getVal() == 0 ? " " : "#");
		    }
		System.out.println();
	    }
    }
    
    private static void postConstraint(Model m, 
				       IntegerVariable[] ligne, final int[] blocs) 
    {
	m.addConstraint(
			Choco.relationTupleAC(ligne, new TuplesTest() 
			    {
				public boolean checkTuple(int[] tuple) 
				{
				    int currentBloc = 0;
				    int i = 0;
				    while (i < tuple.length) 
					{
					    while (i < tuple.length && tuple[i] == 0) i++; 
					    if (currentBloc == blocs.length && i == tuple.length) 
						return true;
					    if (currentBloc >= blocs.length) return false;
					    int l = 0;
					    while (i < tuple.length && tuple[i] == 1) 
						{
						    i++;
						    l++;
						}
					    if (l != blocs[currentBloc]) return false;
					    currentBloc++;
					    if (currentBloc == blocs.length && i == tuple.length)
						return true;
					}
				    return false;
				}
			    })
			);
    }
}

