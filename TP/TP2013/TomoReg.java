import choco.cp.model.CPModel;
import choco.kernel.model.constraints.automaton.DFA;
import choco.kernel.model.constraints.automaton.Transition;
import choco.kernel.model.constraints.Constraint;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.Solver;
import choco.Choco;

public class TomoReg 
{
    static int nbCol = 10;
    static int nbLig = 10;
    static int[][] colBlocs = new int[][]{
	new int[]{2, 3},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{2, 3},
	new int[]{3, 4},
	new int[]{8},
	new int[]{9},
    };
    
    static int[][] ligBlocs = new int[][]{
	new int[]{3, 6},
	new int[]{1, 4},
	new int[]{1, 1, 3},
	new int[]{2},
	new int[]{3, 3},
	new int[]{1, 4},
	new int[]{2, 5},
	new int[]{2, 5},
	new int[]{1, 1},
	new int[]{3},
    };
    
    public static void main(String[] args) 
    {
	Model m = new CPModel();
	IntegerVariable[][] vars = 
	    Choco.makeIntVarArray("case", nbCol, nbLig, 0, 1);
	
	for (int i = 0; i < ligBlocs.length; i++) 
	    {
		int[] blocs = ligBlocs[i];
		StringBuilder sb = new StringBuilder();
		sb.append("0*");
		for (int j = 0; j < blocs.length; j++) 
		    {
			if (j > 0) sb.append("0+");
			int bloc = blocs[j];
			for (int n = 0; n < bloc; n++) sb.append("1");
		    }
		sb.append("0*");
		
		IntegerVariable[] ligne = new IntegerVariable[nbCol];
		for (int j = 0; j < ligne.length; j++) 
		    {
			ligne[j] = vars[j][i];
		    }
		
		m.addConstraint(
				Choco.regular(new DFA(sb.toString(), nbCol), ligne));
	    }
	
	for (int i = 0; i < colBlocs.length; i++) 
	    {
		int[] blocs = colBlocs[i];
		StringBuilder sb = new StringBuilder();
		sb.append("0*");
		for (int j = 0; j < blocs.length; j++) 
		    {
			if (j > 0) sb.append("0+");
			int bloc = blocs[j];
			for (int n = 0; n < bloc; n++) sb.append("1");
		    }
		sb.append("0*");
		
		IntegerVariable[] colonne = new IntegerVariable[nbLig];
		for (int j = 0; j < colonne.length; j++) 
		    {
			colonne[j] = vars[i][j];
		    }
		
		m.addConstraint(
				Choco.regular(new DFA(sb.toString(), nbLig), colonne));
	    }
	
	Solver s = new CPSolver();
	s.read(m);
	
	s.solve();
	s.printRuntimeStatistics();
	for (int i = 0; i < nbLig; i++) 
	    {
		for (int j = 0; j < nbCol; j++) 
		    {
			IntegerVariable v = vars[j][i];
			System.out.print(s.getVar(v).getVal() == 0 ? " " : "#");
		    }
		System.out.println();
	    }
    }
}
