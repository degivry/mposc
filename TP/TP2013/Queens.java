import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.Solver;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.Choco;

public class NReinesModel1 {
    
    public static final int NB_REINES = 10;
    
    public static void main(String[] args) {
	
	// 0. Création du problème 
	Model m = new CPModel();
	
	// 1. Création des variables 
	IntegerVariable[] vars = createVariables(m);
	
	// 2. Création des contraintes 
	postConstraints ( m, vars );
	
	// 3. Choix solver et heuristique 
	Solver s = new CPSolver ();
	s.read(m);
	setHeuristic(s);
	
	// 4. Résolution du problème 
	s.solve();
	
	// 5. Récupérer la solution 
	displayResult(s, vars);
    }
    
    // 1. Création des variables 
    private static IntegerVariable[] createVariables(Model m) {
	IntegerVariable[] vars = new IntegerVariable[NB_REINES];
	for (int i = 0; i < NB_REINES; i++) {
	    vars[i] = Choco.makeIntVar("x" + i, 0, NB_REINES - 1);
	}
	return vars;
    }
    
    // 2. Création des contraintes 
    private static void postConstraints(Model m, IntegerVariable[] vars) {
	postConstraints1(m, vars);
	postConstraints2(m, vars);
    }
    
    // 2.1. Une reine par colonne
    private static void postConstraints1(Model m, IntegerVariable[] vars) {
	for(int i = 0; i < NB_REINES; i++) {
	    for(int j = i+1; j < NB_REINES; j++) {
		m.addConstraint( Choco.neq(vars[i], vars[j]) );
	    }
	}
    }
    
    // 2.2. Une reine par diagonale
    private static void postConstraints2(Model m, IntegerVariable[] vars) {
	for (int i = 0; i < NB_REINES; i++) {
	    for (int j = i + 1; j < NB_REINES; j++) {
		int k = j - i;
		m.addConstraint(Choco.neq(vars[i], Choco.plus(vars[j], k)));
		m.addConstraint(Choco.neq(vars[i], Choco.minus(vars[j], k)));
	    }
	}
    }
    
    // 3. Réglage de l'heuristique de choix de valeurs
    private static void setHeuristic(Solver s) {
	s.setValIntIterator(new DecreasingDomain());
    }
    
    // 5. Affichage des résultats
    private static void displayResult(Solver s, IntegerVariable[] vars) {
	if (s.getNbSolutions() > 0) {
	    System.out.println("Solution trouvŽe : ");
	    for (int i = 0; i < NB_REINES; i++) {
		int val = s.getVar(vars[i]).getVal();
		for (int j = 0; j < NB_REINES; j++) {
		    System.out.print(val == j ? "R " : ". ");
		}
		System.out.println("");
	    }
	} else {
	    System.out.println("Pas de solution trouvŽe !!");
	}
    }
}
    
    