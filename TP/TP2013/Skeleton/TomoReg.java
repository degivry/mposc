import choco.cp.model.CPModel;
import choco.kernel.model.constraints.automaton.DFA;
import choco.kernel.model.constraints.automaton.Transition;
import choco.kernel.model.constraints.Constraint;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.Solver;
import choco.Choco;

public class TomoReg 
{
    static int nbCol = 10;
    static int nbLig = 10;
    static int[][] colBlocs = new int[][]{
	new int[]{2, 3},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{2, 3},
	new int[]{3, 4},
	new int[]{8},
	new int[]{9},
    };
    
    static int[][] ligBlocs = new int[][]{
	new int[]{3, 6},
	new int[]{1, 4},
	new int[]{1, 1, 3},
	new int[]{2},
	new int[]{3, 3},
	new int[]{1, 4},
	new int[]{2, 5},
	new int[]{2, 5},
	new int[]{1, 1},
	new int[]{3},
    };
    
    public static void main(String[] args) 
    {
	Model m = xxx;; // declaration modele
	IntegerVariable[][] vars = xxx;//variables
	
	for (int i = 0; i < ligBlocs.length; i++) 
	    {
		// créer l'automate et poster un regular
		StringBuilder sb = new StringBuilder();
		sb.append("0*");
		for (int j = 0; j < blocs.length; j++) 
		    {
			// créer un bloc de 1
		    }
		// puis, autant de 0 que l'on veut
		sb.append("xxxx");
		
		// créer la ligne de variable
		// créer une regular et la poster sur la ligne
		m.addConstraint(xxx);
			
	    }
	
	for (int i = 0; i < colBlocs.length; i++) 
	    {
		//Idem sur les colonnes
		xxx;
	    }
	
	Solver s = new CPSolver();
	s.read(m);
	
	s.solve();
	s.printRuntimeStatistics();
	for (int i = 0; i < nbLig; i++) 
	    {
		for (int j = 0; j < nbCol; j++) 
		    {
			IntegerVariable v = vars[j][i];
			System.out.print(s.getVar(v).getVal() == 0 ? " " : "#");
		    }
		System.out.println();
	    }
    }
}
