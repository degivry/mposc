import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.Solver;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.Choco;

public class SendMoreMoney {
    
    public static void main(String[] args) {
	
	// Création du modèle
	Model m = new CPModel();
	
	// Création des variables avec leur domaine
	
	IntegerVariable S = Choco.makeIntVar("S", 0, 9);
	IntegerVariable E = Choco.makeIntVar("E", 0, 9);
	IntegerVariable N = Choco.makeIntVar("N", 0, 9);
	IntegerVariable D = Choco.makeIntVar("D", 0, 9);
	IntegerVariable M = Choco.makeIntVar("M", 0, 9);
	IntegerVariable O = Choco.makeIntVar("O", 0, 9);
	IntegerVariable R = Choco.makeIntVar("R", 0, 9);
	IntegerVariable Y = Choco.makeIntVar("Y", 0, 9);
	IntegerVariable[] letters = {S, E, N, D, M, O, R, Y};
	
	// Ajout des contraintes d'unicité de chaque lettre
	
	for (int i = 1; i <= 7; i++) {
	    for (int j = 0; j < i; j++) {
		m.addConstraint( Choco.neq(letters[i], letters[j]) );
	    }
	}
	
	// Ajout de la contrainte de vérification de la somme
	
	m.addConstraint( Choco.eq( Choco.scalar( letters, new int[]{-1000, -91, 90, -1, 9000, 900, -10, 1}), 0));
	
	// Création du solveur
	
	Solver s = new CPSolver();
	
	// Le solveur lit le modèle
	
	s.read(m);
	
	// On cherche l'énumération de toutes les solutions de ce problème
	
	s.solveAll();
	
	// On affiche les résultats
	
	System.out.println("nb solutions = " + s.getNbSolutions());
	
	System.out.println(s.pretty());
	
    }   
}