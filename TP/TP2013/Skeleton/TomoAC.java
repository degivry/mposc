import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.Solver;
import choco.kernel.solver.constraints.integer.extension.TuplesTest;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.Choco;

public class TomoAC {
    
    static int nbCol = 10;
    static int nbLig = 10;
    static int[][] colBlocs = new int[][]{
	new int[]{2, 3},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{1, 1, 1, 1},
	new int[]{1, 2},
	new int[]{2, 3},
	new int[]{3, 4},
	new int[]{8},
	new int[]{9},
    };
    
    static int[][] ligBlocs = new int[][]{
	new int[]{3, 6},
	new int[]{1, 4},
	new int[]{1, 1, 3},
	new int[]{2},
	new int[]{3, 3},
	new int[]{1, 4},
	new int[]{2, 5},
	new int[]{2, 5},
	new int[]{1, 1},
	new int[]{3},
    };
    
    public static void main(String[] args) 
    {
	Model m = new xxx(); //déclarer le modele
	IntegerVariable[][] vars = xxx; //les variables 0/1 en tableau
	
	for (int i = 0; i < ligBlocs.length; i++) 
	    {
		IntegerVariable[] ligne = new IntegerVariable[nbCol];
		for (int j = 0; j < ligne.length; j++)
		    {
			ligne[j] = vars[j][i];
		    }
		int[] blocs = ligBlocs[i];
		postConstraint(m, ligne, blocs);
	    }
	
	for (int i = 0; i < colBlocs.length; i++) 
	    {
		IntegerVariable[] colonne = new IntegerVariable[nbLig];
		for (int j = 0; j < colonne.length; j++) 
		    {
			colonne[j] = vars[i][j];
		    }
		int[] blocs = colBlocs[i];
		postConstraint(m, colonne, blocs);
	    }
	
	Solver s = new xxx;
	// lire le modle et lancer la recherche
	s.xxx;
	s.xxx();
	s.printRuntimeStatistics();

	for (int i = 0; i < nbLig; i++) 
	    { 
		for (int j = 0; j < nbCol; j++) 
		    {
			IntegerVariable v = xxx;
			System.out.print(s.xxx() == 0 ? " " : "#"); //aller chercher la valeur d'une variable dans le solveur
		    }
		System.out.println();
	    }
    }
    
    private static void postConstraint(Model m, 
				       IntegerVariable[] ligne, final int[] blocs) 
    {
	m.addConstraint(
			Choco.relationTupleAC(ligne, new TuplesTest() 
			    {
				public boolean checkTuple(int[] tuple) 
				{
				    // Retourne True/False si le tuple vérifie la contrainte (contient les bon blocs ou non)
				    xxx;
				    return true;
				    xxx;
				    return false;
			    })
			);
    }
}

