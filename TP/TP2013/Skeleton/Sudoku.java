import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.cp.solver.search.integer.valiterator.*;
import choco.cp.solver.search.integer.varselector.*;
import choco.cp.solver.search.integer.valselector.*;
import choco.kernel.model.Model;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.solver.Solver;
import choco.Choco;
import java.lang.Math;

public class Sudoku {
    
    public static int n = 9;
    
    public static void main(String[] args) {
	System.out.println("Sudoku");
	// declarer le modèle
	Model m = xxx;
	// créer les variables
	xxx vars = createVariables(m);
	//poser les contraintes
	postConstraints(m, vars);
	//créer un "solver"
	xxx s = new xxx();
	// lire le modele
	s.xxx;

	// changer d'heuristqiue (éventuellement)
	setHeuristic(s);

	// résoudre
	s.xxx();
	//afficher
	displayResult(s, vars);
    }
    
    // 1. Création des variables
    private static IntegerVariable[][] createVariables(Model m) {
	return xxx;
    }
    
    // 2. Création des contraintes sur les lignes, colonnes, carrés
    private static void postConstraints(Model m, IntegerVariable[][] vars) {
	for(int i = 0; i < n; i++) {
	    IntegerVariable[] lignes = xxx;
	    IntegerVariable[] colonnes = xxx;
	    IntegerVariable[] carres = xxx;
	    for(int j = 0; j < n; j++) {
		lignes[j] = vars[i][j];
		colonnes[j] = vars[j][i];
		int sqrtn=(int) Math.sqrt(n);
		carres[j] = vars[j%sqrtn + (i % sqrtn) * sqrtn][j/sqrtn + (i / sqrtn) * sqrtn];
	    }
	    //poser les AllDiffs
	    postAlldiff(m, lignes);
	    postAlldiff(m, colonnes);
	    postAlldiff(m, carres);
}
}
    
    // 3. Création d'une contrainte Alldiff
    private static void postAlldiff(Model m, IntegerVariable[] vars) {
	for(int i = 0; i < vars.length; i++) {
	    for(int j = i+1; j < vars.length; j++) {
		xxx; // pose de différences binaires
	    }
	}
    }
    
    // 4. Réglage des heuristiques de choix de variables et de valeurs
    private static void setHeuristic(Solver s) {
	s.xxx; // variables
	s.xxx; //valeurs
    }
    
    // 5. Affichages des résultats
    private static void displayResult(Solver s, IntegerVariable[][] vars) {
	int sqrtn=(int) Math.sqrt(n);
	for(int i = 0; i < n; i++) {
	    if (i%sqrtn == 0) {
		String str="";
		for(int k=0;k<n+(2*(sqrtn+1));k++) str=str.concat("-");
		System.out.println(str);
	    }
	    for(int j = 0; j < n; j++) {
		if (j%sqrtn == 0) System.out.print("||");
		int val=s.getVar(vars[i][j]).getVal();
		if(val>=10){
		    char c= (char) ('A'+val-10);
		    System.out.print(c);
		}
		else System.out.print(val);
	    }
	    System.out.println("||");
	}
	String str="";
	for(int k=0;k<n+(2*(sqrtn+1));k++) str=str.concat("-");
	System.out.println(str);
    }
}
