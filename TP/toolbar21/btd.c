/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File:solver.c
  $Id: btd.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tbsystem.h"
#include "wcsp.h"
#include "solver.h"
#include "tbelim.h"
#include "tbsingleton.h"
#include "treedec.h"
#include "btd.h"
#include "tbgoods.h"

#define BTDVERBOSE 2
/*  #define CHECKLB 1 */

/* TO BE DONE : 
 * BUG: cluster merge with connected components (cf SPOT5)
 * SolvingTime ne prends pas en compte temps pour la decomposition!
 * verification validite tree decomposition en precalcul et non durant execution (via assert)
 * choix du cluster root: exploiter la connaissance d'une solution initiale (certificat)
 * integrer les variables du separateur dans precalcul des minorants initiaux
 * eventuellement, deconnecter sous-arbre si minorant initial non nul
 * BTD-RDS
 * utilisation de lower bounds initiales par cluster (eventuellement aggregees, dynamiqut avec Tries)
 * inference de goods par substitutability
 * memorise et exploite upper bounds
 * dynamic cluster selection (incompatible avec -H6?)
 * deplacer appel useGood avant lookahead et projectB: attention lors reconnexion a refaire projectB!
 * tbelim.c: num_future modifie mais pas restaure?
 * profondeur maxi de sauvegarde du contexte: hauteur de l'arbre de decompositon
 */

/* Imp. note: every list contains in the first position its size then all its elements */
int NbCluster = 0;  /* total number of clusters */
int Root = 0; /* root cluster in the tree decomposition */
int *Father = NULL; /* for each cluster, the index of its cluster father */
/* root cluster has its father equals to -1 */
int **Sons = NULL; /* list of cluster children for each cluster */
int *Var2Cluster = NULL; /* for each variable, which cluster it belongs to */
int **Cluster2Vars = NULL; /* list of variables inside a given cluster (each variable is associated to a unique cluster) */
int **ClusterTree2Vars = NULL; /* list of variables in the subtree rooted at a given cluster, except the variables which belong to the separator with its father */
int **Separator = NULL; /* list of separator variables for each cluster (relative to its father) */
int *SeparatorArity = NULL; /* number of unassigned variables in the separator between a given cluster and its father */
sepstatus_t *SeparatorStatus = NULL; /* separator state computed only once when it is completly assigned */
int **Var2Separators = NULL; /* for each variable, the list of separator clusters where it appears */
int *IsAncestor = NULL; /* IsAncestor[a*NbCluster+c]=TRUE if cluster a is an ancestor of cluster c */
/* set to zero to be compatible with initial propagation in solver.c */
int CurrentCluster = 0;  /* current cluster solved by BTD */
cost_t *ClusterBottom = NULL; /* split Bottom into clusters */ 
cost_t TopCurrentClusterTree; /* specific upper bound for current cluster subtree */
cost_t BottomCurrentClusterTree; /* specific lower bound for current cluster subtree */ 
cost_t *UnaryConstraintsBTD; /* save cost moves between clusters */
int **SaveSeparatorArity;
List *LSeparators; /* list of disconnected separators */

//-----------------------------------------------------------------------------------
// Cluster Selection for next son: use var. heur.
//-----------------------------------------------------------------------------------

typedef int (*func2i_t)(int);

// This version would be consistent with the general intent but I put
// a more naive version below that behaves as no heuristics (without choice)
int selec_clustSTATIC(int from)
{
 int idx_min=-1,i,j,var,min=INT_MAX;

 for (i=from; i <= Sons[CurrentCluster][0]; i++)
   for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
     var = Cluster2Vars[Sons[CurrentCluster][i]][j];
     if (order3[var] < min) {
	min = order3[var];
	idx_min=i;
     }
   }
 return idx_min;
}

/*  int selec_clustSTATIC(int from) */
/*  { */
/*    return from; */
/*  } */


int selec_clustRND(int from)
{
  return from+(rand() % (Sons[CurrentCluster][0]-from+1));
}

int selec_clustDOM(int from)
{
  int idx_min=-1,i,j,var,min=INT_MAX;

  for (i=from; i <= Sons[CurrentCluster][0]; i++)
    for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
      var = Cluster2Vars[Sons[CurrentCluster][i]][j];
      if (List_n_of_elem(&feasible_values[var]) < min) {
	min = List_n_of_elem(&feasible_values[var]);
	idx_min=i;
      }
    }
  return idx_min;
}

int selec_clustDOMDG(int from)
{
  int var,idx_min=-1,i,j=0;
  double min = 1e100;
  
  for (i=from; i <= Sons[CurrentCluster][0]; i++)
    for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
      var = Cluster2Vars[Sons[CurrentCluster][i]][j];
      //      printf("Trying var %d from cluster %d, %d'th son of cluster %d\n",
      //	     var,Sons[CurrentCluster][i],i,CurrentCluster);
      if ((double)List_n_of_elem(&feasible_values[var])/(double)(degree[var]+1) < min) {
	//	printf("I like it\n");
	min = (double)List_n_of_elem(&feasible_values[var])/(double)(degree[var]+1);
	idx_min=i;
      }
    }
  return idx_min;
}

int selec_clustJEROW(int from)
{
  int idx_min=-1,i,j=0,k,l,m,b=0,ties=1,dice,var1,var2;
  double w,min=1e100;

  for (i=from; i <= Sons[CurrentCluster][0]; i++)
    for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
      var1 = Cluster2Vars[Sons[CurrentCluster][i]][j];
      H[var1]=1.0;
    }


  for (i=from; i <= Sons[CurrentCluster][0]; i++)
    for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
      var1 = Cluster2Vars[Sons[CurrentCluster][i]][j];
      w=0.0;
      k=0;
      List_reset_traversal(&feasible_values[var1]);
      while(!List_end_traversal(&feasible_values[var1]))	{ 
	l=List_consult1(&feasible_values[var1]); 
	List_next(&feasible_values[var1]);
	k++;
	w+=CostU(var1,l);
      }
      if(w!=0)b++;
      H[var1] += (w/(double)k);
      
      for(k=j+1; k <= Cluster2Vars[Sons[CurrentCluster][i]][0]; k++) {
	var2 = Cluster2Vars[Sons[CurrentCluster][i]][k];
	if (ISBINARY(var1,var2)) {
	  b++;
	  w=0.0;
	  k=0;
	  List_reset_traversal(&feasible_values[var1]);
	  while(!List_end_traversal(&feasible_values[var1])) { 
	    l=List_consult1(&feasible_values[var1]); 
	    List_next(&feasible_values[var1]);
	    List_reset_traversal(&feasible_values[var2]);
	    while(!List_end_traversal(&feasible_values[var2])) { 
	      m=List_consult1(&feasible_values[var2]); 
	      List_next(&feasible_values[var2]);
	      k++;
	      w+=CostB(i,l,j,m);
	    }
	  }
	  H[var1]+=(w/(double)k);
	  H[var2]+=(w/(double)k);
	}
      }
    }
  // Many useless operations !!!
  if(b==0) for(i=0;i<NConstr;i++) if(currentArity[i]>2) 
    for(k=0;k<ARITY(i);k++) {
      j=SCOPEONE(i,k);
      H[j]+=WEIGHTCONSTRAINT(i);
    }
  
for (i=from; i <= Sons[CurrentCluster][0]; i++)
    for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
      var1 = Cluster2Vars[Sons[CurrentCluster][i]][j];
      w=((double)List_n_of_elem(&feasible_values[var1]))/H[var1];
      if(w==min) ties++;
      if(w<min){min=w; idx_min=i; ties=1;}
    }
 
  if(HeurVar == VAR_JEROSLOW_LIKE || ties==1) {
    return idx_min;
  }
  else/* HeurVar == VAR_JEROSLOW_LIKE2 */
    {
      dice=rand()%ties;
      for (i=from; i <= Sons[CurrentCluster][0]; i++)
	for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
	  var1 = Cluster2Vars[Sons[CurrentCluster][i]][j];
	  if(((double)List_n_of_elem(&feasible_values[var1]))/H[var1]==min) {
	    if(dice==0) return i;
	    else dice--;
	  }
	}
      return -1;
    }
}

int selec_clustSINGLETON(int from)
{
  int idx_max=-1,var,i,j=0;
  double max=0.0;
  
  for (i=from; i <= Sons[CurrentCluster][0]; i++)
    for (j=1; j<= Cluster2Vars[Sons[CurrentCluster][i]][0]; j++) {
      var = Cluster2Vars[Sons[CurrentCluster][i]][j];
      if (H[var] > max) {
	max=H[var];
	idx_max=i;
      }
    }
  return idx_max;
}

func2i_t HeurClustFunc[] = {
  selec_clustSTATIC, 
  selec_clustSTATIC,
  selec_clustSTATIC,
  selec_clustRND,
  selec_clustDOM,
  selec_clustDOMDG,
  selec_clustJEROW,
  selec_clustJEROW,
  selec_clustSINGLETON
};

/* update IsAncestor data recursively */
void setAncestors(int a, int c)
{
  ISANCESTOR(a,c) = TRUE;
  if (Father[a] != -1) {
    setAncestors(Father[a],c);
  }
}

/* recursive initialization of BTD data structures */
void explore(int c, int father) 
{
  int i, size, index, son, sepsize, sepindex;

  Father[c] = father;
  setAncestors(c,c);
  /* count the number of variables in cluster c not yet in an ancestor cluster of c */
  /* and count the number of variables in cluster c and in its father */
  size=0;
  sepsize=0;
  for (i= 1; i <= Junction.cliques[c][0]; i++) {
    if (Var2Cluster[Ordering[Junction.cliques[c][i]]]==-1) {
      size++;
    } else {
      sepsize++;
    }
  }
  assert(size + sepsize <= Junction.cliques[c][0]);
  /* create links between cluster c and its variables */
  /* and separator between cluster c and its father */
  index=1;
  Cluster2Vars[c] = (int *) mycalloc(size+1, sizeof(int));
  Cluster2Vars[c][0] = size;
  sepindex=1;
  Separator[c] = (int *) mycalloc(sepsize+1, sizeof(int));
  Separator[c][0] = sepsize;
  SeparatorArity[c] = sepsize;
  SeparatorStatus[c].connected = TRUE;
  SeparatorStatus[c].optimum = FALSE;
  SeparatorStatus[c].lb = BOTTOM;
  SeparatorStatus[c].initlb = BOTTOM;
  SeparatorStatus[c].unarymove = BOTTOM;
  SeparatorStatus[c].propa = BOTTOM;
  SeparatorStatus[c].bottomove = BOTTOM;
  SeparatorStatus[c].depth = 0;
  for (i= 1; i <= Junction.cliques[c][0]; i++) {
    if (Var2Cluster[Ordering[Junction.cliques[c][i]]]==-1) {
      Var2Cluster[Ordering[Junction.cliques[c][i]]] = c;
      Cluster2Vars[c][index] = Ordering[Junction.cliques[c][i]];
      index++;
    } else {
      Separator[c][sepindex] = Ordering[Junction.cliques[c][i]];
      sepindex++;
    }
  }
  /* count the number of cluster sons of c */
  size=0;
  for (son = 0; son < Junction.nCliques ; son++) {
    if (Junction.edges[c][son]!=0 && son != c && son != father) {
      size++;
    }
  }
  Sons[c] = (int *) mycalloc(size+1, sizeof(int));
  Sons[c][0] = size;
  /* explore all cluster sons of c */
  index=1;
  for (son = 0; son < Junction.nCliques ; son++) {
    if (Junction.edges[c][son]!=0 && son != c && son != father) {
      Sons[c][index] = son;
      index++;
      explore(son, c);
    }
  }
}

/* return TRUE if cluster a is an ancestor of cluster c */
int isAncestor(int a, int c)
{
  if (c < 0) {
    return FALSE;
  } else if (c==a) {
    return TRUE;
  } else {
    return isAncestor(a, Father[c]);
  }
}

/* return the distance in number of clusters bewteen c and its ancestor a */
int cdist(int a, int c)
{
  if (a==c) return 0;
  else if (Father[c] != -1) return 1 + cdist(a, Father[c]);
  else return NbCluster;
}

/* recursive dump of tree decomposition */
void printTreeDec(int c, int indent)
{
  int i,son;
 
  for (i=0; i<indent; i++) {
    printf(" ");
  }
  printf("C%d: father = %d, sons =", c, Father[c]);
  for (son=1; son<=Sons[c][0]; son++) {
    printf(" %d", Sons[c][son]);
  }
  printf(", separator =");
  assert(c==Root || Father[c]==Root || Separator[c][0]>=1);
  for (i=1; i<=Separator[c][0]; i++) {
    printf(" %d", Separator[c][i]);
  }
  printf(", vars =");
  for (i=1; i<=Cluster2Vars[c][0]; i++) {
    printf(" %d", Cluster2Vars[c][i]);
  }
  printf("\n");
  for (son=1; son<=Sons[c][0]; son++) {
    printTreeDec(Sons[c][son], indent+3);
  }
}

int subtreeSize(int cluster)
{
  int son, res = Cluster2Vars[cluster][0];
  for (son=1; son <= Sons[cluster][0]; son++) {
    res += subtreeSize(Sons[cluster][son]);
  }
  return res;
}

void subtreeVars(int subtreeroot, int cluster)
{
  int i, son;
  for (i = 1; i <= Cluster2Vars[cluster][0]; i++) {
    ClusterTree2Vars[subtreeroot][++(ClusterTree2Vars[subtreeroot][0])] = Cluster2Vars[cluster][i];
  }
  for (son=1; son <= Sons[cluster][0]; son++) {
    subtreeVars(subtreeroot, Sons[cluster][son]);
  }
}

/* main initialization function */
void initTD()
{
  int i,c,sizes[NVar];

  if (Ordering==NULL) {
    printf("You must choose a tree decomposition method (option -T[Number])!\n");
    exit(0);
  }
  /* create links between variables and clusters */
  Var2Cluster = (int *) mymalloc(NVar, sizeof(int));
  for (i = 0 ; i < NVar ; i++) {
    Var2Cluster[i] = -1;
  }
  NbCluster = Junction.nCliques;
  Root = Junction.root;
  Father = (int *) mymalloc(NbCluster, sizeof(int));
  for (c = 0 ; c < NbCluster ; c++) {
    Father[c] = -1;
  }
  Sons = (int **) mycalloc(NbCluster, sizeof(int *));
  Cluster2Vars = (int **) mycalloc(NbCluster, sizeof(int *));
  ClusterTree2Vars = (int **) mycalloc(NbCluster, sizeof(int *));
  Separator = (int **) mycalloc(NbCluster, sizeof(int *));
  SeparatorArity = (int *) mycalloc(NbCluster, sizeof(int));
  SeparatorStatus = (sepstatus_t *) mycalloc(NbCluster, sizeof(sepstatus_t));
  Var2Separators = (int **) mycalloc(NVar, sizeof(int *));
  IsAncestor = (int *) mycalloc(NbCluster*NbCluster, sizeof(int));
  explore(Root,-1);

  LSeparators=mymalloc(NVar+1,sizeof(List));
  SaveSeparatorArity = (int **) mycalloc(NVar+1, sizeof(int *));
  for (i = 0 ; i < NVar+1; i++) {
    SaveSeparatorArity[i] = (int *) mycalloc(NbCluster,sizeof(int));
    sizes[i] = 0;
  }
  for (c = 0; c < NbCluster; c++) {
    for (i = 1 ; i <= Separator[c][0] ; i++) {
      sizes[Separator[c][i]]++;
    }
  }
  for (i = 0 ; i < NVar; i++) {
    Var2Separators[i] = (int *) mycalloc(sizes[i] + 1, sizeof(int));
    Var2Separators[i][0] = 0;
  }
  for (c = 0; c < NbCluster; c++) {
    for (i = 1 ; i <= Separator[c][0] ; i++) {
      Var2Separators[Separator[c][i]][++(Var2Separators[Separator[c][i]][0])] = c;
    }
  }
  for (c = 0; c < NbCluster; c++) {
    ClusterTree2Vars[c] = (int *) mycalloc(subtreeSize(c) + 1, sizeof(int));
  }
  for (c = 0; c < NbCluster; c++) {
    subtreeVars(c, c);
  }
  
  /* used by BTD for splitting Bottom into cluster */
  ClusterBottom = (cost_t *) mymalloc(NbCluster, sizeof(cost_t));
  for (i = 0 ; i < NbCluster ; i++) {
    ClusterBottom[i] = BOTTOM;
  }
  UnaryConstraintsBTD = (cost_t *) mycalloc(NbCluster*NVar*NValues,sizeof(cost_t));
  TopCurrentClusterTree = MAXCOST;
  BottomCurrentClusterTree = BOTTOM;
  CurrentCluster = Root;

  if (Verbose) {
    printTreeDec(Root, 0);
  }
  if (Verbose >= BTDVERBOSE) {

    for (i = 0 ; i < NVar ; i++) {
      printf("%d of C%d:", i, Var2Cluster[i]);
      for (c = 1 ; c <= Var2Separators[i][0]; c++) {
	printf(" C%d", Var2Separators[i][c]);
      }
      printf("\n");
    }
    for (c = 0 ; c < NbCluster ; c++) {
      printf("separator of C%d with father C%d : size = %d arity = %d status = %d vars_in_subtree =\n", c, Father[c], Separator[c][0], SeparatorArity[c], SeparatorStatus[c].connected);
      for (i = 1 ; i <= ClusterTree2Vars[c][0] ; i++) { 
	printf(" %d", ClusterTree2Vars[c][i]);
      }
      printf("\n");
    }
    for (i = 0 ; i < NVar ; i++) {
      printf("%d in C%d , and:", i, Var2Cluster[i]);
      for (c = 0 ; c < NbCluster ; c++) {
	assert(isAncestor(Var2Cluster[i], c) == ISANCESTOR(Var2Cluster[i], c));
	if (isAncestor(Var2Cluster[i], c)) {
	  printf(" %d", c);
	}
      }
      printf("\n");
    }
    for (i=0; i<NbCluster; i++) {
      for (c=0; c<NbCluster; c++) {
	assert(ISANCESTOR(i,c) == isAncestor(i,c));
      }
    }
  }
}

#ifndef NOBACKTRACKBOTTOMUPDATE
extern cost_t *l_b2; /* Bottom backtrack data structure */
extern cost_t *l_b_clustertree; /* BottomCurrentClusterTree backtrack data structure */
extern cost_t **l_b_cluster; /* ClusterBottom backtrack data structure */

cost_t subtreeBottomDepth(int cluster, int depth)
{
  int i;
  cost_t res = l_b_cluster[depth][cluster];
  for (i=1;i<=Sons[cluster][0];i++) {
    res += subtreeBottomDepth(Sons[cluster][i], depth);
  }
  return res;
}

void updateBacktrack(int separator, int connected, cost_t newlb, cost_t prevlb, int depth1, int depth2)
{
  int d;
  cost_t propa;

  if (!connected && newlb <= prevlb) return;

  for (d=depth1; d<=depth2; d++) {
    if (connected) {
      propa = subtreeBottomDepth(separator, d);
      if (newlb > propa) {
	l_b2[d] += newlb - propa;
	l_b_clustertree[d] += newlb - propa;
      }
    } else {
      l_b2[d] += newlb - prevlb;
      l_b_clustertree[d] += newlb - prevlb;
    }
  }
}
#endif

cost_t subtreeBottom(int cluster)
{
  int i;
  cost_t res = ClusterBottom[cluster];
  for (i=1;i<=Sons[cluster][0];i++) {
    res += subtreeBottom(Sons[cluster][i]);
  }
  return res;
}

/* called by local consistency functions (w_ac.c) */
void UNARYCOSTMOVE(int i, int a, cost_t cost, int j, int c)
{
  if (SearchMethod != DFBB && depth!=0 && !BTDSAMECLUSTER(i,j) && BTDSAMECLUSTERTREE(i,j)) {
    List_insert3pc(&l3BTD[depth],i,j,a,cost);
    for (c=1;c<=Var2Separators[i][0]; c++) {
      if (ISANCESTOR(Var2Separators[i][c],Var2Cluster[j])) {
	UNARYCOSTBTD(Var2Separators[i][c],i,a)+=cost;
      } 
    }
  }
}

/* collect cost moves out/in the cluster */
cost_t unaryCostBTD(int cluster)
{
  int i;
  cost_t res = BOTTOM;

  for (i=1; i<=Separator[cluster][0]; i++) {
    assert(assignments[Separator[cluster][i]] != -1);
    res += UNARYCOSTBTD(cluster, Separator[cluster][i], assignments[Separator[cluster][i]]);
  }
  // assert(res < Top); // is it true?
#ifdef NOCOSTMOVE
  assert(res == BOTTOM);
#endif
  return res;
}

#ifndef NOPROACTIVEGOOD
void separatorDisconnect(int cluster, cost_t lb, int optimum, cost_t unarymove, int sepdepth, cost_t bottomove)
{
  int i;

  if (Verbose >= BTDVERBOSE) {
    printf("[%d] separatorDisconnect(%d ", depth, cluster);
    PRINTCOST(lb);
    printf(" %d ", optimum);
    PRINTCOST(unarymove);
    printf(" ");
    PRINTCOST(bottomove);
    printf(" %d)\n", sepdepth);
  }

  assert(SeparatorStatus[cluster].connected == TRUE);

  SeparatorStatus[cluster].connected = FALSE;
  SeparatorStatus[cluster].lb = lb;
  SeparatorStatus[cluster].optimum = optimum;
  SeparatorStatus[cluster].unarymove = unarymove;
  SeparatorStatus[cluster].bottomove = bottomove;
  if (sepdepth>=1) SeparatorStatus[cluster].depth = sepdepth;
  for (i=1; i<=ClusterTree2Vars[cluster][0]; i++) {
    if (assignments[ClusterTree2Vars[cluster][i]] == -1) {
      assignments[ClusterTree2Vars[cluster][i]] = DOMSIZE(ClusterTree2Vars[cluster][i]);
      num_future--;
    } else {
      assert(assignments[ClusterTree2Vars[cluster][i]] == DOMSIZE(ClusterTree2Vars[cluster][i]));
    }
  }

  assert(sepdepth <= depth);
  if (sepdepth >= 1) {
#ifndef NDEBUG
    List_reset_traversal(&LSeparators[sepdepth]);
    while(!List_end_traversal(&LSeparators[sepdepth]))
      {
	assert(List_consult1(&LSeparators[sepdepth]) != cluster);
	List_next(&LSeparators[sepdepth]);
      }
#endif
    List_insert1(&LSeparators[sepdepth], cluster);
  }
}

void separatorKeepConnected(int cluster, cost_t lb, int optimum, cost_t unarymove, int sepdepth, cost_t bottomove)
{
  int i;

  if (Verbose >= BTDVERBOSE) {
    printf("[%d] separatorKeepConnected(%d ", depth, cluster);
    PRINTCOST(lb);
    printf(" %d ", optimum);
    PRINTCOST(unarymove);
    printf(" ");
    PRINTCOST(bottomove);
    printf(" %d)\n", sepdepth);
  }

  assert(SeparatorStatus[cluster].connected == TRUE);

  SeparatorStatus[cluster].connected = TRUE;
  SeparatorStatus[cluster].lb = lb;
  SeparatorStatus[cluster].optimum = optimum;
  SeparatorStatus[cluster].unarymove = unarymove;
  SeparatorStatus[cluster].bottomove = bottomove;
  SeparatorStatus[cluster].depth = sepdepth;
  if (sepdepth>=1) SeparatorStatus[cluster].depth = sepdepth;

#ifndef NDEBUG
  for (i=1; i<=ClusterTree2Vars[cluster][0]; i++) {
    assert(assignments[ClusterTree2Vars[cluster][i]] == -1);
  }
#endif
}

void separatorReconnect(int cluster)
{
  int i;

  if (Verbose >= BTDVERBOSE) {
    printf("[%d] separatorReconnect(%d ", depth, cluster);
    PRINTCOST(SeparatorStatus[cluster].lb);
    printf(" %d ", SeparatorStatus[cluster].optimum);
    PRINTCOST(SeparatorStatus[cluster].unarymove);
    printf(" ");
    PRINTCOST(SeparatorStatus[cluster].bottomove);
    printf(" %d)\n", SeparatorStatus[cluster].depth);
  }

  SeparatorStatus[cluster].connected = TRUE;
  for (i=1; i<=ClusterTree2Vars[cluster][0]; i++) {
    if (assignments[ClusterTree2Vars[cluster][i]] != -1) {
      assert(assignments[ClusterTree2Vars[cluster][i]] == DOMSIZE(ClusterTree2Vars[cluster][i]));      
      assignments[ClusterTree2Vars[cluster][i]] = -1;
      num_future++;
    }
  }
}

void reconnectSeparators()
{
  List_reset_traversal(&LSeparators[depth]);
  while(!List_end_traversal(&LSeparators[depth]))
    {
      separatorReconnect(List_consult1(&LSeparators[depth]));
      List_next(&LSeparators[depth]);
    }
}

int useGoods(int i)
{
  int optimum = FALSE;
  cost_t lb = BOTTOM;
  cost_t unarymove = BOTTOM;
  cost_t propa = BOTTOM;
  int stop = FALSE;
  int c,separator;

  for (c=1;!stop && c<=Var2Separators[i][0]; c++) {
    separator = Var2Separators[i][c];
    assert(SeparatorStatus[separator].connected == TRUE);
    SeparatorArity[separator]--;
    assert(SeparatorArity[separator] >= 0);
    if (SeparatorArity[separator] == 0) {
      /* check if tree decomposition structure is correct */
      //      printf("%d %d %d\n", separator, Father[separator],Var2Cluster[i]);
      assert(Father[separator] == Var2Cluster[i]);
      lb = getGood(separator, assignments, &optimum);
      lb = MAX(lb, SeparatorStatus[separator].initlb);
      unarymove = unaryCostBTD(separator);
      assert(optimum==FALSE || lb >= unarymove);
      if (lb < Top) lb = MAX(lb - unarymove, BOTTOM);
      propa = subtreeBottom(separator);
      assert(propa >= ClusterBottom[separator]);
      assert(optimum==FALSE || lb >= propa);
      if (lb > propa) {
	if (BottomCurrentClusterTree + lb - propa >= TopCurrentClusterTree) {
	  BottomCurrentClusterTree += lb - propa;
	  return FALSE;
	}
	BottomCurrentClusterTree += lb - propa;
#ifndef NOGLOBALCUT
	if (Bottom + lb - propa >= Top) {
	  Bottom = Top;
	  return FALSE;
	}
#endif
	separatorDisconnect(separator, lb, optimum, unarymove, depth, lb - propa);
	Bottom += lb - propa;
	pruneVars(&stop); /* applies NC propagation */
      } else if (optimum) {
	separatorDisconnect(separator, lb, optimum, unarymove, depth, BOTTOM);
      }	else {
	separatorKeepConnected(separator, lb, optimum, unarymove, depth, BOTTOM);
      }
    }
  }
  return !stop;
}
#endif

/* look ahead with forward checking (independent from the rest of toolbar) */
cost_t btdLookAhead(int var, int val)
{
  int j,val2;
  cost_t temp;
  cost_t lb;
  cost_t min;
  int val_min;

  lb = BOTTOM;
  for(j=0;j<NVar;j++) if(assignments[j]==-1 && ISBINARY(var,j)) {
    List_reset_traversal(&feasible_values[j]);
    while(!List_end_traversal(&feasible_values[j])) {
      val2=List_consult1(&feasible_values[j]);
      List_next(&feasible_values[j]);
      temp=CostB(var,val,j,val2);
      if(temp>BOTTOM) {
	UNARYCOST(j,val2)+=temp; /* propagates assignment */
	List_insert3pc(&l2[depth],j,val2,0,temp); /* records the change for context restoration */
      }
    }
    if(current_domains[j][SupU[j]]!=-1 || CostU(j,SupU[j])!=BOTTOM) {
      min=MAXCOST; 
      val_min=-1;
      List_reset_traversal(&feasible_values[j]);
      while(!List_end_traversal(&feasible_values[j]) && min!=BOTTOM) {
	val2=List_consult1(&feasible_values[j]);
	List_next(&feasible_values[j]);
	if (CostU(j,val2) < min) {
	  min=CostU(j,val2); 
	  val_min=val2;
	}
      } 
      assert(val_min != -1);
      if(min>BOTTOM) {
	lb+=min;
	DeltaU[j]+=min;
	ClusterBottom[Var2Cluster[j]]+=min;
      }
      SupU[j]=val_min;
    }
  }
  return lb;
}


/* recursive FC-BTD exploiting goods and forward checking only */
/* cluster : current cluster to be solved */
/* lb : lower bound for cluster  */
/* fc : lower bound for subtree rooted at cluster  */
/* ub : upper bound for subtree rooted at cluster  */
/* returns the optimum for subtree rooted at cluster */
cost_t fcbtd_rec(int cluster, cost_t lb, cost_t fc, cost_t ub)
{
  int i; /* current variable */
  int l; /* current value */
  int j;
  int son, separator;
  cost_t good;
  cost_t res;
  cost_t lc;
#ifdef FCBTDPLUS
  cost_t lub;
#endif

  if (Verbose>=BTDVERBOSE) {
    printf("[%d] fcbtd(C%d,", depth, cluster);
    PRINTCOST(lb);
    printf(",");
    PRINTCOST(fc);
    printf(",");
    PRINTCOST(ub);
    printf(")\n");
  }

  assert(lb == ClusterBottom[cluster]);
  assert(fc == subtreeBottom(cluster));
  assert(ub <= Top);
  CurrentCluster = cluster; /* global variable used by variable heuristics */
  i = (*HeurVarFunc[HeurVar])(); /* i becomes the new current variable */
  if (i<0) {
    /* cluster variables are all assigned */
    if (Sons[cluster][0] == 0) {
      /* cluster has no children */
      return lb;
    } else {
      /* compute the sum of optimum of every children */
      for (son=1; son <= Sons[cluster][0] && lb < ub; son++) {
	separator = Sons[cluster][son];
	SeparatorStatus[separator].lb = getGood(separator, assignments, &SeparatorStatus[separator].optimum);
#ifdef FCBTDPLUS
	lb += MAX(subtreeBottom(separator),SeparatorStatus[separator].lb);
#else
	lb += SeparatorStatus[separator].lb;
#endif
      }
      /* examine every child */
      son = 1;
      while (son <= Sons[cluster][0] && lb < ub) {
	separator = Sons[cluster][son];
	if (SeparatorStatus[separator].optimum == FALSE) {
#ifdef FCBTDPLUS
	  lub = ub - lb + MAX(subtreeBottom(separator),SeparatorStatus[separator].lb);
	  good = fcbtd_rec(separator, ClusterBottom[separator], subtreeBottom(separator), lub);
	  assert(good >= MAX(subtreeBottom(separator),SeparatorStatus[separator].lb));
	  lb += good - MAX(subtreeBottom(separator),SeparatorStatus[separator].lb);
	  setGood(separator, assignments, good, good < lub);
#else
	  assert(SeparatorStatus[separator].lb == BOTTOM);
	  good = fcbtd_rec(separator, ClusterBottom[separator], subtreeBottom(separator), Top);
	  lb += good;
	  setGood(separator, assignments, good, TRUE);
#endif
	}
	son++;
      }
      return lb;
    }
  } else {
    /* classic depth first branch and bound on the current cluster for variable i */
    assert(Var2Cluster[i] == cluster);

    num_future--;       /* one more variable is assigned */
    depth++;
    assignments[i]=1000;
    sort_fut_var();
    assignments[i]=-1;
    update_degree(i);
    save_parameters1();
    for(j=0;j<NConstr;j++)
      {fut_var_r2[depth][j]=currentArity[j]; if(inScope(i,j))currentArity[j]--;}
    List_create(&feasible_values2[i]);
    (*HeurValFunc[HeurVal])(i);
    List_reset_traversal(&feasible_values2[i]);

    /*at least one entrance in the loop */
    assert(fc < ub);
    assert(!List_end_traversal(&feasible_values2[i]));

    /* enumerate all domain values */
    while(fc < ub && !List_end_traversal(&feasible_values2[i])) {
      l=List_consult1(&feasible_values2[i]); /* l is the new current value*/
      List_next(&feasible_values2[i]);
      if (Verbose>=2) {
	if (Verbose>=3) {
	  showCosts();
	}
	printf("[C%d,", cluster);
	PRINTCOST(lb);
	printf(",");
	PRINTCOST(fc);
	printf(",");
	PRINTCOST(ub);
	printf("]");
	printf("[%d,", depth);
	PRINTCOST(Bottom);
	printf(",");
	PRINTCOST(Top);
	printf(",%d]", numberOfValues());
	printf(" x%d = %d\n", i, l);
      }
      assignments[i]=l;                /* l is assigned to i */
      if (NNodes >= NodeLimit) nodeOut(); 
      NNodes++;
      List_create(&l1[depth]); List_create(&l2[depth]); 
      List_create(&l3[depth]); List_create(&l3BTD[depth]); 
      List_create(&l4[depth]);
      List_create(&l5[depth]); List_create(&l6[depth]); 
      List_create(&LSeparators[depth]);

      ClusterBottom[cluster] += CostU(i,l);
      lc = fc + CostU(i,l);
      if (lc < ub) {
	lc += btdLookAhead(i, l);
	if (lc < ub) {
	  res = fcbtd_rec(cluster, ClusterBottom[cluster], lc, ub);
	  if (res < ub) {
	    ub = res;
	  }
	  if (cluster == Root && res < Top) {
	    /* new optimum has been found */
	    Top = res;
	    if (Verbose) {
	      printf("New Top: "); PRINTCOST(Top); printf(" (%lu nodes, %.2lf secs)\n",NNodes,cpuTime() - SolvingTime);
	    }
	  }
	}
      }

      restore_context();
      restore_parameters1();
      List_dispose_memory(&l1[depth]); List_dispose_memory(&l2[depth]);
      List_dispose_memory(&l3[depth]); List_dispose_memory(&l3BTD[depth]); 
      List_dispose_memory(&l4[depth]);
      List_dispose_memory(&l5[depth]); List_dispose_memory(&l6[depth]); 
      List_dispose_memory(&LSeparators[depth]);
    }
    assignments[i]=-1; /* unassigns current variable */
    for(j=0;j<NConstr;j++){currentArity[j]=fut_var_r2[depth][j];}
    List_dispose_memory(&feasible_values2[i]);
    num_future++; /* current variable becomes future */
    depth--;

    return ub;
  }
}

/* main function for FC-BTD exploiting goods and forward-checking */
void fcbtd()
{
  /* WARNING! ONLY FOR BINARY WCSPS */
  if (NConstr != 0) {
    printf("Sorry, FC-BTD does not work with n-ary constraints!\n");
    exit(1);
  }
  initTD();
  initGood(Log2HashSize);
  assert(depth == 0);
  CurrentCluster = Root;
  ClusterBottom[Root] = Bottom; /* to take into account initial propagation */
  fcbtd_rec(Root, Bottom, Bottom, Top);
  statGood(Root, 0);
}

/* recursive BTD exploiting goods */
/* cluster : current cluster to be solved */
/* ub : upper bound for subtree rooted at cluster  */
/* returns the lower and upper bounds for subtree rooted at cluster */
bounds_t btd_rec(int cluster, cost_t ub, int clusterdepth)
{
  int i; /* current variable */
  int l; /* current value */
  int j;
  int son, separator;
  cost_t alpha;
  cost_t good;
  cost_t newgub;
  bounds_t res;
  bounds_t result;
  int c;
#ifndef NOPROACTIVEGOOD
  int connected;
#endif
/*  #ifdef CHECKLB */
/*    FILE *file; */
/*    char filename[1024]; */
/*  #endif */

  if (Verbose>=BTDVERBOSE) {
    printf("[%d] btd(C%d,", depth, cluster);
    PRINTCOST(ClusterBottom[cluster]);
    printf(",");
    PRINTCOST(ub);
    printf(")\n");
  }

  assert(ub <= Top);
  CurrentCluster = cluster; /* global variable used by variable heuristics */
  i = (*HeurVarFunc[HeurVar])(); /* i becomes the new current variable */
  if (i<0) {
    /* cluster variables are all assigned */
    result.lb = ClusterBottom[cluster];
    result.ub = ClusterBottom[cluster];
    assert(result.lb < Top);
    if (Sons[cluster][0] == 0) {
      /* cluster has no children */
      if (Verbose>=BTDVERBOSE) {
	printf("[%d] ", depth);
	PRINTCOST(result.lb);
	printf(" ");
	PRINTCOST(result.ub);
	printf(" (C%d) leaf node\n",cluster);
      }
      assert(result.lb <= ub);
      assert(result.lb <= result.ub);
      return result;
    } else {
      /* compute the sum of lowerbound/optimum of every children */
      for (son=1; son <= Sons[cluster][0] && result.lb < Top; son++) {
	separator = Sons[cluster][son];

	/* propagation may have been updated since the assignment of this separator */
	/* if the separator was not disconnected */
	/* or if a backtrack occurs after solving sperartor's subtree a first time */
	SeparatorStatus[separator].propa = subtreeBottom(separator);

#ifdef NOPROACTIVEGOOD
	SeparatorStatus[separator].lb = getGood(separator, assignments, &SeparatorStatus[separator].optimum);
	SeparatorStatus[separator].lb = MAX(SeparatorStatus[separator].lb, SeparatorStatus[separator].initlb);
	SeparatorStatus[separator].unarymove = unaryCostBTD(separator);
	assert(SeparatorStatus[separator].optimum==FALSE || SeparatorStatus[separator].lb >= SeparatorStatus[separator].unarymove);
	if (SeparatorStatus[separator].lb < Top) SeparatorStatus[separator].lb = MAX(SeparatorStatus[separator].lb - SeparatorStatus[separator].unarymove, BOTTOM);

/*  	if (!(SeparatorStatus[separator].optimum==FALSE || SeparatorStatus[separator].lb >= SeparatorStatus[separator].propa)) { */
/*  	  Verbose = 4; */
/*  	  showCosts(); */
/*  	  for (c=0;c<NbCluster;c++) { */
/*  	    printf(" C%d:", c); */
/*  	    PRINTCOST(ClusterBottom[c]); */
/*  	  } */
/*  	  printf("\n"); */
/*  	  printf("sep= %d , lb= %d , propa= %d , unarymove= %d\n", separator, SeparatorStatus[separator].lb, SeparatorStatus[separator].propa, SeparatorStatus[separator].unarymove); */
/*  	} */
	assert(SeparatorStatus[separator].optimum==FALSE || SeparatorStatus[separator].lb >= SeparatorStatus[separator].propa);
#endif
	/* note that lb takes into account unary cost moves already */
	result.lb += MAX(SeparatorStatus[separator].lb, SeparatorStatus[separator].propa);
      }
      /* examine every child */
      j = 1;
      /* global cut necessary due to possible NC propagation on uncle clusters */
#ifdef NOGLOBALCUT
      while (j <= Sons[cluster][0] && result.lb < ub) {
#else
      while (j <= Sons[cluster][0] && result.lb < ub && Bottom < Top) {
#endif
	/* Set CurrentCluster for Heuristics */
	CurrentCluster = cluster;
	son = j; // (*HeurClustFunc[HeurVar])(j); 
	/* Swap cluster j with selected cluster */
	separator = Sons[cluster][j];
	Sons[cluster][j]=Sons[cluster][son];
	Sons[cluster][son] = separator;
	/* Use cluster j */
	separator = Sons[cluster][j];
	assert(SeparatorStatus[separator].propa == subtreeBottom(separator));
	if (SeparatorStatus[separator].optimum == FALSE) {
#ifndef NOPROACTIVEGOOD
	  if (SeparatorStatus[separator].connected == FALSE) {
	    connected = FALSE;
	    /* reconnect subtree if previously disconnected */
	    separatorReconnect(separator);
	    /* and update Bottom accordingly */
	    assert(SeparatorStatus[separator].lb >= SeparatorStatus[separator].propa);
	    Bottom -= SeparatorStatus[separator].lb - SeparatorStatus[separator].propa;
	  } else {
	    connected = TRUE;
	  }
#endif
	  result.lb -= MAX(SeparatorStatus[separator].lb, SeparatorStatus[separator].propa);
	  /* global cut may be better informed than local cut due to NC propagation on uncle clusters */
#ifdef NOGLOBALCUT
	  alpha = ub - result.lb;
#else
	  alpha = MIN(ub - result.lb, Top - Bottom + SeparatorStatus[separator].propa);
#endif
	  assert(alpha >= 1);
	  BottomCurrentClusterTree = SeparatorStatus[separator].propa;
	  assert(alpha > BottomCurrentClusterTree);
	  assert(assignments[Cluster2Vars[separator][1]]==-1);
	  res = btd_rec(separator, alpha, clusterdepth + 1);
	  assert(res.lb <= alpha);
	  assert(res.lb <= res.ub);
	  assert(SeparatorStatus[separator].propa == subtreeBottom(separator));
	  /* res.lb can be lower than SeparatorStatus[separator].propa due to global cut effects */
	  assert(SeparatorStatus[separator].lb <= res.ub);
	  /* res.lb can be lower than the previous known lower bound due to global cut effects */
	  // res.lb = MAX(res.lb, SeparatorStatus[separator].lb);
	  result.lb = MIN(result.lb + res.lb, Top);
	  result.ub = MIN(result.ub + res.ub, Top);
	  good = (res.lb<Top)?MAX(res.lb + SeparatorStatus[separator].unarymove, BOTTOM):Top;
	  assert(res.lb <= res.ub);
	  setGood(separator, assignments, good, res.lb==res.ub && res.lb < alpha);
#ifndef NOPROACTIVEGOOD
	  /* separator is completly solved: update Bottom accordingly */
	  Bottom += res.lb - SeparatorStatus[separator].propa;
	  /* update Bottom and BottomCurrentClusterTree data at previous depths with the new good */
#ifndef NOBACKTRACKBOTTOMUPDATE
	  updateBacktrack(separator, connected, res.lb, SeparatorStatus[separator].lb, SeparatorStatus[separator].depth + 1, depth);
#endif
	  /* disconnects the separator until one of its variable is unassigned */
	  assert(res.lb <= res.ub);
	  separatorDisconnect(separator, res.lb, res.lb==res.ub && res.lb < alpha, SeparatorStatus[separator].unarymove, (connected)?SeparatorStatus[separator].depth:0, res.lb - SeparatorStatus[separator].propa);
#endif
	} else {
	  /* we know the optimum of this cluster */
	  assert(SeparatorStatus[separator].lb >= SeparatorStatus[separator].propa);
	  result.ub = MIN(result.ub + SeparatorStatus[separator].lb, Top);
	}
	j++;
      }
      if (result.lb > ub) result.lb = ub;
#ifdef NOGLOBALCUT
      if (j <= Sons[cluster][0] || result.lb >= ub) {
#else
      if (j <= Sons[cluster][0] || result.lb >= ub || Bottom >= Top) {
#endif
	/* some children have not been explored */
	result.ub = Top;
      }
      if (Verbose>=BTDVERBOSE) {
	printf("[%d] ", depth);
	PRINTCOST(result.lb);
	printf(" ");
	PRINTCOST(result.ub);
	printf(" (C%d) AND node\n",cluster);
      }
      assert(result.lb <= ub);
      assert(result.lb <= result.ub);
      return result;
    }
  } else {
    /* classic depth first branch and bound on the current cluster for variable i */
    assert(Var2Cluster[i] == cluster);
    result.lb = ub; /* NC supprime des valeurs en utilisant la coupe locale => result.lb <= ub */
    result.ub = Top;

/*  #ifdef CHECKLB */
/*      sprintf(filename, "checklbC%d.wcsp", cluster); */
/*      file = fopen(filename,"w"); */
/*      saveWCSPsubtree(file, cluster, INT_MAX); */
/*      fclose(file); */
/*  #endif */
    
    num_future--;       /* one more variable is assigned */
    depth++;
    assignments[i]=1000;
    sort_fut_var();
    assignments[i]=-1;
    update_degree(i);
    save_parameters1();
    for(j=0;j<NConstr;j++)
      {fut_var_r2[depth][j]=currentArity[j]; if(inScope(i,j))currentArity[j]--;}
#ifndef NOPROACTIVEGOOD
    for(c=0; c<NbCluster; c++){SaveSeparatorArity[depth][c] = SeparatorArity[c];}
#endif
    List_create(&feasible_values2[i]);
    (*HeurValFunc[HeurVal])(i);
    List_reset_traversal(&feasible_values2[i]);

    /*at least one entrance in the loop */
    assert(BottomCurrentClusterTree < ub);
#ifndef NOGLOBALCUT
    assert(Bottom < Top);
#endif
    assert(!List_end_traversal(&feasible_values2[i]));

    /* enumerate all domain values */
#ifdef NOGLOBALCUT
    while(BottomCurrentClusterTree < ub && !List_end_traversal(&feasible_values2[i])) {
#else
    while(BottomCurrentClusterTree < ub && Bottom < Top && !List_end_traversal(&feasible_values2[i])) {
#endif
      l=List_consult1(&feasible_values2[i]); /* l is the new current value*/
      List_next(&feasible_values2[i]);
      if (Verbose>=2) {
	if (Verbose>=3) {
	  showCosts();
	  for (c=0;c<NbCluster;c++) {
	    printf(" C%d:", c);
	    PRINTCOST(ClusterBottom[c]);
	  }
	  printf("\n");
	}
	printf("[C%d,", cluster);
	PRINTCOST(BottomCurrentClusterTree);
	printf(",");
	PRINTCOST(ub);
	printf("]");
	printf("[%d,", depth);
	PRINTCOST(Bottom);
	printf(",");
	PRINTCOST(Top);
	printf(",%d]", numberOfValues());
	printf(" x%d = %d\n", i, l);
      }
      assignments[i]=l;                /* l is assigned to i */
      if (NNodes >= NodeLimit) nodeOut(); 
      NNodes++;
      List_create(&l1[depth]); List_create(&l2[depth]); 
      List_create(&l3[depth]); List_create(&l3BTD[depth]); 
      List_create(&l4[depth]);
      List_create(&l5[depth]); List_create(&l6[depth]); 
      List_create(&LSeparators[depth]);

      Bottom+=CostU(i,l); 
      ClusterBottom[cluster]+=CostU(i,l);
      BottomCurrentClusterTree+=CostU(i,l); 
      TopCurrentClusterTree = ub;
      CurrentCluster = cluster;

#ifdef NOGLOBALCUT
#ifdef NOPROACTIVEGOOD
      if (BottomCurrentClusterTree < ub && look_ahead(i,l) && projectB(i) && (*LcFunc[LcLevel])()) {
#else
      if (BottomCurrentClusterTree < ub && look_ahead(i,l) && projectB(i) && (*LcFunc[LcLevel])() && useGoods(i) && (*LcFunc[LcLevel])()) {
#endif
#else
#ifdef NOPROACTIVEGOOD
      if (BottomCurrentClusterTree < ub && Bottom < Top && look_ahead(i,l) && projectB(i) && (*LcFunc[LcLevel])()) {
#else
      if (BottomCurrentClusterTree < ub && Bottom < Top && look_ahead(i,l) && projectB(i) && (*LcFunc[LcLevel])() && useGoods(i) && (*LcFunc[LcLevel])()) {
	//      if (BottomCurrentClusterTree < ub && Bottom < Top && look_ahead(i,l) && projectB(i) && useGoods(i) && (*LcFunc[LcLevel])() && (Options==0 || metaLC(FALSE, Options & OPTIONS_DOMINANCE_BB, Options & OPTIONS_SINGLETON_BB, RestrictedSingletonNbVar)) && (ElimLevel<0 || elim())) {
#endif
#endif
	assert(BottomCurrentClusterTree <= ub);
#ifndef NOGLOBALCUT
	assert(Bottom < Top);
#endif
	assert(ub <= Top);

	res = btd_rec(cluster, ub, clusterdepth);
	result.lb = MIN(result.lb, res.lb);
	result.ub = MIN(result.ub, res.ub);
	ub = MIN(ub, res.ub);
	if (cluster == Root && res.ub < Top) {
	  /* new optimum has been found */
	  Top = res.ub;
	  if (Verbose) {
	    printf("New Top: "); PRINTCOST(Top); printf(" (%lu nodes, %.2lf secs)\n",NNodes,cpuTime() - SolvingTime);
	  }
	}
      } else {
	result.lb = MIN(result.lb,BottomCurrentClusterTree);
	newgub = Top-Bottom+BottomCurrentClusterTree;
	newgub = MAX(1, newgub); // at least one constraint has been violated in the current subproblem
	result.lb = MIN(result.lb,newgub);
      }

      restore_context();
#ifndef NOPROACTIVEGOOD
      reconnectSeparators();
      for(c=0; c<NbCluster; c++){SeparatorArity[c] = SaveSeparatorArity[depth][c];}
#endif
      restore_parameters1();
      List_dispose_memory(&l1[depth]); List_dispose_memory(&l2[depth]);
      List_dispose_memory(&l3[depth]); List_dispose_memory(&l3BTD[depth]); 
      List_dispose_memory(&l4[depth]);
      List_dispose_memory(&l5[depth]); List_dispose_memory(&l6[depth]); 
      List_dispose_memory(&LSeparators[depth]);
    }
    assignments[i]=-1; /* unassigns current variable */
    for(j=0;j<NConstr;j++){currentArity[j]=fut_var_r2[depth][j];}
    List_dispose_memory(&feasible_values2[i]);
    num_future++; /* current variable becomes future */
    depth--;
    
    assert(result.lb <= Top);
    assert(result.lb <= ub); /* obligatoire a cause de la coupe locale faite par la noeud coherence */

    if (Verbose>=BTDVERBOSE) {
      printf("[%d] ", depth);
      PRINTCOST(result.lb);
      printf(" ");
      PRINTCOST(result.ub);
      printf(" (C%d) OR node\n",cluster);
    }
    assert(result.lb <= result.ub);
    return result;
  }
}

/* use a given complete assignment for computing an upperbound of the sub-problem rooted at cluster */
/* does not take into account any constraints involving variables in its separator with its father */
/* cannot be called during BTD search */
cost_t subtreeUpperBound(int *completeAssignment, int cluster)
{
  cost_t Sum = BOTTOM;
  int c,i,j,pos,pos2;
  int scopeinside;

  assert(cluster != Root && subtreeBottom(cluster) == BOTTOM);

  for (pos=1; pos<=ClusterTree2Vars[cluster][0];pos++) {
    i = ClusterTree2Vars[cluster][pos];
    Sum += CostU(i,completeAssignment[i]);
  }
  for (pos=1; pos<=ClusterTree2Vars[cluster][0];pos++) {
    i = ClusterTree2Vars[cluster][pos];
    for (pos2=pos+1; pos2<=ClusterTree2Vars[cluster][0];pos2++) {
      j = ClusterTree2Vars[cluster][pos2];
      if (ISBINARY(i,j)) {
	Sum += CostB(i,completeAssignment[i],j,completeAssignment[j]);
      }
    }
  }
  for (c=0;c<NConstr;c++) {
    scopeinside = 1;
    for (i=0; i<ARITY(c); i++) {
      if (!ISANCESTOR(cluster,Var2Cluster[SCOPEONE(c,i)])) {
	scopeinside = 0;
	break;
      }
    }
    if (scopeinside) {
      Sum += cost(c,completeAssignment);
    }
  }
  return Sum;
}

cost_t clusterTreeOptimum(int cluster, int maxcdepth)
{
  cost_t lb = BOTTOM;
  FILE *file;
  FILE *file2;
  char command[1024];
  int res;
  cost_t ub;
  unsigned long nbnodes = 0;
  double cputime = 0;
  char filename[1024];
  char filename2[1024];

  sprintf(filename,"tmpwcsp%d",processid());
  file = fopen(filename,"w");
  saveWCSPsubtree(file, cluster, maxcdepth);
  fclose(file);
  if (BestSol[0] != -1) {
    ub = subtreeUpperBound(BestSol, cluster);
    ub = MIN(ub,Top);
  } else {
    ub = Top;
  }
  res = 0;
  if (ub > BOTTOM) {    
    sprintf(filename2,"tmpres%d",processid());
#if defined(VALLONGLONG)
    sprintf(command, "toolbarl %s -H5 -p3 -u%lld | awk '/^Optimum: /{print $2,$4,$7} /^Lower bound: /{print $3,$5,$8}' > %s", filename, ub, filename2);
#else
    //    sprintf(command, "./toolbar -l%d -M%d -u%ld -V%d -H%d -T%d -k%d -F%d -Z%d -s%d %s | awk '/^Optimum: /{print $2,$4,$7} /^Lower bound: /{print $3,$5,$8}' > %s", LcLevel, SearchMethod, ub, HeurVal, HeurVar, HeurTreedec, ClusterSel, MaxSeparatorSize, MaximalClusterDepth, Seed, filename, filename2); /* | tee /dev/stderr */
    sprintf(command, "toolbar %s -H6 -u%ld | awk '/^Optimum: /{print $2,$4,$7} /^Lower bound: /{print $3,$5,$8}' > %s", filename, ub, filename2);
#endif
    res = system(command);
    file2 = fopen(filename2,"r");
    if (fseek(file2, 0, SEEK_END), ftell(file2) == 0) abort(); fseek(file2, 0, SEEK_SET);
#if defined(VALLONGLONG)
    fscanf(file2,"%lld",&lb);
#else
    fscanf(file2,"%ld",&lb);
#endif
    fscanf(file2,"%lu %lf", &nbnodes, &cputime);
    fclose(file2);
    sprintf(command, "rm -f %s", filename2);
    system(command);
  } else {
    lb = BOTTOM;
  }

  sprintf(command, "rm -f %s", filename);
  system(command);
  sprintf(command, "rm -f %s", filename2);
  system(command);

  if (Verbose) {
    printf("C%d: upperbound= ", cluster);
    PRINTCOST(ub);
    printf(" , lowerbound = ");
    PRINTCOST(lb);
    printf(" (in %lu nodes and %.2f seconds)\n",nbnodes,cputime);
  }
  if (res != 0) {
    abort();
  }

  return lb;
}

cost_t btdIDRDS(int cluster, int maxcdepth)
{
  cost_t sum = BOTTOM;
  cost_t res = BOTTOM;
  int son;

  if (Sons[cluster][0] == 0) {
    res = clusterTreeOptimum(cluster,maxcdepth);
  } else {
    for (son=1; son<=Sons[cluster][0]; son++) {
      sum += btdIDRDS(Sons[cluster][son], maxcdepth);
    }
    if (Father[cluster] != -1) res = clusterTreeOptimum(cluster, maxcdepth);
    res = MAX(res, sum);
  }
  if (Verbose) {
    printf("C%d: lowerbound = ",cluster);
    PRINTCOST(res);
    printf("\n");
  }
  SeparatorStatus[cluster].initlb = res;
  return res;
}

/* main function for BTD exploiting goods */
void btd()
{
  bounds_t result;
  int i,c;
  FILE *fileHandle;
/*    int j; */

#ifdef NOGLOBALCUT
  if (LcLevel==LC_EDAC) {
    printf("Sorry, BTD (without using global cuts) is incompatible with EDAC!\n");
    exit(EXIT_FAILURE);
  }
#endif

  initTD();
  initGood(Log2HashSize);
  assert(depth == 0);

/*    if (Verbose) { */
/*      for (i=0; i<NVar; i++) { */
/*        printf("%d: (%d)", i, degree[i]); */
/*        for (j=0; j<NVar; j++) { */
/*  	if (ISBINARY(i,j)) printf(" %d", j); */
/*        } */
/*        printf("\n"); */
/*      } */
/*    } */

  if (ClusterRoot!=-1) {
    Root = ClusterRoot;
    for (i=1;i<=Separator[Root][0];i++) {
      assignments[Separator[Root][i]] = 0;
      for (c=1;c<=Var2Separators[Separator[Root][i]][0]; c++) {
	SeparatorArity[Var2Separators[Separator[Root][i]][c]]--;
      }
    }
    if (SaveFileName != NULL && ClusterRoot!=-1) {
      fileHandle = fopen(SaveFileName, "w");
      if(fileHandle == NULL) {
	perror("Error: bad output filename (problem saving after preprocessing)!"); 
	abort();
      }
      for (i=0; i<NVar; i++) {
	if (assignments[i] == -1 && !ISANCESTOR(ClusterRoot, Var2Cluster[i])) assignments[i] = 0;
      }
      saveWCSPsubtree(fileHandle, ClusterRoot, (MaximalClusterDepth>0)?MaximalClusterDepth:INT_MAX);
      fclose(fileHandle);
      exit(0);
    }
  }

  if (MaximalClusterDepth > 0) btdIDRDS(Root, MaximalClusterDepth);

  CurrentCluster = Root;
  ClusterBottom[Root] = Bottom; /* to take into account initial propagation */
  BottomCurrentClusterTree = Bottom;
  TopCurrentClusterTree = Top;
  result = btd_rec(Root, Top, 0);
  if (Verbose >= BTDVERBOSE) {
    PRINTCOST(result.lb);
    printf(" ");
    PRINTCOST(result.ub);
    printf("\n");
  }
  statGood(Root, 0);
}
