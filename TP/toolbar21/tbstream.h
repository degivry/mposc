/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tbstream.h
  $Id: tbstream.h,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#ifndef STREAM_H
#define STREAM_H



typedef struct
{
	int *str_content;
	int *str_stack;
	int str_n_elem;
	int str_head;
} myStream;

typedef struct
{
	int *stream2;
	int stream2_n;
} heap;


extern myStream *Q; /* Q stream is used to propagate deletions to simple supports*/

extern myStream *Rdac,*S,*P;


extern myStream * createStream();
extern void destroyStream(myStream *s);
extern int stream_size(myStream *s);
extern int stream_empty(myStream *s);
extern int stream_pop_last_in(myStream *s);
extern int stream_pop_first_in(myStream *s);
extern int stream_pop_random(myStream *s);
extern int stream_pop_max(myStream *s);
extern int stream_pop_min(myStream *s);
extern int stream_pop_min_dom(myStream *s);
extern int stream_pop_max_dom(myStream *s);
extern void stream_ini(myStream *s);
extern void fillStream(myStream *s);
extern void fillHeap(heap *h);
extern void stream_push(myStream *s,int i);

extern heap * createHeap();
extern void destroyHeap(heap *h);
extern void heap_push(heap *h, int i);
extern int heap_pop(heap *h);
extern int heap_pop2(heap *h);
extern int heap_empty(heap *h);
extern void heap_ini(heap *h);
extern int heap_existsValue(heap *h, int i);

extern myStream ** createStreamCopies();
extern void destroyStreamCopies(myStream **s);
extern void heap_show(heap *s);


#endif
