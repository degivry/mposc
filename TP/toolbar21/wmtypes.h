/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmtypes.h
  $Id: wmtypes.h,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
--------------------------------------------------------------------- */

#include "wmlist.h"



#define MEM_FACT 100 // Factor to increment memory allocation



typedef struct

{

	int size;

	int * content;

	int * elems;

	int n_elem;

} stack;



typedef struct

{

	int boolValue;

	int idVar;

} literal;



typedef struct

{

	int indexToClause;



} ReferenceToClause;





typedef struct

{

	ListS * pointerToLiterals;

	int weight;

	int literalsWithoutAssign;

	int elim;

	NodeLS * terminator;

	literal termLit;

	literal termLit2;

} clause;





typedef struct

{

	int indexToVariable;



} ReferenceToVariable;





typedef struct

{

	ListS * listLiterals;

	ListS * listNoLiterals;



} variable;



typedef struct

{

	int usedTrue;

	int usedFalse;

	int not_considered_values;



} decision;



typedef struct

{

	clause * clauses; // Array of clauses

	int totalClauses; // UpperBound of array of clauses

	variable * variables; // Array of variables

	int totalVariables; // UpperBound of array of variables

	decision * ArrayOfDecision; // Array for decide the variable to take at any iteration

	int ** UnaryCosts; // Array to manage unary costs

	int ****BinaryCosts; // Array to manage binary costs

	ListD * notAssignedVars; // list of not yet assigned variables.

	ListD * notAssignedCla;

	int UB; // Upper Bound for wmaxsat

	int LB; // Low Bound for wmaxsat

	int * Result;

	int * bestResult;

	int totalWeight;

	int maximumArity;

	unsigned long totTime;

	char fileName[MAX_CARS];

	ListR restoreList;  // List wich nodes will be used to save and load restoration information

	stack *Q; // This stack contains all variables to check AC local consistency

	stack *R; // This stack contains all variables to check DAC local consistency

	int * pows; // Array to save the pows

	int * jeroslows; // Array to save the jeroslow heuristic for each variable

	int * tmpArr;

} problem;



typedef struct

{

	int var;

	int val;

} PrunedValue;





typedef struct

{

	int var;

	int val;

	int qty;

} DacPrunedValue;



typedef struct

{

	int var1;

	int var2;

	int val1;

	int val2;

	int qty;

} DacPrunedValue2;



typedef struct

{

	int var;

	int val;

	ListR pru;

	NodeLD * xAct;

	int actualLB;

	int updatePrune;

	ListR satList;

	ListR pruneList;

	ListR NCList;

	ListR ACList;

	int NClimit;

} restoreStruct;





// Functions for literal Lists

void showLiterals(ListS * l);

void showLiteral(literal * li);

literal * getActualLiteral(ListS * l);

literal * getLiteral(NodeLS * n);

void insertLiteral(ListS * l, literal * li);

literal * findFirstVariableWithoutAssign(problem *p, int c);

literal * findSecondVariableWithoutAssign(problem *p, int c);

void passJeroslow(problem *p,int c);

void nopassJeroslow(problem *p,int c);



// Functions for references to clauses

void insertReference(ListS * l, ReferenceToClause * c);

ReferenceToClause * getActualReference(ListS * l);

ReferenceToClause * getReference(NodeLS * n);

void showReferences(ListS * l);

void showReference(ReferenceToClause * rc);

ReferenceToClause * createReference(int index);

void clearReferences(ListS * l);



// functions for references to not assigned clauses

void insertReferenceCla(ListD * l, ReferenceToClause * rc);

ReferenceToClause * getActualReferenceCla(ListD * l);

ReferenceToClause * getReferenceCla(NodeLD * n);

void showReferencesCla(ListD * l);

void showReferenceCla(ReferenceToClause * rc);

ReferenceToClause * createReferenceCla(int index);

void clearReferencesCla(ListD * l);

void createListOfReferencesClauses(problem * p);



// functions for references to variables

void insertReferenceVar(ListD * l, ReferenceToVariable * rv);

ReferenceToVariable * getActualReferenceVar(ListD * l);

ReferenceToVariable * getReferenceVar(NodeLD * n);

void showReferencesVar(ListD * l);

void showReferenceVar(ReferenceToVariable * rv);

ReferenceToVariable * createReferenceVar(int index);

void clearReferencesVar(ListD * l);

void createListOfReferencesVariables(problem * p);



// Functions for clauses

void createArrayClauses(problem * p,int num_cla);

void clearArrayClauses(problem * p);

void showClauses(problem * p);



// Functions for variables

void createArrayVariables(problem * p,int num_var);

void clearArrayVariables(problem *p);

void createArrayOfDecision(problem * p);

void showArrayOfDecision(problem * p);

void showVariables(problem * p);

void clearArrayOfDecision(problem *p);





// Functions for array of results

void createArrayResults(problem *p);

void destroyArrayResults(problem *p);

void showArrayResults(problem *p);



// Functions for array of Best results

void createArrayBestResults(problem *p);

void destroyArrayBestResults(problem *p);

void showArrayBestResults(problem *p);

void saveBestResult(problem *p);



// Functions for array of prune

void createUnaryCosts(problem *p);

void showUnaryCosts(problem *p);



// Functions for BinaryCosts



void createBinaryCosts(problem *p);

void destroyBinaryCosts(problem *p);

void showBinaryCosts(problem *p);



// Functions for problems

void initProblem(problem *p);

void showProblem(problem * p);

void showProblemResults(problem * p);

void clearProblem(problem * p);

int readProblem(FILE *f, problem *p,int isCnf);



void showRestoreInformation();



// Functions for stacks



stack * iniStack(int size);

void showStack(stack *s);

void pushStack(stack *s,int i);

int popStack(stack *s);

int popStackMinDom(stack *s);

int popStackMaxDom(stack *s);

void clearStackValues(stack *s);

void clearStack(stack *s);



// Funtions for array of pows



void createArrayPows(problem *p);

void destroyArrayPows(problem *p);

void showArrayPows(problem *p);



// Functions for array of jeroslows



void createArrayJeroslows(problem *p);

void destroyArrayJeroslows(problem *p);

void showArrayJeroslows(problem *p);

// Functions for MAX-SAT RULES
int compClauses(problem *p,int refCla1,int refCla2,int i,literal *l1,literal *l2);