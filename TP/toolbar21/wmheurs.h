/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmheurs.h
  $Id: wmheurs.h,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
// PRECOMPUTED JEROSLOWS



int emptyFunction1(problem *p,int i,int j);
int emptyFunction2(problem *p,int i);
int nopassBinaryJeroslowExecute(problem *p,int i,int j);



int passBinaryJeroslowExecute(problem *p,int i,int j);
int nopassJeroslowDeactivateExecute(problem *p,int i);
int passJeroslowDeactivateExecute(problem *p,int i);



int iniVarJeroslow(int i, problem *p);
void initializeJeroslow(problem *p);



extern func_t nopassBinaryJeroslow[7];
extern func_t passBinaryJeroslow[7];
extern func_t nopassJeroslowDeactivate[7];
extern func_t passJeroslowDeactivate[7];



// SELECTION VARIABLES HERISTICS

int WM_Jeroslow_like(problem *p);
int WM_sort_var_lex(problem *p);
extern func_t HeurVarFuncWM[7];


// SELECTION VALUES HEURISTICS

int WM_sort_val_lex(problem *p,restoreStruct *r);
int WM_min_unary_cost(problem *p,restoreStruct *r);
extern func_t HeurValFuncWM[2];


// LOCAL CONSISTENCY ENFORCE

void initQandR(problem * p, restoreStruct * r);
int checkNodeConsistency(problem *p);
int executeNodeConsistency(problem *p, restoreStruct * r);
int binaryToUnaryAndNodeConsistency(problem *p,restoreStruct *r);
int restoreBinaryToUnaryLimited(problem *p,restoreStruct *r);
void restoreNodeConsistency(problem *p, restoreStruct * r);
int existSupport(problem * p,int i,int j);
int existFullSupport(problem * p,int i,int j);
int executeAC3(problem * p, restoreStruct * r);
void restoreAC3(problem *p, restoreStruct * r);
int executeDAC(problem *p,restoreStruct * r);
int executeFDAC(problem *p,restoreStruct *r);

extern func_t LcFuncWM[5];
