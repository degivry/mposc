/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: context_restoration.c
  $Id: context_restoration.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
--------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "wcsp.h"
#include "solver.h"
#include "btd.h"

void restore_context()
{
  int c,kk1,kk2,kk3,kk5,kk6,kk7,kk8;
  cost_t cc;

  // no cost
  List_reset_traversal(&l1[depth]);
  while(!List_end_traversal(&l1[depth]))
    {
      List_consult2(&l1[depth],&kk5,&kk6);
      current_domains[kk5][kk6]=-1;
      List_insert1(&feasible_values[kk5],kk6);
      List_next(&l1[depth]);
    }

  // cost kk8
  List_reset_traversal(&l2[depth]);
  while(!List_end_traversal(&l2[depth]))
    {
      List_consult3pc(&l2[depth],&kk5,&kk6,&kk7,&cc);
      UNARYCOST(kk5,kk6)-=cc;
      List_next(&l2[depth]);
    }

  // cost kk8
  List_reset_traversal(&l3[depth]);
  while(!List_end_traversal(&l3[depth]))
    {
      List_consult3pc(&l3[depth],&kk5,&kk6,&kk7,&cc);
      UNARYCOST(kk5,kk7)-=cc;
	  DeltaB[kk5][kk6][kk7]-=cc;
      List_next(&l3[depth]);
    }

  // cost BTD
  if (SearchMethod != DFBB && depth!=0) {
    List_reset_traversal(&l3BTD[depth]);
    while(!List_end_traversal(&l3BTD[depth]))
      {
	List_consult3pc(&l3BTD[depth],&kk5,&kk6,&kk7,&cc);
	for (c=1;c<=Var2Separators[kk5][0]; c++) {
	  if (ISANCESTOR(Var2Separators[kk5][c],Var2Cluster[kk6])) {
	    UNARYCOSTBTD(Var2Separators[kk5][c],kk5,kk7)-=cc;
	  } 
	}
	List_next(&l3BTD[depth]);
      }
  }

  // no cost
  List_reset_traversal(&l4[depth]);
  while(!List_end_traversal(&l4[depth]))
    {
      List_consult4(&l4[depth],&kk5,&kk6,&kk7,&kk8);
	  SupB[kk6][kk7][kk5]=kk8;
      List_next(&l4[depth]);
    }

    // cost is kk1 (every 2 blocks)
  List_reset_traversal(&l6[depth]);
  while(!List_end_traversal(&l6[depth]))
    {
      List_consult4(&l6[depth],&kk5,&kk6,&kk7,&kk8);
      List_next(&l6[depth]);
      List_consult3pc(&l6[depth],&kk1,&kk2,&kk3,&cc);
      List_next(&l6[depth]);
	  if(kk5<kk6) BINARYCOST(kk5,kk6,kk7,kk8)-=cc;
	  else BINARYCOST(kk6,kk5,kk8,kk7)-=cc;
    }

// no cost
  List_reset_traversal(&l5[depth]);
  while(!List_end_traversal(&l5[depth]))
    {
      List_consult4(&l5[depth],&kk5,&kk6,&kk7,&kk8);
      destroyBinaryConstraint(kk5,kk6);
      degree[kk5]--; degree[kk6]--;
      List_next(&l5[depth]);
    }
}

