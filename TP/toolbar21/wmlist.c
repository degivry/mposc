/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmlist.c
  $Id: wmlist.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdlib.h>
#include <stdio.h>
#include "wmlist.h"
#include "wmauxiliar.h"

/* FUNCTIONS FOR SIMPLE LISTS */

ListS * newListS()
{
	ListS * l;
	l =(ListS *)malloc(sizeof(ListS));
	if (l==NULL) error(INS_MEM);
	createListS(l);
	return l;
}

void createListS(ListS * l)
{
	// Create a simple list
	l->first=NULL;
	l->last=NULL;
	l->actual=NULL;
}

void setFirstLS(ListS * l)
{
	l->actual=l->first;
}

void getNextLS(ListS * l)
{
	// PRE: The list is pointing to some existent element (is not pointing NULL)
	l->actual=l->actual->next;
}

int endListLS(ListS * l)
{

	//if (l->actual == NULL) return TRUE;
	//else return FALSE;
	return (l->actual == NULL);
}

int isEmptyLS(ListS * l)
{
	if (l->first==NULL) return TRUE;
	else return FALSE;
}

/* FUNCTIONS FOR DOUBLE LISTS */

ListD * newListD()
{
	ListD * l;
	l =(ListD *)malloc(sizeof(ListD));
	if (l==NULL) error(INS_MEM);
	createListD(l);
	return l;
}

void createListD(ListD * l)
{
	// Create a double list
	l->first=NULL;
	l->last=NULL;
	l->actual=NULL;
}

void setFirstLD(ListD * l)
{
	l->actual=l->first;
}

void getNextLD(ListD * l)
{
	// PRE: The list is pointing to some existent element (is not pointing NULL)
	l->actual=l->actual->next;
}

void getPreviousLD(ListD * l)
{
	if (l->first==NULL) l->actual = NULL;
	else if (l->first!=NULL && l->actual==NULL)
	{
		l->actual=l->first;
	}
	else if (l->first!=NULL && l->actual!=NULL)
	{
		l->actual=l->actual->prev;
	}
}


int endListLD(ListD * l)
{
	//if (l->actual == NULL) return TRUE;
	//else return FALSE;
	return (l->actual == NULL);
}

int isEmptyLD(ListD * l)
{
	if (l->first==NULL) return TRUE;
	else return FALSE;
}

void insertExistentNode (ListD *l,NodeLD * n)
{
	n->next=NULL;

	if (l->first==NULL)
	{
		// First element to insert
		n->prev=NULL;
		l->first=n;
		l->last=n;
	}
	else
	{
		n->next=l->first;
		l->first->prev=n;
		n->prev=NULL;
		l->first=n;
	}
}

void insertExistentNode2 (ListD *l,NodeLD * n)
{
	n->next=NULL;

	if (l->first==NULL)
	{
		// First element to insert
		n->prev=NULL;
		l->first=n;
		l->last=n;
	}

	else
	{
		/* the new node must point to actual last as previous node */
		n->prev=l->last;
		/* last element must point to the new element */
		l->last->next = n;
		/* and the new is now the last */
		l->last = n;
	}
}


NodeLD * extractActual(ListD * l)
{
	NodeLD * n;
	if (l->actual == NULL) return NULL;

	if((l->actual==l->first) && (l->actual==l->last))
	{
		// extract the node
		n=l->actual;
		n->next =NULL;
		n->prev=NULL;

		// empty list
		createListD(l);
	}
	else
	{
		// extract the node
		n=l->actual;
		if (l->actual==l->first)
		{
			/*
			l->actual->next->prev=NULL;
			l->first=l->actual->next;
			l->actual=l->first;
			*/
			l->first=l->actual->next;
			l->actual=l->first;
			l->actual->prev=NULL;

		}
		else if (l->actual==l->last)
		{
			l->actual->prev->next=NULL;
			l->last=l->actual->prev;
			l->actual=NULL;
		}
		else
		{
			l->actual->prev->next=l->actual->next;
			l->actual->next->prev=l->actual->prev;
			l->actual=l->actual->next;
		}
		n->next =NULL;
		n->prev=NULL;
	}
	return n;
}

void showListD(ListD *l)
{
	setFirstLD(l);

	printf("\n FIR:%x ",(int) l->first);
	printf(" LAS:%x ",(int) l->last);

	while (!endListLD(l))
	{
		printf("\n ACT:%x ",(int) l->actual);
		printf(" PRE:%x ",(int) l->actual->prev);
		printf(" NEX:%x ",(int) l->actual->next);
		getNextLD(l);
	}
}

/* FUNCTIONS FOR RESTORATION LISTS */

void createListR(ListR * l,int mem)
{
	// Create a double list
	l->first=NULL;
	l->last=NULL;
	l->actual=NULL;
	l->memory=mem;
}

void createListRAux(ListR * l)
{
	// Create a double list quickly
	l->first=NULL;
}


NodeR * createElement()
{
	NodeR * n;
	/* reserve memory for new element */
    n = ( NodeR *) malloc (sizeof(NodeR));
    if (n==NULL) error(INS_MEM);
	return n;
}

void insertElement(ListR * l,NodeR *n)
{	
	n->next=NULL;

	if (l->first==NULL)
	{
		// First element to insert
		n->prev=NULL;
		l->first=n;
		l->last=n;
	}
	else
	{
		/* the new node must point to actual last as previous node */
		n->prev=l->last;
		/* last element must point to the new element */
		l->last->next = n;
		/* and the new is now the last */
		l->last = n;
	}
}


NodeR * getActualElment (ListR * l)
{
	// Precondition: Actual pointer not is null
	return l->actual;
}

void setFirstLR(ListR * l)
{
	l->actual=l->first;
}


void getNextLR(ListR * l)
{
	// PRE: The list is pointing to some existent element (is not pointing NULL)
	l->actual=l->actual->next;

}

void createXNodes(ListR * l, int num)
{
	int i;
	for(i=0;i<num;i++)
	{
		insertElement(l,createElement());
	}
}

int endListLR(ListR * l)
{
	return (l->actual == NULL);
}

void showActualElement(ListR * l)
{
	printf("\n (V1:%d V2:%d V3:%d V4:%d V5:%d) ",l->actual->val1,l->actual->val2,l->actual->val3,l->actual->val4,l->actual->val5);

}

void showElement(NodeR * n)
{
	printf("\n (V1:%d V2:%d V3:%d V4:%d V5:%d) ",n->val1,n->val2,n->val3,n->val4,n->val5);
}

void showElements(ListR * l)
{
	NodeR * n;
	setFirstLR(l);
	while (endListLR(l)==FALSE)
	{
		n=getActualElment(l);
		printf("\n (V1:%d V2:%d V3:%d V4:%d V5:%d) ",n->val1,n->val2,n->val3,n->val4,n->val5);
		getNextLR(l);
	}
}

void getPreviousLR(ListR * l)
{
	if (l->first==NULL) l->actual = NULL;
	else if (l->first!=NULL && l->actual==NULL)
	{
		l->actual=l->first;
	}
	else if (l->first!=NULL && l->actual!=NULL)
	{
		l->actual=l->actual->prev;
	}
}

int isEmptyLR(ListR * l)
{
	if (l->first==NULL) return TRUE;
	else return FALSE;
}

NodeR * extractActualR(ListR * l)
{
	NodeR * n;
	if (l->actual == NULL) return NULL;

	// extract the node
	n=l->actual;
	if((l->actual==l->first) && (l->actual==l->last))
	{

		// empty list
		createListR(l,l->memory);
	}
	else
	{
		if (l->actual==l->first)
		{
			l->first=l->actual->next;
			l->actual=l->first;
			l->actual->prev=NULL;

		}
		else if (l->actual==l->last)
		{
			l->actual->prev->next=NULL;
			l->last=l->actual->prev;
			l->actual=NULL;
		}
		else
		{
			l->actual->prev->next=l->actual->next;
			l->actual->next->prev=l->actual->prev;
			l->actual=l->actual->next;
		}
	}
	return n;
}

void clearListR(ListR * l)
{
	NodeR * auxi;
	setFirstLR(l);
	while (endListLR(l)==FALSE)
	{
		auxi=l->actual;
		getNextLR(l);
		free(auxi);
	}
}

NodeR * passNode(ListR *o)
{
	NodeR * n;
	setFirstLR(o);
	n=extractActualR(o);

	if(n!=NULL)
	{
		return n;
	}
	else
	{
		// load more memory
		createXNodes(o,o->memory);
		setFirstLR(o);
		n=extractActualR(o);
		//printf("\nMore memory");
		return n;
	}

}


NodeR * getNewNode1(ListR *o,int val1)
{
	NodeR *n;
	n=passNode(o);
	n->val1=val1;
	return n;
}

NodeR * getNewNode2(ListR *o,int val1,int val2)
{
	NodeR *n;
	n=passNode(o);
	n->val1=val1;
	n->val2=val2;
	return n;
}

NodeR * getNewNode3(ListR *o,int val1,int val2,int val3)
{
	NodeR *n;
	n=passNode(o);
	n->val1=val1;
	n->val2=val2;
	n->val3=val3;
	return n;
}

NodeR * getNewNode4(ListR *o,int val1,int val2,int val3,int val4)
{
	NodeR *n;
	n=passNode(o);
	n->val1=val1;
	n->val2=val2;
	n->val3=val3;
	n->val4=val4;
	return n;
}

NodeR * getNewNode5(ListR *o,int val1,int val2,int val3,int val4,int val5)
{
	NodeR *n;
	n=passNode(o);
	n->val1=val1;
	n->val2=val2;
	n->val3=val3;
	n->val4=val4;
	n->val5=val5;
	return n;
}

void unloadNodes(ListR *o,ListR *d)
{
	setFirstLR(o);
	while(endListLR(o)==FALSE)
	{
		insertElement(d,extractActualR(o));
	}
}
