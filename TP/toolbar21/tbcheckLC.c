/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tbcheckLC.c
  $Id: tbcheckLC.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */

void checkSupportU(int j)
{
	int a;

	if(current_domains[j][SupU[j]]!=-1 || CostU(j,SupU[j])!=BOTTOM)
	{printf("error node support\n");}

	List_reset_traversal(&feasible_values[j]);
	while(!List_end_traversal(&feasible_values[j]))
	{
		a=List_consult1(&feasible_values[j]);
		if((Bottom + CostU(j,a)) > Top)
			{printf("error not pruned value\n");}
		List_next(&feasible_values[j]);
	}

}

void checkSupport(int i, int j)
{
  int a;

  List_reset_traversal(&feasible_values[i]);
  while(!List_end_traversal(&feasible_values[i]))
    {
      a=List_consult1(&feasible_values[i]);
      List_next(&feasible_values[i]);

      if(SupB[i][j][a]<BOTTOM || SupB[i][j][a]>=DOMSIZE(j))
	{printf("error support AC 1 %i\n",SupB[i][j][a]);}
      else if(current_domains[j][SupB[i][j][a]]!=-1 ||
	      CostB(i,a,j,SupB[i][j][a])!=BOTTOM)
	{
	  printf("error support AC (%i %i %i %i) %i ",i,j,a,SupB[i][j][a],
		 current_domains[j][SupB[i][j][a]]);
	  PRINTCOST(CostB(i,a,j,SupB[i][j][a]));
	  printf("\n");
	}
    }
}


void checkSupportDAC(int i, int j) // order[i]<order[j]
{
	int a;

	List_reset_traversal(&feasible_values[i]);
	while(!List_end_traversal(&feasible_values[i]))
	{
		a=List_consult1(&feasible_values[i]);
		List_next(&feasible_values[i]);
		if(current_domains[j][SupB[i][j][a]]!=-1 ||
		   CostB(i,a,j,SupB[i][j][a])+CostU(j,SupB[i][j][a])!=BOTTOM)
		  {
		    printf("error soporte DAC %i ",
			   current_domains[j][SupB[i][j][a]]);
		    PRINTCOST(CostB(i,a,j,SupB[i][j][a]));
		    printf(" ");
		    PRINTCOST(CostU(j,SupB[i][j][a]));
		    printf(" (%i %i %i %i)\n",a,i,j,SupB[i][j][a]);
		  }
	}
}



void docheckAC()
{
  int i,j;
  for(i=0;i<NVar;i++)for(j=0;j<NVar;j++)
    if(assignments[i]==-1 && assignments[j]==-1 && ISBINARY(i,j))
      checkSupport(i,j);
}

void docheckNCstar()
{
  int i;
  for(i=0;i<NVar;i++) if(assignments[i]==-1)checkSupportU(i);
}

void docheckACstar()
{
	docheckNCstar();
	docheckAC();
}

void docheckDAC()
{
	int i,j;
	docheckNCstar();
	for(i=0;i<NVar;i++)for(j=0;j<NVar;j++)
	{
		if(order[j]<order[i] && assignments[i]==-1 && assignments[j]==-1 && ISBINARY(i,j))
		{
			checkSupportDAC(j,i);
		}
	}

}


void docheckFDAC()
{
	int i,j;
	docheckNCstar();
	for(i=0;i<NVar;i++)for(j=0;j<NVar;j++)
	{
		if(assignments[i]==-1 && assignments[j]==-1 && ISBINARY(i,j))
		{
			if(order[j]<order[i])checkSupportDAC(j,i);
			else{checkSupport(i,j);}
		}
	}

}

int WCSPexistsExistentialSupport(int i)
{
	int a,b,val_min,j;
	cost_t min,k,res=BOTTOM,minT;

	List_reset_traversal(&feasible_values[i]);

	minT=MAXCOST;

	while(!List_end_traversal(&feasible_values[i]))
	{
		a=List_consult1(&feasible_values[i]);

		if(CostU(i,a)==BOTTOM)
		{
			res=BOTTOM;

			for(j=0;j<NVar;j++)
			{
				if(assignments[j]==-1 && ISBINARY(i,j) && j!=i)
				{
				  //					if(current_domains[j][SupB[i][j][a]]!=-1 || CostB(i,a,j,SupB[i][j][a])+CostU(j,SupB[i][j][a])>BOTTOM)
					{
						min=MAXCOST; val_min=-1;
						List_reset_traversal(&feasible_values[j]);
						while(!List_end_traversal(&feasible_values[j]) && min!=BOTTOM)
						{

							b=List_consult1(&feasible_values[j]);
							List_next(&feasible_values[j]);
							k=CostB(i,a,j,b)+CostU(j,b);

							if(k<min)
							{
								min=k;
								val_min=b;
							}

						} // end while b's

						if(val_min==-1) printf("\n MIN: %ld\n",min);
						res+=min;
					}
				}
			} // end for j

			if(res<minT) minT=res;
		}
		List_next(&feasible_values[i]);
	}

	if(minT==BOTTOM) return TRUE;
	return FALSE;
}

void showEAC(int i)
{
	int a,b,val_min,j;
	cost_t min,k,res=BOTTOM,minT;

	List_reset_traversal(&feasible_values[i]);
	minT=MAXCOST;

	while(!List_end_traversal(&feasible_values[i]))
	{
		a=List_consult1(&feasible_values[i]);
		if(CostU(i,a)==BOTTOM)
		{
			res=BOTTOM;

			for(j=0;j<NVar;j++)
			{
				if(assignments[j]==-1 && ISBINARY(i,j) && i!=j)
				{
					if(current_domains[j][SupB[i][j][a]]!=-1
								|| CostB(i,a,j,SupB[i][j][a])+CostU(j,SupB[i][j][a])>BOTTOM)
					{
						min=MAXCOST; val_min=-1;
						List_reset_traversal(&feasible_values[j]);
						while(!List_end_traversal(&feasible_values[j]) && min!=BOTTOM)
						{

							b=List_consult1(&feasible_values[j]);
							List_next(&feasible_values[j]);
							k=CostB(i,a,j,b)+CostU(j,b);

							if(k<min)
							{
								min=k;
								val_min=b;
							}

						} // end while b's

						res+=min;
					}
				}
			} // end for j
			printf("error existential support X%d :=%d COSTS:%ld + %ld DEPTH:%d\n",i,a,CostU(i,a),res,depth);
			if(res<minT) minT=res;
		}
		List_next(&feasible_values[i]);
	}
}

void docheckEDAC()
{
	int i;
	docheckFDAC();
	if((Top-Bottom)>1)
	{
		for(i=0;i<NVar;i++)
		{
			if(assignments[i]==-1)
			{
				checkSupportU(i);
				if(WCSPexistsExistentialSupport(i)==FALSE)
				{
					showEAC(i);
				}
			}
		}
	}
}



func3_t CheckACFunc[] = {
  docheckNCstar,
  docheckACstar,
  docheckDAC,
  docheckFDAC,
  docheckEDAC
};

