/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: w_ac.c
  $Id: w_ac.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors:
     Simon de Givry (1), Federico Heras,(2)
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "wcsp.h"
#include "solver.h"

#define MSUP 0

int findSupportU(int j, int *flag)
/*Projects unary costs of variable j to the lower bound
 If it produces node inconsistency, it returns FALSE
 flag becomes TRUE iff the lower bound is increased*/
{
  int val_min,l2,stop;
  cost_t min;

  *flag=FALSE;
  stop=FALSE;
  if(current_domains[j][SupU[j]]!=-1 || CostU(j,SupU[j])!=BOTTOM)
    {
      min=Top; val_min=-1;
      List_reset_traversal(&feasible_values[j]);
      while(!List_end_traversal(&feasible_values[j]) && min!=BOTTOM)
	{
	  l2=List_consult1(&feasible_values[j]);
	  List_next(&feasible_values[j]);
	  //	  NChecks++;
	  if(min>CostU(j,l2)){min=CostU(j,l2); val_min=l2;}
	}
      if(min>BOTTOM)
	{
	  *flag=TRUE;
	  Bottom+=min;
	  if (SearchMethod != DFBB && depth != 0) {
	    ClusterBottom[Var2Cluster[j]]+=min;
	    if (BTDINCURRENTCLUSTERTREE(j)) {
	      //	      printf("[%d] BottomCurrentClusterTree += %d (findSupportU %d)\n",depth,min,j); // A RETIRER
	      BottomCurrentClusterTree+=min;
	      //	      assert(BottomCurrentClusterTree == subtreeBottom(CurrentCluster));
	      if(BottomCurrentClusterTree >= TopCurrentClusterTree) stop=TRUE;
	    }
	  }
	  DeltaU[j]+=min;
#ifdef NOGLOBALCUT
	  if(Bottom>=Top && (SearchMethod == DFBB || depth == 0)) stop=TRUE;
#else
	  if(Bottom>=Top) stop=TRUE;
#endif
	}
      SupU[j]=val_min;
    }
  return(!stop);

}


/* findSupport as in the paper submitted to the AIJ */
int findSupportNext(int i, int j, int *flag1, int *flag3)
     /*Projects the binary constraint between i and j to the
       unary constraint of i. Then it projects the unary constraint of i
       to the lower bound.
       It finds the next support (it starts searching after the current support)
       It returns FALSE iff inconsistency is found.
       Flag1 becomes TRUE iff the lower bound is increased.
       Flag3 becomes TRUE iff some unary cost of i is increased*/
{
  int a,b,c,s,k,val_min,flag2;
  cost_t min;

  *flag1=FALSE;
  flag2=FALSE;
  *flag3=FALSE;
  List_reset_traversal(&feasible_values[i]);
  while(!List_end_traversal(&feasible_values[i]))
    {
      a=List_consult1(&feasible_values[i]);
      List_next(&feasible_values[i]);
      s=SupB[i][j][a];
      if(current_domains[j][s]!=-1)
	{
	  min=Top; val_min=-1;
	  for(k=0;k<NValues;k++)
	    {
	      b=(s+k)%NValues;
	      if(current_domains[j][b]==-1 && CostB(i,a,j,b)<min)
		{
		  min=CostB(i,a,j,b); val_min=b;
		}
	      if (min==BOTTOM) k=NValues;
	    }
	  if(min>BOTTOM)
	    {
	      *flag3=TRUE;
	      List_insert3pc(&l3[depth],i,j,a,min);
	      if(SupU[i]==a)flag2=TRUE;
	      UNARYCOST(i,a)+=min;
	      UNARYCOSTMOVE(i,a,min,j,c);
	      DeltaB[i][j][a]+=min;
	      if(CostU(i,a)-min==BOTTOM)
		{
		  stream_push(Rdac,i);
		  stream_push(S,i);
		}
	    }
#if (MSUP)
	  List_insert4(&l4[depth],a,i,j,SupB[i][j][a]);
#endif
	  assert(val_min != -1);
	  SupB[i][j][a]=val_min;
	}
    }
  if(flag2) return(findSupportU(i,flag1));
  else return(TRUE);
}


/* findSupport as in the AAAI-02 paper */
int findSupportFirst(int i, int j, int *flag1, int *flag3)
     /*Projects the binary constraint between i and j to the
       unary constraint of i. Then it projects the unary constraint of i
       to the lower bound.
       It finds the first support (it starts searching from the beginning).
       It returns FALSE iff inconsistency is found.
       Flag1 becomes TRUE iff the lower bound is increased.
       Flag3 becomes TRUE iff some unary cost of i is increased
       d_inc and supports record changes for context recovery upoon backtracing*/
{
  int a,b,c,k,val_min,flag2,flag4;
  cost_t min;

  *flag1=FALSE;
  flag2=FALSE;
  *flag3=FALSE;
  flag4=FALSE;
  List_reset_traversal(&feasible_values[i]);
  while(!List_end_traversal(&feasible_values[i]))
    {
      a=List_consult1(&feasible_values[i]);
      List_next(&feasible_values[i]);
     if(current_domains[j][SupB[i][j][a]]!=-1 || CostB(i,a,j,SupB[i][j][a])>BOTTOM)
	{
	  min=MAXCOST; val_min=-1;
	  List_reset_traversal(&feasible_values[j]);
	  while(!List_end_traversal(&feasible_values[j]) && min!=BOTTOM)
	    {
	      b=List_consult1(&feasible_values[j]);
	      List_next(&feasible_values[j]);
	      //	      NChecks++;
	      k=CostB(i,a,j,b);
	      if(k<min){min=k; val_min=b;}
	    }
	  if(min>BOTTOM)
	    {
	      *flag3=TRUE;
	      List_insert3pc(&l3[depth],i,j,a,min);
	      if(SupU[i]==a) flag2=TRUE;
	      UNARYCOST(i,a)+=min;
	      UNARYCOSTMOVE(i,a,min,j,c);
	      DeltaB[i][j][a]+=min;
	      if(CostU(i,a)-min==BOTTOM) flag4=TRUE;
	    }
#if (MSUP)
	  List_insert4(&l4[depth],a,i,j,SupB[i][j][a]);
#endif
	  assert(val_min != -1);
	  SupB[i][j][a]=val_min;

	}
    }
  if (flag4) {
    stream_push(Rdac,i);
    stream_push(S,i);
  }
  if(flag2) return(findSupportU(i,flag1));
  else return(TRUE);
}


int AC()
{
  int i,j,stop,flag1,flag3,change_lb;

  stop=FALSE;
  while(!stream_empty(Q) && !stop) {
    change_lb=FALSE;
    while(!stream_empty(Q) && !stop)
      {
	j=stream_pop_min(Q);
	for(i=0;i<NVar;i++)
	  /*	  if(i>j && assignments[i]==-1 && ISBINARY(i,j)) */
#ifdef NOCOSTMOVE
	  if((LcLevel==LC_AC || i>j) && assignments[i]==-1 && ISBINARY(i,j) && 
	     (SearchMethod == DFBB || depth == 0 || BTDSAMECLUSTERTREE(j,i)))
#else
	  if((LcLevel==LC_AC || i>j) && assignments[i]==-1 && ISBINARY(i,j))
#endif
	    {
	      if(!findSupportFirst(i,j,&flag1,&flag3))
		{
		  stop=TRUE;
		  i=NVar;
		}
	      else
		{
		  change_lb=(change_lb || flag1);
		  if(flag3 && prune_variable(i,&stop)) {stream_push(Q,i);}
		}
	    }
      }
    if(!stop && change_lb)
      pruneVars(&stop);
/*        for(i=0;!stop && i<NVar;i++) if(assignments[i]==-1) */
/*  	{ */
/*  	  if(prune_variable(i,&stop)) {stream_push(Q,i);} */
/*  	} */
  }

  return(!stop);
}

int findFullSupport(int i, int j,int *flag1, int *flag3)
/*order[i]<order[j]*/
{
	/*flag1 returns true if the lower bound is increased*/
	int a,b,c,val_min,flag2,flag4;
	cost_t min,beta,k;

	flag2=FALSE; *flag1=FALSE;
	flag4=FALSE;
	*flag3=FALSE;
	List_reset_traversal(&feasible_values[i]);
	while(!List_end_traversal(&feasible_values[i]))
	{
		a=List_consult1(&feasible_values[i]);
		List_next(&feasible_values[i]);
		if(current_domains[j][SupB[i][j][a]]!=-1
			|| CostB(i,a,j,SupB[i][j][a])+CostU(j,SupB[i][j][a])>BOTTOM)
		{
			min=MAXCOST; val_min=-1;
			List_reset_traversal(&feasible_values[j]);
			while(!List_end_traversal(&feasible_values[j]) && min!=BOTTOM)
			{
			  b=List_consult1(&feasible_values[j]);
			  List_next(&feasible_values[j]);
			  //			  NChecks++;
			  k=CostB(i,a,j,b)+CostU(j,b);
			  if(k<min)
			  {min=k; val_min=b;}
			}
			if(min>BOTTOM)
			{
			  *flag3 = TRUE;
				List_reset_traversal(&feasible_values[j]);
				while(!List_end_traversal(&feasible_values[j]))
				{
					b=List_consult1(&feasible_values[j]);
					List_next(&feasible_values[j]);
					beta=CostB(i,a,j,b);
					if(min>beta)
					{
						DeltaB[j][i][b]-=(min-beta);
						UNARYCOST(j,b)-=(min-beta);
						UNARYCOSTMOVE(j,b,-(min-beta),i,c);
						List_insert3pc(&l3[depth],j,i,b,-1*(min-beta));
#if (MSUP)
						List_insert4(&l4[depth],b,j,i,SupB[j][i][b]);
#endif
						SupB[j][i][b]=a;
					}
				}
				List_insert3pc(&l3[depth],i,j,a,min);
				UNARYCOST(i,a)+=min;
				UNARYCOSTMOVE(i,a,min,j,c);
				DeltaB[i][j][a]+=min;
				if(SupU[i]==a) flag2=TRUE;
				if(CostU(i,a)-min==BOTTOM) flag4=TRUE;
			}
#if (MSUP)
			List_insert4(&l4[depth],a,i,j,SupB[i][j][a]);
#endif
			assert(val_min != -1);
			SupB[i][j][a]=val_min;
		}
	}
	if (flag4) {
	  stream_push(Rdac,i);
	  stream_push(S,i);
	}
	if(flag2) return(findSupportU(i,flag1));
	else return(TRUE);
}


int ACplus()
{
	int i,j,stop,flag1,change_lb,flag3=FALSE;
	stop=FALSE;
	while(!stream_empty(Q) && !stop)
	{
		j=stream_pop_last_in(Q);
		change_lb=FALSE;
		for(i=0;i<NVar;i++)
			if(assignments[i]==-1 && ISBINARY(i,j))
			{
			  if(!findFullSupport(i,j,&flag1,&flag3))
			  {
				  stop=TRUE;
				  i=NVar;
			  }
			  else
			  {
				  change_lb=(change_lb || flag1);
				  if(prune_variable(i,&stop)) {stream_push(Q,i);}
			  }
			}
		if(!stop && change_lb)
		  pruneVars(&stop);
/*  			for(i=0;i<NVar;i++) if(assignments[i]==-1) */
/*  			{ */
/*  				if(prune_variable(i,&stop)) {stream_push(Q,i);} */
/*  			} */

	}
	return(!stop);
}


int DAC()
{
	int i,j,k,stop,flag1,changelb,flag3=FALSE;
	stop=FALSE;
	flag1=FALSE;
	changelb = FALSE;

	while(!stream_empty(Rdac) && !stop)
	{
		j=stream_pop_max(Rdac);

		if(assignments[j]==-1)
		{
		  if(prune_variable(j,&stop)) stream_push(Q,j); // TO BE REMOVED?

			if(!stop)for(k=order[j]-1;k>=0;k--)
			{
				i=order2[k];
#ifdef NOCOSTMOVE
				if(assignments[i]==-1 && ISBINARY(j,i) &&
				   (SearchMethod == DFBB || depth == 0 || BTDSAMECLUSTER(j,i)))
#else
				if(assignments[i]==-1 && ISBINARY(j,i))
#endif
				{
				  if(!findFullSupport(i,j,&flag1,&flag3)) {
				    stop=TRUE; k=-1;
				  } else {
				    changelb = (changelb || flag1);
				    if(flag3 && prune_variable(i,&stop)) {stream_push(Q,i);}
				  }
				}
			}
		}
	}
	if(changelb && !stop)
	{
	  pruneVars(&stop);
/*  		for(i=0;!stop && i<NVar;i++) */
/*  		{ */
/*  			if(assignments[i]==-1 && prune_variable(i,&stop)) */
/*  				{stream_push(Q,i);} */
/*  		} */
	}
	return(!stop);
}




int FDAC()
{
	int stop;
	stop=FALSE;
	while((!stream_empty(Rdac) || !stream_empty(Q)) && !stop)
	{
	  stop= !AC() || !DAC();
	}
	return(!stop);
}

int findFullSupportEpsilon(int i, int j,int *flag1,int *flag3,int *flag4)
{
	/* warning! flag3 is not reset */
	/*flag1 returns true if the lower bound is increased*/
	int a,b,c,val_min,flag2;
	cost_t min,beta,k;

	flag2=FALSE; *flag1=FALSE;
	List_reset_traversal(&feasible_values[i]);
	while(!List_end_traversal(&feasible_values[i]))
	{
		a=List_consult1(&feasible_values[i]);
		List_next(&feasible_values[i]);
		if(current_domains[j][SupB[i][j][a]]!=-1
		   || CostB(i,a,j,SupB[i][j][a])+CostU(j,SupB[i][j][a])>BOTTOM)
		{
			min=MAXCOST; val_min=-1;
			List_reset_traversal(&feasible_values[j]);
			while(!List_end_traversal(&feasible_values[j]) && min!=BOTTOM)
			{
			  b=List_consult1(&feasible_values[j]);
			  List_next(&feasible_values[j]);
			  //			  NChecks++;
			  k=CostB(i,a,j,b)+CostU(j,b);
			  if(k<min)
			  {min=k; val_min=b;}
			}
			if(min>BOTTOM)
			{
			  *flag3=TRUE;
				List_reset_traversal(&feasible_values[j]);
				while(!List_end_traversal(&feasible_values[j]))
				{
					b=List_consult1(&feasible_values[j]);
					List_next(&feasible_values[j]);
					beta=CostB(i,a,j,b);
					if(min>beta)
					{
						DeltaB[j][i][b]-=(min-beta);
						UNARYCOST(j,b)-=(min-beta);
						UNARYCOSTMOVE(j,b,-(min-beta),i,c);
						List_insert3pc(&l3[depth],j,i,b,-1*(min-beta));
#if (MSUP)
						List_insert4(&l4[depth],b,j,i,SupB[j][i][b]);
#endif
						SupB[j][i][b]=a;
					}
				}
				List_insert3pc(&l3[depth],i,j,a,min);
				UNARYCOST(i,a)+=min;
				UNARYCOSTMOVE(i,a,min,j,c);
				DeltaB[i][j][a]+=min;
				if(SupU[i]==a) flag2=TRUE;
				if(CostU(i,a)-min==BOTTOM) *flag4=TRUE;
			}
#if (MSUP)
			List_insert4(&l4[depth],a,i,j,SupB[i][j][a]);
#endif
			assert(val_min != -1);
			SupB[i][j][a]=val_min;
		}
	}
	if(flag2) return(findSupportU(i,flag1));
	else return(TRUE);
}

int isAFullSupportAC(int i,int a)
{
	int j,b,esupport,fsupport,stop1,stop2;

	if(CostU(i,a)==BOTTOM)
	{
		esupport=TRUE;
		stop1=FALSE;
		for(j=0;j<NVar && !stop1;j++)
		{
			if(assignments[j]==-1 && i>j && ISBINARY(i,j))
			{
				if(current_domains[j][SupB[i][j][a]]!=-1 ||
				   (CostB(i,a,j,SupB[i][j][a])+CostU(j,SupB[i][j][a]))>BOTTOM)
				{
					fsupport=FALSE;
					stop2=FALSE;
					List_reset_traversal(&feasible_values[j]);
					while(!List_end_traversal(&feasible_values[j]) && !stop2)
					{
						b=List_consult1(&feasible_values[j]);

						if(CostB(i,a,j,b)+CostU(j,b)==BOTTOM)
						{
#if (MSUP)
						        List_insert4(&l4[depth],a,i,j,SupB[i][j][a]);
#endif
							SupB[i][j][a]=b;
							fsupport=TRUE;
							stop2=TRUE;
						}
						List_next(&feasible_values[j]);
					} // end while b's
					if(fsupport==FALSE)
					{
						esupport=FALSE;
						stop1=TRUE;
					}
				}
			}
		} // end for j's
		if(esupport==TRUE)
		{
			SupU[i]=a;
			return TRUE;
		}
	}
	return FALSE;
}

int noFullSupportAC(int i)
{
	int a,sup;
	sup=SupU[i];

	if(current_domains[i][sup]==-1 && isAFullSupportAC(i,sup))
	{
		return FALSE;
	}
	else
	{
		List_reset_traversal(&feasible_values[i]);
		while(!List_end_traversal(&feasible_values[i]))
		{
			a=List_consult1(&feasible_values[i]);
			if(a!=sup)
			{
				if(isAFullSupportAC(i,a)) return FALSE;
			}
			List_next(&feasible_values[i]);
		} // end while a's
	}

	return TRUE;

}

void insertToP(myStream *s,int i,int self)
{
	int j;

	  if(self && (current_domains[i][SupU[i]]!=-1 || CostU(i,SupU[i])>BOTTOM)) stream_push(s,i);

	  for(j=0;j<NVar;j++)
	    {
			if(j>i && assignments[j]==-1 && ISBINARY(i,j) && (current_domains[j][SupU[j]]!=-1 || CostU(i,SupB[j][i][SupU[j]])>BOTTOM || CostB(j,SupU[j],i,SupB[j][i][SupU[j]])>BOTTOM))
			{
				stream_push(s,j);
			}
	    }
}


int findExistentialSupportAC(int i, int *flag3)
{
	int j,flag1=FALSE,flag4=FALSE,changelb=FALSE;

	if(noFullSupportAC(i))
	{
		for(j=0;j<NVar;j++)
		{
			if(assignments[j]==-1 && i>j && ISBINARY(i,j))
			{
			  if (!findFullSupportEpsilon(i,j,&flag1,flag3,&flag4)) {
			    return TRUE;
			  } else {
			    changelb = changelb || flag1;
			  }
			}
		}
		if (flag4) {
		  stream_push(Rdac,i);
		  insertToP(P,i,FALSE);
		}
	}
	return changelb;
}

void fillStackEAC()
{

	int i,j;
	stream_ini(P);
	while(!stream_empty(S))
	{
		i=stream_pop_min(S);

		stream_push(P,i);
		for(j=0;j<NVar;j++)
		{
			if(j>i && assignments[j]==-1 && ISBINARY(i,j) && (current_domains[j][SupU[j]]!=-1 || CostU(i,SupB[j][i][SupU[j]])>BOTTOM || CostB(j,SupU[j],i,SupB[j][i][SupU[j]])>BOTTOM))
			{
				stream_push(P,j);
			}
		}
	}
	stream_ini(S);
}

void pruneVars(int *stop)
{
	int j;

	for(j=0;!(*stop) && j<NVar;j++)
	{
#ifdef NOGLOBALCUT
		if(assignments[j]==-1 && 
		   (SearchMethod == DFBB || depth == 0 || BTDINCURRENTCLUSTERTREE(j)) && 
		   prune_variable(j,stop))
#else
		if(assignments[j]==-1 && prune_variable(j,stop))
#endif
		{
			stream_push(Q,j);
		}
	}

}


int EAC()
{
	int i,stop=FALSE,changelb=FALSE,flag3;

        fillStackEAC();

	while(!stream_empty(P) && !stop)
	{
		i=stream_pop_min(P);

		if(assignments[i]==-1)
		{
		  flag3=FALSE;
		  if(findExistentialSupportAC(i,&flag3))
			{
				if(Bottom>=Top || 
				   (SearchMethod!=DFBB && depth!=0 &&
				    BottomCurrentClusterTree >= TopCurrentClusterTree)) {
				  return FALSE;
				}
				changelb = TRUE;
			}
		  if(flag3 && prune_variable(i,&stop)) {stream_push(Q,i);}
		}
	}
	if(changelb && !stop)
	{
	  pruneVars(&stop);
/*  		for(i=0;!stop && i<NVar;i++) */
/*  		{ */
/*  			if(assignments[i]==-1 && prune_variable(i,&stop)) */
/*  				{stream_push(Q,i);} */
/*  		} */
	}
	return !stop;
}

int EDAC()
{
	// Existential directional arc consistency
	int stop;

#ifdef NOGLOBALCUT
			  assert(LcLevel==LC_EDAC && !NOGLOBALCUT);
#endif

	stop=FALSE;

	while(!stop && (!stream_empty(Rdac) || !stream_empty(Q) || (Bottom<Top-1 && !stream_empty(S))))
	{
		if(Bottom<Top-1) stop=!EAC();
		if(!stop) stop=!DAC();
		if(!stop) stop=!AC();
	}
	return(!stop);
}

int NC() {return TRUE;}

func_t LcFunc[] = {NC, AC, DAC, FDAC, EDAC};
