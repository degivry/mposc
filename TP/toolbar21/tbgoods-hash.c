/* ---------------------------------------------------------------------
TOOLBAR - A constraint optimization toolbox

File: solver.h
$Id: tbgoods-hash.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

Authors: 
Simon de Givry (1), Federico Heras,(2) 
Javier Larrosa (2), Thomas Schiex (1)

(1) INRA, Biometry and AI Lab. Toulouse, France
(2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

Copyright 2003 by the authors.
This file is distributed under the Artistic License (See LICENSE file)
--------------------------------------------------------------------- */
/* A single huge hashtable for stocking separator costs. 
   It resolves collision by list chaining   */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "wcsp.h"
#include "solver.h"
#include "btd.h"
#include "tbgoods.h"

#define GOODVERBOSE 2
#define STATUSE 1

/* data for statistics on good usage */
int *StatGoodGetTot;
int *StatGoodGetLb;
int *StatGoodGetOpt;
int *StatGoodSetTot;
int *StatGoodSetLb;
int *StatGoodSetOpt;

// ---------------------------------------------------------------------
// Data types
// ---------------------------------------------------------------------
// we use a single hashtable and need a lot of space for keys
typedef unsigned long long int Keytype;
typedef struct Hashentry Hashentry;

struct Hashentry
{
	Hashentry* next;
	Keytype key;
#ifdef STATUSE
	unsigned long int usage;
#endif
	cost_t val;
};
// ---------------------------------------------------------------------
// Globals
// ---------------------------------------------------------------------
static unsigned int TableSize;
static unsigned int TableLoad;

Keytype *SeparatorOffset;

Hashentry **HashTable;
Hashentry *FreeList;

// These primes are too close to power of 2. We need better ones.
static const unsigned primes[] = {
	1,2,3,7, 17, 31, 61, 127, 251, 509, 1021, 2039, 4093, 8191, 16381, 32749, 65521,
	131071, 262139, 524287, 1048573, 2097143, 4194301, 8388593, 16777199, 
	33554383, 67108747, 134217593, 268435723, 536871449, 1073742913, 
	2147485829UL, 4294967291UL, 0
};

// ---------------------------------------------------------------------
// The hash function is a stupid modulo function which 
// should work fine with primes
// ---------------------------------------------------------------------
static unsigned hash(Keytype key)
{
	return key % TableSize;
}

// ---------------------------------------------------------------------
// Link a list in a block of free Hashentry
// ---------------------------------------------------------------------
void linkList(Hashentry bloc[], int size)
{
	size--;
	bloc[size].next = NULL;
#ifdef STATUSE
	bloc[size].usage = 0L;
#endif
	while (size > 0) {
		bloc[size-1].next = &bloc[size];
#ifdef STATUSE
		bloc[size-1].usage = 0L;
#endif
		size--;
	}
}

// ---------------------------------------------------------------------
// Allocate a hash and suitable conses for the required number of elemts
// ---------------------------------------------------------------------
void hashInit(int pow2)
{	
	TableSize = primes[pow2];
	TableLoad = 0;
	
	if (Verbose >= GOODVERBOSE) printf("Allocating %d buckets (size %u)\n",TableSize,sizeof(Hashentry *));
	HashTable = mycalloc(TableSize,sizeof(Hashentry *));
	
	//the rest will be used as CONS
	if (Verbose >= GOODVERBOSE) printf("Allocating %d Conses (size %u)\n",TableSize,sizeof(Hashentry));
	FreeList = mymalloc(TableSize,sizeof(Hashentry));
	linkList(FreeList,TableSize);
}

//#define TESTULLD(ULL,D) assert((double)(ULL) == (D));
#define TESTULLD(ULL,D) if (!((double)(ULL) == (D))) {perror("Error: separator size too large for the hash table key!");exit(1);}
// ---------------------------------------------------------------------
/* allocate memory for goods */
// ---------------------------------------------------------------------
void initGood(unsigned int pow2)
{
	int c,i;
	unsigned long long int size;
	double dsize;
	double dCumSize = 0.0;
	
	SeparatorOffset =  (Keytype *) mycalloc(NbCluster+1, sizeof(Keytype));
	SeparatorOffset[0] = 0;
	
	for (c = 0 ; c < NbCluster ; c++) {
		size = 1; dsize = 1.0;
		for (i = 1; i <= Separator[c][0] ; i++) {
			size *= DOMSIZE(Separator[c][i]);
			dsize *= DOMSIZE(Separator[c][i]);
			TESTULLD(size,dsize);
		}
		SeparatorOffset[c+1] = SeparatorOffset[c]+size;
		dCumSize += dsize;
		TESTULLD(SeparatorOffset[c+1],dCumSize);
	}

	if (Verbose)
		printf("All separators represent %lld values to handle\n",SeparatorOffset[NbCluster]);

	hashInit(pow2);

	StatGoodSetTot = (int *) mycalloc(NbCluster, sizeof(int));
	StatGoodSetLb = (int *) mycalloc(NbCluster, sizeof(int));
	StatGoodSetOpt = (int *) mycalloc(NbCluster, sizeof(int));
	StatGoodGetTot = (int *) mycalloc(NbCluster, sizeof(int));
	StatGoodGetLb = (int *) mycalloc(NbCluster, sizeof(int));
	StatGoodGetOpt = (int *) mycalloc(NbCluster, sizeof(int));  
}
// ---------------------------------------------------------------------
// return the good value between c and its father wrt completeAssignment
// NB: it is useless to check for ULL overflows here, they would have
// been detected in initGood().
// ---------------------------------------------------------------------
cost_t getGood_(int son, int *completeAssignment)
{
	int *var_p =  &Separator[son][1];
	int *end = var_p + Separator[son][0];
	unsigned long long int index = ((Separator[son][0]>0)?completeAssignment[*var_p]:0);
	var_p++;
	
	while (var_p < end) {
		index *= DOMSIZE(*var_p);
		index += completeAssignment[*var_p];
		var_p++;
	}
	
	Keytype key = index+SeparatorOffset[son];
	Hashentry *iter = HashTable[hash(key)];

	while (iter) {
		if (key == iter->key) return iter->val;
		else iter = iter->next;
	}
	return -MAXCOST;
}

// ---------------------------------------------------------------------
// return the good value between c and its father wrt completeAssignment
// and set optimality to TRUE if this good was an optimum 
// ---------------------------------------------------------------------
cost_t getGood(int son, int *completeAssignment, int *optimality)
{
	cost_t res = getGood_(son, completeAssignment);
	if (Verbose >= GOODVERBOSE) {
	  StatGoodGetTot[son]++;
	  if (res < BOTTOM) {
	    if (res!=-MAXCOST) {
	      StatGoodGetLb[son]++;
	    }
	  } else {
	    StatGoodGetOpt[son]++;
	  }
	}
	if (res < BOTTOM) {
		*optimality = FALSE;
		return (res==-MAXCOST)?BOTTOM:-res;
	} else {
	  *optimality = TRUE;
	  return res;
	}
}

// ---------------------------------------------------------------------
// set the good value between c and its father wrt completeAssignment 
// NB: it is useless to check for ULL overflows here, they would have
// been detected in initGood().
// ---------------------------------------------------------------------
void setGood_(int son, int *completeAssignment, cost_t v) 
{
	int *var_p =  &Separator[son][1];
	int *end = var_p + Separator[son][0];
	unsigned long long int index = ((Separator[son][0]>0)?completeAssignment[*var_p]:0);

	var_p++;
	while (var_p < end) {
		index *= DOMSIZE(*var_p);
		index += completeAssignment[*var_p];
		var_p++;
	}  
	Keytype key = index+SeparatorOffset[son];
	Hashentry *prev =NULL,*iter = HashTable[hash(key)];

	while (iter) {
		if (key == iter->key) {
#ifdef STATUSE
			iter->usage++;
/*  			if (Verbose) { */
/*  			  printf("repeat C%d: ", son); */
/*  			  PRINTCOST(v); */
/*  			  printf(" "); */
/*  			  PRINTCOST(iter->val); */
/*  			  printf("\n"); */
/*  			} */
#endif
			assert(iter->val < 0);
			assert(iter->val > -MAXCOST);
			if (v >= 0 || v < iter->val) { // mise a jour uniquement si mieux
			  iter->val=v; 
			} else {
/*  			  if (Verbose >= GOODVERBOSE) { */
/*  			    printf("useless repeat C%d: ",son); */
/*  			    PRINTCOST(v); */
/*  			    printf(" "); */
/*  			    PRINTCOST(iter->val); */
/*  			    printf("\n"); */
/*  			  } */
			}

			return; }
		else { prev = iter; iter = iter->next;}
	}
	
	// necessarily iter == NULL here
	// do we have CONSes ?
	if (!FreeList) {
		if (Verbose >= GOODVERBOSE) printf("Allocating %d extra Conses (size %u)\n",TableSize,sizeof(Hashentry));			
		FreeList = mymalloc(TableSize,sizeof(Hashentry));
		linkList(FreeList,TableSize);
	}
	
	iter = FreeList;
	FreeList = FreeList->next;
	if (prev) prev->next = iter;
	else HashTable[hash(key)]= iter;
	iter->key = key;
	iter->val =v;
	iter->next = NULL;
	TableLoad++;
}

// ---------------------------------------------------------------------
// set the good value between c and its father wrt completeAssignment
// recording also if this good was an optimum 
// ---------------------------------------------------------------------
void setGood(int son, int *completeAssignment, cost_t v, int optimality)
{
  if (Verbose >= GOODVERBOSE) {
      StatGoodSetTot[son]++;
      if (!optimality) {
	StatGoodSetLb[son]++;
      } else {
	StatGoodSetOpt[son]++;
      }
  }
  assert(v >= BOTTOM);
  if (v > BOTTOM || optimality == TRUE) { // impossible de distinguer BOTTOM et -BOTTOM
    setGood_(son, completeAssignment, (optimality)?v:-v);
  }
}

// ---------------------------------------------------------------------
// Computes usage stat (repeated call for the same separator assignment)
// ---------------------------------------------------------------------
unsigned long int MaxUsage(double *mean)
{
	int i;
	int num = 0;
	unsigned long Max = 0;
	double mymean = 0.0;
	int cluster = -1;

	for (i=0; i<TableSize; i++) {
		Hashentry *iter = HashTable[i];

		while (iter) {
			num++;
			if (iter->usage > Max) {
			  Max = iter->usage;
			  cluster = 0;
			  while (iter->key >= SeparatorOffset[cluster]) {
			    assert(cluster < NbCluster);
			    cluster++;
			  }
			  cluster--;
			  assert(cluster >= 0 && cluster < NbCluster);
			}
			mymean += iter->usage;
			iter = iter->next;
		}
	}
	mymean /= (double) num;
	*mean = mymean;
	if (Verbose) printf(" (C%d) ", cluster);
	return Max;
}

// ---------------------------------------------------------------------
// return statistics on good usage 
// ---------------------------------------------------------------------
void statGood(int cluster, int depth)
{  
	int i;
	double mean;	
	
	if (Verbose && depth==0) {
		printf("A total of %d elements are stored in the hash-table (%g %% overall, load factor = %f)\n",
			   TableLoad, 100*TableLoad/(double)SeparatorOffset[NbCluster],TableLoad/(double)TableSize);
#ifdef STATUSE
		printf("Repeated evaluation: %lu (max) %f (mean)\n",MaxUsage(&mean),mean);
#endif
	}
	if (Verbose >= GOODVERBOSE) {
		for (i=0; i<depth; i++) printf(" ");
		printf("C%d: #set=%d #set_opt=%5.1f%% #get=%d #get_lb=%5.1f%% #get_opt=%5.1f%% #get_lb+#get_opt=%5.1f%%\n", cluster, StatGoodSetTot[cluster], 100. * StatGoodSetOpt[cluster] / StatGoodSetTot[cluster], StatGoodGetTot[cluster], 100. * StatGoodGetLb[cluster] / StatGoodGetTot[cluster], 100. * StatGoodGetOpt[cluster] / StatGoodGetTot[cluster], 100. * StatGoodGetLb[cluster] / StatGoodGetTot[cluster] + 100. * StatGoodGetOpt[cluster] / StatGoodGetTot[cluster]);
		for (i=1; i<=Sons[cluster][0]; i++) {
			statGood(Sons[cluster][i], depth+2);
		}
	}
}
