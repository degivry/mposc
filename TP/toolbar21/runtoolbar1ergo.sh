#!/bin/sh

# usage: runtoolbar1ergo.sh problem.erg
# returns: problem initial_upperbound optimum(or "-" if unknown) nodes time(in seconds)

timelimit=3600

dir=`dirname $1`
base=`basename $1 .erg`
ub="2305843009213693951"
printf "${dir}/$base $ub "
./toolbarli -t${timelimit} -l3 -p10 -f3 $1 | awk 'BEGIN{opt="-";nodes=0;time=0} /Loglikelihood/{loglike=$0} /^Lower bound/{opt=$3; nodes=$5; time=$8;} /^Optimum/{opt=$2; nodes=$4; time=$7;} /^Best bound/{nodes=$5;time=$8;} /^No solution found/{nodes=$5;time=$8;} END{print opt,nodes,time,loglike;}'
./toolbarli -t${timelimit} -l3 -p10 -f3 $1 -M1 -T4 -k4 | awk 'BEGIN{opt="-";nodes=0;time=0} /Loglikelihood/{loglike=$0} /^Lower bound/{opt=$3; nodes=$5; time=$8;} /^Optimum/{opt=$2; nodes=$4; time=$7;} /^Best bound/{nodes=$5;time=$8;} /^No solution found/{nodes=$5;time=$8;} END{print opt,nodes,time,loglike;}'
