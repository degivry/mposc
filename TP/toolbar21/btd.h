/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: solver.h
  $Id: btd.h,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */

#ifndef BTD_H
#define BTD_H

/* connected=FALSE: all variables in the subtree are assigned to DOMSIZE()
 * lb: known lower bound for the current separator assignment
 * optimum=TRUE if optimality proof has been produced
 * unarymove: sum of unary cost moves on current separator assignment
 * depth: depth of complete assignment of the separator
 * propa: lowerbound deduced by propagation before subtree was disconnected
 * Note that lb already takes into account unarymove */
typedef struct {int connected; cost_t lb; cost_t initlb; int optimum; cost_t unarymove; int depth; cost_t propa; cost_t bottomove;} sepstatus_t;

typedef struct {cost_t lb; cost_t ub;} bounds_t;

/* Imp. note: every list contains in the first position its size then all its elements */
extern int CurrentCluster; /* current cluster solved by BTD */
extern int Root; /* root cluster */
extern int *Var2Cluster; /* cluster index for each variable */
extern int **Separator; /* list of separator variables for each cluster (relative to his father) */
extern sepstatus_t *SeparatorStatus; /* separator state computed only once when it is completly assigned */
extern int **ClusterTree2Vars; /* list of variables in the subtree rooted at a given cluster, except the variables which belong to the separator with its father */
extern int NbCluster; /* total number of clusters */
extern cost_t *ClusterBottom; /* split Bottom into clusters */ 
extern int *Father; /* for each cluster, the index of its cluster father */
extern int **Sons; /* list of cluster children for each cluster */
extern int *IsAncestor; /* parental relation between clusters */
extern int **Var2Separators; /* for each variable, the list of separator clusters where it appears */

extern cost_t TopCurrentClusterTree; /* special upper bound for current cluster subtree */
extern cost_t BottomCurrentClusterTree; /* specific lower bound for current cluster subtree */ 
extern cost_t *UnaryConstraintsBTD; /* save cost moves between clusters */

extern cost_t subtreeBottom(int cluster);
extern int isAncestor(int a, int c);
extern int cdist(int a, int c);

extern void btd(); /* main function for BTD exploiting goods and local consistency */
extern void fcbtd(); /* main function for FC-BTD exploiting goods and forward checking only */

#define ISANCESTOR(a,c) IsAncestor[a*NbCluster+c] /* TRUE if cluster a is an ancestor of cluster c */ 

/* restricted to the current cluster (subtree) */
#define BTDINCURRENTCLUSTER(i) (Var2Cluster[i]==CurrentCluster)
#define BTDINCURRENTCLUSTERTREE(i) (ISANCESTOR(CurrentCluster,Var2Cluster[i]))

/* forbid propagation between clusters */
#define BTDSAMECLUSTER(i,j) (Var2Cluster[i]==Var2Cluster[j])
#define BTDSAMECLUSTERTREE(i,j) (ISANCESTOR(Var2Cluster[i],Var2Cluster[j]))

/* get the cost removed from the cluster tree rooted at the Var2Cluster[i] cluster for the assignment of the variable i to the value a */
#define UNARYCOSTBTD(cluster,i,a) UnaryConstraintsBTD[cluster * NVar * NValues + i * NValues + a]

void UNARYCOSTMOVE(int i, int a, cost_t cost, int j, int c);

/*  #define UNARYCOSTMOVE(i,a,cost,j,c) \ */
/*      (if (SearchMethod != DFBB && depth!=0 && !BTDSAMECLUSTER(i,j) && BTDSAMECLUSTERTREE(i,j)) { \ */
/*         List_insert3pc(&l3BTD[depth],i,j,a,cost); \ */
/*         for (c=1;c<=Var2Separators[i][0]; c++) { \ */
/*           if (ISANCESTOR(Var2Separators[i][c],Var2Cluster[j])) { \ */
/*             assert(UNARYCOSTBTD(Var2Separators[i][c],i,a) < Top); \ */
/*             UNARYCOSTBTD(Var2Separators[i][c],i,a)+=cost; \ */
/*  	 } \ */
/*         } \ */
/*      }) */

#endif
