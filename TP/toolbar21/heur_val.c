/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: heurval.c
  $Id: heur_val.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"

void sort_val_bestsol_minunarycout(int var)
{
  int elem;
  int bestsolval = -1;

  if (BestSol[var]!=-1 && current_domains[var][BestSol[var]]==-1) { /* check preferred value */
    bestsolval = BestSol[var];
    List_insert_first(&feasible_values2[var],bestsolval); /* and insert it first */
  }
  List_reset_traversal(&feasible_values[var]);
  while(!List_end_traversal(&feasible_values[var]))
    {
      elem=List_consult1(&feasible_values[var]);
      List_next(&feasible_values[var]);
      if (elem != bestsolval) { /* do not reinsert preferred value */
	List_reset_traversal(&feasible_values2[var]);
	if (bestsolval != -1) { /* skip first preferred value */
	  List_next(&feasible_values2[var]);
	}
	while(!List_end_traversal(&feasible_values2[var]) && 
	      CostU(var, List_consult1(&feasible_values2[var]))<CostU(var,elem))
	  {
	    List_next(&feasible_values2[var]);
	  }
	List_insert1(&feasible_values2[var],elem);
      }
    }
}

void sort_val_minunarycout(int var)
{
  int elem;
  List_reset_traversal(&feasible_values[var]);
  while(!List_end_traversal(&feasible_values[var]))
    {
      elem=List_consult1(&feasible_values[var]);
      List_next(&feasible_values[var]);
      List_reset_traversal(&feasible_values2[var]);
      while(!List_end_traversal(&feasible_values2[var]) && 
	    CostU(var, List_consult1(&feasible_values2[var]))<CostU(var,elem))
	{
	  List_next(&feasible_values2[var]);
	}
      List_insert1(&feasible_values2[var],elem);
    }
}

void sort_val_minunarycoutRND(int var)
{
  int elem;
  List_reset_traversal(&feasible_values[var]);
  while(!List_end_traversal(&feasible_values[var]))
    {
      elem=List_consult1(&feasible_values[var]);
      HeurValRND[elem]=rand();
      List_next(&feasible_values[var]);
      List_reset_traversal(&feasible_values2[var]);
      while(!List_end_traversal(&feasible_values2[var]) && 
	    (CostU(var, List_consult1(&feasible_values2[var]))<CostU(var,elem)||
	    (CostU(var, List_consult1(&feasible_values2[var]))==CostU(var,elem) &&
	     HeurValRND[List_consult1(&feasible_values2[var])]<HeurValRND[elem])))
	{
	  List_next(&feasible_values2[var]);
	}
      List_insert1(&feasible_values2[var],elem);
    }
}

void sort_val_lex(int var)
{
  int elem;
  List_reset_traversal(&feasible_values[var]);
  while(!List_end_traversal(&feasible_values[var]))
    {
      elem=List_consult1(&feasible_values[var]);
      List_next(&feasible_values[var]);
      List_reset_traversal(&feasible_values2[var]);
      while(!List_end_traversal(&feasible_values2[var]) && 
	    List_consult1(&feasible_values2[var])<elem)
	{
	  List_next(&feasible_values2[var]);
	}
      List_insert1(&feasible_values2[var],elem);
    }
}

func2_t HeurValFunc[] = {sort_val_lex, sort_val_minunarycout,sort_val_minunarycoutRND,sort_val_bestsol_minunarycout};
