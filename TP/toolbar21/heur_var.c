/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: heurvar.c
  $Id: heur_var.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"

int selec_varSTATIC()
{
  int var_min=-1,j=0,i,min=0;

  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])==1) return(i);
      j=i;
      min=order3[i];
      var_min=i;
      i=NVar;
    }
  for(i=j+1;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])==1) return(i);
      if(order3[i]<min)
	{
	  min=order3[i];
	  var_min=i;
	}
    }
  return(var_min);
}

int selec_varRND()
{
  int dice,i;
  
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {if(List_n_of_elem(&feasible_values[i])==1) return(i);}

  dice=rand()%num_future;
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(dice==0) return i;
      else dice--;
    }
  return(-1);
}

int selec_varDOM()
{
  int var_min=-1,i,j=0, min=0;
  
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])==1) return(i);
      j=i;
      min=List_n_of_elem(&feasible_values[i]);
      var_min=i;
      i=NVar;
    }
  for(i=j+1;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])<min)
	{
	  min=List_n_of_elem(&feasible_values[i]);
	  var_min=i;
	  if(min==1)return(var_min);
	}
    }
  return(var_min);
}

int selec_varDOMDG()
{
  int var_min=-1,i,j=0;
  float min=0.0;
  
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      j=i;
      if(List_n_of_elem(&feasible_values[i])==1) return(i);
      min=(float)List_n_of_elem(&feasible_values[i])/(float)(degree[i]+1);
      /*remove the "+1" in the previous line, if isolated variables are 
	eliminated*/
      var_min=i;
      i=NVar;
    }
  for(i=j+1;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])==1) return(i);
      if(((float)List_n_of_elem(&feasible_values[i])/
	  (float)(degree[i]+1)<min))
	{
	  min=(float)List_n_of_elem(&feasible_values[i])/
	    (float)(degree[i]+1);
      /*remove the "+1" in the previous line, if isolated variables are 
	eliminated*/
	  var_min=i;
	}
    }  
  return(var_min);
}

int selec_varJEROW()
{
  int var_min=-1,i,j=0,k,l,m,b=0,ties=1,dice;
  double w,min=0.0;

  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])==1)return(i);
      H[i]=1.0;
    }

  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      w=0.0;
      k=0;
      List_reset_traversal(&feasible_values[i]);
      while(!List_end_traversal(&feasible_values[i]))
	{ 
	  l=List_consult1(&feasible_values[i]); 
	  List_next(&feasible_values[i]);
	  k++;
	  w+=CostU(i,l);
	}
      if(w!=0)b++;
      H[i]+=(w/(double)k);
      
      for(j=i+1;j<NVar;j++)if(assignments[j]==-1 && ISBINARY(i,j) && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(j)))
	{
	  b++;
	  w=0.0;
	  k=0;
	  List_reset_traversal(&feasible_values[i]);
	  while(!List_end_traversal(&feasible_values[i]))
	    { 
	      l=List_consult1(&feasible_values[i]); 
	      List_next(&feasible_values[i]);
	      List_reset_traversal(&feasible_values[j]);
	      while(!List_end_traversal(&feasible_values[j]))
		{ 
		  m=List_consult1(&feasible_values[j]); 
		  List_next(&feasible_values[j]);
		  k++;
		  w+=CostB(i,l,j,m);
		}
	    }
	  H[i]+=(w/(double)k);
	  H[j]+=(w/(double)k);
	}
    }

  if(b==0) for(i=0;i<NConstr;i++) if(currentArity[i]>2) 
    for(k=0;k<ARITY(i);k++)
      {
	j=SCOPEONE(i,k);
	if(assignments[j]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(j))) H[j]+=WEIGHTCONSTRAINT(i);
      }
  
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      min=(double)List_n_of_elem(&feasible_values[i])/H[i]; var_min=i;
      ties=1;
      j=i;
      i=NVar;
    }

  for(i=j+1;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      w=((double)List_n_of_elem(&feasible_values[i]))/H[i];
      if(w==min) ties++;
      if(w<min){min=w; var_min=i; ties=1;}
    } 
 
  if(HeurVar == VAR_JEROSLOW_LIKE || ties==1) return(var_min);
  else/* HeurVar == VAR_JEROSLOW_LIKE2 */
    {
      dice=rand()%ties;
      for(i=0;i<NVar;i++)
	if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)) && 
	   ((double)List_n_of_elem(&feasible_values[i]))/H[i]==min)
	{
	  if(dice==0) return i;
	  else dice--;
	}
    }
  return -1;
}

int selec_varSINGLETON()
{
  int var_max=-1,i,j=0;
  double max=0.;
  
  for(i=0;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])==1) return(i);
      j=i;
      max=H[i];
      var_max=i;
      i=NVar;
    }
  for(i=j+1;i<NVar;i++)if(assignments[i]==-1 && (SearchMethod==DFBB || BTDINCURRENTCLUSTER(i)))
    {
      if(List_n_of_elem(&feasible_values[i])==1) return(i);
      if(H[i] > max)
	{
	  max=H[i];
	  var_max=i;
	}
    }
  return(var_max);
}

int selec_varDUMMY(){
  printf("heuristica de seleccion de variable inexistente\n");
  return -1;}

func_t HeurVarFunc[] = {
  selec_varSTATIC, 
  selec_varSTATIC,
  selec_varSTATIC,
  selec_varRND,
  selec_varDOM,
  selec_varDOMDG,
  selec_varJEROW,
  selec_varJEROW,
  selec_varSINGLETON
};


void order_varLEX()
{
  int i;
  for(i=0;i<NVar;i++)  {order3[i]=i;}
}

void order_varDG()
{
  int i,j,max,max_arg;
  int *selected;

  selected=mymalloc(NVar,sizeof(int));
  for(i=0;i<NVar;i++)selected[i]=FALSE;
  for(i=0;i<NVar;i++)  
    {
      max=-1; max_arg=-1;
      for(j=0;j<NVar;j++)
	{
	  if(!selected[j] && degree[j]>max)
	    {max=degree[j];max_arg=j;}

	}
      order3[max_arg]=i;
      selected[max_arg]=TRUE;
    }
  free(selected);
}

void order_varRND()
{
  int i,j,dice;
  int *selected;

  selected=mymalloc(NVar,sizeof(int));
  for(i=0;i<NVar;i++)selected[i]=FALSE;
  for(i=0;i<NVar;i++)  
    {
      dice=rand()%(NVar-i);
      for(j=0;j<NVar;j++)
	{
	  if(!selected[j]) {
	    if (dice==0)
	      {
		order3[j]=i;
		selected[j]=TRUE;
		j=NVar;
	      }
	    else dice--;
	  }
	}
    }
  free(selected);
}

void order_varDUMMY(){
  printf("heuristica de seleccion de variable inexistente\n");}

func3_t HeurVar2Func[] = {
  order_varLEX,
  order_varDG,
  order_varRND,
  order_varLEX,
  order_varLEX,
  order_varLEX,
  order_varLEX,
  order_varLEX,
  order_varLEX
};

int donothing() {return -1;}

int updateVarRND() {
  order_varRND();
  return -1;
}

func_t UpdateHeurVarFunc[] = {
  donothing,
  donothing,
  donothing,
  updateVarRND,
  donothing,
  donothing,
  selec_varJEROW,
  selec_varJEROW,
  selec_varJEROW
};

int cmpVarNever(int *a, int *b) {
  fprintf(stderr, "Error: bad variable ordering heuristic for singleton consistency!\n");
  abort();
}

int cmpVarStatic(int *i,int *j)
{
  if (*i == *j) return 0;
  else if (assignments[*j]!=-1) return -1;
  else if (assignments[*i]!=-1) return 1;
  else if (order3[*i] <= order3[*j]) return -1;
  else return 1;
}

int cmpVarDom(int *i,int *j)
{
  if (*i == *j) return 0;
  else if (assignments[*j]!=-1) return -1;
  else if (assignments[*i]!=-1) return 1;
  else if (List_n_of_elem(&feasible_values[*i]) < List_n_of_elem(&feasible_values[*j])) return -1;
  else if ((List_n_of_elem(&feasible_values[*i]) == List_n_of_elem(&feasible_values[*j])) && (*i < *j)) return -1;
  else return 1;
}

int cmpVarDomDeg(int *i,int *j)
{
  if (*i == *j) return 0;
  else if (assignments[*j]!=-1) return -1;
  else if (assignments[*i]!=-1) return 1;
  else if (((float)(List_n_of_elem(&feasible_values[*i])))/((float)(degree[*i]+1)) < ((float)(List_n_of_elem(&feasible_values[*j])))/((float)(degree[*j]+1))) return -1;
  else if ((((float)(List_n_of_elem(&feasible_values[*i])))/((float)(degree[*i]+1)) == ((float)(List_n_of_elem(&feasible_values[*j])))/((float)(degree[*j]+1))) && (*i < *j)) return -1;
  else return 1;
}

int cmpVarJEROW(int *i,int *j)
{
  if (*i == *j) return 0;
  else if (assignments[*j]!=-1) return -1;
  else if (assignments[*i]!=-1) return 1;
  else if (((double)List_n_of_elem(&feasible_values[*i]))/H[*i] < ((double)List_n_of_elem(&feasible_values[*j]))/H[*j]) return -1;
  else return 1;
}

func4_t CmpHeurVarFunc[] = {
  cmpVarNever,
  cmpVarNever,
  cmpVarNever,
  cmpVarStatic, /* Warning! Randomized order (do not follow the variable ordering heuristic) */
  cmpVarDom,
  cmpVarDomDeg,
  cmpVarJEROW,
  cmpVarJEROW, /* TO BE DONE. Randomizing equal choices */
  cmpVarJEROW
};
