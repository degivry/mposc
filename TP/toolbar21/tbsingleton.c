/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File:solver.c
  $Id: tbsingleton.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */

#define VARSINGLETONCOEF 3.0

#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"
#include "tbsingleton.h"
extern int selec_varSTATIC();

/* singleton consistency data structure */
int *VarOrdering;
int CDISJ = FALSE; /* if true, prune_value can update Z */
int Success;
int *R;
VarVal_t *Z;
int ZIndex;

/* should be equivalent to prune_value(h,l) except there is no special update for constructive disjunction */
void prune_this_value(int h, int l)
/* prunes (h,l) and updates structures accordingly */
{
	List_delete_this_elem(&feasible_values[h],l); /* removes l from h's domain*/
	current_domains[h][l]=1; /* updates current_domains */
	List_insert2(&l1[depth],h,l); /* records the pruning for future context restoration */
}

/* show unary and binary cost functions */
void showCosts() {
  int i,j,b,a;

  printf("\nTop= ");
  PRINTCOST(Top);
  printf(" , Bottom= ");
  PRINTCOST(Bottom);
  printf("\n");
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      printf("%d: ",i);
      for (a=0; a<DOMSIZE(i); a++) {
	if (current_domains[i][a]==-1) {
	  PRINTCOST(CostU(i,a));
	  printf(" ");
	} else printf("* ");
      }
      printf("\n");
    }
  }
  if (Verbose >= 4) {
    for (i=0; i<NVar; i++) {
      if (assignments[i]==-1) {
	for (j=i+1; j <NVar; j++) {
	  if (assignments[j]==-1) {
	    if (ISBINARY(i,j)) {
	      for (a=0; a<DOMSIZE(i); a++) {
		for (b=0; b<DOMSIZE(j); b++) {
		  if (current_domains[i][a]==-1 && current_domains[j][b]==-1 && CostB(i,a,j,b) > BOTTOM) {
		    printf("(%d,%d),(%d,%d): ", i,a,j,b);
		    PRINTCOST(CostB(i,a,j,b));
		    printf(" ");
		  }
		}
	      }
	      printf("\n");
	    }
	  }
	}
      }
    }
  }
}

/* return the current number of values in unassigned variables */
int numberOfValues()
{
  int res = 0,i;
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      res += List_n_of_elem(&feasible_values[i]);
    }
  }
  return res;
}

/* singleton consistency with constructive disjunction and subproblem aggregation for variable i */
int singletonLCcurrentvar(int i, int *wipeout)
{
  int l; /* current value */
  int j;
  int bt;
  int flag, pruned;
  int nbpruned = 0;
  int cdnbpruned = 0;
  int k;
  cost_t previousBottom,lookAheadBottom,minLookAheadBottom;
  cost_t minNewLowerBound = Top, maxNewLowerBound = BOTTOM; /* HeurVar==VAR_SINGLETON */
  int merged;
  int prevZIndex;
  int prevprevZIndex;
  int flag3=FALSE;
  int flag4=FALSE;

  flag = FALSE;
  pruned = FALSE;

  merged = TRUE;
  previousBottom = Bottom;
  minLookAheadBottom = Top;

  /* init memory struct for Constructive Disjunction */
  CDISJ = TRUE;
  ZIndex = 0;
  prevZIndex = 0;
  prevprevZIndex = 0;
  Success = 0;
  for(j=0;j<NVar*NValues;j++) R[j]=0;

  num_future--;       /* one more variable is assigned */
  depth++;
  assignments[i]=1000;
  sort_fut_var();
  assignments[i]=-1;
  save_parameters1();
  for(j=0;j<NConstr;j++)
    {fut_var_r2[depth][j]=currentArity[j]; if(inScope(i,j))currentArity[j]--;}
  List_create(&feasible_values2[i]);
  (*HeurValFunc[HeurVal])(i);
  List_reset_traversal(&feasible_values2[i]);
  while(!List_end_traversal(&feasible_values2[i]))
    {
      l=List_consult1(&feasible_values2[i]); /* l is the new current value*/
      List_next(&feasible_values2[i]);
      assignments[i]=l;                /* l is assigned to i */
      SingletonNNodes++;
      List_create(&l1[depth]); List_create(&l2[depth]);
      List_create(&l3[depth]); List_create(&l3BTD[depth]); List_create(&l4[depth]);
      List_create(&l5[depth]); List_create(&l6[depth]);
      lookAheadBottom = Bottom;
      Bottom+=CostU(i,l);         /* Computes current distance */
      bt = (Bottom<Top &&
	    look_ahead(i,l) &&
	    (lookAheadBottom = Bottom, TRUE) &&
	    projectB(i) &&
	    (*LcFunc[LcLevel])());
      if (bt) {
	Success++;
	for (k=prevZIndex; k<ZIndex; k++) {
	  R[Z[k].thevar * NValues + Z[k].theval]++;
	}
	prevprevZIndex = prevZIndex;
	prevZIndex = ZIndex;
	if (lookAheadBottom == previousBottom) merged = FALSE;
	else if (lookAheadBottom < minLookAheadBottom) minLookAheadBottom = lookAheadBottom;
	if (HeurVar==VAR_SINGLETON) {
	  if (Bottom < minNewLowerBound) minNewLowerBound = Bottom;
	  if (Bottom > maxNewLowerBound) maxNewLowerBound = Bottom;
	}
      }
      restore_context();
      restore_parameters1();
      List_dispose_memory(&l1[depth]); List_dispose_memory(&l2[depth]);
      List_dispose_memory(&l3[depth]); List_dispose_memory(&l3BTD[depth]); List_dispose_memory(&l4[depth]);
      List_dispose_memory(&l5[depth]); List_dispose_memory(&l6[depth]);
      if (!bt) {
	ZIndex = prevZIndex;
	depth--;
	prune_this_value(i,l);
	depth++;
	nbpruned++;
	pruned = TRUE;
	if (CostU(i,l) == BOTTOM) flag = TRUE;
      }
    }
  assignments[i]=-1; /* deassignes current variable */
  for(j=0;j<NConstr;j++){currentArity[j]=fut_var_r2[depth][j];}
  List_dispose_memory(&feasible_values2[i]);
  num_future++; /* current variable becomes future */
  depth--;
  CDISJ = FALSE;

  if (Success == 0) { /* all values have been pruned */
    *wipeout = TRUE;
    return 0;
  }

  if (pruned) {
    stream_push(Q,i);
  }
  if (flag) {
    stream_push(Rdac,i);
	stream_push(S,i);
  }

  /* constructive disjunction */
  for (k=prevprevZIndex; k<prevZIndex; k++) {
    prune_this_value(Z[k].thevar,Z[k].theval);
    cdnbpruned++;
    stream_push(Q,Z[k].thevar);
    if (CostU(Z[k].thevar,Z[k].theval) == BOTTOM)
	{
		stream_push(Rdac,Z[k].thevar);
		stream_push(S,Z[k].thevar);
	}
  }

  /* subproblem aggregation by changing DAC order */
  if (merged && LcLevel != LC_EDAC) {
    for (j=0; j<NVar; j++) {
      if (assignments[j]==-1 && j != i && ISBINARY(j,i)) {
	if (!findFullSupportEpsilon(i,j,&flag,&flag3,&flag4)) {
	  *wipeout = TRUE;
	  return 0;
	}
      }
    }
    if (flag4) {
      stream_push(Rdac,i);
    }
    if(flag3 && prune_variable(i,wipeout)) {stream_push(Q,i);}
    if(*wipeout) return 0;
  }

  /* enforce local consistency */
  for(j=0;j<NVar;j++) {
    if(assignments[j]==-1 && !findSupportU(j,&flag)) { /* and also prune_variable ??? */
      *wipeout = TRUE;
      return 0;
    }
  }
  if (!(*LcFunc[LcLevel])()) {
    *wipeout = TRUE;
    return 0;
  }

  if (HeurVar==VAR_SINGLETON) {
    H[i] = minNewLowerBound + VARSINGLETONCOEF * maxNewLowerBound;
  }

  if (Verbose>=2 && (nbpruned>=1 || cdnbpruned>=1))
    printf("[%d] singleton(%d): %d+%d removals\n", depth, i, nbpruned, cdnbpruned);
  if (Verbose>=2 && merged) {
    printf("[%d] singleton(%d): improved lower bound by ", depth, i);
    PRINTCOST(minLookAheadBottom - previousBottom);
    printf("\n");
  }
  return nbpruned + cdnbpruned;
}

/* singleton consistency with constructive disjunction and subproblem aggregation */
int singletonLC(int *wipeout, int nbselectedvar) {
  int i,n=0,nbpruned=0;

  if (HeurVarFunc[HeurVar] != selec_varSTATIC) {
    UpdateHeurVarFunc[HeurVar]();
    qsort(VarOrdering, NVar, sizeof(int), (int (*)(const void *,const void *)) CmpHeurVarFunc[HeurVar]);
  }
  for (i=0; i<NVar; i++) {
    if (n >= nbselectedvar) break;
    if (assignments[VarOrdering[i]]==-1){
      nbpruned += singletonLCcurrentvar(VarOrdering[i], wipeout);
      if (*wipeout) return 0;
      n++;
    }
  }
  return nbpruned;
}

/* Koster's dominance rule applied on variable i, only for binary WCSPs */
int dominanceKcurrentvar(int i) {
  int a, u, j, b, dominated;
  cost_t max, sum;
  int flag, pruned;
  int nbpruned = 0;

  /* do nothing if non binary constraints remain on variable i */
  for(j=0;j<NConstr;j++) {
    if (inScope(i,j) && currentArity[j]>=3) return 0;
  }

  flag = FALSE;
  pruned = FALSE;
  for (b=0; b <DOMSIZE(i); b++) {
    if (current_domains[i][b]==-1) {
      dominated = 0;
      for (a=0; a <DOMSIZE(i); a++) {
	if (a!=b && current_domains[i][a]==-1) {
	  sum = CostU(i,a);
	  /*	  if (sum >= Top) continue; */
	  for (j=0; j<NVar; j++) {
	    if (assignments[j]==-1 && i!=j && ISBINARY(i,j)) {
	      max = -MAXCOST;
	      for (u=0; u<DOMSIZE(j); u++) {
		if (current_domains[j][u]==-1) {
		  if (CostB(i,a,j,u) >= Top) {
		    if (CostB(i,b,j,u) < Top) {
		      max = Top;
		      break;
		    }
		  } else {
		    if (CostB(i,b,j,u) < Top) {
		      if (CostB(i,a,j,u) - CostB(i,b,j,u) > max) {
			max = CostB(i,a,j,u) - CostB(i,b,j,u);
		      }
		    }
		  }
		}
	      }
	      if (max == -MAXCOST) {
		sum = 0; 
		break;
	      } else if (sum+max>=Top) {
		sum = Top; 
		break;
	      } else {
		sum += max;
	      }
	    }
	  }
	  if (CostU(i,b) >= sum) {
	    dominated = 1;
	    break;
	  }
	}
      }
      if (dominated) {
	prune_this_value(i,b);
	nbpruned++;
	pruned = TRUE;
	if (CostU(i,b) == BOTTOM) flag = TRUE;
      }
      if (List_n_of_elem(&feasible_values[i]) <= 1) break;
    }
  }
  if (pruned) {
    stream_push(Q,i);
  }
  if (flag) {
    stream_push(Rdac,i); 
	stream_push(S,i); 
  }
  if (Verbose>=2 && nbpruned>=1) printf("[%d] dominance(%d): %d removals\n", depth, i, nbpruned);
  return nbpruned;
}

/* Koster's dominance rule */
int dominanceK() {
  int i,nbpruned=0;
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1 && List_n_of_elem(&feasible_values[i]) >= 2){
      nbpruned += dominanceKcurrentvar(i);
    }
  }
  return nbpruned;
}

/* metalevel consistency:
    - iterated dominance and iterated singleton consistency at the root node
    - single dominance and restricted singleton consistency at the other nodes */
int metaLC(int iterated, int dominance, int singleton, int nbselectedvar)
{
  int i,nbpruned,flag,wipeout,previousBottom;
	  
  do {
    nbpruned = 0;
    previousBottom = Bottom;
    /* dominance rules */
    if (dominance) {
      nbpruned += dominanceK();
      for(i=0;i<NVar;i++)if(assignments[i]==-1 && !findSupportU(i,&flag)) {
	return FALSE;
      }
      if (!(*LcFunc[LcLevel])()) {
	return FALSE;
      }
    }
    /* singleton consistency */
    if (singleton) {
      wipeout = FALSE;
      nbpruned += singletonLC(&wipeout, nbselectedvar);
      if (wipeout) {
	return FALSE;	      
      }
    }
#if (AC_CHECK == 1)
    (*CheckACFunc[LcLevel])();
#endif
  } while (iterated && (nbpruned>=1 || Bottom>previousBottom));

  return TRUE;
}
