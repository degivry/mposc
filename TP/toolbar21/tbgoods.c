/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File:solver.c
  $Id: tbgoods.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "wcsp.h"
#include "solver.h"
#include "btd.h"
#include "tbgoods.h"

#define GOODVERBOSE 2
// #define CHECKLB 1

cost_t **GoodSeparator; /* list of separator for each cluster (relative to his father) */
/* each separator is represented by an array as it is done for nary constraints */
/* negative cost values are lowerbounds, positive cost values are optimum */
/* special cost value: -MAXCOST == lowerbound unknown (i.e. = 0) */

/* data for statistics on good usage */
int *StatGoodGetTot;
int *StatGoodGetLb;
int *StatGoodGetOpt;
int *StatGoodSetTot;
int *StatGoodSetLb;
int *StatGoodSetOpt;
int *StatGoodSetMiss;

typedef struct {int number; int level;} clusterbfs_t;

int levelCluster(int cluster)
{ 
  if (Father[cluster] == -1) return 0;
  else return(1+levelCluster(Father[cluster]));
}

int cmpCluster(clusterbfs_t *c1, clusterbfs_t *c2)
{
  if (c1->level == c2->level) return 0;
  else if (c1->level < c2->level) return -1;
  else return 1;
}

/* allocate memory for goods */
void initGood(unsigned int pow2)
{
  int c,e,i;
  double fsize;
  clusterbfs_t *clustersbfs;

  StatGoodSetTot = (int *) mycalloc(NbCluster, sizeof(int));
  StatGoodSetLb = (int *) mycalloc(NbCluster, sizeof(int));
  StatGoodSetOpt = (int *) mycalloc(NbCluster, sizeof(int));
  StatGoodGetTot = (int *) mycalloc(NbCluster, sizeof(int));
  StatGoodGetLb = (int *) mycalloc(NbCluster, sizeof(int));
  StatGoodGetOpt = (int *) mycalloc(NbCluster, sizeof(int));  
  StatGoodSetMiss = (int *) mycalloc(NbCluster, sizeof(int));  

  GoodSeparator =  (cost_t **) mycalloc(NbCluster, sizeof(cost_t *));
  clustersbfs = (clusterbfs_t *) mycalloc(NbCluster, sizeof(clusterbfs_t));
  for (c = 0 ; c < NbCluster ; c++) {
    clustersbfs[c].number = c;
    clustersbfs[c].level = levelCluster(c);
  }
  qsort(clustersbfs, NbCluster, sizeof(clusterbfs_t), (int (*)(const void *,const void *)) cmpCluster);
  for (e = 0 ; e < NbCluster ; e++) {
    c = clustersbfs[e].number;
    fsize = 1.;
    for (i = 1; i <= Separator[c][0] ; i++) {
      fsize *= (double) DOMSIZE(Separator[c][i]);
    }
    if (fsize <= (1UL << pow2) && fsize <= INT_MAX) {
      GoodSeparator[c] = (cost_t *) malloc(((size_t) fsize) * sizeof(cost_t));
      if (GoodSeparator[c] == NULL) {
	printf("warning! no good memorization for cluster C%d and remaining clusters\n", c);
	break;
      } else {
	for (i=0; i<(int) fsize; i++) {
	  GoodSeparator[c][i] = -MAXCOST;
	}
      }
    } else {
      printf("warning! no good memorization for cluster C%d with a separator size of %.0f greater than maximum limit of %lu\n", c, fsize, MIN((1UL << pow2),INT_MAX));
    }
  }
  free(clustersbfs);
}

/* return the good value between c and its father wrt completeAssignment */
cost_t getGood_(int son, int *completeAssignment)
{
  int *var_p =  &Separator[son][1];
  int *end = var_p + Separator[son][0];
  cost_t *costs = GoodSeparator[son];
  if (costs != NULL) {
    int index = ((Separator[son][0]>0)?completeAssignment[*var_p]:0);
    var_p++;
    while (var_p < end) {
      index *= DOMSIZE(*var_p);
      index += completeAssignment[*var_p];
      var_p++;
    }
    return costs[index];
  } else {
    return(-MAXCOST);
  }
}

/* return the good value between c and its father wrt completeAssignment
   and set optimality to TRUE if this good was an optimum */
cost_t getGood(int son, int *completeAssignment, int *optimality)
{
/*    *optimality = FALSE; */
/*    return BOTTOM; */

  cost_t res = getGood_(son, completeAssignment);
  if (Verbose >= GOODVERBOSE) {
      StatGoodGetTot[son]++;
      if (res < BOTTOM) {
	if (res!=-MAXCOST) {
	  StatGoodGetLb[son]++;
	}
      } else {
	StatGoodGetOpt[son]++;
      }
  }
  if (res < BOTTOM) {
    *optimality = FALSE;
    return (res==-MAXCOST)?BOTTOM:-res;
  } else {
    *optimality = TRUE;
    return res;
  }
}

/* set the good value between c and its father wrt completeAssignment */
void setGood_(int son, int *completeAssignment, cost_t v) 
{
  int *var_p =  &Separator[son][1];
  int *end = var_p + Separator[son][0];
  cost_t *costs = GoodSeparator[son];
  if (costs != NULL) {
    int index = ((Separator[son][0]>0)?completeAssignment[*var_p]:0);
    var_p++;
    while (var_p < end) {
      index *= DOMSIZE(*var_p);
      index += completeAssignment[*var_p];
      var_p++;
    }
    assert(costs[index] < 0);
    if (v >= 0 || costs[index]==-MAXCOST || v < costs[index]) {
      costs[index] = v;
    }
  } else {
    if (Verbose >= GOODVERBOSE) {
      StatGoodSetMiss[son]++;
    }
  }
}

/* set the good value between c and its father wrt completeAssignment
   recording also if this good was an optimum */
void setGood(int son, int *completeAssignment, cost_t v, int optimality)
{
#ifdef CHECKLB
  FILE *file;
  char command[1024];
  int res;
#endif
  if (Verbose >= GOODVERBOSE) {
      StatGoodSetTot[son]++;
      if (!optimality) {
	StatGoodSetLb[son]++;
      } else {
	StatGoodSetOpt[son]++;
      }
  }
  assert(v >= BOTTOM);
#ifdef CHECKLB
  if (Verbose) printf("[%d] CHECKLB for C%d:\n", depth, son);
  system("\\rm checklb.wcsp");
  file = fopen("checklb.wcsp","w");
  saveWCSPsubtree(file, son, INT_MAX);
  fclose(file);
  SPRINTCOST(command, "expr ", " \\<= `toolbar checklb.wcsp | awk '{print $2}'`", v - SeparatorStatus[son].unarymove);
  res = system(command);
  if (Verbose) {
    printf("System call result: %d, Lower bound: ", res);
    PRINTCOST(v);
    printf(" - ");
    PRINTCOST(SeparatorStatus[son].unarymove);
    printf(" = ");
    PRINTCOST(v - SeparatorStatus[son].unarymove);
    printf("\n");
  }
  if (res != 0) {
    abort();
  }
#endif
  if (v > BOTTOM || optimality == TRUE) {
    setGood_(son, completeAssignment, (optimality)?v:-v);
  }
}

/* return statistics on good usage */
void statGood(int cluster, int depth)
{
  int i;

  if (Verbose >= GOODVERBOSE) {
    for (i=0; i<depth; i++) printf(" ");
    if (GoodSeparator[cluster] != NULL) {
      printf("C%d: #set=%d #set_opt=%5.1f%% #get=%d #get_lb=%5.1f%% #get_opt=%5.1f%% #get_lb+#get_opt=%5.1f%%\n", cluster, StatGoodSetTot[cluster], 100. * StatGoodSetOpt[cluster] / StatGoodSetTot[cluster], StatGoodGetTot[cluster], 100. * StatGoodGetLb[cluster] / StatGoodGetTot[cluster], 100. * StatGoodGetOpt[cluster] / StatGoodGetTot[cluster], 100. * StatGoodGetLb[cluster] / StatGoodGetTot[cluster] + 100. * StatGoodGetOpt[cluster] / StatGoodGetTot[cluster]);
    } else {
      printf("C%d: #set=%d #set_opt=%5.1f%% #get=%d #get_lb=%5.1f%% #get_opt=%5.1f%% #get_lb+#get_opt=%5.1f%% #set_miss=%d\n", cluster, StatGoodSetTot[cluster], 100. * StatGoodSetOpt[cluster] / StatGoodSetTot[cluster], StatGoodGetTot[cluster], 100. * StatGoodGetLb[cluster] / StatGoodGetTot[cluster], 100. * StatGoodGetOpt[cluster] / StatGoodGetTot[cluster], 100. * StatGoodGetLb[cluster] / StatGoodGetTot[cluster] + 100. * StatGoodGetOpt[cluster] / StatGoodGetTot[cluster], StatGoodSetMiss[cluster]);
    }
    for (i=1; i<=Sons[cluster][0]; i++) {
      statGood(Sons[cluster][i], depth+2);
    }
  }
}
