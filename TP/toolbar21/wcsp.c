/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wcsp.c
  $Id: wcsp.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
--------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h> 
#include <limits.h>
#include "wcsp.h"

/* initialize memory with zero */
void *mycalloc(int nobj, size_t size) 
{
  void *res = calloc((size_t) nobj, size);
  if (res == NULL) {
    perror("Error: not enough memory!");
    abort();
  } else {
    return res;
  }
}

void *mymalloc(int nobj, size_t size) 
{
  void *res = malloc(((size_t) nobj) * size);
  if (res == NULL) {
    perror("Error: not enough memory!");
    abort();
  } else {
    return res;
  }
}

/* Global variables with default values */

/* total number of variables */
int NVar = 0;
/* maximum number of values per domain */
int NValues = 0;
/* total number of n-ary constraints (with n>2) */
int NConstr = 0;
/* maximum arity of n-ary constraints */
int MaxArity = 0;

/* global lower bound */
cost_t Bottom = BOTTOM;
/* global upper bound */
cost_t Top = MAXCOST / 4;
/* Initial upper bound */
cost_t InitialUB = MAXCOST / 2;
/* resolution for probabilities (log10) */
int Resolution = 7;
/* initial cluster for BTD */
int ClusterRoot = -1;
int MaximalClusterDepth = 0;
int MaxSeparatorSize = INT_MAX;

/* number of nodes */
unsigned long NNodes = 0;
unsigned long SingletonNNodes = 0;
/* number of checks */
int NChecks = 0;
/* best solution found so far */
int *BestSol = NULL;
/* number of solutions better than Top */
int AllSolutions = FALSE; /* flag: TRUE = search for all solutions, FALSE = search optimum */
int NumberOfSolutions = 0;

/* level of local consistency */
LC_t LcLevel = LC_EDAC;

/* preProjection max arity : no preprojection*/
int preProject = 2;

/* selection of search method */
SearchMethod_t SearchMethod = DFBB;

/* main variable ordering heuristic */
HeuristicVar_t HeurVar = VAR_MIN_DOMBYDEG;

/* Value ordering */
HeuristicVal_t HeurVal = VAL_MINUNARYCOUNT;

/* Tree decomposition (heuristic used) */

HeuristicTreedec_t HeurTreedec = MAX_CARD;

bool_t TreedecMode = FALSE;
bool_t ExportMode = FALSE;
bool_t BEMode = FALSE;

/* First Cluster choice */
ClusterSelection_t ClusterSel = PRIMMAXEXPECTEDCOST;

/* default value for the size of the hash table (log2) 
   here 65k elements for around 1.5 Mbytes */
int Log2HashSize = 16;

int FileFormat = FORMAT_WCSP;

int Verbose = FALSE;
int TimeLimit = 0;
unsigned long NodeLimit = ULONG_MAX;
int Seed = 0;
int Options = 0; /* by default, no options */
int RestrictedSingletonNbVar = INT_MAX;
int ElimLevel = -1;

int IniDirection=FALSE; // For alternating variable ordering in DAC-based solvers
int WCSPdirection=FALSE; // For alternating variable ordering in DAC-based solvers
int WCSPmaxDACK=1; // For alternating variable ordering in DAC-based solvers
int max_sat_rules=FALSE;

char *Filename = NULL;

int Width=INT_MAX;
short *EliminationOrder=NULL;

int PreAssignNumber = 0;
int VariablePreAssign[MAXPREOP];
int ValuePreAssign[MAXPREOP];
int PreRemoveNumber = 0;
int VariablePreRemove[MAXPREOP];
int ValuePreRemove[MAXPREOP];

/* scopes of nary constraints + pointer to nary cost table (pointers to NARYSTACK memory) */
int **NaryConstraints = NULL;

/* cost tables of nary constraints */
cost_t **NaryCosts = NULL;

/* static mean costs of nary constraints */
double *MeanCosts = NULL;
/* and unary ones */
double *MeanUnaryCosts = NULL;

/* lists of nary constraint numbers where each variable appears (pointers to VARSTACK memory) */
int **Variables = NULL;

/* domain size for each variable */
int *DomainSize = NULL;

/* unary costs for any variable assignment */
cost_t *UnaryConstraints = NULL;

/* stack of binary constraint costs */
cost_t *BinaryConstraints = NULL;
/* current free binary constraint stack pointer */
cost_t *BinaryFree = NULL;
/* end of stack of binary constraint costs */
cost_t *BinaryFreeLimit = NULL;

/* binary constraint graph (NULL (no binary constraint) or pointer to BINARYCONSTRAINTS) */
cost_t **Graph = NULL;

/* create a binary constraint between i and j */
void createBinaryConstraint(int i, int j) {
  ISBINARY(i,j) = BinaryFree;
  ISBINARY(j,i) = BinaryFree;
  BinaryFree += DOMSIZE(i) * DOMSIZE(j);
}

/* destroy a binary constraint between i and j */
void destroyBinaryConstraint(int i, int j) {
  ISBINARY(i,j) = NULL;
  ISBINARY(j,i) = NULL;
  BinaryFree -= DOMSIZE(i) * DOMSIZE(j);
}

/* return the cost of assignment i := a and j := b */
cost_t binaryCost(int i, int j, int a, int b) {
  cost_t *c = NULL;
  if (i < j) {
    if ((c = ISBINARY(i,j))) {
      return c[a * DOMSIZE(j) + b];
    } else {
      return BOTTOM;
    }
  } else {
    if ((c = ISBINARY(j,i))) {
      return c[b * DOMSIZE(i) + a];
    } else {
      return BOTTOM;
    }
  }
}

/* set the cost of assignment i := a and j := b */
void setBinaryCost(int i, int j, int a, int b, cost_t v) {
  cost_t *c = NULL;
  if (i < j) {
    if ((c = ISBINARY(i,j))) {
      c[a * DOMSIZE(j) + b] = v;
    } else {
      fprintf(stderr,"Error: no binary constraint here!\n");
      abort();
    }
  } else {
    if ((c = ISBINARY(j,i))) {
      c[b * DOMSIZE(i) + a] = v;
    } else {
      fprintf(stderr,"Error: no binary constraint here!\n");
      abort();
    }
  }
}

/* return the cartesian product of domain sizes involved in the scope of the nary constraint i */
int cartesianProduct(int i) {
  int res = 1;
  int *var_p =  SCOPE(i);
  int *end = var_p + ARITY(i);
  while (var_p < end) {
    res *= DOMSIZE(*var_p);
    var_p++;
  }
  return res;
}

/* return the cost of a complete assignment for the nary constraint i */
cost_t cost(int i, int *completeAssignment){
  int *var_p =  SCOPE(i);
  int *end = var_p + ARITY(i);
  cost_t *costs = COSTS(i);
  int index = completeAssignment[*var_p];
  var_p++;
  while (var_p < end) {
    index *= DOMSIZE(*var_p);
    index += completeAssignment[*var_p];
    var_p++;
  }
  return costs[index];
}

/* set the cost of a tuple for the nary constraint i */
void setCost(int i, int *completeAssignment, cost_t v) {
  int *var_p =  SCOPE(i);
  int *end = var_p + ARITY(i);
  cost_t *costs = COSTS(i);
  int index = completeAssignment[*var_p];
  var_p++;
  while (var_p < end) {
    index *= DOMSIZE(*var_p);
    index += completeAssignment[*var_p];
    var_p++;
  }
  costs[index] = v;
}

int inScope(int i, int c) {
  int *var_p =  SCOPE(c);
  int *end = var_p + ARITY(c);
  while (var_p < end) {
    if (i == *var_p) return TRUE;
    var_p++;
  }
  return FALSE;
}

void allocateWCSP() {
  int nbinary = 0, i, j;

  for (i=0; i<NVar; i++) {
    for (j=i+1; j<NVar; j++) {
      if (ISBINARY(i,j)) nbinary++;
    }
  }

  /* allocate memory for dynamic nary constraint projections to new binary constraints */
  BinaryConstraints = mycalloc(MIN(NVar + NConstr,
				   ((NVar * (NVar - 1))/2 - nbinary)) * NValues * NValues, 
			       sizeof(cost_t));
  BinaryFreeLimit = BinaryConstraints + MIN(NVar + NConstr, ((NVar * (NVar - 1))/2 - nbinary)) * NValues * NValues * sizeof(cost_t);
  BinaryFree = BinaryConstraints;
}

/* check an assignment wrt to the  problem definition
   Should not be called after solve() */
cost_t certificateCheck(int *completeAssignment)
{
  cost_t Sum = BOTTOM;
  int i,j;

  if (Verbose >= 3) printf("Zero arity constraint = "); 
  if (Verbose >= 3) PRINTCOST(Bottom);
  Sum += Bottom;
  if (Verbose >= 3) printf("\n");

  if (Verbose >= 3) printf("Unary constraints:\n");
  for (i=0; i<NVar;i++) {
    if (Verbose >= 3) printf("%d ",i);
    Sum += UNARYCOST(i,completeAssignment[i]);
    if (Verbose >= 3) PRINTCOST(UNARYCOST(i,completeAssignment[i]));
    if (Verbose >= 3) printf("\n");
  }

  if (Verbose >= 3) printf("Binary Constraints\n");
  for (i=0; i<NVar-1; i++)
    for (j=i+1; j<NVar; j++){
      if (ISBINARY(i,j)) {
	if (Verbose >= 3) printf("[%d %d] ",i,j);
	Sum += BINARYCOST(i,j,completeAssignment[i],completeAssignment[j]);
	if (Verbose >= 3) PRINTCOST(BINARYCOST(i,j,completeAssignment[i],completeAssignment[j]));
	if (Verbose >= 3) printf("\n");
      }
    }

  if (Verbose >= 3) printf("Nary constraints\n");
  for (i=0;i<NConstr;i++) {
    if (Verbose >= 3) printf("C%d ", i);
    Sum += cost(i,completeAssignment);
    if (Verbose >= 3) PRINTCOST(cost(i,completeAssignment));
    if (Verbose >= 3) printf("\n");
  }
  if (Verbose) printf("Total cost = ");
  if (Verbose) PRINTCOST(Sum);
  if (Verbose) printf("\n");
  return Sum;
}

void allocateSol() {
  int i;

  /* set up BestSol */
  BestSol = mycalloc(NVar, sizeof(int));
  for (i=0; i<NVar; i++) {
    BestSol[i] = -1;
  }
}

void freeWCSP() {
  int i,j,k;
  cost_t *c;

  free(DomainSize);
  for (i=0; i<NVar; i++) {
    for (j=i+1; j<NVar; j++) {
      if ((c = ISBINARY(i,j)) && c < BinaryConstraints && c >= BinaryFreeLimit) {
	free(c);
      }
    }
  }
  free(Graph);
  free(UnaryConstraints);
  for (k=0; k<NConstr; k++) {
    free(NaryConstraints[k]);
    free(NaryCosts[k]);
  }
  free(NaryConstraints);
  free(NaryCosts);
  free(MeanCosts);
  for (i=0; i<NVar; i++) {
    free(Variables[i]);
  }
  free(Variables);
  free(BinaryConstraints);
}

void freeSol() {
  free(BestSol);
}
