/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File:solver.c
  $Id: tbgoods-never.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "wcsp.h"
#include "solver.h"
#include "btd.h"
#include "tbgoods.h"

/* dummy tbgood file for test purposes. NEVER memorize anything! */

void initGood(unsigned int pow2){}
void setGood(int son, int *completeAssignment, cost_t v, int optimality){}
void statGood(int cluster, int depth){}
cost_t getGood(int son, int *completeAssignment, int *optimality)
{ 
  *optimality = FALSE ;
  return BOTTOM;
}
