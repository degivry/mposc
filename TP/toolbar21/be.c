#include <memory.h>
#include <time.h>
#include "be.h"
#include "wcsp.h"
#include "ergoreader.h"

static double SolvingTime;

int	exactBuckets = 0;
short max_degree = 0;


//Maximum width exceeded. It deals with the implementation of calloc, as only accept unsigned int
void wLimitExceeded() {
	printf("The maximum width of %d has been exceeded. The current implementation of calloc limits this number.\n", WLIMIT);
	exit(0);
}

//Induced width of the function resulting from the sum and projection of bucket numbucket
short degreeBucket (short numbucket) {
	short i;
	float j;
	nodofunc *f;
	bool_t *v;

	v = mymalloc (NVar,sizeof(bool_t));
	for (i=0; i<NVar; i++) v[i] =FALSE;

	f = Bucket[numbucket].primera_func_original;
	while (f!=NULL) {
		//The first variable [0] is the one to be projected
		for (i=1; i<f->aridad; i++) v[f->vars[i]] = TRUE;
		f = f->sig_func;
	}

	f = Bucket[numbucket].primera_func_anadida;
	while (f!=NULL) {
		for (i=1; i<f->aridad; i++) v[f->vars[i]] = TRUE;
		f = f->sig_func;
	}

	j=0;
	for (i=0; i<NVar; i++) {
		if (v[i]==TRUE) {
			j=j+log2i(DOMSIZE(i));
		}
	} 

	free(v);
	return (ceil(j));
}

//It merges+orders in decrease order
//v1: vector of variables
//v2: vector of variables
//aridad: arity of the merge
short *merge_dec (short *v1, short aridad1, short *v2, short aridad2, short *aridad) {
	short i, j, aux, x;
	short *mezcla;
	short *res;
	bool_t *pertenece_a_mezcla;

	res=NULL;
	mezcla = mycalloc (NVar,sizeof(short));
	pertenece_a_mezcla = mycalloc (NVar,sizeof(bool_t));
	*aridad = 0;

	for (i=0; i<NVar; i++) {
		pertenece_a_mezcla[i] = FALSE;
	}

	for (i=0; i<aridad1; i++) {
		x = v1[i];
		if (!pertenece_a_mezcla[x]) {
			pertenece_a_mezcla[x] = TRUE;
			mezcla[*aridad] = x;
			(*aridad) ++;
			//pos++;
		}
	}

	for (i=0; i<aridad2; i++) {
		x = v2[i];
		if (!pertenece_a_mezcla[x]) {
			pertenece_a_mezcla[x] = TRUE;
			mezcla[*aridad] = x;
			(*aridad) ++;
		}
	}

	
	for (i=0;i<*aridad;i++)
	{
		for (j=i+1;j<*aridad;j++)
		{
			if (mezcla[i] < mezcla[j])
			{
				aux=mezcla[i];
				mezcla[i]=mezcla[j];
				mezcla[j]=aux;
			}
		}
	}

	
	res = mycalloc ((*aridad),sizeof(short));
	for (i=0; i<*aridad; i++) {
		res[i] = mezcla[i];
	}
	free (mezcla);
	free (pertenece_a_mezcla);
	return (res);
}

//The functions are inserted ordered in increase order of number of added variables
void assignBucket (nodofunc *g) {
	if ( g->aridad>0) {//The cost function still has a variable -> added to a bucket
		g->sig_func = Bucket[g->vars[0]].primera_func_anadida;
		Bucket[g->vars[0]].primera_func_anadida = g;
		Bucket [g->vars[0]].num_funcs++;
	} else if (g->aridad == 0) { //The cost function has zero-arity -> added to result functions
		g->sig_func = Result->primera_func_anadida;
		Result->primera_func_anadida = g;
		Result->num_funcs++;
			
	} else {
		#ifdef VERBOSE 
			printf("Nos viene una funciones con aridad negativa!!!! ERROR\n");
		#endif
	}

}

// Evaluates the cost function (nodo) in a given tuple (completeAssignment)
cost_t evalua(nodofunc *nodo, short *completeAssignment, short *pos_var)
{
	short i;
	long int pos;

	pos=0;
	for (i=0;i<nodo->aridad;i++)
	{
		pos=pos+(completeAssignment[pos_var[nodo->vars[i]]])*(nodo->despl_var[i]);
	}
	return nodo->costs[pos];
}


nodofunc *newCostFunction (short aridad, short *variables)
{
	nodofunc *nodo;
	long int i;

	nodo=mymalloc(1,sizeof(nodofunc));
	nodo->aridad=aridad;
	nodo->vars=mycalloc(aridad,sizeof(short));
	if (aridad > 0) {
		memcpy (nodo->vars, variables, aridad*sizeof(short));
	} 
	nodo->despl_var=mycalloc(aridad,sizeof(int));
 
	if (aridad>0) {
		nodo->despl_var[0]=1;
		nodo->ntuples=DOMSIZE( nodo->vars[0] );
	} else 
		nodo->ntuples=1; //The constant cost

	for (i=1; i<aridad; i++)
	{
		nodo->despl_var[i]=nodo->despl_var[i-1]*DOMSIZE( nodo->vars[i-1] );
		nodo->ntuples*=DOMSIZE( nodo->vars[i] );//num_bits;
	}
	//i=1<<suma_bits; // number of tuples of the function
	//nodo->ntuples=i;

	nodo->costs=mycalloc(nodo->ntuples,sizeof(cost_t));
	for (i=0; i<nodo->ntuples; i++){
		nodo->costs[i]=Top;
	}
	nodo->sig_func= NULL;

	return nodo;
}

/* Next assignment */
/* var0 var1 .... varN */
/* 1 0 ..... 0 */
/* 0 1 0 ... 0 */
/* 1 1 0 ... 0 */
/* ........... */
bool_t siguiente_asignacion (short num_vars, short *variables, short *tupla)
{
	short i;

	i=0;
	while ((i<num_vars)&&(tupla[i]==DOMSIZE(variables[i])-1))
	{
		tupla[i]=0;
		i++;
	}
	if (i<num_vars)
	{
		tupla[i]++;
		return FALSE;
	}
	else return TRUE;
}

cost_t evaluateBucket (bucket b, short *completeAssignment, short *posVar) {
	nodofunc *node;
	cost_t sum=BOTTOM;
	bool_t original=TRUE;

	if ((node = b.primera_func_original)==NULL) {
		node = b.primera_func_anadida;
		original=FALSE;
	}
	while (node!=NULL) {
		sum+=evalua (node,completeAssignment,posVar);
		if (node->sig_func==NULL && original) {
			node=b.primera_func_anadida;
			original=FALSE;
		} else node=node->sig_func;
	}
	return sum;
}

nodofunc* varProjection (bucket b) {
	short i;
	long int pos;
	nodofunc *nodo_res;
	short *completeAssignment;
	short var_proy;
	short *func_vars, *posVar, arity=0;
	bool_t *inScope, original, overflow;
	nodofunc *node;
	cost_t value, v;
	int dominio_var_elim;

	//Variables of the result function in decrease order
	func_vars = mycalloc (NVar, sizeof(short));
	inScope = mycalloc (NVar, sizeof(bool_t));

	if ((node = b.primera_func_original)!=NULL) { 
		original=TRUE;
	} else {
		node = b.primera_func_anadida;
		original=FALSE;
	}
	while (node!=NULL) {
		for (i=0; i<node->aridad; i++) {
			if (!inScope[node->vars[i]]) {
				inScope[node->vars[i]]=TRUE;
				func_vars[arity++]=node->vars[i];
			}
		}
		if (node->sig_func==NULL && original) {
			node=b.primera_func_anadida;
			original=FALSE;
		} else node=node->sig_func;
	}
	decreaseOrder(func_vars,arity);
	posVar = mycalloc (NVar, sizeof(short));
	for (i=0; i<arity; i++) posVar[func_vars[i]]=i;

	var_proy = func_vars[0];
	nodo_res=newCostFunction (arity-1,func_vars+1);

	completeAssignment=mycalloc(arity,sizeof(short));

	//Domain size of the variable to be projected
	dominio_var_elim=DOMSIZE(func_vars[0]);

	overflow=FALSE;
	pos = 0;
	while (!overflow) {
		completeAssignment[0]=0;
		value = Top;
		while (completeAssignment[0]<dominio_var_elim) {
			v = evaluateBucket (b, completeAssignment, posVar);
			if (value>v) value=v;
			completeAssignment[0]++;
		}
		nodo_res->costs[pos]=value;
		pos++;
		completeAssignment[0]--;
		overflow=siguiente_asignacion (arity,func_vars,completeAssignment);
	}

	free (posVar);
	free (completeAssignment);
	return (nodo_res);
}

cost_t constantFunctionsSum (bucket *b) {
	nodofunc *node;
	cost_t sum=BOTTOM;

		node = b->primera_func_anadida;
	while (node!=NULL) {
		sum+=node->costs[0];
		node=node->sig_func;
	}
	return sum;
}

nodofunc *searchMinibucketFunction (short i)
{
	nodofunc *nodo;

	if (Bucket[i].primera_func_anadida!=NULL) {
		nodo=Bucket[i].primera_func_anadida;
		Bucket[i].primera_func_anadida = nodo->sig_func;
	} else { 
		nodo=Bucket[i].primera_func_original;
		Bucket[i].primera_func_original = nodo->sig_func;
	} 

	Bucket[i].num_funcs--;
	nodo->sig_func=NULL;
	return nodo;
}

nodofunc *bestMiniBucketFunction (nodofunc **f, bool_t *pertenece_a_func, float w_actual, 
														float *w_anadido) {
	nodofunc *nodo;
	nodofunc *nodo_mejor;
	nodofunc *ant_mejor;
	nodofunc *ant;
	short j;
	float cuanto, mejor_cuanto;


	nodo=*f;
	ant=NULL;
	nodo_mejor=NULL;
	ant_mejor=NULL;

	mejor_cuanto=NValues*NVar+1;
	while (nodo!=NULL) {
		cuanto=0; // domain not included in the scope
		for (j=0;j<nodo->aridad;j++) {
			if (!pertenece_a_func[nodo->vars[j]])
			{
				cuanto=cuanto+log2i(DOMSIZE(nodo->vars[j]));
			}
		}
		if (cuanto<mejor_cuanto && ceil(w_actual+cuanto)<=Width)
		{
			ant_mejor = ant;
			nodo_mejor=nodo;
			mejor_cuanto=cuanto;
		}
		ant = nodo;
		nodo=nodo->sig_func;
	}
		

	*w_anadido=mejor_cuanto;

	// erase best node from the bucket
	if (nodo_mejor!=NULL) { //best node exists
		if (ant_mejor==NULL) {
			*f=nodo_mejor->sig_func;
		} else ant_mejor->sig_func=nodo_mejor->sig_func;
	}

	return nodo_mejor;
	
}

void minibuckets (short numbucket) {
	short j;
	float w_actual,w_anadido;
	nodofunc *nodo;
	nodofunc *f_proyectada;
	bool_t seguir;
	int num_funciones_restantes;
	bool_t *pertenece_a_func_actual; // variables included in the result function

	num_funciones_restantes=Bucket[numbucket].num_funcs;
	pertenece_a_func_actual=mycalloc(NVar,sizeof(bool_t));

	while (num_funciones_restantes>0)
	{

		for (j=0;j<NVar;j++)
		{
			pertenece_a_func_actual[j]=FALSE;
		}
		nodo=searchMinibucketFunction(numbucket); 
		w_actual=log2i(nodo->ntuples);
		num_funciones_restantes--;

		if (ceil(w_actual)<=Width) { 
			for (j=0;j<nodo->aridad;j++) 
			{
				pertenece_a_func_actual[nodo->vars[j]]=TRUE;
			}
			MB->primera_func_anadida=nodo;
			MB->num_funcs=1;
			seguir=TRUE;
			while ((num_funciones_restantes>0)&&(seguir)) {

				nodo = bestMiniBucketFunction (&Bucket[numbucket].primera_func_anadida, 
							pertenece_a_func_actual, w_actual, &w_anadido);

				if (nodo==NULL) nodo = bestMiniBucketFunction (&Bucket[numbucket].primera_func_original, 
											pertenece_a_func_actual, w_actual, &w_anadido);

				if (nodo!=NULL) {
					Bucket[numbucket].num_funcs--;
					w_actual=w_actual+w_anadido;
	
					for (j=0;j<nodo->aridad;j++) {
						if (!pertenece_a_func_actual[nodo->vars[j]]) {
							pertenece_a_func_actual[nodo->vars[j]]=TRUE;
						}
					}

					num_funciones_restantes--;
					nodo->sig_func=MB->primera_func_anadida;
					MB->primera_func_anadida=nodo;
					MB->num_funcs++;
				} else {
					seguir=FALSE;
				}
			}

			f_proyectada=varProjection (*MB);
			freeBucket(MB);
			assignBucket (f_proyectada);


		} //else, the function is skipped as it is too costly to compute
	}

	free (pertenece_a_func_actual);
}

void solveBE () {
	nodofunc *g;
	int i, d;
	cost_t s;

	initStructures();

  if (TimeLimit) {
    timer(TimeLimit);
  }

  SolvingTime = cpuTime();
  #ifdef UNIX
	signal(SIGINT,  timeout);
  #endif

	for (i=NVar-1; i>=0; i--) {
		if (Bucket[i].num_funcs!=0) {
			d = degreeBucket(i);
			//Bucket to be processed
			if (Verbose) 
				printf("Bucket %d with degree %d\n",i,d);
		

			if (d <= Width) { //bucket elimination 
				max_degree = MAX(max_degree,d); 
				if (d<=WLIMIT) { //Maximum width allowed regarding the memory implementation
					g = varProjection (Bucket[i]);
					assignBucket (g);
					exactBuckets++;
				} else 
					wLimitExceeded();
			} else { //minibucket elimination
				max_degree=Width;
				minibuckets (i);
			}

			freeBucket (Bucket+i); 
		} //else, there is nothing to do with that bucket
	}

	s = constantFunctionsSum(Result);
  SolvingTime = cpuTime() - SolvingTime;

  if (TimeLimit) {
    timerStop();
  }

  if (exactBuckets==NVar) {
    printf("Bucket Elimination: Optimum ");
    PRINTCOST(s);
    printf(" with maximal memory usage of 2^%d in %g sec and %d buckets processed exactly.\n", 
	   max_degree, SolvingTime, exactBuckets);
  } else {
    printf("Mini-Bucket Elimination: Lower bound ");
    PRINTCOST(s);
    printf(" with maximal memory usage of 2^%d in %g sec and %d buckets processed exactly.\n", 
	   max_degree, SolvingTime, exactBuckets);
  }
  if (FileFormat == FORMAT_BAYESNET)
    printf("Loglikelihood %lf (p = %le)\n", Cost2LogLike(s),Cost2Proba(s));
}

