
/* ---------------------------------------------------------------------
TOOLBAR - A constraint optimization toolbox

File: tblist.h
$Id: treedec.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

Authors: 
Simon de Givry (1), Federico Heras,(2) 
Javier Larrosa (2), Thomas Schiex (1)

(1) INRA, Biometry and AI Lab. Toulouse, France
(2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

Copyright 2003 by the authors.
This file is distributed under the Artistic License (See LICENSE file)
--------------------------------------------------------------------- */

#define TREEDECVERBOSE 3
#define DRAWFILLIN 1

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tbsystem.h"
#include "treedec.h"

int *Ordering=NULL;
int *InverseOrdering;
int **ElimGraph;
int *HeuristicArray;
JunctionGraph Junction;

int *separateurs;
int graphicFormat;

int getDegree (int vertex);

typedef void (*functvoid_t)();
extern functvoid_t ClusterSelectFunc[];

/*====================================================*/
/*                                                    */
/*       Main procedures (compute the treedec)        */
/*                                                    */
/*====================================================*/
void computeTreedec ()
{
	int tw, th, sep, totsep;
	double dcs, dss;
	int i,j,c,nbedge = 0;
	
	Ordering = (int *) mymalloc (NVar, sizeof (int));
	InverseOrdering = (int *) mymalloc (NVar, sizeof (int));
	
	initAdj ();
	
	for (i = 0 ; i < NVar ; i++) {
		for (j = i+1 ; j < NVar ; j++) {
			nbedge += (ElimGraph[i][j] ? 1 : 0);
		}
	}
	
	if (Verbose>=TREEDECVERBOSE) printf ("Constraint graph : %d vertices, %d edges.\n", NVar, nbedge); 
	
	switch (HeurTreedec) {
		case MAX_CARD:
			computeOrder (MAX_CARD);
			break;
		case MIN_FILL:
			computeOrder (MIN_FILL);
			break;
		case MIN_WIDTH:
			computeOrder (MIN_WIDTH);
			break;
		case MIN_DEGREE:
			computeOrder (MIN_DEGREE);
			break;
		case  MIN_WEIGHT:
			printf("Sorry, not implemented yet!\n");
			abort();
			computeOrder (MIN_WEIGHT);
			break;
		case KOSTER:
			KosterTreedec ();
			
			/* create a clique for each cluster */
			for (i = 0 ; i < NVar ; i++) {
				for (j = 0 ; j < NVar ; j++) {
					ElimGraph[i][j] = 0;
				}
			}
				for (c = 0 ; c < Junction.nCliques ; c++) {
					for (i= 1; i < Junction.cliques[c][0]; i++) {
						for (j= i + 1; j <= Junction.cliques[c][0]; j++) {
							ElimGraph[Junction.cliques[c][i]][Junction.cliques[c][j]] = EDGE;
							ElimGraph[Junction.cliques[c][j]][Junction.cliques[c][i]] = EDGE;
						}
					}
				}
				/* apply max cardinality heuristic to produce a variable elimination order */
				HeurTreedec = MAX_CARD;
			initHeuristicArray();
			setRoot();
			Ordering[NVar-1] = Junction.cliques[Junction.root][1];
			InverseOrdering[Ordering[NVar-1]] = NVar-1;
			removeVertex(Ordering[NVar-1]);
			for (i = NVar-2 ; i >= 0 ;i--)
			{
				Ordering[i] = getMaxHeuristic ();
				InverseOrdering[Ordering[i]] = i;
				removeVertex (Ordering[i]);
			}
				/* display a possible variable elimination order having the same treewidth as Koster */
				if (Verbose>=TREEDECVERBOSE) {
					printf("[");
					for (i = 0 ; i < NVar ; i++)
					{
						printf(" %d", Ordering[i]);
					}
					printf(" ]\n");
				}
				HeurTreedec = KOSTER;
			
			for (i = 0 ; i < NVar ; i++)
			{
				Ordering[i] = i;
				InverseOrdering[i] = i;
			}
				break;
		case LEXICO_ELIM:
			computeOrder (LEXICO_ELIM);
			break;
		case REVLEXICO_ELIM:
			computeOrder (REVLEXICO_ELIM);
			break;
		default:
			break;
    }
	
	if (Verbose>=TREEDECVERBOSE) {
		printf("\n");
		for (c = 0 ; c < Junction.nCliques ; c++) {
			printf("C%d %d:", c,Junction.cliques[c][0]);
			for (i= 1; i <= Junction.cliques[c][0]; i++) {
				printf(" %d", Ordering[Junction.cliques[c][i]]);	  
			}
			printf("\n");
		}
	}
	
	tw = getTreeWidth ();
	if (Verbose) printf("The treewidth (induced width) of the ordering is %d.\n", tw);
	//	th = setRoot ();
	th = getTreeHeight();
	if (Verbose) printf("The minimal tree-height of the tree decomposition is %d.\n", th);
	if (Verbose>=TREEDECVERBOSE) {printf("The corresponding root is %d.\n", Junction.root);}
	
	sep = getMaximumSeparatorSize ();
	if (Verbose) printf("The maximal separator size of the tree-decomposition is %d.\n", sep);
	
	totsep = getTotalSeparatorSize ();
	if (Verbose) printf("The total separator size of the tree-decomposition is %d.\n", totsep);
	
	dcs = getDomCliqueSize ();
	if (Verbose) printf("The parameter sum(prod(d)) for the set of cliques is %g.\n", dcs);
	
	dss = getDomSepSize ();
	if (Verbose) printf("The parameter sum(prod(d)) for the set of separators is %g.\n", dss);
	
	if (Verbose) testIfTreedec (Junction, ElimGraph, NVar, Ordering);
	
	if (ExportMode) exportTreedecToGraphic ();
}

/*====================================================*/
// Compute heuristic variable elimination orders
/*====================================================*/
void computeOrder (int heuristic)
{
	int i, j = 0;
/*  	int nb; */
/*  	int **connected; */
	
	//initAdj ();
	
	initHeuristicArray ();
	switch (heuristic)
    {
		case MAX_CARD:
			Ordering[NVar-1] = rand()%NVar; /* SdG rand()%(NVar-1); */
			InverseOrdering[Ordering[NVar-1]] = NVar-1;
			if (Verbose>=TREEDECVERBOSE) {printf("The variable choosen for the last position is %d.\n", Ordering[NVar-1]);}
				removeVertex(Ordering[NVar-1]); /* The name of the proc. is explicit enough... */
			for (i = NVar-2 ; i >= 0 ;i--) {
				Ordering[i] = getMaxHeuristic (); /* Get the max card vertex */
				InverseOrdering[Ordering[i]] = i;
				removeVertex (Ordering[i]);
			}
				/* We now have to compute the fill-in... */
				initAdj ();
			for (i = 0 ; i < NVar ; i++) {
				addFillIn (Ordering[i]); /* The fill-in is computed, but not taken
				into account for the heuristic matrix */
				removeVertex (Ordering[i]);
			}
				break;
		case MIN_FILL:
			for (i = 0 ; i < NVar ; i++) {
				Ordering[i] = getMinHeuristic (); /* Get the min fill vertex */
				InverseOrdering[Ordering[i]] = i;
				addFillIn (Ordering[i]);
				removeVertex (Ordering[i]);
			}
			break;
			
		case MIN_WIDTH:
			for (i = 0 ; i < NVar ; i++) {
				Ordering[i] = getMinHeuristic (); /* Get the min width vertex */
				InverseOrdering[Ordering[i]] = i;
				addFillIn (Ordering[i]); /* The fill-in is computed, but not taken
					into account for the heuristic matrix */
				removeVertex (Ordering[i]);
			}
			break;
			
		case MIN_DEGREE:
			for (i = 0 ; i < NVar ; i++) {
				Ordering[i] = getMinHeuristic (); /* Get the min degree vertex */
				InverseOrdering[Ordering[i]] = i;
				addFillIn (Ordering[i]); /* The fill-in is computed,*/
				removeVertex (Ordering[i]);
			}
			break;
			
		case MIN_WEIGHT:
			break;

		case LEXICO_ELIM:
			for (i = 0 ; i < NVar ; i++) {
			  Ordering[i] = NVar - i - 1;
			  InverseOrdering[Ordering[i]] = i;
			  addFillIn (Ordering[i]); /* The fill-in is computed,*/
			  removeVertex (Ordering[i]);
			}
			break;
		case REVLEXICO_ELIM:
			for (i = 0 ; i < NVar ; i++) {
			  Ordering[i] = i;
			  InverseOrdering[Ordering[i]] = i;
			  addFillIn (Ordering[i]); /* The fill-in is computed,*/
			  removeVertex (Ordering[i]);
			}
			break;

		default:
			/* use a predefined ordering in Ordering */
			/* We now have to compute the fill-in... */
			initAdj ();
			for (i = 0 ; i < NVar ; i++)
			{
				addFillIn (Ordering[i]); /* The fill-in is computed, but not taken
				into account for the heuristic matrix */
				removeVertex (Ordering[i]);
			}
				break;
    }
	
	if (Verbose>=TREEDECVERBOSE)
    {
		printf("Ordering found :\n[ ");
		for (i = 0 ; i < NVar ; i++) {
			printf("%d ", Ordering[i]);
		}
		printf("]\n");
		printf("Incidence matrix :\n");
		for (i = 0 ; i < NVar ; i++) {
			printf("| ");
			for (j = 0 ; j < NVar ; j++) {
				printf("%d ", ElimGraph[i][j]);
			}
			printf("|\n");
		}
    }
	
	if (BEMode) {
		EliminationOrder=mycalloc(NVar, sizeof(short));
		for (i=NVar-1; i>=0; i--) 
			EliminationOrder[NVar-1-i]=Ordering[i];
	}
	
	reorderMatrix ();
	
	buildJunctionGraph ();
	(*ClusterSelectFunc[ClusterSel])();
	if (MaxSeparatorSize < NVar) merging(&Junction, Junction.root, MaxSeparatorSize);

/*  	nb = 0; */
/*  	connected = (int **) mymalloc (Junction.nCliques, sizeof (int *)); */
	/*if ((nb = getConnectedComponents (Junction.edges, voidMatrix, connected, Junction.nCliques)) > 1)
    {
		printf ("Il y a plusieurs composantes...\n");
		for (i = 0 ; i < nb-1 ; i++)
		{
			Junction.edges[connected[i][1]][connected[i+1][1]] = -1;
			Junction.edges[connected[i+1][1]][connected[i][1]] = -1;
		}
	}*/
	
/*  	for (i = 0 ; i < nb ; i++) { */
/*  		free (connected[i]); */
/*      } */
/*  	free (connected); */
	
	if (Verbose>=TREEDECVERBOSE) {
		printf("Junction Weights :\n");
		for (i = 0 ; i < Junction.nCliques ; i++)	{
			printf("| ");
			for (j = 0 ; j < Junction.nCliques ; j++) {
				printf("%d ", Junction.edges[i][j]);
			}
			printf(" |\n");
		}
    }
	
	/* display the variable elimination order */
	if (Verbose>=TREEDECVERBOSE) {
		printf("[");
		for (i = 0 ; i < NVar ; i++) {
			printf(" %d", Ordering[i]);
		}
		printf(" ]\n");  
	}
}

/*************************************************/
/* This following procedure computes a tree-dec  */
/* with the method of Koster.                    */
/*************************************************/
void KosterTreedec ()
{
	int i;
	
	Junction.cliques = (int **) mymalloc (1, sizeof (int *));
	Junction.nCliques = 1; /* One cluster at the beginning */
	
	Junction.edges = (int **) mymalloc (Junction.nCliques, sizeof (int *));
	
	//  for (i = 0 ; i < Junction.nCliques ; i++) /* For each clique */
	//    {
	Junction.edges[0] = (int *) mycalloc (Junction.nCliques, sizeof (int));
	//    }
	
	Junction.cliques[0] = (int *) mymalloc (NVar+1, sizeof (int));
	Junction.cliques[0][0] = NVar; /* The first clique contains all the vertices of the graph at the beginning. */
	for (i = 0 ; i < NVar ; i++) {
		Junction.cliques[0][i+1] = i;
    }
	
	/*  printf("| ");
	for (i = 0 ; i < NVar+1 ; i++)
    {
		printf ("%d ", Junction.cliques[0][i]);
    }
    printf ("|\n");*/
	
	separateurs = (int *) mycalloc (10*NVar, sizeof (int));
	
	splitCluster (0);
	
	printf("                                                                              \r");
	// Erases the entire line (avoids displaying problems)
	
}

/*====================================================*/
/*                                                    */
/*            Initialization procedures               */
/*                                                    */
/*====================================================*/

/************************************************/
/* The following procedure initializes the      */
/* incidence matrix.                            */
/************************************************/
void initAdj ()
{
	int i, j, k, nb, constraint, arity, var;
	ElimGraph = (int **) mymalloc (NVar, sizeof (int *));
	
	if (Verbose>=TREEDECVERBOSE) {printf("Initializing Incidence Matrix... ");}
	for (i = 0 ; i < NVar ; i++) /* For each variable */
    {      
		ElimGraph[i] = (int *) mycalloc (NVar, sizeof (int));
		nb = VARNCONSTR(i); /* Gets the number of Nary constraints where i appears */
		for (j = 0 ; j < nb ; j++) /* For each constraint on the variable i */
		{
			constraint = VARCONSTRONE(i,j);
			arity = ARITY(constraint);
			for (k = 0 ; k < arity ; k++) /* For each variable in the scope of the constraint */
			{
				var = SCOPEONE(constraint,k);
				if (i != var) ElimGraph[i][var] = EDGE;
			}
		}   
		
		/* But we also have to find all binary constraints... */
		for (j = 0 ; j < NVar ; j++)	{
			if (ISBINARY(i,j))   {
				ElimGraph[i][j] = EDGE;
			}
		}
    }
	if (Verbose>=TREEDECVERBOSE) {printf("done.\n");}  
	
	if (Verbose>=TREEDECVERBOSE) {
		printf("Incidence matrix :\n");
		for (i = 0 ; i < NVar ; i++)
		{
			printf("| ");
			for (j = 0 ; j < NVar ; j++)  {
				printf("%d ", ElimGraph[i][j]);
			}
			printf("|\n");
		}
    }
}

/************************************************/
/* The following procedure initializes the      */
/* heuristic array with the values given by the */
/* heuristic choosen.                           */
/************************************************/
void initHeuristicArray ()
{
	int i = 0, j = 0;
	
	if (Verbose>=TREEDECVERBOSE) {printf("Initializing HeuristicArray... ");}
	
	HeuristicArray = (int *) mycalloc(NVar, sizeof(int));
	switch (HeurTreedec)
    {
		case MAX_CARD:
			break;
		case MIN_FILL:
			for (i = 0 ; i < NVar ; i++)	{
				HeuristicArray[i] = getFillIn(i);
			}
			break;
			
		case MIN_DEGREE:
			for (i = 0 ; i < NVar ; i++) {
				HeuristicArray[i] = getDegree(i);
			}
			break;
			
			
		case MIN_WEIGHT:
			
			break;
		case MIN_WIDTH:
			for (i = 0 ; i < NVar ; i++)	{
				for (j = 0 ; j < NVar ; j++)  {
					HeuristicArray[i] += ElimGraph[i][j];
				}
			}
			break;
		default:
			
			break;
    }
	if (Verbose>=TREEDECVERBOSE) {printf("done.\n");}
}

/*====================================================*/
/*                                                    */
/*  Subfunctions for the computation of an ordering   */
/*                                                    */
/*====================================================*/
/************************************************/
/* The following procedure "removes" a vertex   */
/* from the graph by correctly updating the     */
/* array of heuristic and the incidence matrix. */
/************************************************/
void removeVertex (int vertex)
{
	int i = 0;
	int *Neighbours = mycalloc (NVar, sizeof(int));
	
	if (Verbose>=TREEDECVERBOSE) {printf("Removing vertex %d... ", vertex);}
	switch (HeurTreedec) {
		case MAX_CARD:
			/* We just have to update the cardinality of the not already deleted neighbours */
			for (i = 0 ; i < NVar ; i++) {
				if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i)) {
					HeuristicArray[i] += IS_FILL_IN(vertex, i) ? 0 : 1;
					/* The vertex i has maybe a new labeled neighbour */
					ElimGraph[vertex][i] = ElimGraph[vertex][i]|PSEUDO;
					/* The vertex i has an edge transformed to a pseudo-edge */
					ElimGraph[i][vertex] = ElimGraph[i][vertex]|PSEUDO;
				}
			}
			HeuristicArray[vertex] = -1; /* So that the vertex won't be selected anymore */
			break;
		case MIN_FILL:
			/* We just have to update the fill-in of the not already deleted neighbours */
			for (i = 0 ; i < NVar ; i++) {
				/* Here we label the not already deleted neighbours */
				Neighbours[i] = IS_EDGE_BUT_NOT_PSEUDO(vertex, i);
				if (Neighbours[i]) {
					ElimGraph[vertex][i] = ElimGraph[vertex][i]|PSEUDO;
					/* The vertex i has an edge transformed to a pseudo-edge */
					ElimGraph[i][vertex] = ElimGraph[i][vertex]|PSEUDO;
				}
			}
			for (i = 0 ; i < NVar ; i++) {
				/* Here we update the previously labeled vertices with the new fill-in */
				if (Neighbours[i]) {HeuristicArray[i] = getFillIn (i);}
			}
			HeuristicArray[vertex] = NVar+1; /* So that the vertex won't be selected anymore */
			break;
			
		case LEXICO_ELIM:
		case REVLEXICO_ELIM:
	        case MIN_DEGREE:
		/* We just have to update the degree of the not already deleted neighbours */
			for (i = 0 ; i < NVar ; i++) {
				/* Here we label the not already deleted neighbours */
				Neighbours[i] = IS_EDGE_BUT_NOT_PSEUDO(vertex, i);
				if (Neighbours[i]){
					HeuristicArray[i]--;
					ElimGraph[vertex][i] = ElimGraph[vertex][i]|PSEUDO;
					/* The vertex i has an edge transformed to a pseudo-edge */
					ElimGraph[i][vertex] = ElimGraph[i][vertex]|PSEUDO;
				}
			}
			HeuristicArray[vertex] = NVar+1; /* So that the vertex won't be selected anymore */
			break;
			
			
		case MIN_WEIGHT:
			
			break;
		case MIN_WIDTH:
			for (i = 0 ; i < NVar ; i++)	{
				if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i))  {
					HeuristicArray[i] -= IS_FILL_IN(vertex, i) ? 0 : 1;
					/* The vertex i has maybe one less neighbour */
					ElimGraph[vertex][i] = ElimGraph[vertex][i]|PSEUDO;
					/* The vertex i has an edge transformed to a pseudo-edge */
					ElimGraph[i][vertex] = ElimGraph[i][vertex]|PSEUDO;
				}
			}
			HeuristicArray[vertex] = NVar+1; /* So that the vertex won't be selected anymore */
			break;
		default:
			
			break;
    }
	free(Neighbours);
	if (Verbose>=TREEDECVERBOSE) {printf("done.\n");}
	
	/*  if (Verbose)
    {
		printf("Incidence matrix :\n");
		for (i = 0 ; i < NVar ; i++)
		{
			printf("| ");
			for (j = 0 ; j < NVar ; j++)
			{
				printf("%d ", ElimGraph[i][j]);
			}
			printf("|\n");
		}
	}*/
}

/************************************************/
/* The following procedure adds the fill-in of  */
/* a vertex to the elimgraph by updating the    */
/* incidence matrix with the fill-in edges.     */
/************************************************/
void addFillIn (int vertex)
{
	int i = 0, j = 0, k = 0;
	
	if (Verbose>=TREEDECVERBOSE) {printf("Adding fill-in from vertex %d...", vertex);}
	
	switch (HeurTreedec) {
		case MAX_CARD:
			for (i = 0 ; i < NVar ; i++) {/* For each neighbour */
				if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i)) { /* that isn't deleted */
					for (j = i+1 ; j < NVar ; j++) { /* for each other neighbour */
						if (IS_EDGE_BUT_NOT_PSEUDO(vertex, j)){ /* that isn't deleted */
							ElimGraph[i][j] = ElimGraph[i][j] ? ElimGraph[i][j] : EDGE+FILL_IN;
							ElimGraph[j][i] = ElimGraph[j][i] ? ElimGraph[j][i] : EDGE+FILL_IN;
						} /* mark the edge as fill-in one if it isn't already an edge */
					}
				}
			} /* Nothing else to do for max-card, since the vertex isn't 
removed at this stage */
break;
case MIN_FILL:
	for (i = 0 ; i < NVar ; i++) { /* For each neighbour */
		if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i)) { /* that isn't deleted */
			for (j = i+1 ; j < NVar ; j++) {/* for each other neighbour */
				if (IS_EDGE_BUT_NOT_PSEUDO(vertex, j)) {/* that isn't deleted */
					if (!ElimGraph[i][j]) {/* If there is no edge btw i and j */
						for (k = 0 ; k < NVar ; k++) {/* We look for the common neighbours
							of the 2 previous vertices */
							if ((k!=vertex)&&(IS_EDGE_BUT_NOT_PSEUDO(i, k))&&(ElimGraph[j][k])) {
								HeuristicArray[k]--;
							} /* Since the edge appears, this can't belong
				     to the fill-in set of any vertex anymore */
						}
						ElimGraph[i][j] = EDGE+FILL_IN;
						ElimGraph[j][i] = EDGE+FILL_IN;
					} /* mark the edge as fill-in one */
				}
			}
	    }
	}
break;

case LEXICO_ELIM:
case REVLEXICO_ELIM:
case MIN_DEGREE:
	for (i = 0 ; i < NVar ; i++) {/* For each neighbour */
		if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i)){ /* that isn't deleted */
			for (j = i+1 ; j < NVar ; j++) {/* for each other neighbour */
				if (IS_EDGE_BUT_NOT_PSEUDO(vertex, j)) {/* that isn't deleted */
					if (!ElimGraph[i][j]) {
						ElimGraph[i][j] = EDGE+FILL_IN;
						ElimGraph[j][i] = EDGE+FILL_IN;
						// Update degrees
						HeuristicArray[i]++;
						HeuristicArray[j]++;
					}
				} 
			}
		}
	}
break;

case MIN_WEIGHT:
	break;
	
case MIN_WIDTH:
	for (i = 0 ; i < NVar ; i++) {/* For each neighbour */
		if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i)) {/* that isn't deleted */
			for (j = i+1 ; j < NVar ; j++) {/* for each other neighbour */
				if (IS_EDGE_BUT_NOT_PSEUDO(vertex, j)){ /* that isn't deleted */
					if (!ElimGraph[i][j]) {
						ElimGraph[i][j] = EDGE+FILL_IN;
						ElimGraph[j][i] = EDGE+FILL_IN;
					}
				} /* mark the edge as fill-in one if it isn't already an edge */
			}
	    }
	} /* Nothing else to do for max-card, since the vertex isn't 
removed at this stage */
break;

default:
	break;
    }

if (Verbose>=TREEDECVERBOSE) {printf("done.\n");}

/* if (Verbose) {
printf("Incidence matrix :\n");
for (i = 0 ; i < NVar ; i++) {
	printf("| ");
	for (j = 0 ; j < NVar ; j++) {
		printf("%d ", ElimGraph[i][j]);
	}
	printf("|\n");
}
}*/
}

/*************************************************/
/* The following procedures return a vertex      */
/* choosen by a particular heuristic...          */
/*************************************************/
int getMaxHeuristic ()
{
	int i;
	int argMax = 0, max = -1;
	
	for (i = 0 ; i < NVar ; i++) {
		if (HeuristicArray[i] == max)	{
			argMax = (rand()%2 == 1) ? argMax : i; /* Randomly chooses between ties */
		}
		if (HeuristicArray[i] > max) {
			max = HeuristicArray[i];
			argMax = i;
		}
    }
	return (argMax);
}

int getMinHeuristic ()
{
	int i;
	int argMin = 0, min = NVar+1;
	
	for (i = 0 ; i < NVar ; i++) {
		if (HeuristicArray[i] == min)	{
			argMin = (rand()%2 == 1) ? argMin : i; /* Randomly chooses between ties */
		}
		if (HeuristicArray[i] < min) {
			min = HeuristicArray[i];
			argMin = i;
		}
    }
	return (argMin);
}

/*************************************************/
/* The following procedure returns the fillIn    */
/* produced by the elimination of the vertex.    */
/*************************************************/
int getFillIn (int vertex)
{
	int fillIn = 0;
	int i, j;
	
	for (i = 0 ; i < NVar ; i++) { /* For each neighbour */
		if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i)) {/* that isn't deleted */
			for (j = i+1 ; j < NVar ; j++) {/* for each other neighbour */
				if (IS_EDGE_BUT_NOT_PSEUDO(vertex, j)){ /* that isn't deleted */
					if (!ElimGraph[i][j]){ /* If ther is no edge btw i and j */
						fillIn++; /* then it's a fill-in edge ! */
					}
				}
			}
		}
    }
return (fillIn);
}

/*************************************************/
/* The following procedure returns the degree    */
/* of the vertex.                                */
/*************************************************/
int getDegree (int vertex)
{
	int degree = 0;
	int i;
	
	for (i = 0 ; i < NVar ; i++) { /* For each neighbour */
		if (IS_EDGE_BUT_NOT_PSEUDO(vertex, i)) {/* that isn't deleted */
			degree++; /* then it's a true neighbor ! */
		}
	}
return degree;
}

/*************************************************/
/* The following procedure swaps two vertices in */
/* the matrix of incidence.                      */
/*************************************************/
void swapVertices (int v1, int v2, int *tmpOrdering)
{
	int i, tmp;
	/* First swaps the rows */
	for (i = 0 ; i < NVar ; i++) {
		tmp = ElimGraph[v1][i];
		ElimGraph[v1][i] = ElimGraph[v2][i];
		ElimGraph[v2][i] = tmp;
    }
	/* Then swaps the columns */
	for (i = 0 ; i < NVar ; i++) {
		tmp = ElimGraph[i][v1];
		ElimGraph[i][v1] = ElimGraph[i][v2];
		ElimGraph[i][v2] = tmp;
    }
	tmp = tmpOrdering[v1];
	tmpOrdering[v1] = tmpOrdering[v2];
	tmpOrdering[v2] = tmp;
}

/*************************************************/
/* The following procedure reorders the matrix   */
/* of incidence thanks to the ordering found.    */
/*************************************************/
void reorderMatrix ()
{
	int i, j;
	int *tmpOrdering = (int *)mymalloc(NVar, sizeof (int));
	
	if (Verbose>=TREEDECVERBOSE) {printf("Reordering matrix...");}
	for (i = 0 ; i < NVar ; i++) {
		tmpOrdering[i] = InverseOrdering[i];
    }
	
	for (i = 0 ; i < NVar ; i++) {
		for (j = i ; (j < NVar)&&(tmpOrdering[j]!=i) ; j++) {}
		if (j != i) {
			swapVertices(i, j, tmpOrdering);
		}
    }
	if (Verbose>=TREEDECVERBOSE) {printf("done.\n");}
	if (Verbose>=TREEDECVERBOSE) {
		printf("Incidence matrix :\n");
		for (i = 0 ; i < NVar ; i++) {
			printf("| ");
			for (j = 0 ; j < NVar ; j++) {
				printf("%d ", ElimGraph[i][j]);
			}
			printf("|\n");
		}
    }
}

/*************************************************/
/* The following procedure computes the junction */
/* graph.                                        */
/*************************************************/
void buildJunctionGraph ()
{
	int i, j, k, l, w;
	int *parents, nbP;
	
	if (Verbose>=TREEDECVERBOSE) {printf("Building junction graph... ");}
	
	Junction.nCliques = 0;
	Junction.cliques = (int **) mymalloc (NVar, sizeof (int *));
	/* The list of parents is used to find the common parents of some vertices
		in a clique : if they have at least one, then the clique isn't maximal */
	parents = (int *) mymalloc (NVar, sizeof (int));
	
	/* NB : The number of the variables in the cliques are the new
		numbers, not the initial ones */
	for (i = 0 ; i < NVar ; i++) {
		w = 0;
		nbP = 0;
		for (j = i ; j < NVar ; j++) {
			w += (IS_EDGE(i, j) ? 1 : 0);
		}
		for (j = 0 ; j < i ; j++)	{
			parents[j] = (IS_EDGE(i, j) ? 1 : 0); /* Initializes the list of parents */
			nbP += parents[j];
		}
		/* First we allocate the memory for the current clique */
		Junction.cliques[Junction.nCliques] = (int *) mymalloc(w+2, sizeof (int));
		Junction.cliques[Junction.nCliques][0] = 1;
		Junction.cliques[Junction.nCliques][1] = i;
		for (j = i ; j < NVar ; j++) {
			if (IS_EDGE(i, j)) {
				Junction.cliques[Junction.nCliques][0]++;
				Junction.cliques[Junction.nCliques][Junction.cliques[Junction.nCliques][0]] = j;
				/* One more vertex in the clique ! */
				for (k = 0 ; nbP&&(k < i) ; k++) {
					if (!(IS_EDGE(j, k))) {
						nbP -= parents[k];
						parents[k] = 0;
					}
				} /* This loop eliminates from the list of parents the vertices
		     that aren't parents of vertex j */
			}
		}
		if (nbP){
			free (Junction.cliques[Junction.nCliques]); /* If there's a common parent,
			then remove the clique
			(it isn't maximal). */
		}
		else {Junction.nCliques++;}
		
    }
	Junction.cliques[Junction.nCliques] = NULL;
	free (parents);
	
	if (Verbose>=TREEDECVERBOSE) {printf("done.\n");}
	if (Verbose>=TREEDECVERBOSE) {
		printf("Junction Graph :\n");
		for (i = 0 ; i < Junction.nCliques ; i++)	{
			printf("| ");
			printf("%d ", Junction.cliques[i][0]);
			for (j = 1 ; j <= Junction.cliques[i][0] ; j++)  {
				printf("%d ", Ordering[Junction.cliques[i][j]]);
			}
			printf(" | (C%d)\n",i);
		}
    }
	
	/* Now we are going to compute the weights of the edges */
	
	Junction.edges = (int **) mymalloc (Junction.nCliques, sizeof (int *));
	
	for (i = 0 ; i < Junction.nCliques ; i++) { /* For each clique */
		Junction.edges[i] = (int *) mycalloc (Junction.nCliques, sizeof (int));
    }

for (i = 0 ; i < Junction.nCliques ; i++) { /* For each clique */
for (j = i+1 ; j < Junction.nCliques ; j++) {/* for each other clique */
for (k = 1 ; k <= Junction.cliques[i][0] ; k++) /* for each vertex of the
first clique */
{
	for (l = 1 ; (l < Junction.cliques[j][0])
		 &&(Junction.cliques[j][l] < Junction.cliques[i][k]) ; l++) {}
	if (Junction.cliques[i][k] == Junction.cliques[j][l])
		/* If the vertex is also in the second clique */
	{
		//		  printf("On parcourt les cliques %d et %d. On a trouv� des points identiques : %d et %d...\n", i, j, Ordering[Junction.cliques[i][k]], Ordering[Junction.cliques[j][l]]); 
		Junction.edges[i][j]++; /* increase the weight of the edge */
		Junction.edges[j][i]++; /* increase the weight of the edge */
	}
}
}
}
if (Verbose>=TREEDECVERBOSE)
{
	printf("Junction Weights :\n");
	for (i = 0 ; i < Junction.nCliques ; i++)
	{
		printf("| ");
		for (j = 0 ; j < Junction.nCliques ; j++)
	    {
			printf("%d ", Junction.edges[i][j]);
	    }
		printf(" |\n");
	}
}
}


/*====================================================*/
/*                                                    */
/*    Subfunctions for the computation of a tree      */
/*    decomposition with the algorithm of Koster      */
/*                                                    */
/*====================================================*/
/*************************************************/
/* The sub-procedure for the tree decomposition. */
/*************************************************/
void splitCluster (int cluster)
{
	/*------ Welcome to SplitCluster ! --------*/
	/*------ Declaration / Allocation -------*/
	
	int i, j, k;                  /* iterators */
	int *vertices;                /* set of vertices in the cluster */
	int *inverseVertices;         /* index of each vertex */
	int *separator;
	int **tmpInc;                 /* matrix of incidence of the procedure */
	int *minCut;
	int **connected;
	int nbConnected = 0;
	int *oldSepNeighbours;
	int *cpArray;
	int oldNCliques = 0;
	int sepSize;
	
	int *simplicial;
	int sepSize2;
	
	
	
	printf("Splitting cluster %d, nvertices = %d. Created by %d.\r", cluster, Junction.cliques[cluster][0], separateurs[cluster]);
	fflush(stdout);
	
	/*for (i = 0 ; i < Junction.nCliques ; i++)
    {
		printf("| ");
		for (j = 0 ; j < Junction.cliques[i][0] ; i++)
		{
			printf("%d ", Junction.cliques[i][j+1]);
		}
		printf ("|\n%d.\n", Junction.cliques[cluster][0]);
    }
    getchar ();*/
	
	vertices = (int *) mymalloc (Junction.cliques[cluster][0]+1, sizeof (int));
	inverseVertices = (int *) mycalloc (NVar+1, sizeof (int));
	tmpInc = (int **) mymalloc (Junction.cliques[cluster][0], sizeof (int *));
	oldSepNeighbours = (int *) mymalloc (Junction.nCliques, sizeof (int));
	cpArray = (int *) mymalloc (Junction.nCliques, sizeof (int));
	
	simplicial = (int *) mycalloc (Junction.cliques[cluster][0], sizeof (int));
	
	/*----------------------------------------*/
	
	
	/*------ Initialization ----------*/
	
	vertices[0] = Junction.cliques[cluster][0];
	inverseVertices[0] = NVar;
	
	
	/* This loop initializes the new graph with the vertices of the cluster */
	for (i = 1 ; i <= Junction.cliques[cluster][0] ; i++)
    {
		tmpInc[i-1] = (int *) mycalloc (Junction.cliques[cluster][0], sizeof (int));
		vertices[i] = Junction.cliques[cluster][i];
		inverseVertices[Junction.cliques[cluster][i]+1] = i-1;
    }
	/* This loop initializes the new graph with the vertices of the cluster */
	
	connected = (int **) mymalloc (Junction.cliques[cluster][0]-1, sizeof (int*));
	
	for (i = 0 ; i < Junction.cliques[cluster][0]-1 ; i++)
    {
		connected[i] = (int *) mycalloc (Junction.cliques[cluster][0]-2, sizeof (int));
    }
	
	
	/*printf("| ");
	for (i = 1 ; i <= Junction.cliques[cluster][0] ; i++)
    {
		printf ("%d ", vertices[i]);
    }
	printf ("|\n");
	getchar ();*/
	
	
	/* This loop initializes the new graph with the edges of the cluster */
	for (i = 0 ; i < Junction.cliques[cluster][0] ; i++)
    {
		for (j = i+1 ; j < Junction.cliques[cluster][0] ; j++)
		{
			tmpInc[i][j] = ElimGraph[vertices[i+1]][vertices[j+1]];
			tmpInc[j][i] = ElimGraph[vertices[i+1]][vertices[j+1]];
		}
    }
	/* This loop initializes the new graph with the edges of the cluster */
	
	/*if ((cluster == 128)||(cluster == 169))
    {
		for (i = 0 ; i < Junction.cliques[cluster][0] ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.cliques[cluster][0] ; j++)
			{
				printf("%d ", tmpInc[i][j]);
			}
			printf("|\n");
		}
		getchar ();
	}*/
	
	/* The following loop makes cliques of the separators between the cluster and
		its neighbours. */
	for (i = 0 ; i < Junction.nCliques ; i++) /* Look for the neighbours */
    {
		if (Junction.edges[cluster][i]) /* A neighbour is found */
		{
			separator = (int *) mycalloc (Junction.cliques[cluster][0]+1, sizeof (int));
			getSeparator (separator, cluster, i); /* We can get the separator... */
			for (j = 1 ; j <= separator[0] ; j++)
			{
				for (k = j+1 ; k <= separator[0] ; k++)
				{
					tmpInc[inverseVertices[separator[j]+1]][inverseVertices[separator[k]+1]] = 1;
					tmpInc[inverseVertices[separator[k]+1]][inverseVertices[separator[j]+1]] = 1;
				}
			}
			/*
			 for (j = 0 ; j < Junction.edges[cluster][i] ; j++)
			 {
				 for (k = j+1 ; k < Junction.edges[cluster][i] ; k++) 
				 {
					 tmpInc[j][k] = 1;
					 tmpInc[k][j] = 1;
				 }
			 }
			 */
			free (separator);
		}
    }
	/* The following loop makes cliques of the separators between the cluster and
		its neighbours. */
	
	//while (preprocess (simplicial, vertices, tmpInc));
	free (simplicial);
	
	/*----------------------------------------*/
	
	//printf ("On cherche un s�parateur de taille minimale...\n");
	//getchar ();
	
	/*------------ Find a cut of minimum vertices ----------------*/
	
	minCut = (int *) mymalloc (Junction.cliques[cluster][0], sizeof (int *));
	sepSize = findMinCut (tmpInc, minCut, Junction.cliques[cluster][0]);
	
	/* We get the minimum vertex separator of the modified cluster.
		This is the main point of the procedure. */
	
	/*------------------------------------------------------------*/
	
	if (sepSize < Junction.cliques[cluster][0]) /* This condition means that the graph isn't complete */
    {
		//  printf("separateur size : %d\n", sepSize);
		
		//printf ("On cherche les composantes connexes...\n");
		//getchar ();
		
		/*--------- Get the connected components ----------*/
		
		nbConnected = getConnectedComponents (tmpInc, minCut, connected, Junction.cliques[cluster][0]);
		
		/*-------------------------------------------------*/
		
		/*printf ("Connected components :\n");
		
		for (i = 0 ; i < nbConnected ; i++)
	{
			printf ("| ");
			for (j = 1 ; j <= connected[i][0] ; j++)
			{
				//	      connected[i][j] = vertices[connected[i][j]+1];
				printf ("%d ", vertices[connected[i][j]+1]);
			}
			printf ("|\n");
	}
		getchar ();*/
		
		/*-------- Build the new junction graph (update the old one) ----------*/
		
		/*free (Junction.cliques[cluster]);
		Junction.cliques[cluster] = (int *) mymalloc (minCut[0]+1, sizeof (int));,
		Junction.cliques[cluster][0] = minCut[0];*/
		
		/* The incidence matrix... */
		Junction.edges = (int **) realloc (Junction.edges, (Junction.nCliques + nbConnected - 1)*sizeof (int *));
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			for (j = 0 ; j < Junction.nCliques ; j++)
			{
				cpArray[j] = Junction.edges[i][j];
			}
			free (Junction.edges[i]);
			Junction.edges[i] = (int *) mycalloc (Junction.nCliques + nbConnected - 1, sizeof (int));
			for (j = 0 ; j < Junction.nCliques ; j++)
			{
				Junction.edges[i][j] = cpArray[j];
			}
		}
		for ( ; i < Junction.nCliques + nbConnected - 1 ; i++)
		{
			Junction.edges[i] = (int *) mycalloc (Junction.nCliques + nbConnected - 1, sizeof (int));
		}
		
		/* Now the incidence matrix has the good dimensions */
		
		
		/*printf ("On redimensionne la matrice edges :\n");
		
		for (i = 0 ; i < Junction.nCliques+nbConnected ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.nCliques+nbConnected ; j++)
			{
				printf("%d ", Junction.edges[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		
		
		/* We save the neighbours of the former cluster
			and we delete the values concerning the cluster in the 
			incidence matrix */
		for (j = 0 ; j < Junction.nCliques ; j++)
		{
			oldSepNeighbours[j] = Junction.edges[cluster][j];
			Junction.edges[cluster][j] = 0;
			Junction.edges[j][cluster] = 0;
		}
		oldNCliques = Junction.nCliques ;
		
		/* printf ("On vire les valeurs concernant les anciens voisins dans la matrice edges :\n");
		
		for (i = 0 ; i < Junction.nCliques+nbConnected ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.nCliques+nbConnected ; j++)
			{
				printf("%d ", Junction.edges[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		
		Junction.cliques = (int **) realloc (Junction.cliques, (Junction.nCliques+nbConnected-1)*sizeof (int *));
		
		free (Junction.cliques[cluster]);
		Junction.cliques[cluster] = (int *) mymalloc (minCut[0]+1+connected[0][0], sizeof (int));
		
		Junction.cliques[cluster][0] = minCut[0]+connected[0][0]; 
		for (i = 1 ; i <= connected[0][0] ; i++)
		{
			Junction.cliques[cluster][i] = vertices[connected[0][i]+1];
		}
		
		for ( ; i <= Junction.cliques[cluster][0] ; i++)
		{
			Junction.cliques[cluster][i] = vertices[minCut[i-connected[0][0]]+1];
		}
		
		/* The cluster becomes the first connected component. */
		
		for (i = 1 ; i < nbConnected ; i++)
		{
			separateurs[Junction.nCliques] = cluster;
			Junction.cliques[Junction.nCliques] = (int *) mymalloc (connected[i][0]+1+minCut[0], sizeof (int));
			Junction.cliques[Junction.nCliques][0] = connected[i][0]+minCut[0];
			for (j = 1 ; j <= connected[i][0] ; j++)
			{
				Junction.cliques[Junction.nCliques][j] = vertices[connected[i][j]+1];
			}
			for ( ; j <= Junction.cliques[Junction.nCliques][0] ; j++)
			{
				Junction.cliques[Junction.nCliques][j] = vertices[minCut[j-connected[i][0]]+1];
			}
			Junction.nCliques++;
		}
		/* Creates the new clusters with one connected component each. */
		
		
		/*printf ("On cr�e la matrice cliques...\n");
		
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			printf("| ");
			for (j = 1 ; j <= Junction.cliques[i][0] ; j++)
			{
				printf("%d ", Junction.cliques[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		
		
		for (i = 0 ; i < oldNCliques ; i++)
		{
			if (oldSepNeighbours[i]) /* For each former neighbour */
			{
				sepSize = getSeparatorSize (cluster, i); /* First get the number of vertices in common with the separator */
				sepSize2 = 0;
				for (j = oldNCliques ; (j < Junction.nCliques) && (sepSize2 <= sepSize) ; j++)
				{
					sepSize2 = getSeparatorSize (i, j); /* Then get the number of vertices in common with each connected component */
				}
				j--;
				if (sepSize2 <= sepSize){j = cluster; sepSize2 = sepSize ? sepSize : -1;}
				Junction.edges[i][j] = sepSize2;
				Junction.edges[j][i] = sepSize2;
			}
		}
		
		for (i = oldNCliques ; i < Junction.nCliques ; i++)
		{
			sepSize = getSeparatorSize (cluster, i);
			Junction.edges[i][cluster] = sepSize ? sepSize : -1;
			Junction.edges[cluster][i] = sepSize ? sepSize : -1;
		}
		
		
		/*--------------------------------------------------------------------------*/
		
		/*printf ("On r�actualise la matrice d'incidence...\n");
		
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.nCliques ; j++)
			{
				printf("%d ", Junction.edges[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		//      exit(0);
		
	}
	
	for (i = 0 ; i < vertices[0]-1 ; i++)
    {
		free (connected[i]);
    }
	free (connected);
	
	for (i = 0 ; i < Junction.cliques[cluster][0] ; i++)
    {
		free (tmpInc[i]);
    }
	
	free (tmpInc);
	free (vertices);
	free (inverseVertices);
	free (minCut);
	free (oldSepNeighbours);
	free (cpArray);
	
	if (nbConnected > 1) splitCluster (cluster);
	
	for (j = oldNCliques ; (j < oldNCliques+nbConnected-1) ; j++)
    {
		splitCluster (j); /* recursively call itself on the new nodes created */
    }
}

int getSeparatorSize (int clique1, int clique2)
{
	int k, l, n = 0;
	
	for (k = 1 ; (k <= Junction.cliques[clique1][0]) ; k++) /* for each vertex of the
		first clique */
    {
		for (l = 1 ; (l < Junction.cliques[clique2][0])
			 &&(Junction.cliques[clique2][l] != Junction.cliques[clique1][k]) ; l++){}
		if (Junction.cliques[clique1][k] == Junction.cliques[clique2][l])
			/* If the vertex is also in the second clique */
		{
			n++;
		}
    }
	
	return (n);
}

void getSeparator (int *separator, int clique1, int clique2)
{
	int k, l, n = 0;
	
	for (k = 1 ; (k <= Junction.cliques[clique1][0]) ; k++) /* for each vertex of the
		first clique */
    {
		for (l = 1 ; (l < Junction.cliques[clique2][0])
			 &&(Junction.cliques[clique2][l] != Junction.cliques[clique1][k]) ; l++) {}
		if (Junction.cliques[clique1][k] == Junction.cliques[clique2][l])
			/* If the vertex is also in the second clique */
		{
			n++;
			separator[n] = Junction.cliques[clique1][k];
		}
    }
	separator[0] = n;
}

int preprocess (int *simplicialArray, int *vertices, int **incidence)
{
	int i, j, k;
	int simplicial;
	int nb = 0;
	
	printf ("Preprocessing : liste des points simpliciaux : \n| ");
	
	for (i = 0 ; i < vertices[0] ; i++)
    {
		if (!simplicialArray[i])
		{
			simplicial = 1;
			for (j = 0 ; (j < vertices[0]) && (simplicial) ; j++)
			{
				if (!simplicialArray[j])
				{
					for (k = j+1 ; (k < vertices[0]) && (simplicial) ; k++)
					{
						if (!simplicialArray[k] && incidence[i][j] && incidence[i][k] && !incidence[j][k])
						{
							simplicial--;
						}
					}
				}
			}
			if (simplicial)
			{
				nb++;
				simplicialArray[i] = 1;
				printf("%d ", vertices[i+1]);
			}
		}
    }
	printf ("|\n");
	return (nb);
}

/*====================================================*/
/*                                                    */
/* Subfunctions for the computation of the parameters */
/*                                                    */
/*====================================================*/

/*************************************************/
/* The following procedure computes the treewidth*/
/* (induced width) of the elim-graph.            */
/*************************************************/

int getTreeWidth ()
{
	/* This procedure computes the width of the graph, with the ordering
	given by the incidence matrix. */
	
	int max = -1;
	int i;
	
	/*for (i = 0 ; i < NVar ; i++)
    {
		w = 0;
		for (j = i ; j < NVar ; j++)
		{
			w += (IS_EDGE(i, j) ? 1 : 0);
		}
		max = (w > max) ? w : max;
	}*/
	
	for (i = 0 ; i < Junction.nCliques ; i++)
    {
		max = MAX(Junction.cliques[i][0], max);
    }
	
	return (max-1);
}


/*************************************************/
/* The following procedure computes the lowest   */
/* height of the tree by trying to root the tree */
/* with each node.                               */
/*************************************************/

int setRoot ()
{
	int min = NVar+1, w, r = 0;
	int i;
	
	for (i = 0 ; i < Junction.nCliques ; i++)
    {
		Junction.root = i ;
		//printf ("*** Trying node %d as root. ***\n", i);
		w = getTreeHeight ();
		if (w < min)
		{
			min = w;
			r = i;
		}
    }
	
	Junction.root = r;
	return (min);
}

int getTreeHeight ()
{
	int *labnodes = (int *) mycalloc (Junction.nCliques, sizeof (int));
	int w;
	
	w = recursiveTreeHeight (labnodes, Junction.root);
	
	free (labnodes);
	return (w);
}

int recursiveTreeHeight (int *labnodes, int node)
{
	int w = 0, max = -1, i;
	
	labnodes[node] = 1; /* label the node, so that it won't be considered anymore */
	for (i = 0 ; i < Junction.nCliques ; i++) /* For each clique */
    {
      if (Junction.edges[node][i]>0) {
	if ((Junction.edges[node][i])&&(!labnodes[i])) /* If it is a not already labeled neighbour, i.e. a new child */
	  {
	    w = recursiveTreeHeight (labnodes, i) - Junction.edges[node][i]; /*Compute the width of the subtree (don't forget
									       to remove the vertices in the separator that are
									       considered twice... */
	  }
	if (w+Junction.cliques[node][0] > max) {
	  max = w+Junction.cliques[node][0];
	}
      } else {
	if ((Junction.edges[node][i])&&(!labnodes[i]))
	  {
	    w = recursiveTreeHeight (labnodes, i);
	  }
	if (MAX(w,Junction.cliques[node][0]) > max) {
	  max = MAX(w,Junction.cliques[node][0]);
	}
	
      }
    }
	//  printf ("The partial tree-height found for node %d is %d...\n", node, max);
	
	return (max);
}

/*************************************************/
/* The following procedure computes the maximum  */
/* separator size, just by finding the max in    */
/* the incidence matrix of the junction tree.    */
/*************************************************/
int getMaximumSeparatorSize ()
{
	int i, j, max = -1;
	
	for (i = 0 ; i < Junction.nCliques ; i++) {
		for (j = i+1 ; j < Junction.nCliques ; j++) {
			max = Junction.edges[i][j] > max ? Junction.edges[i][j] : max;
		}
	}
	return (max);
}

/*************************************************/
/* The following procedure computes the total    */
/* number of vertices in the separators, just by */
/* reading the incidence matrix of the junc tree.*/
/*************************************************/
int getTotalSeparatorSize ()
{
	int i, j, sum = 0;
	
	for (i = 0 ; i < Junction.nCliques ; i++) {
		for (j = i+1 ; j < Junction.nCliques ; j++) {
			sum += Junction.edges[i][j];
		}
	}
	return (sum);
}

/*************************************************/
/* The following procedure computes the parameter*/
/*  ====      =====      |                       */
/*  \         || ||    __|                       */
/*  /         || ||   /  |                       */
/*  ====      || ||   \__|                       */
/* cliques  variables                            */
/*************************************************/
double getDomCliqueSize ()
{
	int i, j;
	double sum = 0, prod;
	
	for (i = 0 ; i < Junction.nCliques ; i++) {
		prod = 0;
		for (j = 1 ; j <= Junction.cliques[i][0] ; j++) {
			if (j == 1) {prod = 1;}
			prod *= DOMSIZE(Ordering[Junction.cliques[i][j]]);
		}
		sum += prod;
    }
	// printf("Nb de cliques : %d.\n", Junction.nCliques);
	return (sum);
}

/*************************************************/
/* The following procedure computes the parameter*/
/*  ====      =====      |                       */
/*  \         || ||    __|                       */
/*  /         || ||   /  |                       */
/*  ====      || ||   \__|                       */
/*  seps    variables                            */
/*************************************************/
double getDomSepSize ()
{
	int i, j, k;
	double sum = 0, prod = 0;
	int *separator;
	int sep = 0;
	
	for (i = 0 ; i < Junction.nCliques ; i++) {
		for (j = i+1 ; j < Junction.nCliques ; j++) {
			prod = 0;
			separator = (int *) mymalloc (Junction.cliques[i][0]+1, sizeof (int)); /* !!!!!!!!!!!*/
			if (Junction.edges[i][j]) {sep++;}
			getSeparator (separator, i, j);
			if (separator[0]) {prod = DOMSIZE(Ordering[separator[1]]);}
			for (k = 2 ; k <= separator[0] ; k++) { /* !!!!!!!!!!!!! */
				prod *= DOMSIZE(Ordering[separator[k]]);
			}
			free (separator);
			sum += prod;
		}
    }

//  printf("Nb de separateurs : %d.\n", sep);
return (sum);
}

/*====================================================*/
/*                                                    */
/* Some test procedures to check the algorithm (to be */
/*  removed as soon as we're sure that the algorithm  */
/*                 works as expected...               */
/*                                                    */
/*====================================================*/
void testIfTreedec (JunctionGraph junc, int **incidence, int nvertices, int *ordering)
{
	int i, j, k, l;
	int b = 0;
	int nb, nbi, nbj;
	int **inc2;
	
	int *vertices = (int *) mycalloc (junc.nCliques, sizeof (int));
	
	printf ("Step 1 : Testing if the graph is a tree ...\t\t\t ");
	if (!testIfTree (junc.edges, vertices, junc.nCliques, 0, -1)) {
		printf ("[ No !]\n");
		abort();
		free (vertices);
		return;
    }
	printf ("[ Yes ]\n");
	
	printf ("Step 2 : Testing if each vertex is in a cluster ...\t\t ");
	for (i = 0 ; i < nvertices ; i++) {
		b = 0;
		for (j = 0 ; (j < junc.nCliques) && !b ; j++) {
			for (k = 1 ; (k <= junc.cliques[j][0]) && !b ; k++) {
				//printf ("testing %d - %d - %d.\n", i, j, k);
				if (junc.cliques[j][k] == i) b = 1;
			}
		}
		if (!b) {
			printf ("[ No !]\n");
			printf ("Missing vertex %d...\n", ordering[i]);
			abort();
			free (vertices);
			return;
		}
    }
	printf ("[ Yes ]\n");
	
	free (vertices);
	
	printf ("Step 3 : Testing if each edge is inside a cluster ...\t\t ");
	for (i = 0 ; i < nvertices ; i++) {
		for (j = 0 ; j < nvertices ; j++)	{
			if (incidence[i][j]) {
				b = 0;
				for (k = 0 ; (k < junc.nCliques) && (b < 2); k++) {
					//printf ("%d - %d : %d\n", i, j, incidence [i][j]);
					b = 0;
					for (l = 1 ; (l <= junc.cliques[k][0]) && (b < 2) ; l++) {
						//printf ("Testing %d : %d - %d\n", junc.cliques[k][l], ordering[i], ordering[j]);
						if ((junc.cliques[k][l] == i) || (junc.cliques[k][l] == j))  b++;
					}
				}
				if (b < 2) {
					printf ("[ No !]\n");
					printf ("The edge (%d-%d) isn't inside a cluster...\n", ordering[i], ordering[j]);
					abort();
					return;
				}
			}
		}
    }
	printf ("[ Yes ]\n");
	
	printf ("Step 4 : Testing the running intersection property ...\t\t ");
	for (i = 0 ; i < nvertices ; i++) {
		nb = 0;
		vertices = (int *) mycalloc (junc.nCliques, sizeof (int));
		for (j = 0 ; j < junc.nCliques ; j++)	{
			for (k = 1 ; (k <= junc.cliques[j][0]) ; k++) {
				if (junc.cliques[j][k] == i) {
					vertices[j] = 1;
					nb++;
				}
			}
		}
		inc2 = (int **) mymalloc (nb, sizeof (int *));
		nbi = 0;
		for (j = 0 ; j < junc.nCliques ; j++)	{
			nbj = 0;
			if (vertices[j]) {
				inc2[nbi] = (int *) mycalloc (nb, sizeof (int));
				for (k = 0 ; (k < junc.nCliques) ; k++) {
					if (vertices[k]) {
						inc2[nbi][nbj] = junc.edges[j][k];
						nbj++;
					}
				}
				nbi++;
			}
		}
		free (vertices);
		vertices = (int *) mycalloc (nb, sizeof (int));
		
		if (!testIfTree (inc2, vertices, nb, 0, -1)) {
			printf ("[ No !]\n");
			printf ("The set of clusters including vertex %d isn't a tree.\n", ordering[i]);
			abort();
			free (vertices);
			return;
		}
		free (vertices);
    }
	printf ("[ Yes ]\n");
}

int testIfTree (int **incidence, int *vertices, int nvertices, int vertex, int pa)
{
	int i;
	
	vertices[vertex] = 1;
	for (i = 0 ; i < nvertices ; i++) {
		if ((i != pa)&&(incidence[vertex][i])) {
			if (vertices[i]) return (0);
			if (!testIfTree (incidence, vertices, nvertices, i, vertex)) return (0);
		}
    }
	
	if (pa == -1) {
		for (i = 0 ; i < nvertices ; i++)	{
			if (!vertices[i]) return (0);
		}
    }
	
	return (1);
}

/*====================================================*/
/*                                                    */
/*  Some graphical utility to draw constraint graphs  */
/*   with a kind of clustering to LaTeX with eepic.   */
/*                                                    */
/*====================================================*/
void exportTreedecToGraphic ()
{
	int i, j, k, nb;
	float floatx, floaty;
	/*    int figure = 0; */
	
	int *cliques = (int *) mycalloc (Junction.nCliques, sizeof (int));
	int *vertices = (int *) mycalloc (NVar, sizeof (int));
	
	float **coordonnees = (float **) mymalloc (NVar, sizeof (int *));
	int **clustCoordonnees = (int **) mymalloc (Junction.nCliques, sizeof (int *));
	/*    char *fileName; */
	
	graphicFormat = EPS;
	
	/*    while ((figure < 1) || (figure > 2)) */
	/*      { */
	/*        printf("Which figure do you want to draw ?\n"); */
	/*        printf("(1: Junction-tree / 2: Initial Graph) > "); */
	/*        scanf("%d", &figure); */
	/*      } */
	
	/*    while ((graphicFormat < 1) || (graphicFormat > 2)) */
	/*      { */
	/*        printf("In which graphical format do you wish to export the figure ?\n"); */
	/*        printf("(1: eepic LaTeX / 2: Encapsulated PostScript) > "); */
	/*        scanf("%d", &graphicFormat); */
	/*      } */
	
	
	for (i = 0 ; i < NVar ; i++)
    {
		coordonnees[i] = (float *) mycalloc (2, sizeof (int));
    }
	
	for (i = 0 ; i < Junction.nCliques ; i++)
    {
		clustCoordonnees[i] = (int *) mycalloc (2, sizeof (int));
    }
	
	recursiveClustGraphic (Junction.root, clustCoordonnees, 0);
	
	
	for (i = 0 ; i < NVar ; i++)
    {
		nb = 0;
		for (j = 0 ; j < Junction.nCliques ; j++)
		{
			for (k = 1 ; (k <= Junction.cliques[j][0]) ; k++)
			{
				if (Junction.cliques[j][k] == i)
				{
					nb++;
				}
			}
		}
		
		floatx = 0;
		floaty = 0;
		
		for (j = 0 ; j < Junction.nCliques ; j++)
		{
			for (k = 1 ; (k <= Junction.cliques[j][0]) ; k++)
			{
				if (Junction.cliques[j][k] == i)
				{
					floatx += (1. * clustCoordonnees[j][0]) / nb;
					floaty += (1. * clustCoordonnees[j][1]) / nb;
				}
			}
		}
		
		//floatx += (rand () % 100) / 50. - 0.5;
		floaty += (rand () % 100) / 20. - 2.5;
		
		coordonnees[i][0] = floatx;
		coordonnees[i][1] = floaty;
		
    }
	
	/*    if (graphicFormat == LATEX) */
	/*      { */
	/*        printf ("Exporting to LaTeX eepic...\nOutput file : output.eepic.\n"); */
	/*        fileName = "output.eepic"; */
	/*      } */
	
	if (graphicFormat == EPS)
    {
		printf ("Exporting to Encapsulated PostScript...\nOutput files : tree.eps and graph.eps\n");
		/*        fileName = "output.eps"; */
    }
	
	writeGraphicFile ("tree.eps", clustCoordonnees, coordonnees, 1);
	writeGraphicFile ("graph.eps", clustCoordonnees, coordonnees, 2);
	
	for (i = 0 ; i < NVar ; i++)
    {
		free (coordonnees[i]);
    }
	for (i = 0 ; i < Junction.nCliques ; i++)
    {
		free (clustCoordonnees[i]);
    }
	
	free (coordonnees);
	free (cliques);
	free (vertices);
}

void recursiveClustGraphic (int s, int **clustCoordonnees, int level)
{
	int nvertices = Junction.nCliques;
	
	int *colors = (int *) mycalloc (nvertices, sizeof (int));
	int *pi = (int *) mymalloc (nvertices, sizeof (int));
	
	int *m = (int *) mymalloc (1, sizeof (int));
	
	PPGraphic (s, clustCoordonnees, colors, pi, 0, 1, m);
}

int PPGraphic (int vertex, int **clustCoordonnees, int *colors, int *pi, int depth, int breadth, int *m)
{
	int i;
	int n = 0, total = 0;
	float mean;
	
	colors[vertex] = 1;
	
	for (i = 0 ; i < Junction.nCliques ; i++)
    {
		if (!colors[i] && Junction.edges[vertex][i])
		{
			n++;
			pi[i] = vertex;
			breadth = PPGraphic (i, clustCoordonnees, colors, pi, depth+1, breadth+2*(n!=1), m);
			total+=breadth;
		}
    }
	colors[vertex] = 2;
	
	if (!n)
    {
		*m = breadth;
    }
	
	if (n <= 1)
    {
		mean = breadth;
		clustCoordonnees[vertex][1] = *m;
    }
	else
    {
		mean = total / n;
		*m = (int) mean;
		clustCoordonnees[vertex][1] = (int) mean;
    }
	
	clustCoordonnees[vertex][0] = depth;
	
	return (breadth);
}

void writeGraphicFile (char *fileName, int **clustCoordonnees, float **coordonnees, int figure)
{
	FILE *ptrfile = fopen (fileName, "w");
	float imax = 0;
	float jmax = 0;
	float imin = INT_MAX;
	float jmin = INT_MAX;
	float unit;
	int i, j;
	float divy = 1.;
	int maxd = 0;
	float a;
	char *c = (char *) mymalloc (10, sizeof (char));;
	/*  char drawFill='y'; */
	
	if (!ptrfile)
    {
		printf ("Erreur ouverture fichier...\n");
		exit (1);
    }
	
	printHeader (ptrfile);
	
	
	if (figure == 1)
    { 
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			imax = MAX (imax, clustCoordonnees[i][0]);
			jmax = MAX (jmax, clustCoordonnees[i][1]);
			imin = MIN (imin, clustCoordonnees[i][0]);
			jmin = MIN (jmin, clustCoordonnees[i][1]);
		}
		unit = 15. / (imax-imin);
		
		if ((jmax-jmin)*unit > 18) {divy = 18. / ((jmax-jmin)*unit);}
		
		//printf ("%1.2f\n",divy);
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			for (j = 0 ; j < Junction.nCliques ; j++)
			{
				maxd = MAX (clustCoordonnees[j][0] - clustCoordonnees[i][0], maxd);
				maxd = MAX (clustCoordonnees[j][1] - clustCoordonnees[i][1], maxd);
			}
		}
		
		maxd = (int) (100. / maxd);
		
		if (graphicFormat == LATEX)
		{  
			fprintf (ptrfile, "\\setlength{\\unitlength}{%1.1fmm}\n", unit);
			fprintf (ptrfile, "\\begin{picture}(%1.0f, %1.0f)(%1.0f,%1.0f)\n", (imax)*10, (jmax)*10.*divy, (imin)*10, (jmin)*10.*divy);
		}
		
		if (graphicFormat == EPS)
		{
			fprintf (ptrfile, "%%%%BoundingBox: %1.0f %1.0f %1.0f %1.0f\n", imin*10.-2., jmin*10.*divy-2., (imax)*10.+2., (jmax)*10.*divy+2.);
			fprintf (ptrfile, "1 %1.0f translate\n%1.2f %1.2f scale\n", 1., 1., 1.);
		}
		
		
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			sprintf (c, "%d", i);
			drawCircle (ptrfile, clustCoordonnees[i][0]*10, clustCoordonnees[i][1]*10.*divy, 2.);
			drawText (ptrfile, clustCoordonnees[i][0]*10, (clustCoordonnees[i][1]*10.-2.)*divy, c);
			for (j = i+1 ; j < Junction.nCliques ; j++)
			{
				if (Junction.edges[i][j])
				{
					drawLine (ptrfile, clustCoordonnees[i][0]*10, clustCoordonnees[i][1]*10.*divy,
							  clustCoordonnees[j][0]*10, clustCoordonnees[j][1]*10.*divy
							  );
				}
			}
		}
    }
	
	if (figure == 2)
    {
		for (i = 0 ; i < NVar ; i++)
		{
			imax = MAX (imax, coordonnees[i][0]);
			jmax = MAX (jmax, coordonnees[i][1]);
			imin = MIN (imin, coordonnees[i][0]);
			jmin = MIN (jmin, coordonnees[i][1]);
		}
		unit = 15. / (imax-imin);
		maxd = 0;
		
		for (i = 0 ; i < NVar ; i++)
		{
			for (j = 0 ; j < NVar ; j++)
			{
				maxd = MAX (coordonnees[j][0] - coordonnees[i][0], maxd);
				maxd = MAX (coordonnees[j][1] - coordonnees[i][1], maxd);
			}
		}
		
		maxd = (int) (100. / maxd);
		divy = 1.;
		
		if ((jmax-jmin)*unit > 18) {divy = 18. / ((jmax-jmin)*unit);}
		
		if (graphicFormat == LATEX)
		{      
			fprintf (ptrfile, "\\setlength{\\unitlength}{%1.1fmm}\n", unit);
			fprintf (ptrfile, "\\begin{picture}(%1.0f, %1.0f)(%1.0f,%1.0f)\n", (imax)*10, (jmax)*10.*divy, (imin)*10, (jmin)*10.*divy);
		}
		
		if (graphicFormat == EPS)
		{
			fprintf (ptrfile, "%%%%BoundingBox: %1.0f %1.0f %1.0f %1.0f\n", imin*10.-2., jmin*10.*divy-2., (imax)*10.+2., (jmax)*10.*divy+2.);
			fprintf (ptrfile, "1 %1.0f translate\n%1.2f %1.2f scale\n", 1., 1., 1.);
			/*	  drawFill = getchar();
			drawFill = 't';
			printf("Do you want to draw the fill-in edges (Y/n) ?\n");
			
			while ((drawFill!='y')&&(drawFill!='n')&&(drawFill!=10)){drawFill = getchar();}
			if (drawFill == '\n'){drawFill = 'y';} */
		}
		
		for (i = 0 ; i < NVar ; i++)
		{
			for (j = i+1 ; j < NVar ; j++)
			{
				if (IS_EDGE_BUT_NOT_FILL_IN (i, j))
				{
					a = ((coordonnees[j][0] - coordonnees[i][0]) * 10.);
					if (a < 0) a = -a;
					drawLine (ptrfile, coordonnees[i][0]*10, coordonnees[i][1]*10.*divy,
							  coordonnees[j][0]*10, coordonnees[j][1]*10.*divy);
				}
				/*	      if (drawFill=='y'&&IS_FILL_IN (i, j)) */
				if (DRAWFILLIN && IS_FILL_IN (i, j))
				{
					a = ((coordonnees[j][0] - coordonnees[i][0]) * 10.);
					if (a < 0) a = -a;
					dottedLine (ptrfile, coordonnees[i][0]*10, coordonnees[i][1]*10.*divy,
								coordonnees[j][0]*10, coordonnees[j][1]*10.*divy);
					
				}
			}
		}
    }
	
	printEndDocument (ptrfile);
	
	fclose (ptrfile);
}


void drawLine (FILE *ptrfile, int x1, int y1, int x2, int y2)
{
	switch (graphicFormat)
    {
		case (LATEX):
			fprintf (ptrfile, "\t\\drawline(%d, %d)(%d, %d)\n", x1, y1, x2, y2);
			break;
		case (EPS):
			fprintf (ptrfile, "\tnewpath\n\t%d %d moveto\n\t%d %d lineto\n\t.1 setlinewidth\n\tstroke\n", x1, y1, x2, y2);
			break;
    }
}

void dottedLine (FILE *ptrfile, int x1, int y1, int x2, int y2)
{
	switch (graphicFormat)
    {
		case (LATEX):
			fprintf (ptrfile, "\t\\dottedline{3}(%d, %d)(%d, %d)\n", x1, y1, x2, y2);
			break;
		case (EPS):
			fprintf (ptrfile, "\t[3 3] 0 setdash\n");
			fprintf (ptrfile, "\tnewpath\n\t%d %d moveto\n\t%d %d lineto\n\t.1 setlinewidth\n\tstroke\n", x1, y1, x2, y2);
			fprintf (ptrfile, "\t[1 0] 0 setdash\n");
			break;
    }
}

void drawCircle (FILE *ptrfile, int x1, int y1, float r)
{
	switch (graphicFormat)
    {
		case(LATEX):
			fprintf (ptrfile, "\t\\put(%d, %d){\\circle{%1.1f}}\n", x1, y1, r);      
			break;
		case (EPS):
			fprintf (ptrfile, "\tnewpath\n\t%d %d %1.1f 0 360 arc\n\t.1 setlinewidth\n\tstroke", x1, y1, r);
			break;
    }
}

void drawText (FILE *ptrfile, int x1, int y1, char *text)
{
	switch (graphicFormat)
    {
		case(LATEX):
			fprintf (ptrfile, "\t\\put(%d, %d){\\makebox(0, 0)[t]{%s}}\n", x1, y1, text);
			break;
		case (EPS):
			fprintf (ptrfile, "\t/Garamond findfont\n\t4 scalefont\n\tsetfont\n\t%d %d moveto\n\t(%s) show\n", x1, y1+4, text);
			break;
    }
}


void printHeader (FILE *ptrfile)
{
	switch (graphicFormat)
    {
		case (LATEX):
			/*fprintf(ptrfile, "\\documentclass[french, a4paper,11pt]{article}\n\n");
			
			fprintf(ptrfile, "\\usepackage{babel,indentfirst}\n"); 
			fprintf(ptrfile, "\\usepackage[latin1]{inputenc}\n"); 
			
			fprintf(ptrfile, "\\usepackage{epic}\n"); 
			fprintf(ptrfile, "\\usepackage{eepic}\n"); 
			
			
			fprintf(ptrfile, "\\textwidth 16cm \\textheight 21.2cm \\oddsidemargin -0.24cm\n"); 
			fprintf(ptrfile, "\\evensidemargin -1.24cm \\topskip 0cm \\headheight 0cm\n"); 
			
			fprintf(ptrfile, "\\begin{document}\n");*/
			
			// Uncomment if you want to build a complete LaTeX file...
			break;
		case (EPS):
			fprintf (ptrfile, "%%!PS-Adobe-2.0 EPSF-2.0\n%%%%Creator: Generated with toolbar\n");
			fprintf (ptrfile, "%%%%Title: Output.eps.\n");
			break;
    }
}

void printEndDocument (FILE *ptrfile)
{
	switch (graphicFormat)
    {
		case (LATEX):
			fprintf (ptrfile, "\\end{picture}\n");
			//fprintf("\\end{document}\n");
			
			// Uncomment if you want to build a complete LaTeX file...
			break;
		case (EPS):
			break;
    }
}






/*====================================================*/
/*                                                    */
/*      Merging clusters depending on separator size  */
/*                                                    */
/*====================================================*/

void merging_rec(JunctionGraph *Junction, int *cliqueState, int cluster, int maxsepsize)
{
  int *clique;
  int n,c,c2,i,j;
  int found;

  cliqueState[cluster] = -1;
  /* merges all sons of cluster with their father if separator size superior to maxsepsize */
  c = 0;
  while (c<Junction->nCliques) {
    if (cliqueState[c] > 0 && Junction->edges[cluster][c] > maxsepsize) {
      cliqueState[c] = 0;
      /* merges c's variables with cluster's variables */
      assert(Junction->cliques[c][0] > Junction->edges[cluster][c]);
      clique = mycalloc(Junction->cliques[cluster][0] + Junction->cliques[c][0] - Junction->edges[cluster][c] + 1, sizeof(int));
      for (i=1;i<=Junction->cliques[cluster][0];i++) {
	clique[i] = Junction->cliques[cluster][i];
      }
      n = Junction->cliques[cluster][0];
      for (j=1;j<=Junction->cliques[c][0];j++) {
	found = 0;
	for (i=1;i<=Junction->cliques[cluster][0];i++) {
	  if (Junction->cliques[c][j] == Junction->cliques[cluster][i]) {
	    found = 1;
	    break;
	  }
	}
	if (!found) {
	  n++;
	  clique[n] = Junction->cliques[c][j];
	}
      }
      clique[0] = n;
      assert(Junction->cliques[cluster][0] + Junction->cliques[c][0] - Junction->edges[cluster][c] == n);
      free(Junction->cliques[cluster]);
      free(Junction->cliques[c]);
      Junction->cliques[cluster] = clique;
      /* collects sons of c for being sons of cluster */
      for (c2=0; c2<Junction->nCliques; c2++) {
	if (c2 != c && cliqueState[c2] > 0 && Junction->edges[c][c2] > 0) {
	  assert(Junction->edges[c][c2] >= Junction->edges[cluster][c2]);
	  Junction->edges[cluster][c2] = Junction->edges[c][c2];
	  Junction->edges[c2][cluster] = Junction->edges[c][c2];
	}
      }
      /* restarts from zero: new sons may have to be merged */
      c = 0;
    } else {
      c++;
    }
  }
  /* recursive call on cluster's sons */
  for (c=0; c<Junction->nCliques; c++) {
    if (cliqueState[c] > 0 && Junction->edges[cluster][c] > 0) {
      merging_rec(Junction, cliqueState, c, maxsepsize);
    }
  }
}

void merging(JunctionGraph *Junction, int root, int maxsepsize)
{
  int c,c2,n;
  int *cliqueState = mycalloc(Junction->nCliques, sizeof(int));
  int *oldpos = mycalloc(Junction->nCliques, sizeof(int));
  int **newedges;

  if (Verbose) {
    printf("Merge any pairs of clusters with a separator strictly greater than %d...\n",maxsepsize);
  }
  for (c=0; c<Junction->nCliques; c++) {
    cliqueState[c] = 1;
  }
  merging_rec(Junction, cliqueState, root, maxsepsize);
  /* removes forbidden clusters from Junction data structure */
  assert(cliqueState[root] == -1);
  n = 0;
  for (c=0; c<Junction->nCliques; c++) {
    if (cliqueState[c] == -1) {
      Junction->cliques[n] = Junction->cliques[c];
      oldpos[n] = c;
      if (c == root) Junction->root = n;
      n++;
    }
  }
  newedges = mycalloc(n, sizeof(int *));
  for (c=0; c<n; c++) {
    newedges[c] = mycalloc(n, sizeof(int));
    for (c2=0; c2<=c; c2++) {
      newedges[c][c2] = Junction->edges[oldpos[c]][oldpos[c2]];
      newedges[c2][c] = newedges[c][c2];
    }
  }
  for (c=0; c<Junction->nCliques; c++) {
    free(Junction->edges[c]);
  }
  free(Junction->edges);
  Junction->edges = newedges;
  if (Verbose) {
    printf("\t%d clusters removed among %d clusters.\n",Junction->nCliques - n, Junction->nCliques);
  }
  Junction->nCliques = n;
}



/*====================================================*/
/*                                                    */
/* Some graph tools such as flow algorithms that are  */
/*              used by the program.                  */
/*                                                    */
/*====================================================*/


/*************************************************/
/* The following procedure implements the Prim's */
/* algorithm to find a spanning tree of maximal  */
/* weight...                                     */
/*************************************************/

typedef double (*clusterweight_t)(int i);

double *ClusterWeight = NULL;
int *ClusterWeightComputed = NULL;

/* Chooses the first cluster in lexicographic ordering */
double clusterLexico(int i)
{
  return (double) -i;
}

/* Chooses the cluster with the maximum cartesian product of variable domains */
double clusterSize(int i)
{
  double weight;
  int j;

  if (ClusterWeight == NULL) {
    ClusterWeight = mycalloc(Junction.nCliques, sizeof(double));
    ClusterWeightComputed = mycalloc(Junction.nCliques, sizeof(int));
  }
  if (ClusterWeightComputed[i]) {
    return ClusterWeight[i];
  }
  weight = 1;
  for (j=1; j<=Junction.cliques[i][0]; j++) {
    weight *= DOMSIZE(Ordering[Junction.cliques[i][j]]);
  }
  if (Verbose) printf("C%d: size= %g\n",i,weight);
  ClusterWeight[i] = weight;
  ClusterWeightComputed[i] = 1;
  return weight;
}

/* Chooses the cluster with the largest expected cost */
double clusterExpectedCost(int i)
{
	int j,k,l, found = 0;
	double weight = 0.0;

	if (ClusterWeight == NULL) {
	  ClusterWeight = mycalloc(Junction.nCliques, sizeof(double));
	  ClusterWeightComputed = mycalloc(Junction.nCliques, sizeof(int));
	}
	if (ClusterWeightComputed[i]) {
	  return ClusterWeight[i];
	}

	for (j = 1 ; j <= Junction.cliques[i][0] ; j++) {
	  // Unary costs
	  weight += WEIGHT1(Ordering[Junction.cliques[i][j]]);
			
			// Binary costs
	  for (k = j+1 ; k <= Junction.cliques[i][0] ; k++) 
	    if (ISBINARY(Ordering[Junction.cliques[i][j]],Ordering[Junction.cliques[i][k]])) {
	      weight += WEIGHT2(Ordering[Junction.cliques[i][j]],Ordering[Junction.cliques[i][k]]);
	    }
	}
	// Other constraints
	for (j = 0; j < NConstr; j++) {
	  for (k = 0 ; k < ARITY(j) ; k++) {
	    found = 0;
	    for (l = 1 ; l <= Junction.cliques[i][0] && !found; l++)
	      found = (SCOPEONE(j,k) == Ordering[Junction.cliques[i][l]]);
	  }
	  if (found) {
	    weight += WEIGHTCONSTRAINT(j);
	  }
	}
	if (Verbose) printf("C%d: weight= %g\n",i,weight);
	ClusterWeight[i] = weight;
	ClusterWeightComputed[i] = 1;
	return weight;
}

int findInSet(int i, int *vars)
{
  int j;
  for (j=1; j<=vars[0]; j++) {
    if (vars[j] == i) return TRUE;
  }
  return FALSE;
}

void recursiveNaryPrintOriginal(FILE *file, int c, int i, int *complassignment)
{
  int a;

  if (i < ARITY(c)) {
    for (a=0; a<DOMSIZE(SCOPEONE(c,i)); a++) {
      complassignment[SCOPEONE(c,i)] = a;
      recursiveNaryPrintOriginal(file, c, i+1, complassignment);
    }
  } else {
    for (i=0; i<ARITY(c); i++) {
      fprintf(file, "%d ", complassignment[SCOPEONE(c,i)]);
    }
    FPRINTCOST(file, cost(c, complassignment));
    fprintf(file, "\n");
  }
}

/* vars contains a list of variable indexes with first list element being the size of the list */
void saveOriginalSubWCSP(FILE* file, int *vars) 
{
  int i,j,a,b,c,nbvar = 0, nbval = 0, nbconstr = 0,pos,pos2;
  int num[NVar];
  int unassigned[NConstr];
  int recursiveNaryAssignment[NVar];

  for (i=0; i<NVar; i++) {
    num[i] = -1;
  }

  /* count the number of constraints */
  for (pos=1; pos<=vars[0]; pos++) {
    i = vars[pos];
    num[i] = nbvar;
    nbvar++;
    nbval = MAX(nbval, DOMSIZE(i));
    for (pos2=pos+1; pos2<=vars[0]; pos2++) {
      j = vars[pos2];
      if (ISBINARY(i,j)) {
	nbconstr++;
      }
    }
  }
  assert(nbvar = vars[0]);
  nbconstr += nbvar; /* unary costs */
  for (c=0; c<NConstr; c++) { /* n-ary constraints */
    unassigned[c] = TRUE;
    for (i=0; i<ARITY(c); i++) {
      if (!findInSet(SCOPEONE(c,i),vars)) {
	unassigned[c] = FALSE;
	break;
      }
    }
    if (unassigned[c]) nbconstr++;
  }

  /* header */
  fprintf(file, "%s %d %d %d ", Filename, nbvar, nbval, nbconstr);
  FPRINTCOST(file, Top);
  fprintf(file, "\n");
  /* domain sizes */
  if (nbvar > 0) {
    fprintf(file, "%d", DOMSIZE(vars[1]));
    for (i=2; i<=vars[0]; i++) {
      fprintf(file, " %d", DOMSIZE(vars[i]));
    }
    fprintf(file, "\n");
  }
  /* n-ary constraints */
  for (c=0; c<NConstr; c++) {
    if (unassigned[c]) {
      assert(ARITY(c) > 2);
      fprintf(file, "%d", ARITY(c));
      for (i=0; i<ARITY(c); i++) {
	fprintf(file, " %d", num[SCOPEONE(c,i)]);
      }
      fprintf(file, " ");
      FPRINTCOST(file, BOTTOM);
      fprintf(file, " %d\n", cartesianProduct(c));
      recursiveNaryPrintOriginal(file, c, 0, recursiveNaryAssignment);
    }
  }
  /* binary constraints */
  for (pos=1; pos<=vars[0]; pos++) {
    i = vars[pos];
    for (pos2=pos+1; pos2<=vars[0]; pos2++) {
      j = vars[pos2];
      if (ISBINARY(i,j)) {
	assert(i != j);
	assert(num[i] != num[j]);
	fprintf(file, "2 %d %d ", num[i], num[j]);
	FPRINTCOST(file, BOTTOM);
	fprintf(file, " %d\n", DOMSIZE(i)*DOMSIZE(j));
	for (a=0; a<DOMSIZE(i); a++) {
	  for (b=0; b<DOMSIZE(j); b++) {
	    fprintf(file, "%d %d ", a, b);
	    FPRINTCOST(file, binaryCost(i, j, a, b));
	    fprintf(file, "\n");
	  }
	}
      }
    }
  }
  /* unary constraints */
  for (pos=1; pos<=vars[0]; pos++) {
    i = vars[pos];
    fprintf(file, "1 %d ", num[i]);
    FPRINTCOST(file, BOTTOM);
    fprintf(file, " %d\n", DOMSIZE(i));
    for (a=0; a<DOMSIZE(i); a++) {
      fprintf(file, "%d ", a);
      FPRINTCOST(file, UNARYCOST(i,a));
      fprintf(file, "\n");
    }
  }
}

/* Chooses the cluster with the largest lowerbound */
double clusterLowerBound(int i)
{
  int j;
  double weight = 0.0;
  FILE *file;
  FILE *file2;
  char command[1024];
  int res;
  int *clustervars;
  unsigned long nbnodes = 0;
  double cputime = 0;
  char filename[1024];
  char filename2[1024];

  if (ClusterWeight == NULL) {
    ClusterWeight = mycalloc(Junction.nCliques, sizeof(double));
    ClusterWeightComputed = mycalloc(Junction.nCliques, sizeof(int));
  }
  if (ClusterWeightComputed[i]) {
    return ClusterWeight[i];
  }

  clustervars = mycalloc(Junction.cliques[i][0] + 1, sizeof(int));
  clustervars[0] = Junction.cliques[i][0];
  for (j = 1 ; j <= Junction.cliques[i][0] ; j++) {
    clustervars[j] = Ordering[Junction.cliques[i][j]];
  }

  sprintf(filename,"tmpwcsp%d",processid());
  file = fopen(filename,"w");
  saveOriginalSubWCSP(file, clustervars);
  free(clustervars);
  fclose(file);
  sprintf(filename2,"tmpres%d",processid());
#if defined(VALLONGLONG)
  sprintf(command, "toolbar %s -H6 -u%lld | awk '/^Optimum: /{print $2,$4,$7} /^Lower bound: /{print $3,$5,$8}' > %s", filename, Top, filename2);
#else
  sprintf(command, "toolbar %s -H6 -u%ld | awk '/^Optimum: /{print $2,$4,$7} /^Lower bound: /{print $3,$5,$8}' > %s", filename, Top, filename2); /* | tee /dev/stderr */
#endif
  res = system(command);
  file2 = fopen(filename2,"r");
  if (fseek(file2, 0, SEEK_END), ftell(file2) == 0) abort(); fseek(file2, 0, SEEK_SET);
  fscanf(file2, "%lg", &weight);
  fscanf(file2,"%lu %lf", &nbnodes, &cputime);
  fclose(file2);

  sprintf(command, "rm -f %s", filename);
  system(command);
  sprintf(command, "rm -f %s", filename2);
  system(command);

  if (Verbose) {
    printf("C%d: lowerbound = %g (in %lu nodes and %.2f seconds)\n",i,weight,nbnodes,cputime);
  }
  if (res != 0) {
    abort();
  }

  ClusterWeight[i] = weight;
  ClusterWeightComputed[i] = 1;
  return weight;
}

/* original Prim algorithm without any heuristic
   output: edges matrix contains a maximal spanning tree */
void prim_orig (int **edges, int **cliques, int s)
{
	int i, j, max, argmaxi, argmaxj;
	
	int **tmpEdges = (int **) mymalloc (s, sizeof (int *));
	int *tmpNodes = (int *) mycalloc (s+1, sizeof (int));
	int *vertices = (int *) mycalloc (s, sizeof (int)); /* this matrix is just for the case of more than one connected component */
	
	for (i = 0 ; i < s ; i++)
    {
		tmpEdges[i] = (int *) mycalloc (s, sizeof (int));
    }
	
	tmpNodes[0] = 1;
	tmpNodes[1] = rand()%s; /* The first node is choosen randomly */
	vertices[tmpNodes[1]] = 1;
	
	while (tmpNodes[0] < s)
    {
		/* The first step updates the edges (a negative edge means
		that the two vertices aren't in the same subset, and therefore
		the edge between them can be selected... */
		for (i = 0 ; i < s ; i++)
		{
			edges[tmpNodes[tmpNodes[0]]][i] = -edges[tmpNodes[tmpNodes[0]]][i];
			edges[i][tmpNodes[tmpNodes[0]]] = -edges[i][tmpNodes[tmpNodes[0]]];
		}
		
		/* The second step finds the link of maximal weight. */
		
		argmaxi = 0;
		argmaxj = 0;
		max = 1;
		for (i = 1 ; i <= tmpNodes[0] ; i++)
		{
			for (j = 0 ; j < s ; j++)
			{
				if (!vertices[j] && (edges[tmpNodes[i]][j] < max))
				{
					max = edges[tmpNodes[i]][j];
					argmaxi = tmpNodes[i];
					argmaxj = j;
				}
			}
		}
		
		/*for (i = 1 ; i <=tmpNodes[0] ; i++)
		{
			if (argmaxj == tmpNodes[i]) {printf("Echec... %d\n", argmaxj); exit (1);}
		}*/
		
		tmpEdges[argmaxi][argmaxj] = MAX(-max, 1);
		tmpEdges[argmaxj][argmaxi] = MAX(-max, 1);
		if (!max)
		{
			edges[argmaxi][argmaxj] = -1;
			edges[argmaxj][argmaxi] = -1;
		}
		
		tmpNodes[0]++;
		tmpNodes[tmpNodes[0]] = argmaxj;
		vertices[argmaxj] = 1;
    }
	
	for (i = 0 ; i < s ; i++)
    {
		free (edges[i]);
		edges[i] = tmpEdges[i];
    }
	free (vertices);
	setRoot();
}

/* Prim algorithm computing a valid pseudo tree decomposition 
   (there is no separator variable set included into another one) and using heuristics
   input: starts with a given root
   output: tree height of a valid maximal spanning tree
   does not modify edges matrix */
int prim_rooted_treeheight(int **edges, int **cliques, int s, int start, clusterweight_t clusterWeight)
{
	int th,i, j, max, argmaxi, argmaxj,mindist;
	double maxweight;
	
	int **tmpEdges = (int **) mymalloc (s, sizeof (int *));
	int **copyEdges = (int **) mymalloc (s, sizeof (int *));
	int *tmpNodes = (int *) mycalloc (s+1, sizeof (int));
	int *vertices = (int *) mycalloc (s, sizeof (int)); /* this matrix is just for the case of more than one connected component */
	int *distanceToRoot = (int *) mycalloc (s, sizeof (int));

	for (i = 0 ; i < s ; i++)
    {
		tmpEdges[i] = (int *) mycalloc (s, sizeof (int));
		copyEdges[i] = (int *) mycalloc (s, sizeof (int));
		for (j = 0 ; j < s ; j++) {
		  copyEdges[i][j] = edges[i][j];
		}
    }
	
	tmpNodes[0] = 1;
	tmpNodes[1] = start;
	vertices[tmpNodes[1]] = 1;
	distanceToRoot[tmpNodes[1]] = 0;

	while (tmpNodes[0] < s)
    {
		/* The first step updates the edges (a negative edge means
		that the two vertices aren't in the same subset, and therefore
		the edge between them can be selected... */
		for (i = 0 ; i < s ; i++)
		{
			edges[tmpNodes[tmpNodes[0]]][i] = -edges[tmpNodes[tmpNodes[0]]][i];
			edges[i][tmpNodes[tmpNodes[0]]] = -edges[i][tmpNodes[tmpNodes[0]]];
		}
		
		/* The second step finds the link of maximal weight. */
		
		argmaxi = 0;
		argmaxj = 0;
		max = 1;
		mindist = INT_MAX;
		maxweight = -1e100;
		for (i = 1 ; i <= tmpNodes[0] ; i++)
		{
			for (j = 0 ; j < s ; j++)
			{
				if (!vertices[j] && 
				    ((edges[tmpNodes[i]][j] < max) || 
				     ((edges[tmpNodes[i]][j] == max) && 
				      ((distanceToRoot[tmpNodes[i]] < mindist) ||
				       ((distanceToRoot[tmpNodes[i]] == mindist) &&
					(clusterWeight(j) > maxweight))))))
				{
					max = edges[tmpNodes[i]][j];
					mindist = distanceToRoot[tmpNodes[i]];
					maxweight = clusterWeight(j);
					argmaxi = tmpNodes[i];
					argmaxj = j;
				}
			}
		}
		assert(max < 1);
		tmpEdges[argmaxi][argmaxj] = -max;
		tmpEdges[argmaxj][argmaxi] = -max;
		if (!max)
		{
			tmpEdges[argmaxi][argmaxj] = -1;
			tmpEdges[argmaxj][argmaxi] = -1;
		}
		
		tmpNodes[0]++;
		tmpNodes[tmpNodes[0]] = argmaxj;
		vertices[argmaxj] = 1;
		distanceToRoot[argmaxj] = distanceToRoot[argmaxi] + 1;
    }


	for (i = 0 ; i < s ; i++)
	  {
	    free (edges[i]);
	    edges[i] = tmpEdges[i];
	  }
	Junction.root = start;
	th = getTreeHeight();
	for (i = 0 ; i < s ; i++)
	  {
	    free (edges[i]);
	    edges[i] = copyEdges[i];
	  }

	free (vertices);
	free (distanceToRoot);
	free(tmpNodes);
	
	return th;
}

/* Prim algorithm computing a valid pseudo tree decomposition 
   (there is no separator variable set included into another one) and using heuristics
   input: starts with a given root
   output: edges matrix contains a maximal spanning tree */
void prim_rooted(int **edges, int **cliques, int s, int start, clusterweight_t clusterWeight)
{
	int i, j, max, argmaxi, argmaxj,mindist;
	double maxweight;
	
	int **tmpEdges = (int **) mymalloc (s, sizeof (int *));
	int *tmpNodes = (int *) mycalloc (s+1, sizeof (int));
	int *vertices = (int *) mycalloc (s, sizeof (int)); /* this matrix is just for the case of more than one connected component */
	int *distanceToRoot = (int *) mycalloc (s, sizeof (int));

	for (i = 0 ; i < s ; i++)
    {
		tmpEdges[i] = (int *) mycalloc (s, sizeof (int));
    }
	
	tmpNodes[0] = 1;
	tmpNodes[1] = start;
	vertices[tmpNodes[1]] = 1;
	distanceToRoot[tmpNodes[1]] = 0;

	while (tmpNodes[0] < s)
    {
		/* The first step updates the edges (a negative edge means
		that the two vertices aren't in the same subset, and therefore
		the edge between them can be selected... */
		for (i = 0 ; i < s ; i++)
		{
			edges[tmpNodes[tmpNodes[0]]][i] = -edges[tmpNodes[tmpNodes[0]]][i];
			edges[i][tmpNodes[tmpNodes[0]]] = -edges[i][tmpNodes[tmpNodes[0]]];
		}
		
		/* The second step finds the link of maximal weight. */
		
		argmaxi = 0;
		argmaxj = 0;
		max = 1;
		mindist = INT_MAX;
		maxweight = -1e100;
		for (i = 1 ; i <= tmpNodes[0] ; i++)
		{
			for (j = 0 ; j < s ; j++)
			{
				if (!vertices[j] && 
				    ((edges[tmpNodes[i]][j] < max) || 
				     ((edges[tmpNodes[i]][j] == max) && 
				      ((distanceToRoot[tmpNodes[i]] < mindist) ||
				       ((distanceToRoot[tmpNodes[i]] == mindist) &&
					(clusterWeight(j) > maxweight))))))
				{
					max = edges[tmpNodes[i]][j];
					mindist = distanceToRoot[tmpNodes[i]];
					maxweight = clusterWeight(j);
					argmaxi = tmpNodes[i];
					argmaxj = j;
				}
			}
		}
		assert(max < 1);

		tmpEdges[argmaxi][argmaxj] = -max;
		tmpEdges[argmaxj][argmaxi] = -max;
		if (!max)
		{
			tmpEdges[argmaxi][argmaxj] = -1;
			tmpEdges[argmaxj][argmaxi] = -1;
		}
		
		tmpNodes[0]++;
		tmpNodes[tmpNodes[0]] = argmaxj;
		vertices[argmaxj] = 1;
		distanceToRoot[argmaxj] = distanceToRoot[argmaxi] + 1;
    }

	for (i = 0 ; i < s ; i++)
	  {
	    free (edges[i]);
	    edges[i] = tmpEdges[i];
	  }
	free (vertices);
	free (distanceToRoot);
	free(tmpNodes);
}

int chooseMax(clusterweight_t clusterWeight)
{
	int i, root = 0;
	double weight,max = -1e100;
	
	for (i = 0 ; i < Junction.nCliques ; i++) {
	  weight = clusterWeight(i);
	  if (weight > max) { max = weight; root = i;}
	}
	return root;
}

/* prim without any heuristic */
void primNoHeuritic()
{
 Junction.root = 0;
 prim_rooted(Junction.edges, Junction.cliques, Junction.nCliques, Junction.root, clusterLexico);
}

/* prim with max cluster size heuristic */
void primMaxSize()
{
 Junction.root = chooseMax(clusterSize);
 prim_rooted(Junction.edges, Junction.cliques, Junction.nCliques, Junction.root, clusterSize);
}

/* prim with max cluster expected cost heuristic */
void primMaxExpectedCost()
{
 Junction.root = chooseMax(clusterExpectedCost);
 prim_rooted(Junction.edges, Junction.cliques, Junction.nCliques, Junction.root, clusterExpectedCost);
}

/* prim with max cluster lower bound heuristic */
void primMaxLowerBound()
{
 Junction.root = chooseMax(clusterLowerBound);
 prim_rooted(Junction.edges, Junction.cliques, Junction.nCliques, Junction.root, clusterLowerBound);
}

/* prim with min tree height then max clusterWeight heuristic */
void primMinTreeHeightAndHeuristic(clusterweight_t clusterWeight)
{
  int i,minth=INT_MAX,th,argmin;
  double maxweight=-1e100;

  for (i = 0 ; i < Junction.nCliques ; i++) {
    th = prim_rooted_treeheight(Junction.edges, Junction.cliques, Junction.nCliques, i, clusterWeight);
    if (th < minth || ((th == minth) && (clusterWeight(i) > maxweight))) {
      argmin = i;
      minth = th;
      maxweight = clusterWeight(i);
    }
  }
  Junction.root = argmin;
  prim_rooted(Junction.edges, Junction.cliques, Junction.nCliques, argmin, clusterWeight);
}

/* prim with min tree height then without any heuristic */
void primMinTreeHeight()
{
 primMinTreeHeightAndHeuristic(clusterLexico);
}

/* prim with min tree height then with max cluster size heuristic */
void primMinTreeHeightMaxSize()
{
 primMinTreeHeightAndHeuristic(clusterSize);
}

/* prim with min tree height then with max cluster expected cost heuristic */
void primMinTreeHeightMaxExpectedCost()
{
 primMinTreeHeightAndHeuristic(clusterExpectedCost);
}

/* prim with min tree height then with max cluster lower bound heuristic */
void primMinTreeHeightMaxLowerBound()
{
 primMinTreeHeightAndHeuristic(clusterLowerBound);
}


//-----------------------------------------------------------------------------------
// Cluster Selection for first cluster
//-----------------------------------------------------------------------------------

functvoid_t ClusterSelectFunc[] = {
  primNoHeuritic,
  primMaxSize,
  primMaxExpectedCost,
  primMaxLowerBound,
  primMinTreeHeight,
  primMinTreeHeightMaxSize,
  primMinTreeHeightMaxExpectedCost,
  primMinTreeHeightMaxLowerBound
};



int EK (int **FFmatrix, int s, int t, int nvertices, int ub)
{
	int currentCap, totalCap;
	
	//printf ("Welcome to Ford Fulkerson : %d - %d.\n", s, t);
	//getchar ();
	
	int *colors = (int *) mymalloc (nvertices, sizeof (int));
	int *d = (int *) mymalloc (nvertices, sizeof (int));
	int *pi = (int *) mymalloc (nvertices, sizeof (int));
	
	currentCap = BFS (FFmatrix, s, t, nvertices, NULL, colors, d, pi);
	totalCap = currentCap;
	while (currentCap && (totalCap < ub))
    {
		currentCap = BFS (FFmatrix, s, t, nvertices, NULL, colors, d, pi);
		totalCap += currentCap;
		/*      printf("=================\n");
		for (i = 0 ; i < nvertices ; i++) 
		{ 
			printf ("| "); 
			for (j = 0 ; j < nvertices ; j++) 
			{ 
				printf ("%d ", FFmatrix[i][j]); 
			} 
			printf("|\n"); 
		}
		getchar ();*/
    }
	
	free (colors);
	free (d);
	free (pi);
	
	if (totalCap < ub)
    {
		return (0);
    }
	return (1);
	//  printf ("Byebye : %d - %d.\n", s, t);
	//getchar ();
}

int findMinCut (int **incidence, int *minCut, int nvertices)
{
	int **ADG; /* Like Auxiliary Directed Graph */
	int i, j = 1, k, l;
	int min = nvertices, currentCut;
	int **currentCutset;
	int *currentsimplCutset;
	int stop = 0;
	int **connected;
	int currentN, nbConnected;
	int N = INT_MAX;
	int **FFmatrix;
	
	FFmatrix = (int **) mymalloc (nvertices*2, sizeof (int *));
	
	for (i = 0 ; i < nvertices*2 ; i++) {
		FFmatrix[i] = (int *) mycalloc (nvertices*2, sizeof (int));
    }
	
	for (i = 0 ; (i < nvertices-1) && !stop ; i++) {
		for (j = i+1 ; (j < nvertices) && !stop ; j++) {
			stop = !incidence[i][j];
		}
    }
	
	if (!stop) {
		//      printf ("Complet!!! ! %d\n", nvertices);
		return (nvertices);
    }
	
	ADG = (int **) mymalloc (nvertices*2, sizeof (int *));
	currentCutset = (int **) mymalloc (nvertices+1, sizeof (int *));
	connected = (int **) mymalloc (nvertices, sizeof (int *));
	
	for (i = 0 ; i < nvertices ; i++)
    {
		connected[i] = (int *) mymalloc (nvertices, sizeof (int));
		ADG[2*i] = (int *) mycalloc (nvertices*2, sizeof (int)); /* Even nodes are the "head" nodes */
		ADG[2*i+1] = (int *) mycalloc (nvertices*2, sizeof (int)); /* Odd nodes are the "tail" nodes */
    }
	
	currentCutset[0] = (int *) mycalloc (1, sizeof (int));
	for (i = 0 ; i < nvertices ; i++)
    {
		if (i) {currentCutset[i] = (int *) mycalloc (2, sizeof (int));}
		for (j = 0 ; j < nvertices ; j++) {
			if (incidence[i][j]) {
				ADG[2*i+1][2*j] = INT_MAX;
				ADG[2*j+1][2*i] = INT_MAX;
			}
		}
		ADG[2*i][2*i+1] = 1;
    }
	currentCutset[i] = (int *) mycalloc (2, sizeof (int));
	
	/*  for (i = 0 ; i < 2*nvertices ; i++)
    {
		printf ("| ");
		for (j = 0 ; j < 2*nvertices ; j++)
		{
			printf ("%d ", ADG[i][j]);
		}
		printf ("|\n");
	}*/
	
	
	for (i = 0 ; (i < nvertices) && (min > 1) ; i++) {
		for (j = 0 ; (j < nvertices) && (min > 1) ; j++) {
			currentCutset[0][0] = 0;
			
			for (k = 0 ; k < 2*nvertices ; k++) {
				for (l = 0 ; l < 2*nvertices ; l++) {
					FFmatrix[k][l] = ADG[k][l];
				}
			} /* We work on a copy of the matrix (otherwise we modify the initial matrix) */



	  if (!(ADG[2*i+1][2*j])&&(currentCut = findMinCutst (FFmatrix, ADG, currentCutset, 2*i+1, 2*j, 2*nvertices, min)) <= min)
	  {
	      if (currentCut == min) {
			  //printf ("Hej !\n");
			  currentN = 0; 
			  currentsimplCutset = (int *) mymalloc (currentCutset[0][0]+1, sizeof (int)); 
			  currentsimplCutset[0] = currentCutset[0][0]; 
			  for (k = 1 ; k <= currentCutset[0][0] ; k++) { 
				  currentsimplCutset[k] = (int) currentCutset[k][0]/2; 
			  } 
			  //getchar ();
			  nbConnected = getConnectedComponents (incidence, currentsimplCutset, connected,  nvertices); 
			  for (k = 0 ; k < nbConnected ; k++) 
			  { 
				  currentN = MAX(currentN, connected[k][0]); 
			  } 
			  //getchar ();
			  if (currentN < N) 
			  { 
				  N = currentN; 
				  for (k = 0 ; k <= currentsimplCutset[0] ; k++) 
				  { 
					  minCut[k] = currentsimplCutset[k]; 
				  } 
			  } 
			  free (currentsimplCutset); 
		  } 
		  else 
		  {
			  min = currentCut;
			  minCut[0] = currentCutset[0][0];
			  for (k = 1 ; k <= currentCutset[0][0] ; k++)
			  {
				  minCut[k] = (int) currentCutset[k][0]/2;
			  }
			  nbConnected = getConnectedComponents (incidence, minCut, connected, nvertices);
			  N = 0;
			  for (k = 0 ; k < nbConnected ; k++)
			  {
				  N = MAX(N, connected[k][0]);
			  }
		  }
	  }
	  /*for (k = 1 ; k <= currentCutset[0][0] ; k++)
	    {
	      printf("%d ", currentCutset[k][0]/2);
	    }
	  printf("\nCurrentCut : %d.\n", currentCut);
	  getchar ();*/

		}
    }

  for (i = 0 ; i < nvertices ; i++)
  {
      free (connected[i]);
  }

  free (connected);

  for (i = 0 ; i < 2*nvertices ; i++)
  {
      free (ADG[i]);
      free (FFmatrix[i]);
  }

  for (i = 0 ; i < nvertices+1 ; i++)
  {
      free (currentCutset[i]);
  }
  free (ADG);
  free (FFmatrix);
  free (currentCutset);
  
  return (min);

}


int findMinCutst (int **FFmatrix, int **incidence, int **minCut, int s, int t, int nvertices, int ub)
{
	int i, j;
	int *vertices;
	int *path;
	
	//printf("s = %d, t = %d\n", s, t);
	//getchar ();
	
	
	//  FordFulkersonize (FFmatrix, s, t, nvertices);
	if (EK (FFmatrix, s, t, nvertices, ub))
    {
		//printf ("On coupe !\n");
		return (ub+10);
    }
	
	vertices = (int *) mycalloc (nvertices, sizeof (int));
	path = (int *) mycalloc (nvertices, sizeof (int));
	BFS (FFmatrix, s, t, nvertices, vertices, NULL, NULL, NULL);
	//findAugmentingPath (FFmatrix, path, vertices, s, t, -1, nvertices);
	
	for (i = 0 ; i < nvertices ; i++)
    {
		if (vertices[i])
		{
			for (j = 0 ; j < nvertices ; j++)
			{
				if (incidence[i][j] && (!vertices[j]))
				{
					minCut[0][0]++;
					minCut[minCut[0][0]][0] = i;
					minCut[minCut[0][0]][1] = j;
				}
			}
		}
    }
	
	free (vertices);
	free (path);
	
	return (minCut[0][0]);
	
}



/*=======================================================================================*/

int BFS (int **incidence, int s, int t, int nvertices, int *cut, int *colors, int *d, int *pi)
{
	int i, j;
	int minCap;
	int freecolors = 0;
	int freed = 0;
	int freepi = 0;
	int *stack = (int *) mycalloc (nvertices, sizeof (int));
	
	if (!colors)
    {
		colors = (int *) mymalloc (nvertices, sizeof (int));
		freecolors = 1;
    }
	if (!d)
    {
		d = (int *) mymalloc (nvertices, sizeof (int));
		freed = 1;
    }
	if (!pi)
    {
		pi = (int *) mymalloc (nvertices, sizeof (int));
		freepi = 1;
    }
	
	for (i = 0 ; i < nvertices ; i++)
    {
		colors[i] = 0;
		d[i] = INT_MAX;
		pi[i] = -1;
    }
	
	colors[s] = 1;
	d[s] = 0;
	pi[s] = -1;
	
	stack[0]++;
	stack[1] = s;
	
	while (stack[0] && (stack[1] != t))
    {
		//      printf ("On r�cup�re %d.\n", stack[1]);
		i = stack[1];
		for (j = 0 ; j < nvertices ; j++)
		{
			if (incidence[i][j])
			{
				if (!colors[j])
				{
					colors[j] = 1;
					d[j] = d[i]+1;
					pi[j] = i;
					stack[0]++;
					stack[stack[0]] = j;
					//	  printf ("On empile %d.\n", j);
				}
			}
		}
		stack[0]--;
		//      printf ("on d�pile... Etat de la pile : %d - ", stack[0]);
		for (j = 1 ; j <= stack[0] ; j++)
		{
			//	  printf("%d ", stack[j+1]);
			stack[j] = stack[j+1];
		}
		//      printf ("\n");
		//      getchar ();
		colors[i] = 2;
		if (cut) {cut[i] = 1;}
    }
	
	
	if ((t < 0) || (d[t] == INT_MAX))
    {
		if (freecolors) free (colors);
		if (freed) free (d);
		if (freepi) free (pi);
		free (stack);
		return (0);
    }
	
	minCap = updateFlow (incidence, pi, s, t);
	if (freecolors) free (colors);
	if (freed) free (d);
	if (freepi) free (pi);
	free (stack);
	return (minCap);
	
}


int updateFlow (int **incidence, int *pi, int s, int t)
{
	int cv = t, minCap = INT_MAX;
	
	while (cv != s)
    {
		minCap = MIN(incidence[pi[cv]][cv], minCap);
		cv = pi[cv];
    }
	
	cv = t;
	while (cv != s)
    {
		incidence[pi[cv]][cv] -= minCap;
		incidence[cv][pi[cv]] += minCap;
		cv = pi[cv];
    }
	return (minCap);
}

int getConnectedComponents (int **incidence, int *minCut, int **connected, int nvertices)
{
	int nb = 0, i, j;
	int **tmpInc;
	int *vertices;
	int *cut;
	
	vertices = (int *) mymalloc (nvertices, sizeof (int));
	tmpInc = (int **) mymalloc (nvertices, sizeof (int *));
	
	for (i = 0 ; i < nvertices ; i++)
    {
		//      printf("Adding %d.\n", i);
		vertices[i] = 1;
		tmpInc[i] = (int *) mymalloc (nvertices, sizeof (int));
		for (j = 0 ; j < nvertices ; j++)
		{
			tmpInc[i][j] = incidence[i][j];
		}
    } /* Initializes the incidence matrix */

  for (i = 1 ; i <= minCut[0] ; i++)
  {
      //      printf("Removing %d.\n", minCut[i]);
      vertices[minCut[i]] = 0;
      for (j = 0 ; j < nvertices ; j++)
	  {
		  tmpInc[minCut[i]][j] = 0;
		  tmpInc[j][minCut[i]] = 0;
	  }
  } /* Removes the edges with the separator */

  for (i = 0 ; (i < nvertices) ; i++)
  {
      if (vertices[i])
	  {
		  connected[nb] = (int *) mycalloc (nvertices, sizeof (int));
		  //connected[nb][0] = recurGCC (tmpInc, &(connected[nb][1]), nvertices, 0, vertices, i);
		  cut = (int *) mycalloc (nvertices, sizeof (int));
		  BFS (tmpInc, i, -1, nvertices, cut, NULL, NULL, NULL);
		  for (j = 0 ; j < nvertices ; j++)
		  {
			  if (cut[j])
			  {
				  vertices[j] = 0;
				  connected[nb][0]++;
				  connected[nb][connected[nb][0]] = j;
			  }
		  }
		  free (cut);
		  nb++;
	  }
  }

  for (i = 0 ; i < nvertices ; i++)
  {
      free (tmpInc[i]);
  }
  free (tmpInc);
  free (vertices);


  return (nb);
}

/*=======================================================================================*/
void FordFulkersonize (int **incidence, int o, int t, int nvertices)
{
	int *path; /* The sequence of vertices of the augmenting chain */
	int capacity; /* The capacity of the augmenting chain */
	int globalcapacity = 0;
	int i;
	int *vertices; /* The vertices that are already selected in the current chain */
	
	printf ("Welcome to Ford Fulkerson : %d - %d.\n", o, t);
	getchar ();
	
	path = (int *) mycalloc (nvertices+1, sizeof (int));
	vertices = (int *) mycalloc (nvertices, sizeof (int));
	
	capacity = findAugmentingPath (incidence, path, vertices, o, t, INT_MAX, nvertices); /* Find an augmenting path */
	globalcapacity += capacity;
	
	/*  for (i = 0 ; i < nvertices ; i++)
	{
		printf ("| ");
		for (j = 0 ; j < nvertices ; j++)
		{
			printf ("%d ", incidence[i][j]);
		}
		printf("|\n");
	} Display it */
	
	while (capacity) /* While there is an augmenting chain */
    {
		// printf("Chemin :\n| ");
		for (i = 1 ; i < path[0] ; i++)
		{
			//printf("%d ", path[i]);
			incidence[path[i]][path[i+1]] -= capacity;
			incidence[path[i+1]][path[i]] += capacity;
		} /* Update the capacities of each edge of the path */
      //        printf("%d |\n", path[i]);
      //      printf("capacit� %d.\n", capacity);

      /*for (i = 0 ; i < nvertices ; i++)
	{
		  printf ("| "); 
		  for (j = 0 ; j < nvertices ; j++) 
		  { 
			  printf ("%d ", incidence[i][j]);
		  } 
		  printf("|\n"); 
	} display the new matrix */ 
      path[0] = 0;
      capacity = findAugmentingPath (incidence, path, vertices, o, t, INT_MAX, nvertices); /* Find a new augmenting path */
      globalcapacity += capacity;
      free (vertices);
      vertices = (int *) mycalloc (nvertices, sizeof (int));
      
    }
  // printf("Capacit� du r�seau : %d.\n", globalcapacity);

  free (path);
  free (vertices);
}




int findAugmentingPath (int **incidence, int *path, int *vertices, int v, int t, int capacity, int nvertices)
{
	int currentCapacity = 0;
	int i;
	
	//printf ("v = %d, capacity = %d.\n", v, capacity);
	//getchar();
	
	
	vertices[v] = 1; /* Mark the current vertex as opened (grey) */
	path[0]++; /* Increase the length of the path */
	path[path[0]] = v; /* Add the vertex to the current path */
	
	
	/*  for (i = 1 ; i < path[0] ; i++)
    {
		printf("%d ", path[i]);
    }
    getchar ();*/
	
	if (v == t) /* If the vertex is the sink */
    {
		//      printf ("fin de chemin : %d.\n", path[path[0]]);
		/* vertices[v] = 0; Unlabel it */
		return (capacity); /* Return the final capacity */
    }
	
	for (i = 0 ; (i < nvertices) && (currentCapacity < 1) ; i++)
    {
		if ((!vertices[i]) && incidence[v][i])
		{
			currentCapacity = MIN(capacity, incidence[v][i]);
			currentCapacity = findAugmentingPath (incidence, path, vertices, i, t, currentCapacity, nvertices);
		}
		
    }
	vertices[v] = 2; /* Label the vertex as closed (black) */
	if (!currentCapacity)
    {
		path[0]--;
		//printf ("Backtrack !\n");
    }
	
	if ((capacity != -1))
    {
		vertices[v] = 0;
    }
	
	/*  for (i = 0 ; i < nvertices ; i++)
	{ 
		printf("%d ", vertices[i]);
	}
	printf("\n");*/
	/*  printf ("Fin de procedure : %d.\n", v);
	getchar ();*/
	
	return (currentCapacity);
}

/*==============================================================================================*/
/* Koster, Old version (if you ever find a bug in the new version of splitCluster, just swap    */
/* the new one with the following old one...)                                                   */
/*==============================================================================================*/
void oldSplitCluster (int cluster)
{
	
	/*------ Welcome to SplitCluster ! --------*/
	/*------ Declaration / Allocation -------*/
	
	
	int i, j, k;                  /* iterators */
	int *vertices;                /* set of vertices in the cluster */
	int *inverseVertices;         /* index of each vertex */
	int *separator;
	int **tmpInc;                 /* matrix of incidence of the procedure */
	int *minCut;
	int **connected;
	int nbConnected = 0;
	int *oldSepNeighbours;
	int *cpArray;
	int oldNCliques = 0;
	int sepSize;
	
	int *simplicial;
	int sepSize2;
	
	
	
	
	printf("Splitting cluster %d, nvertices = %d. Created by %d\n", cluster, Junction.cliques[cluster][0], separateurs[cluster]);
	
	/*for (i = 0 ; i < Junction.nCliques ; i++)
    {
		printf("| ");
		for (j = 0 ; j < Junction.cliques[i][0] ; i++)
		{
			printf("%d ", Junction.cliques[i][j+1]);
		}
		printf ("|\n%d.\n", Junction.cliques[cluster][0]);
    }
    getchar ();*/
	
	vertices = (int *) mymalloc (Junction.cliques[cluster][0]+1, sizeof (int));
	inverseVertices = (int *) mycalloc (NVar+1, sizeof (int));
	tmpInc = (int **) mymalloc (Junction.cliques[cluster][0], sizeof (int *));
	oldSepNeighbours = (int *) mymalloc (Junction.nCliques, sizeof (int));
	cpArray = (int *) mymalloc (Junction.nCliques, sizeof (int));
	
	simplicial = (int *) mycalloc (Junction.cliques[cluster][0], sizeof (int));
	
	/*----------------------------------------*/
	
	
	/*------ Initialization ----------*/
	
	vertices[0] = Junction.cliques[cluster][0];
	inverseVertices[0] = NVar;
	
	
	/* This loop initializes the new graph with the vertices of the cluster */
	for (i = 1 ; i <= Junction.cliques[cluster][0] ; i++)
    {
		tmpInc[i-1] = (int *) mycalloc (Junction.cliques[cluster][0], sizeof (int));
		vertices[i] = Junction.cliques[cluster][i];
		inverseVertices[Junction.cliques[cluster][i]+1] = i-1;
    }
	/* This loop initializes the new graph with the vertices of the cluster */
	
	connected = (int **) mymalloc (Junction.cliques[cluster][0]-1, sizeof (int*));
	
	for (i = 0 ; i < Junction.cliques[cluster][0]-1 ; i++)
    {
		connected[i] = (int *) mycalloc (Junction.cliques[cluster][0]-2, sizeof (int));
    }
	
	
	/*printf("| ");
	for (i = 1 ; i <= Junction.cliques[cluster][0] ; i++)
    {
		printf ("%d ", vertices[i]);
    }
	printf ("|\n");
	getchar ();*/
	
	
	
	/* This loop initializes the new graph with the edges of the cluster */
	for (i = 0 ; i < Junction.cliques[cluster][0] ; i++)
    {
		for (j = i+1 ; j < Junction.cliques[cluster][0] ; j++)
		{
			tmpInc[i][j] = ElimGraph[vertices[i+1]][vertices[j+1]];
			tmpInc[j][i] = ElimGraph[vertices[i+1]][vertices[j+1]];
		}
    }
	/* This loop initializes the new graph with the edges of the cluster */
	
	
	
	/*if ((cluster == 128)||(cluster == 169))
    {
		for (i = 0 ; i < Junction.cliques[cluster][0] ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.cliques[cluster][0] ; j++)
			{
				printf("%d ", tmpInc[i][j]);
			}
			printf("|\n");
		}
		getchar ();
	}*/
	
	
	
	
	/* The following loop makes cliques of the separators between the cluster and
		its neighbours. */
	for (i = 0 ; i < Junction.nCliques ; i++) /* Look for the neighbours */
    {
		if (Junction.edges[cluster][i]) /* A neighbour is found */
		{
			separator = (int *) mycalloc (Junction.cliques[cluster][0]+1, sizeof (int));
			getSeparator (separator, cluster, i); /* We can get the separator... */
			//if (cluster == 128) printf("separatorsize : %d.\n", getSeparatorSize (cluster, i));
			for (j = 1 ; j <= separator[0] ; j++)
			{
				for (k = j+1 ; k <= separator[0] ; k++)
				{
					//if (cluster == 128)  printf ("filling %d and %d.\n", separator[j], separator[k]);
					tmpInc[inverseVertices[separator[j]+1]][inverseVertices[separator[k]+1]] = 1;
					tmpInc[inverseVertices[separator[k]+1]][inverseVertices[separator[j]+1]] = 1;
				}
			}
			/*
			 for (j = 0 ; j < Junction.edges[cluster][i] ; j++)
			 {
				 for (k = j+1 ; k < Junction.edges[cluster][i] ; k++) 
				 {
					 tmpInc[j][k] = 1;
					 tmpInc[k][j] = 1;
				 }
			 }
			 */
			free (separator);
		}
    }
	/* The following loop makes cliques of the separators between the cluster and
		its neighbours. */
	
	//while (preprocess (simplicial, vertices, tmpInc));
	free (simplicial);
	
	/*----------------------------------------*/
	
	/*if ((cluster == 128)||(cluster == 169))
    {
		printf ("On fait une clique...\n");
		for (i = 0 ; i < Junction.cliques[cluster][0] ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.cliques[cluster][0] ; j++)
			{
				printf("%d ", tmpInc[i][j]);
			}
			printf("|\n");
		}
		getchar ();
	}*/
	
	//printf ("On cherche un s�parateur de taille minimale...\n");
	//getchar ();
	
	
	/*------------ Find a cut of minimum vertices ----------------*/
	
	minCut = (int *) mymalloc (Junction.cliques[cluster][0], sizeof (int *));
	sepSize = findMinCut (tmpInc, minCut, Junction.cliques[cluster][0]);
	
	if (sepSize == 0)
    {
		printf ("FATAL ERROR. Something very serious has happened. Kernel Panic... Exiting.\n");
		printf ("cluster %d created by cluster %d with %d vertices.\n", cluster, separateurs[cluster], Junction.cliques[separateurs[cluster]][0]);
		exit (1);
    }
	
	
	/* We get the minimum vertex separator of the modified cluster.
		This is the main point of the procedure. */
	
	/*------------------------------------------------------------*/
	
	
	//  oldNVertices = Junction.cliques[cluster][0];
	
	
	
	if (sepSize < Junction.cliques[cluster][0]) /* This condition means that the graph isn't complete */
    {
		
		//printf ("On cherche les composantes connexes...\n");
		//getchar ();
		
		/*--------- Get the connected components ----------*/
		
		nbConnected = getConnectedComponents (tmpInc, minCut, connected, Junction.cliques[cluster][0]);
		
		/*-------------------------------------------------*/
		
		/*      if (cluster == 128)
	{
			printf("| ");
			for (i = 1 ; i <= minCut[0] ; i++)
			{
				//	  minCut[i] = vertices[minCut[i]+1];
				printf("%d ", minCut[i]);
			}
			printf("|\n");
			getchar ();
	}*/
		
		
		/*printf ("Connected components :\n");
		
		for (i = 0 ; i < nbConnected ; i++)
	{
			printf ("| ");
			for (j = 1 ; j <= connected[i][0] ; j++)
			{
				//	      connected[i][j] = vertices[connected[i][j]+1];
				printf ("%d ", vertices[connected[i][j]+1]);
			}
			printf ("|\n");
	}
		getchar ();*/
		
		
		
		/*-------- Build the new junction graph (update the old one) ----------*/
		
		/*free (Junction.cliques[cluster]);
		Junction.cliques[cluster] = (int *) mymalloc (minCut[0]+1, sizeof (int));,
		Junction.cliques[cluster][0] = minCut[0];*/
		
		/* The incidence matrix... */
		Junction.edges = (int **) realloc (Junction.edges, (Junction.nCliques+nbConnected)*sizeof (int *));
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			/* ??? */
			for (j = 0 ; j < Junction.nCliques ; j++)
			{
				cpArray[j] = Junction.edges[i][j];
			}
			free (Junction.edges[i]);
			Junction.edges[i] = (int *) mycalloc (Junction.nCliques + nbConnected, sizeof (int));
			for (j = 0 ; j < Junction.nCliques ; j++)
			{
				Junction.edges[i][j] = cpArray[j];
			}
		}
		for ( ; i < Junction.nCliques+nbConnected ; i++)
		{
			Junction.edges[i] = (int *) mycalloc (Junction.nCliques + nbConnected, sizeof (int));
		}
		
		/* Now the incidence matrix has the good dimensions */
		
		
		/*printf ("On redimensionne la matrice edges :\n");
		
		for (i = 0 ; i < Junction.nCliques+nbConnected ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.nCliques+nbConnected ; j++)
			{
				printf("%d ", Junction.edges[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		
		
		
		
		
		/* We save the neighbours of the former cluster
			and we delete the values concerning the cluster in the 
			incidence matrix */
		for (j = 0 ; j < Junction.nCliques ; j++)
		{
			oldSepNeighbours[j] = Junction.edges[cluster][j];
			Junction.edges[cluster][j] = 0;
			Junction.edges[j][cluster] = 0;
		}
		oldNCliques = Junction.nCliques ;
		
		
		
		
		/* printf ("On vire les valeurs concernant les anciens voisins dans la matrice edges :\n");
		
		for (i = 0 ; i < Junction.nCliques+nbConnected ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.nCliques+nbConnected ; j++)
			{
				printf("%d ", Junction.edges[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		
		
		Junction.cliques = (int **) realloc (Junction.cliques, (Junction.nCliques+nbConnected)*sizeof (int *));
		
		free (Junction.cliques[cluster]);
		Junction.cliques[cluster] = (int *) mymalloc (minCut[0]+1, sizeof (int));
		
		Junction.cliques[cluster][0] = minCut[0]; 
		for (i = 1 ; i <= minCut[0] ; i++)
		{
			//printf ("mincut[%d] = %d. vertices = %d.\n", i, minCut[i], vertices[minCut[i]+1]);
			Junction.cliques[cluster][i] = vertices[minCut[i]+1];
		}
		
		/* The cluster becomes the separator. */
		
		for (i = 0 ; i < nbConnected ; i++)
		{
			separateurs[Junction.nCliques] = cluster;
			if (connected[i][0] == 0)
			{
				printf ("FATAL ERROR. Something very serious has happened. Kernel Panic... Exiting.\n");
				exit (1);
			}
			Junction.cliques[Junction.nCliques] = (int *) mymalloc (connected[i][0]+1+minCut[0], sizeof (int));
			Junction.cliques[Junction.nCliques][0] = connected[i][0]+minCut[0];
			for (j = 1 ; j <= connected[i][0] ; j++)
			{
				//	      printf ("connected[%d][%d] = %d. vertices = %d.\n", i, j, connected[i][j], vertices[connected[i][j]+1]);
				Junction.cliques[Junction.nCliques][j] = vertices[connected[i][j]+1];
			}
			for ( ; j <= Junction.cliques[Junction.nCliques][0] ; j++)
			{
				Junction.cliques[Junction.nCliques][j] = vertices[minCut[j-connected[i][0]]+1];
			}
			Junction.nCliques++;
		}
		/* Creates the new clusters with one connected component each. */
		
		
		
		/*printf ("On cr�e la matrice cliques...\n");
		
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			printf("| ");
			for (j = 1 ; j <= Junction.cliques[i][0] ; j++)
			{
				printf("%d ", Junction.cliques[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		
		
		for (i = 0 ; i < oldNCliques ; i++)
		{
			if (oldSepNeighbours[i]) /* For each former neighbour */
			{
				//if (cluster == 128) printf ("Ancien voisin : %d.\n", i);
				sepSize = getSeparatorSize (cluster, i); /* First get the number of vertices in common with the separator */
				sepSize2 = 0;
				for (j = oldNCliques ; (j < Junction.nCliques) && (sepSize2 <= sepSize) ; j++)
				{
					sepSize2 = getSeparatorSize (i, j); /* Then get the number of vertices in common with each connected component */
				}
				j--;
				if (sepSize2 <= sepSize) {j = cluster; sepSize2 = sepSize;}
				//if (cluster == 128) printf ("s�parateur entre cliques %d et %d : %d\n", i, j, sepSize2);
				Junction.edges[i][j] = sepSize2;
				Junction.edges[j][i] = sepSize2;
			}
		}
		
		for (i = oldNCliques ; i < Junction.nCliques ; i++)
		{
			sepSize = getSeparatorSize (cluster, i);
			//if (cluster == 128) printf ("s�parateur entre cliques %d et %d : %d\n", cluster, i, sepSize);
			Junction.edges[i][cluster] = sepSize;
			Junction.edges[cluster][i] = sepSize;
		}
		
		
		/*--------------------------------------------------------------------------*/
		
		/*printf ("On r�actualise la matrice d'incidence...\n");
		
		for (i = 0 ; i < Junction.nCliques ; i++)
		{
			printf("| ");
			for (j = 0 ; j < Junction.nCliques ; j++)
			{
				printf("%d ", Junction.edges[i][j]);
			}
			printf("|\n");
		}
		getchar ();*/
		//      exit(0);
		
    }
	
	
	for (i = 0 ; i < vertices[0]-1 ; i++)
    {
		free (connected[i]);
    }
	free (connected);
	
	for (i = 0 ; i < Junction.cliques[cluster][0] ; i++)
    {
		free (tmpInc[i]);
    }
	
	free (tmpInc);
	free (vertices);
	free (inverseVertices);
	free (minCut);
	free (oldSepNeighbours);
	free (cpArray);
	
	for (j = oldNCliques ; (j < oldNCliques+nbConnected) ; j++)
    {
		splitCluster (j); /* recursively call itself on the new nodes created */
    }
	
	
}
/*==============================================================================================*/
/* End Old version */
/*==============================================================================================*/
