#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "wcsp.h"


#define WLIMIT 32 //Maximum width allowed. It deals with the implementation as calloc allows unsigned int

/* Type definition */

typedef struct
{
	short aridad; /* arity of the cost function*/
	short *vars; /* scope */
	cost_t *costs; /* cost matrix */
	unsigned int ntuples;      /* number of tuples */
	int *despl_var; /* shift to be done for each variable value in order to obtain the position in costs to a given tuple */
	void *sig_func; /* next function */
} nodofunc;

typedef struct
{
	short num_funcs;
	nodofunc *primera_func_original; // 1a_func_ori -> ... ult_func_or -> NULL
	nodofunc *ultima_func_original; // 1a_func_ori -> ... ult_func_or -> NULL
	nodofunc *primera_func_anadida;  // 1a_func_ana -> ... -> NULL
} bucket;


/* Global variables used in bucket elimination */
extern int Width;		//maximum width allowed for the (mini)buckets

extern bucket *Bucket; //buckets of the problem
extern bucket *Result; //zero-ary cost functions
extern bucket *MB; //minibucket used when the bucket is too costly to be computed exactly

extern short *estruct_a_fich; // para pasar del preorden de vars al preorden del fichero
extern short *fich_a_estruct; // para pasar del preorden del fichero al preorden de vars

extern short max_degree; //maximum degree of the problem
extern int exactBuckets; //number of buckets processed exactly

/* Functions used in bucket elimination */
void decreaseOrder (short *vector, int N);
float log2i (int pot2);
void *freeFunction (nodofunc *nodo);
void freeBucket (bucket *b);

void initStructures ();


void solveBE ();

