/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: tblist.c
  $Id: tblist.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
/* ABSTRACT DATA TYPE "LIST".
 * A list variable is a list of <int1,int2,int3,int4>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "wcsp.h"
#include "solver.h"

node_LIST *MEM_DIM;


void List_allocate_dynamic_memory()
{int i; node_LIST *temp;

 MEM_DIM=NULL;
 for(i=0;i<5000;i++)
   {
     temp=MEM_DIM;
     MEM_DIM=(node_LIST *)malloc(sizeof(node_LIST));
     MEM_DIM->sig=temp;
   }

}

void List_get_more_dynamic_memory()
{int i; node_LIST *temp;

 for(i=0;i<1000;i++)
   {
     temp=MEM_DIM;
     MEM_DIM=(node_LIST *)malloc(sizeof(node_LIST));
     MEM_DIM->sig=temp;
   }

}


void List_deallocate_dynamic_memory()
{node_LIST *temp;

 while(MEM_DIM!=NULL)
 { 
     temp=MEM_DIM;
     MEM_DIM=MEM_DIM->sig;
     free(temp);
   }
}

node_LIST *pido()
{node_LIST *temp;
 if(MEM_DIM==NULL) List_get_more_dynamic_memory();
 temp=MEM_DIM;
 MEM_DIM=MEM_DIM->sig;
 return(temp);
}

void devuelvo(node_LIST *p)
{
 p->sig=MEM_DIM;
 MEM_DIM=p;
}

void List_create(List *l)
/* Creates an empty list */
{
 l->prim=pido();
 l->ult=l->prim;
 l->finger=l->prim;
 l->n_elem=0;
 l->prim->ant=NULL;
 l->prim->sig=NULL;
}
void List_insert_first (List *l, int elem1)
{
 node_LIST *temp;

 l->n_elem++;
 temp=pido();
 temp->val1=elem1;
 temp->ant=l->prim;
 temp->sig=l->prim->sig;
 if(temp->sig==NULL)l->ult=temp;
 else temp->sig->ant=temp;
 l->prim->sig=temp;
}

void List_insert1 (List *l, int elem1)
{
 node_LIST *temp;

 l->n_elem++;
 temp=pido();
 temp->val1=elem1;
 temp->ant=l->finger;
 temp->sig=l->finger->sig;
 if(temp->sig==NULL)l->ult=temp;
 else temp->sig->ant=temp;
 l->finger->sig=temp;
}
void List_insert2 (List *l, int elem1, int elem2)
{
 node_LIST *temp;

 l->n_elem++;
 temp=pido();
 temp->val1=elem1;
 temp->val2=elem2;
 temp->ant=l->finger;
 temp->sig=l->finger->sig;
 if(temp->sig==NULL)l->ult=temp;
 else temp->sig->ant=temp;
 l->finger->sig=temp;
}

void List_insert4 (List *l, int elem1, int elem2, int elem3, int elem4)
{
 node_LIST *temp;

 l->n_elem++;
 temp=pido();
 temp->val1=elem1;
 temp->val2=elem2;
 temp->val3=elem3;
 temp->val4.i=elem4;
  temp->ant=l->finger;
 temp->sig=l->finger->sig;
 if(temp->sig==NULL)l->ult=temp;
 else temp->sig->ant=temp;
 l->finger->sig=temp;
}

void List_insert3pc (List *l, int elem1, int elem2, int elem3, cost_t elem4)
{
 node_LIST *temp;

 l->n_elem++;
 temp=pido();
 temp->val1=elem1;
 temp->val2=elem2;
 temp->val3=elem3;
 temp->val4.c=elem4;
  temp->ant=l->finger;
 temp->sig=l->finger->sig;
 if(temp->sig==NULL)l->ult=temp;
 else temp->sig->ant=temp;
 l->finger->sig=temp;
}

void List_delete_node (List *l)
{
 node_LIST *temp;

 l->n_elem--;
 temp=l->finger->sig;
 l->finger->sig=temp->sig;
 if(temp->sig==NULL){l->ult=l->finger;}
 devuelvo(temp);
}

int List_empty(List *l)
{return(l->n_elem==0);}

int List_n_of_elem(List *l)
{return(l->n_elem);}

void List_reset_traversal(List *l)
{l->finger=l->prim;}

int List_end_traversal(List *l)
{return(l->finger==l->ult);}

void List_next(List *l)
{
 l->finger=l->finger->sig;
}

int List_consult1(List *l)
{
 return(l->finger->sig->val1);
}

void List_consult2(List *l, int *elem1, int *elem2)
{
 *elem1=l->finger->sig->val1;
 *elem2=l->finger->sig->val2;
}

void List_consult4(List *l, int *elem1, int *elem2, int *elem3, int *elem4)
{
 *elem1=l->finger->sig->val1;
 *elem2=l->finger->sig->val2;
 *elem3=l->finger->sig->val3;
 *elem4=l->finger->sig->val4.i;
}

void List_consult3pc(List *l, int *elem1, int *elem2, int *elem3, cost_t *elem4)
{
 *elem1=l->finger->sig->val1;
 *elem2=l->finger->sig->val2;
 *elem3=l->finger->sig->val3;
 *elem4=l->finger->sig->val4.c;
}

void List_dispose_memory(List *l)
{
 if (l->ult != NULL) l->ult->sig=MEM_DIM;
 MEM_DIM=l->prim;l->prim=NULL;
 l->ult=NULL;
 l->finger=NULL;
}

 
void List_delete_this_elem(List *l, int elem)
{
 int found;
 
 found=FALSE;
 List_reset_traversal(l);
 while(!found)
        {
        if(List_end_traversal(l))found=TRUE;
        else 
                {
                if(List_consult1(l)==elem)
                        {
                        List_delete_node(l);
                        found=TRUE;
                        }
                else{List_next(l);}
                }
        }
}
