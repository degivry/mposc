/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wmtypes.c
  $Id: wmtypes.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <math.h>

#include "wcsp.h"

#include "wmauxiliar.h"

#include "wmtypes.h"





/* FUNCTIONS FOR LISTS OF LITERALS */



void insertLiteral(ListS * l, literal * c)

{

	NodeLS * n;



	/* reserve memory for new element */

    n = ( NodeLS *) malloc (sizeof(NodeLS));

    if (n==NULL) error(INS_MEM);

	

	n->next=NULL;

	n->elem=(literal *)c;



	if (l->first==NULL)

	{

		// First element to insert

		l->first=n;

		l->last=n;

	}

	else

	{

       /* last element must point to the new element */

       l->last->next = n;

       /* and the new is now the last */

       l->last = n;

	}

}





literal * getActualLiteral (ListS * l)

{

	// Precondition: Actual pointer not is null

	return (literal *) l->actual->elem;

}



literal * getLiteral(NodeLS * n)

{

	return (literal *) n->elem;

}



void showLiterals(ListS * l)

{

	literal * li;

	setFirstLS(l);

	while (endListLS(l)==FALSE)

	{

		li=getActualLiteral(l);

		printf(" (V:%d X:%d) ",li->boolValue,li->idVar);

		getNextLS(l);

	}

}



void showLiteral(literal * li)

{

	printf("\n V:%d X:%d",li->boolValue ,li->idVar);

}



literal * createLiteral(int bValue, int iVar)

{

	literal * li;

    li = (literal *) malloc (sizeof(literal));

    if (li==NULL) error(INS_MEM);

	li->boolValue =bValue;

	li->idVar=iVar;

	return li;

}

void clearLiterals(ListS * l)

{

	literal * li;

	NodeLS * auxi;

	setFirstLS(l);

	while (endListLS(l)==FALSE)

	{

		li=getActualLiteral(l);

		free(li);

		auxi=l->actual;

		getNextLS(l);

		free(auxi);

	}

	free(l);

}



literal * findFirstVariableWithoutAssign(problem *p, int c)

{

	// c is an index for array of clauses

	ListS *ll;

	literal *res = NULL;

	ll=p->clauses[c].pointerToLiterals;

	setFirstLS(ll);

	

	while (!endListLS(ll))

	{

		res=getActualLiteral(ll);

		if (p->Result[res->idVar]==NO_VALUE) return res;

		else getNextLS(ll);

	}



	printf("\nUNEXPECTED ERROR 1 C=%d %d",c,p->clauses[c].elim);

	return res;

}



literal * findSecondVariableWithoutAssign(problem *p, int c)

{

	// c is an index for array of clauses

	// This function must be executed inmediatly after findFirstVariableWithoutAssign.

	// Else the return value will be wrong.

	ListS *ll;

	literal *res = NULL;

	ll=p->clauses[c].pointerToLiterals;

	

	getNextLS(ll);

	while (!endListLS(ll))

	{

		res=getActualLiteral(ll);

		if (p->Result[res->idVar]==NO_VALUE) return res;

		else getNextLS(ll);

	}



	printf("\nUNEXPECTED ERROR 2 C=%d %d",c,p->clauses[c].elim);

	return res;

}



void passJeroslow(problem *p,int c)

{

	// c is an index for array of clauses

	ListS *ll;

	literal *res = NULL;

	ll=p->clauses[c].pointerToLiterals;

	setFirstLS(ll);

	

	while (!endListLS(ll))

	{

		res=getActualLiteral(ll);

			//p->jeroslows[res->idVar] = p->jeroslows[res->idVar] + ((double)p->clauses[c].weight/(double) p->pows[p->clauses[c].literalsWithoutAssign]);

			p->jeroslows[res->idVar] = p->jeroslows[res->idVar] + (p->clauses[c].weight*p->pows[p->maximumArity-p->clauses[c].literalsWithoutAssign]);

		

		getNextLS(ll);

	}

}



void nopassJeroslow(problem *p,int c)

{

	// c is an index for array of clauses

	ListS *ll;

	literal *res = NULL;

	ll=p->clauses[c].pointerToLiterals;



	setFirstLS(ll);

	

	while (!endListLS(ll))

	{

		res=getActualLiteral(ll);

			//p->jeroslows[res->idVar] = p->jeroslows[res->idVar] - ((double)p->clauses[c].weight/(double) p->pows[p->clauses[c].literalsWithoutAssign]);

		p->jeroslows[res->idVar] = p->jeroslows[res->idVar] - (p->clauses[c].weight*p->pows[p->maximumArity-p->clauses[c].literalsWithoutAssign]);

		

		getNextLS(ll);

	}

}





/* FUNCTIONS FOR REFERENCES TO CLAUSES */



void insertReference(ListS * l, ReferenceToClause * c)

{

	NodeLS * n;

	/* reserve memory for new element */

    n = ( NodeLS *) malloc (sizeof(NodeLS));

    if (n==NULL) error(INS_MEM);

	

	n->next=NULL;

	n->elem=(ReferenceToClause *)c;



	if (l->first==NULL)

	{

		// First element to insert

		l->first=n;

		l->last=n;

	}

	else

	{

       /* last element must point to the new element */

       l->last->next = n;

       /* and the new is now the last */

       l->last = n;

	}

}





ReferenceToClause * getActualReference(ListS * l)

{

	// Precondition: Actual pointer not is null

	return (ReferenceToClause *) l->actual->elem;

}



ReferenceToClause * getReference(NodeLS * n)

{

	return (ReferenceToClause *) n->elem;

}



void showReferences(ListS * l)

{

	ReferenceToClause * rc;

	setFirstLS(l);

	while (endListLS(l)==FALSE)

	{

		rc=getActualReference(l);

		printf(" %d ",rc->indexToClause);

		getNextLS(l);

	}

}



void showReference(ReferenceToClause * rc)

{

	printf("\n I:%d",rc->indexToClause);

}



ReferenceToClause * createReference(int index)

{

	ReferenceToClause * rc;

    rc = (ReferenceToClause *) malloc (sizeof(ReferenceToClause));

    if (rc==NULL) error(INS_MEM);

	rc->indexToClause =index;

	return rc;

}



void clearReferences(ListS * l)

{

	ReferenceToClause * rc;

	NodeLS * auxi;

	setFirstLS(l);

	while (endListLS(l)==FALSE)

	{

		rc=getActualReference(l);

		free(rc);

		auxi=l->actual;

		getNextLS(l);

		free(auxi);

	}

	free(l);

}



/* FUNCTIONS FOR REFERENCES TO VARIABLES */



void insertReferenceVar(ListD * l, ReferenceToVariable * rv)

{

	NodeLD * n;

	/* reserve memory for new element */

    n = ( NodeLD *) malloc (sizeof(NodeLD));

    if (n==NULL) error(INS_MEM);

	

	n->next=NULL;

	n->elem=(ReferenceToVariable *)rv;



	if (l->first==NULL)

	{

		// First element to insert

		n->prev=NULL;

		l->first=n;

		l->last=n;

	}



	else

	{

		/* the new node must point to actual last as previous node */

		n->prev=l->last;

		/* last element must point to the new element */

		l->last->next = n;

		/* and the new is now the last */

		l->last = n;

	}

}





ReferenceToVariable * getActualReferenceVar(ListD * l)

{

	// Precondition: Actual pointer not is null

	return (ReferenceToVariable *) l->actual->elem;

}



ReferenceToVariable * getReferenceVar(NodeLD * n)

{

	return (ReferenceToVariable *) n->elem;

}



void showReferencesVar(ListD * l)

{

	ReferenceToVariable * rv;

	setFirstLD(l);

	printf("\n Not assigned variables:");

	while (endListLD(l)==FALSE)

	{

		rv=getActualReferenceVar(l);

		printf(" %d ",rv->indexToVariable);

		getNextLD(l);

	}

	printf("\n");

}



void showReferenceVar(ReferenceToVariable * rv)

{

	printf("\n I:%d",rv->indexToVariable);

}



ReferenceToVariable * createReferenceVar(int index)

{

	ReferenceToVariable * rv;

    rv = (ReferenceToVariable *) malloc (sizeof(ReferenceToVariable));

    if (rv==NULL) error(INS_MEM);

	rv->indexToVariable =index;

	return rv;

}



void clearReferencesVar(ListD * l)

{

	ReferenceToVariable * rv;

	NodeLD * auxi;

	setFirstLD(l);

	while (endListLD(l)==FALSE)

	{

		rv=getActualReferenceVar(l);

		free(rv);

		auxi=l->actual;

		getNextLD(l);

		free(auxi);

	}

	free(l);

}



void createListOfReferencesVariables(problem * p)

{

	int i;

	ReferenceToVariable *rv;

	p->notAssignedVars=newListD();

	for(i=0;i<p->totalVariables;i++)

	{

		rv=createReferenceVar(i);

		insertReferenceVar(p->notAssignedVars,rv);

	}

}



/* FUNCTIONS FOR CLAUSES */



void createArrayClauses(problem * p,int num_cla)

{

	int i;

	p->totalClauses = num_cla;

	p->clauses =(clause *)malloc(sizeof(clause)*p->totalClauses);

	if (p->clauses==NULL) error(INS_MEM);

	for (i=0;i<p->totalClauses;i++)

	{

		p->clauses[i].pointerToLiterals=newListS();

		p->clauses[i].literalsWithoutAssign=0;

		p->clauses[i].weight=0;

	}

}



void showClauses(problem *p)

{

	int i;

	printf("\n Clauses:");

	for(i=0;i<p->totalClauses;i++)

	{

		printf("\n Clause %d (LWA=%d,W=%d,E:%d)  Literals: ",i,p->clauses[i].literalsWithoutAssign,p->clauses[i].weight,p->clauses[i].elim);

		showLiterals(p->clauses[i].pointerToLiterals);

	}

}



void clearArrayClauses(problem *p)

{

	int i;

	for(i=0;i<p->totalClauses;i++)

	{

		clearLiterals(p->clauses[i].pointerToLiterals);

	}

	free(p->clauses);

}



/* FUNCTIONS FOR LISTS OF NOT CONSIDERED CLAUSES */



void insertReferenceCla(ListD * l, ReferenceToClause * rc)

{

	NodeLD * n;

	/* reserve memory for new element */

    n = ( NodeLD *) malloc (sizeof(NodeLD));

    if (n==NULL) error(INS_MEM);

	

	n->next=NULL;

	n->elem=(ReferenceToClause *)rc;



	if (l->first==NULL)

	{

		// First element to insert

		n->prev=NULL;

		l->first=n;

		l->last=n;

	}



	else

	{

		/* the new node must point to actual last as previous node */

		n->prev=l->last;

		/* last element must point to the new element */

		l->last->next = n;

		/* and the new is now the last */

		l->last = n;

	}

}





ReferenceToClause * getActualReferenceCla(ListD * l)

{

	// Precondition: Actual pointer not is null

	return (ReferenceToClause *) l->actual->elem;

}



ReferenceToClause * getReferenceCla(NodeLD * n)

{

	return (ReferenceToClause *) n->elem;

}



void showReferencesCla(ListD * l)

{

	ReferenceToClause * rc;

	setFirstLD(l);

	printf("\n Not assigned clauses:");

	while (endListLD(l)==FALSE)

	{

		rc=getActualReferenceCla(l);

		printf(" %d ",rc->indexToClause);

		getNextLD(l);

	}

	printf("\n");

}



void showReferenceCla(ReferenceToClause * rc)

{

	printf("\n I:%d",rc->indexToClause);

}



ReferenceToClause * createReferenceCla(int index)

{

	ReferenceToClause * rc;

    rc = (ReferenceToClause *) malloc (sizeof(ReferenceToClause));

    if (rc==NULL) error(INS_MEM);

	rc->indexToClause =index;

	return rc;

}



void clearReferencesCla(ListD * l)

{

	ReferenceToClause * rc;

	NodeLD * auxi;

	setFirstLD(l);

	while (endListLD(l)==FALSE)

	{

		rc=getActualReferenceCla(l);

		free(rc);

		auxi=l->actual;

		getNextLD(l);

		free(auxi);

	}

	free(l);

}



void createListOfReferencesClauses(problem * p)

{

	int i;

	ReferenceToClause *rc;

	p->notAssignedCla=newListD();

	for(i=0;i<p->totalClauses;i++)

	{

		rc=createReferenceCla(i);

		insertReferenceCla(p->notAssignedCla,rc);

	}

}





/* FUNCTIONS FOR VARIABLES */



void createArrayVariables(problem * p,int num_var)

{

	int i;

	p->totalVariables = num_var;

	p->variables =(variable *)malloc(sizeof(variable)*p->totalVariables);

	if(p->variables==NULL) error(INS_MEM);

	for (i=0;i<p->totalVariables;i++)

	{

		p->variables[i].listLiterals=newListS();

		p->variables[i].listNoLiterals=newListS();

	}

}



void clearArrayVariables(problem *p)

{

	int i;

	for (i=0;i<p->totalVariables;i++)

	{

		clearReferences(p->variables[i].listLiterals);

		clearReferences(p->variables[i].listNoLiterals);

	}

	free(p->variables);



}



void createArrayOfDecision(problem * p)

{

	int i;

	p->ArrayOfDecision =(decision *)malloc(sizeof(decision)*p->totalVariables);
	if(p->ArrayOfDecision==NULL) error(INS_MEM);

	for(i=0;i<p->totalVariables;i++)

	{

		p->ArrayOfDecision[i].not_considered_values = 2;

		p->ArrayOfDecision[i].usedFalse = FALSE;

		p->ArrayOfDecision[i].usedTrue = FALSE;

	}



}



void clearArrayOfDecision(problem *p)

{

	free(p->ArrayOfDecision);

}



void showArrayOfDecision(problem * p)

{

	int i;

	printf("\n Array of decision VAR -> (TRUE,FALSE,NOT-CONSIDERED): ");

	for(i=0;i<p->totalVariables;i++)

	{

		printf("\n X%d -> (%d,%d,%d)",i,p->ArrayOfDecision[i].usedTrue,p->ArrayOfDecision[i].usedFalse,p->ArrayOfDecision[i].not_considered_values);

	}

}



void showVariables(problem * p)

{

	int i;

	printf("\n Variables:");

	for(i=0;i<p->totalVariables;i++)

	{

		printf("\n Var %d  Literals: ",i);

		showReferences(p->variables[i].listLiterals);

		printf("\n Var %d -Literals: ",i);

		showReferences(p->variables[i].listNoLiterals);

	}

}



/* FUNCTIONS FOR ARRAY OF RESULTS */



void createArrayResults(problem *p)

{

	int i;

	p->Result=(int *)malloc(sizeof(int)*p->totalVariables);
	if(p->Result==NULL) error(INS_MEM);

	for(i=0;i<p->totalVariables;i++)

	{

		p->Result[i]=NO_VALUE;

	}

}



void destroyArrayResults(problem *p)

{

	free(p->Result);

}



void showArrayResults(problem *p)

{

	int i;

	printf("\nArray of results:\n");

	for(i=0;i<p->totalVariables;i++)

	{

		printf(" (X%d=%d) ",i,p->Result[i]);

	}

}





/* FUNCTIONS FOR ARRAY OF BEST RESULTS */



void createArrayBestResults(problem *p)

{

	int i;

	p->bestResult=(int *)malloc(sizeof(int)*p->totalVariables);
	if(p->bestResult==NULL) error(INS_MEM);

	for(i=0;i<p->totalVariables;i++)

	{

		p->bestResult[i]=NO_VALUE;

	}

}



void destroyArrayBestResults(problem *p)

{

	free(p->bestResult);

}



void showArrayBestResults(problem *p)

{

	int i;

	printf("\n Array of Best results:\n");

	for(i=0;i<p->totalVariables;i++)

	{

		printf(" (X%d=%d) ",i+1,p->bestResult[i]);

	}

}



void saveBestResult(problem *p)

{

	int i;

	for(i=0;i<p->totalVariables;i++)

		p->bestResult[i]=p->Result[i];

}



/* FUNCTIONS FOR ARRAY OF PRUNE */



void createUnaryCosts(problem *p)

{

	// Dimensions array = number of variables * 2

	int i;

	p->UnaryCosts=(int **)malloc(sizeof(int)*p->totalVariables);
	if(p->UnaryCosts==NULL) error(INS_MEM);

	for(i=0;i<p->totalVariables;i++)

	{

		p->UnaryCosts[i]=(int *)malloc(sizeof(int)*2);
		
		if(p->UnaryCosts[i]==NULL) error(INS_MEM);

		p->UnaryCosts[i][FALSE]=0;

		p->UnaryCosts[i][TRUE]=0;

	}

}



void showUnaryCosts(problem *p)

{

	int i;

	printf("\n Array of Prune:\n");

	for(i=0;i<p->totalVariables;i++)

	{

		printf("\n[X%d(FALSE)=%d],[X%d(TRUE)=%d]",i,p->UnaryCosts[i][FALSE],i,p->UnaryCosts[i][TRUE]);

	}

}



void destroyUnaryCosts(problem *p)

{

	int i; 

	for(i=0; i<p->totalVariables; i++) 

	{ 

		free(p->UnaryCosts[i]);

	} 

	free(p->UnaryCosts); 

}



/* FUNCTIONS FOR BinaryCosts */



void createBinaryCosts(problem *p)

{

	// create and initalize to zero array of prune for arc-consistency

	int i,j,k,l; 

	p->BinaryCosts=(int ****)malloc(sizeof(int ***)*p->totalVariables); 
	if(p->BinaryCosts==NULL) error(INS_MEM);

	for(i=0; i<p->totalVariables; i++) 

	{ 

		p->BinaryCosts[i]=(int ***)malloc(sizeof(int **)*p->totalVariables); 
		if(p->BinaryCosts[i]==NULL) error(INS_MEM);

		for(j=0; j<p->totalVariables; j++) 

		{

			p->BinaryCosts[i][j]=(int **)malloc(sizeof(int *)*2);
			if(p->BinaryCosts[i][j]==NULL) error(INS_MEM);

			for(k=0;k<2;k++) 

			{ 

				p->BinaryCosts[i][j][k]=(int *)malloc(sizeof(int)*2);
				if(p->BinaryCosts[i][j][k]==NULL) error(INS_MEM);

				for (l=0;l<2;l++)

				{

					p->BinaryCosts[i][j][k][l]=0;

				}

			}

		}

	} 



}



void destroyBinaryCosts(problem *p)

{

	int i,j,k;

	

	for(i=0; i<p->totalVariables; i++) 

	{ 

		for(j=0; j<p->totalVariables; j++) 

		{ 

			for(k=0; k<2; k++) 

			{ 

				free(p->BinaryCosts[i][j][k]);

			}

			free(p->BinaryCosts[i][j]);

		}

		free(p->BinaryCosts[i]);

	} 

	free(p->BinaryCosts);

}



void showBinaryCosts(problem *p)

{

	int i,j,k,l;

	

	for(i=0; i<p->totalVariables; i++) 

	{ 

		for(j=0; j<p->totalVariables; j++) 

		{

			printf("\n[%d,%d]= ",i,j);

			for(k=0; k<2; k++)

			{ 

				for(l=0; l<2; l++) 

				{ 

					printf("%d ",p->BinaryCosts[i][j][k][l]);

				}

			}

		}

	}

}



/* FUNCTIONS TO MANAGE STACKS */



stack * iniStack(int size) 

{

	int i;

	stack *s;



	s = (stack *) malloc (sizeof(stack));

    if (s==NULL) error(INS_MEM);



	s->size=size;



	s->content = (int *) malloc (sizeof(int)*s->size);

	if (s->content==NULL) error(INS_MEM);

	s->elems = (int *) malloc (sizeof(int)*s->size);

	if (s->elems==NULL) error(INS_MEM);



	for(i=0;i<s->size;i++) s->content[i]=FALSE;

	s->n_elem=0;



	return s;

}



void showStack(stack *s)

{

	int i;

	if(s->n_elem==0)

	{

		printf("\nEmpty stack!");

	}

	else

	{

		printf("\nStack values:");

		for(i=s->n_elem;i>0;i--)

		{

			printf("[%d]",s->elems[i-1]);

		}

		printf("\n");

	}

}



void pushStack(stack *s,int i)

{

	if(s->content[i]==FALSE)

	{

		s->content[i]=TRUE;

		s->elems[s->n_elem]=i;

		s->n_elem++;

	}

}



int popStack(stack *s)

{

	// precondition: there is at least one element in the stack

	// Pop the head of the stack

	int res;

	s->n_elem--;

	res=s->elems[s->n_elem];

	s->content[res]=FALSE;

	return res;

}



int popStackMinDom(stack *s)

{

	// precondition: there is at least one element in the stack

	// Pop the minimum element from the stack.

	int index, min,i;

	

	index=0;

	min=s->elems[index];



	for(i=1;i<s->n_elem;i++)

	{

		if(s->elems[i]<min)

		{

			min=s->elems[i];

			index=i;

		}

	}



	s->content[min]=FALSE;



	for(i=index;i<s->n_elem-1;i++) s->elems[i]=s->elems[i+1];

	

	s->n_elem--;



	return min;



}



int popStackMaxDom(stack *s)

{

	// precondition: there is at least one element in the stack

	// Pop the maximum element from the stack.

	int index, max,i;

	

	index=0;

	max=s->elems[index];



	for(i=1;i<s->n_elem;i++)

	{

		if(s->elems[i]>max)

		{

			max=s->elems[i];

			index=i;

		}

	}



	s->content[max]=FALSE;



	for(i=index;i<s->n_elem-1;i++) s->elems[i]=s->elems[i+1];

	

	s->n_elem--;



	return max;



}

void clearStackValues(stack *s)
{
	int i;
	for(i=0;i<s->n_elem;i++)
		s->content[s->elems[i]]=FALSE;
	s->n_elem=0;
}


void clearStack(stack *s)

{

	free(s->content);

	free(s->elems);

	free(s);

}



/* FUNCTIONS FOR ARRAY OF POWS */



void createArrayPows(problem *p)

{

	int i;

	if(p->maximumArity>30)

	{

		printf("\nArity of clauses must be <= 30");

		exit(-1);

	}

	p->pows=(int *)malloc(sizeof(int)*(p->maximumArity+1));
	if(p->pows==NULL) error(INS_MEM);

	for(i=0;i<=p->maximumArity;i++)

	{

		p->pows[i]=(int) pow(2,i);

	}

}



void destroyArrayPows(problem *p)

{

	free(p->pows);

}



void showArrayPows(problem *p)

{

	int i;

	printf("\n Array of Pows:\n");

	for(i=0;i<=p->maximumArity;i++)

	{

		printf(" (2^%d=%d) ",i,p->pows[i]);

	}

}



/* FUNCTIONS FOR ARRAY OF JEROSLOWS*/



void createArrayJeroslows(problem *p)

{

	int i;

	p->jeroslows=(int *)malloc(sizeof(int)*(p->totalVariables));
	if(p->jeroslows==NULL) error(INS_MEM);

	for(i=0;i<p->totalVariables;i++)

	{

		p->jeroslows[i]=(int)0;

	}

}



void destroyArrayJeroslows(problem *p)

{

	free(p->jeroslows);

}



void showArrayJeroslows(problem *p)

{

	int i;

	printf("\n Array of Jeroslows:\n");

	for(i=0;i<p->totalVariables;i++)

	{

		printf(" (X%d=%d) ",i,p->jeroslows[i]);

	}

}

/* FUNCTIONS FOR TMP ARRAY */

void createTmpArray(problem *p)
{

	int i;

	p->tmpArr=(int *)malloc(sizeof(int)*(p->totalVariables));
	if(p->tmpArr==NULL) error(INS_MEM);

	for(i=0;i<p->totalVariables;i++)
	{
		p->tmpArr[i]=TRUE;
	}

}

void showTmpArray(problem *p)
{
	int i;

	for(i=0;i<p->totalVariables;i++)
	{
		printf("\n X%d = %d TotV:%d ",i,p->tmpArr[i],p->totalVariables);
	}
}

void destroyTmpArray(problem *p)
{
	free(p->tmpArr);
}

/* FUNCTIONS FOR MAX-SAT RULES */

int sign(literal *res1)
{
	if(res1->boolValue==TRUE) return 1;
	else return -1;
}

int compClauses(problem *p,int refCla1,int refCla2,int i,literal *l1,literal *l2)
{

	ListS *ll1;
	int a=-1,b=-1,c=0,d=-1,sameVars=1;

	literal *res1 = NULL;

	ll1=p->clauses[refCla1].pointerToLiterals;

	setFirstLS(ll1);

	while (!endListLS(ll1))
	{
		res1=getActualLiteral(ll1);

		if (p->Result[res1->idVar]==NO_VALUE && i!=res1->idVar)
		{
			if(a==-1)
			{
				a=sign(res1)*(res1->idVar+1);
				l1->idVar=res1->idVar;
				l1->boolValue=res1->boolValue;
			}
			else
			{
				b=sign(res1)*(res1->idVar+1);
				l2->idVar=res1->idVar;
				l2->boolValue=res1->boolValue;
			}
		}
		getNextLS(ll1);
	}

	ll1=p->clauses[refCla2].pointerToLiterals;

	setFirstLS(ll1);

	while (!endListLS(ll1))
	{
		res1=getActualLiteral(ll1);

		if (p->Result[res1->idVar]==NO_VALUE && i!=res1->idVar)
		{
			d=sign(res1)*(res1->idVar+1);

			if( (abs(a)==abs(d)) || (abs(b)==abs(d)))
			{
				sameVars++;
			}

			if((a==d) || (b==d))
			{
				c++;
			}
		}
		getNextLS(ll1);
	}

	/*
	if(sameVars==3 && c==0)
	{
		rep0++;
	}
	else if(sameVars==3 && c==1)
	{
		rep1++;
	}
	*/
	return c;
}




/* FUNCTIONS FOR PROBLEM */



void showProblem(problem * p)

{

	printf("\n Problem information:");

	printf("\n --------------------\n");

	printf("\n Total Variables: %d",p->totalVariables);

	printf("\n Total Clauses: %d",p->totalClauses);

	printf("\n UB:%d",p->UB);

	printf("\n LB:%d",p->LB);

	printf("\n Total Weight:%d",p->totalWeight);

	showClauses(p);

	showVariables(p);

	showArrayOfDecision(p);

	showReferencesVar(p->notAssignedVars);

	showArrayResults(p);

	showUnaryCosts(p);

}



void showProblemResults(problem * p)

{

	printf("\n Total cost: %d",p->UB);

	showArrayBestResults(p);

	printf("\n Total time: %lu",p->totTime);

}


void initProblem(problem *p)

{

	// Arrays

	p->ArrayOfDecision =NULL;

	p->clauses=NULL;

	p->variables = NULL;



	// values

	p->totalClauses =0;

	p->totalVariables =0;

	p->LB =0;

	p->UB =0;

	p->totalWeight=0;

	p->totTime=0;



}



int readProblem(FILE * f, problem *p,int isCnf)

{

	char line[MAX_CARS];

    char aux1[20],aux2[20];

	int num_var=0,num_cla=0;

	int i, actual,var;

	literal * li;

	int bType;

	int n_l;

	ReferenceToClause * rc;



	// precondition: f must be a file pointer to a cnf or wcnf file

	// and it points to the beginning of the file.



	// initialize problem

	initProblem(p);

	

	// Read first the comment lines.

	fgets(line, MAX_CARS, f);

	while (line[0] != 'p')

	{	

		fgets(line, MAX_CARS, f);

	}

	

	// Extract important information from p-line,

	// and ignore the rest.

	// p-line formats:

	// p cnf num_vars num_cla

	// p wcnf num_vars num_cla



	sscanf(line, "%s %s %d %d", aux1, aux2, &num_var, &num_cla);



	// Now we can create all the necessary data structures



	createArrayVariables(p,num_var);

	createArrayOfDecision(p);

	createArrayClauses(p,num_cla);

	createListOfReferencesVariables(p);

	createArrayResults(p);

	createArrayBestResults(p);

	createUnaryCosts(p);

	createBinaryCosts(p);

	p->totalWeight=0;

	p->maximumArity=0;



	// Finally we can read all the clauses



    for (i = 0; i <= p->totalClauses - 1; i++)

	{

		n_l=0;

		if(isCnf)

		{

			actual=1;

			p->clauses[i].weight=actual; // is a cnf file: assign weight 1 to all clauses

		}

		else

		{

			// is a wcnf file: read the weight from the file

			fscanf(f,"%d",&actual);

			p->clauses[i].weight=actual;

		}



		p->clauses[i].elim=FALSE;

		p->clauses[i].terminator=NULL;



		p->totalWeight=p->totalWeight+actual;



		// for each clause, read all associated literals

		fscanf(f, "%d", &actual);

		while (actual != 0)

		{

			var=abs(actual);

			if (actual<0) bType=FALSE;

			else bType=TRUE;

			

			// Create a new literal for the clause

			p->clauses[i].literalsWithoutAssign++;

			li=createLiteral(bType, var-1);

			insertLiteral(p->clauses[i].pointerToLiterals, li);

			// Create a new reference in the variable to the clause

			rc=createReference(i);

			if (actual<0) insertReference(p->variables[var-1].listNoLiterals,rc);

			else insertReference(p->variables[var-1].listLiterals,rc);



			fscanf(f, "%d", &actual);

			n_l++;

		} // end while



		if(n_l>=p->maximumArity) p->maximumArity=n_l;



	}  // end for



	// Structures for memory manage and restoration

	createListR(&p->restoreList,p->totalVariables*MEM_FACT);

	p->Q = iniStack(p->totalVariables);

	p->R = iniStack(p->totalVariables);

	p->UB=MIN(p->totalWeight,Top);



	// Create structures to gain CPU high-speed

	createArrayPows(p);

	createArrayJeroslows(p);

	createTmpArray(p);


	return TRUE;

}



void clearProblem(problem * p)

{

    //printf("\n Clearing problem...\n");

	clearArrayOfDecision(p);

	clearArrayVariables(p);

	clearArrayClauses(p);

	destroyArrayResults(p);

	destroyArrayBestResults(p);

	clearReferencesVar(p->notAssignedVars);

	destroyUnaryCosts(p);

	destroyBinaryCosts(p);

	clearListR(&p->restoreList);

	clearStack(p->Q);

	clearStack(p->R);

	destroyArrayPows(p);

	destroyArrayJeroslows(p);

	destroyTmpArray(p);

}

