/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: solver.h
  $Id: tbgoods.h,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#ifndef GOODS_H
#define GOODS_H

extern void initGood(unsigned int); /* initialize good memories before starting BTD */

/* returns good value for cluster son wrt completeAssigment with his father */
/* optimality is set to TRUE if this good was optimum and FALSE if it was a lowerbound only */
extern cost_t getGood(int son, int *completeAssignment, int *optimality);  

/* set the good value for cluster son wrt completeAssigment with his father */
/* optimality is TRUE if this good was optimum and FALSE if it was a lowerbound only */
extern void setGood(int son, int *completeAssignment, cost_t v, int optimality);

/* return statistics on good usage */
extern void statGood(int cluster, int depth);

#endif
