/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: wcspreader.c
  $Id: wcspreader.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "wcsp.h"
#include "solver.h"
#include "btd.h"

/* warning! limit on stacksize */
#define MAXNVAR 100000
#define MAXARITY 1000

extern void sort_val_lex(int var);

void readWCSP(FILE* file) 
{
  int c,i,j,a,b,k,l,arity,nbc,e;
  int ntuples;
  cost_t defval, val;
  int nbconstr; /* total number of problem constraints */
  cost_t top;
  char pbname[256];
  char dummy[256];
  int completeAssignment[MAXNVAR];
  int nbinary = 0;

  /* read problem name and sizes */
  myfscanf4(file, "%s %d %d %d", pbname, &NVar, &NValues, &nbconstr);
  SCANCOST(file,&top);
  /* printf("Read problem: %s\n", pbname); */
  Top = MIN(Top, top);

  /* read variable domain sizes */
  DomainSize = mycalloc(NVar, sizeof(int));
  for (i=0; i<NVar; i++) {
    myfscanf1(file, "%d", &DOMSIZE(i));
    if (DOMSIZE(i) > NValues) NValues = DOMSIZE(i);
  }

  /* read each constraint */
  Graph = mycalloc(NVar * NVar, sizeof(cost_t *));
  UnaryConstraints = mycalloc(NVar * NValues, sizeof(cost_t));
  MeanUnaryCosts = mycalloc(NVar,sizeof(double));
  NaryConstraints = mycalloc(nbconstr, sizeof(int **));
  NaryCosts = mycalloc(nbconstr, sizeof(cost_t **));
  MeanCosts = mycalloc(nbconstr, sizeof(double));
  Variables = mycalloc(NVar, sizeof(int **));

  for (c=0; c<nbconstr; c++) {
    myfscanf1(file, "%d", &arity);
    if (arity == 2) {
      /* printf("read binary c_%d...\n", c); */
      myfscanf2(file, "%d %d",&i, &j);
      if (i==j) {
	fprintf(stderr,"Error: binary constraint with only one variable in its scope!\n");
	abort();
      }
      SCANCOST(file,&defval);
      if (FileFormat == FORMAT_MAXCSP) defval = MIN(1,defval);
      myfscanf1(file,"%d", &ntuples);

      if (!ISBINARY(i,j)) {
	ISBINARY(i,j) = (cost_t *)((double *)(mycalloc(DOMSIZE(i) * DOMSIZE(j) * sizeof(cost_t) + sizeof(double),1))+1);
	ISBINARY(j,i) = ISBINARY(i,j);
      }
	  double Weight = (double)defval;
      for (a=0; a<DOMSIZE(i); a++) {
	for (b=0; b<DOMSIZE(j); b++) {
	  setBinaryCost(i,j,a,b,binaryCost(i,j,a,b) + defval);
	}
      }
      for (k=0; k<ntuples; k++) {
	myfscanf2(file, "%d %d", &a, &b);
	SCANCOST(file,&val);
	if (FileFormat == FORMAT_MAXCSP) val = MIN(1,val);
	setBinaryCost(i,j,a,b,binaryCost(i,j,a,b) + val - defval);
	Weight += (double)(val-defval)/(DOMSIZE(i)*DOMSIZE(j));
      }
	  WEIGHT2(i,j) += Weight;
      nbinary++;
    } else if (arity == 1) {
      /* printf("read unary c_%d...\n", c); */
      myfscanf1(file, "%d", &i);
      SCANCOST(file, &defval);
      if (FileFormat == FORMAT_MAXCSP) defval = MIN(1,defval);
      myfscanf1(file,"%d",&ntuples);
      for (a=0; a<DOMSIZE(i); a++) {
	UNARYCOST(i,a) += defval;
		  WEIGHT1(i) += (double)(defval)/DOMSIZE(i);
      }
      for (k=0; k<ntuples; k++) {
	myfscanf1(file, "%d", &a);
	SCANCOST(file, &val);
	if (FileFormat == FORMAT_MAXCSP) val = MIN(1,val);
	UNARYCOST(i,a) += val - defval;
      }
    } else if (arity == 0) {
      /* printf("read global lower bound contribution c_%d...\n", c); */
      SCANCOST(file, &defval);
      myfscanf1(file, "%d", &ntuples);
      if (ntuples != 0) {
	fprintf(stderr,"Error: global lower bound contribution with several tuples!\n");
	abort();
      }
      Bottom += defval;
    } else if (arity >= 3) {
      /* printf("read nary c_%d...\n", c); */
      if (arity > MaxArity) MaxArity = arity;
      NaryConstraints[NConstr] = mycalloc((1 + arity), sizeof(int));
      ARITY(NConstr) = arity;
      ntuples = 1;
      for (k=0; k<arity; k++) {
	  SCOPEONE(NConstr,k) = -1;
      }
      for (k=0; k<arity; k++) {
	myfscanf1(file, "%d", &e);
	if (inScope(e,NConstr)) {
	  fprintf(stderr,"Error: nary constraint with two identical variables in its scope!\n");
	  abort();
	} else {
	  SCOPEONE(NConstr,k) = e;
	}
	ntuples *= DOMSIZE(SCOPEONE(NConstr,k));
      }
      COSTS(NConstr) = mycalloc(ntuples, sizeof(cost_t));
      SCANCOST(file, &defval);
      if (FileFormat == FORMAT_MAXCSP) defval = MIN(1,defval);
      for (k=0; k<ntuples; k++) {
	COSTS(NConstr)[k] = defval;
      }
      myfscanf1(file, "%d", &ntuples);
      for (k=0; k<ntuples; k++) {
	for (l=0; l<arity; l++) {
	  myfscanf1(file, "%d", &completeAssignment[SCOPEONE(NConstr,l)]);
	}
	SCANCOST(file, &val);
	if (FileFormat == FORMAT_MAXCSP) val = MIN(1,val);
	setCost(NConstr, completeAssignment, val);
	WEIGHTCONSTRAINT(NConstr) += val - defval;
      }
      WEIGHTCONSTRAINT(NConstr) /= cartesianProduct(NConstr);
      WEIGHTCONSTRAINT(NConstr) += defval;
      NConstr++;
    } else {
      fprintf(stderr,"Error: negative constraint arity!\n");
      abort();
    }
  }
  if (fscanf(file, "%s",dummy) != EOF) {
    fprintf(stderr,"Error: too much information in problem file!\n");
    abort();
  }

  /* TO BE MOVED IN wcsp.c */
  /* create links between variables and nary constraints */
  for (i=0; i<NVar; i++) {
    nbc = 0;
    for (c=0; c<NConstr; c++) {
      for (k=0; k<ARITY(c); k++) {
	if (SCOPEONE(c,k) == i) {
	  nbc++;
	}
      }
    }
    Variables[i] = mycalloc(nbc + 1, sizeof(int));
    VARNCONSTR(i) = nbc;
    nbc = 0;
    for (c=0; c<NConstr; c++) {
      for (k=0; k<ARITY(c); k++) {
	if (SCOPEONE(c,k) == i) {
	  VARCONSTRONE(i,nbc) = c;
	  nbc++;
	}
      }
    }
  }
  /* printf("... problem %s is loaded.\n", pbname); */
  if (Verbose) {
    printf("Read %d variables, with %d values at most, and %d constraints.\n", NVar, NValues, nbconstr);
  }
}

int truecartesianProduct(int i) 
{
  int res = 1;
  int *var_p =  SCOPE(i);
  int *end = var_p + ARITY(i);
  while (var_p < end) {
    res *= List_n_of_elem(&feasible_values[*var_p]);
    var_p++;
  }
  return res;
}

void recursiveNaryPrint(FILE *file, int c, int i, int *complassignment, int *naryidxval)
{
  int a,idxa;

  if (i < ARITY(c)) {
    idxa = 0;
    List_create(&feasible_values2[SCOPEONE(c,i)]);
    sort_val_lex(SCOPEONE(c,i));
    List_reset_traversal(&feasible_values2[SCOPEONE(c,i)]);
    while(!List_end_traversal(&feasible_values2[SCOPEONE(c,i)])) {
      a = List_consult1(&feasible_values2[SCOPEONE(c,i)]);
      List_next(&feasible_values2[SCOPEONE(c,i)]);
      naryidxval[i] = idxa;
      complassignment[SCOPEONE(c,i)] = a;
      recursiveNaryPrint(file, c, i+1, complassignment, naryidxval);
      idxa++;
    }
    List_dispose_memory(&feasible_values2[SCOPEONE(c,i)]);
  } else {
    for (i=0; i<ARITY(c); i++) {
      fprintf(file, "%d ", naryidxval[i]);
    }
    FPRINTCOST(file, cost(c, complassignment));
    fprintf(file, "\n");
  }
}

void saveWCSP(FILE* file) 
{
  int i,j,a,b,c,idxa,idxb,nbvar = 0, nbval = 0, nbconstr = 0;
  int first = NVar;
  int num[NVar];
  int unassigned[NConstr];
  int recursiveNaryAssignment[MAXNVAR];
  int recursiveNaryPrintIdxVal[MAXARITY];

  /* count the number of constraints */
  if (Bottom > BOTTOM) nbconstr++;  
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      if (first==NVar) first = i;
      num[i] = nbvar;
      nbvar++;
      nbval = MAX(nbval, List_n_of_elem(&feasible_values[i]));
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 &&  ISBINARY(i,j)) {
	  nbconstr++;
	}
      }
    }
  }
  nbconstr += nbvar; /* unary costs */
  for (c=0; c<NConstr; c++) { /* n-ary constraints */
    unassigned[c] = TRUE;
    for (i=0; i<ARITY(c); i++) {
      if (assignments[SCOPEONE(c,i)]!=-1) {
	unassigned[c] = FALSE;
	break;
      }
    }
    if (unassigned[c]) nbconstr++;
  }

  /* header */
  fprintf(file, "%s %d %d %d ", Filename, nbvar, nbval, nbconstr);
  FPRINTCOST(file, Top);
  fprintf(file, "\n");
  /* domain sizes */
  if (first < NVar) {
    fprintf(file, "%d", List_n_of_elem(&feasible_values[first]));
    for (i=first+1; i<NVar; i++) {
      if (assignments[i]==-1) {
	fprintf(file, " %d", List_n_of_elem(&feasible_values[i]));
      }
    }
    fprintf(file, "\n");
  }
  /* n-ary constraints */
  for (c=0; c<NConstr; c++) {
    if (unassigned[c]) {
      fprintf(file, "%d", ARITY(c));
      for (i=0; i<ARITY(c); i++) {
	fprintf(file, " %d", num[SCOPEONE(c,i)]);
      }
      fprintf(file, " ");
      FPRINTCOST(file, BOTTOM);
      fprintf(file, " %d\n", truecartesianProduct(c));
      recursiveNaryPrint(file, c, 0, recursiveNaryAssignment, recursiveNaryPrintIdxVal); 
    }
  }
  /* binary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 && ISBINARY(i,j)) {
	  fprintf(file, "2 %d %d ", num[i], num[j]);
	  FPRINTCOST(file, BOTTOM);
	  fprintf(file, " %d\n", List_n_of_elem(&feasible_values[i])*List_n_of_elem(&feasible_values[j]));
	  idxa=0;
	  List_create(&feasible_values2[i]);
	  sort_val_lex(i);
	  List_reset_traversal(&feasible_values2[i]);
	  while(!List_end_traversal(&feasible_values2[i])) {
	    a = List_consult1(&feasible_values2[i]);
	    List_next(&feasible_values2[i]);
	    idxb=0;
	    List_create(&feasible_values2[j]);
	    sort_val_lex(j);
	    List_reset_traversal(&feasible_values2[j]);
	    while(!List_end_traversal(&feasible_values2[j])) {
	      b = List_consult1(&feasible_values2[j]);
	      List_next(&feasible_values2[j]);
	      fprintf(file, "%d %d ", idxa, idxb);
	      FPRINTCOST(file, CostB(i, a, j, b));
	      fprintf(file, "\n");
	      idxb++;
	    }
	    List_dispose_memory(&feasible_values2[j]);
	    idxa++;
	  }
	  List_dispose_memory(&feasible_values2[i]);  
	}
      }
    }
  }
  /* unary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1) {
      fprintf(file, "1 %d ", num[i]);
      FPRINTCOST(file, BOTTOM);
      fprintf(file, " %d\n", List_n_of_elem(&feasible_values[i]));
      idxa=0;
      List_create(&feasible_values2[i]);
      sort_val_lex(i);
      List_reset_traversal(&feasible_values2[i]);
      while(!List_end_traversal(&feasible_values2[i])) {
	a = List_consult1(&feasible_values2[i]);
	List_next(&feasible_values2[i]);
	fprintf(file, "%d ", idxa);
	FPRINTCOST(file, CostU(i, a));
	fprintf(file, "\n");
	idxa++;
      }
      List_dispose_memory(&feasible_values2[i]);
    }
  }
  if (Bottom > BOTTOM) {
    fprintf(file, "0 ");
    FPRINTCOST(file, Bottom);
    fprintf(file, " 0\n");
  }
}

#define ISMAXANCESTOR(a,c,d) (ISANCESTOR(a,c) && cdist(a,c) < d)

void saveWCSPsubtree(FILE* file, int cluster, int maxcdepth) 
{
  int i,j,a,b,c,idxa,idxb,nbvar = 0, nbval = 0, nbconstr = 0;
  int first = NVar;
  int num[NVar];
  int unassigned[NConstr];
  int recursiveNaryAssignment[MAXNVAR];
  int recursiveNaryPrintIdxVal[MAXARITY];

  // warning! n-ary constraints are forgotten if at least one variable in its scope is already assigned

  /* count the number of constraints */
  if (subtreeBottom(cluster) > BOTTOM) nbconstr++;  
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1 && ISMAXANCESTOR(cluster,Var2Cluster[i],maxcdepth)) {
      if (first==NVar) first = i;
      num[i] = nbvar;
      nbvar++;
      nbval = MAX(nbval, List_n_of_elem(&feasible_values[i]));
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 && ISBINARY(i,j) && ISMAXANCESTOR(cluster,Var2Cluster[j],maxcdepth)) {
	  nbconstr++;
	}
      }
    }
  }
  nbconstr += nbvar; /* unary costs */
  for (c=0; c<NConstr; c++) { /* n-ary constraints */
    unassigned[c] = TRUE;
    for (i=0; i<ARITY(c); i++) {
      if (assignments[SCOPEONE(c,i)]!=-1 || !ISMAXANCESTOR(cluster,Var2Cluster[SCOPEONE(c,i)],maxcdepth)) {
	unassigned[c] = FALSE;
	break;
      }
    }
    if (unassigned[c]) nbconstr++;
  }

  /* header */
  fprintf(file, "%s %d %d %d ", Filename, nbvar, nbval, nbconstr);
  FPRINTCOST(file, Top);
  fprintf(file, "\n");
  /* domain sizes */
  if (first < NVar) {
    fprintf(file, "%d", List_n_of_elem(&feasible_values[first]));
    for (i=first+1; i<NVar; i++) {
      if (assignments[i]==-1 && ISMAXANCESTOR(cluster,Var2Cluster[i],maxcdepth)) {
	fprintf(file, " %d", List_n_of_elem(&feasible_values[i]));
      }
    }
    fprintf(file, "\n");
  }
  /* n-ary constraints */
  for (c=0; c<NConstr; c++) {
    if (unassigned[c]) {
      fprintf(file, "%d", ARITY(c));
      for (i=0; i<ARITY(c); i++) {
	fprintf(file, " %d", num[SCOPEONE(c,i)]);
      }
      fprintf(file, " ");
      FPRINTCOST(file, BOTTOM);
      fprintf(file, " %d\n", truecartesianProduct(c));
      recursiveNaryPrint(file, c, 0, recursiveNaryAssignment, recursiveNaryPrintIdxVal); 
    }
  }
  /* binary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1 && ISMAXANCESTOR(cluster,Var2Cluster[i],maxcdepth)) {
      for (j=i+1; j<NVar; j++) {
	if (assignments[j]==-1 && ISBINARY(i,j) && ISMAXANCESTOR(cluster,Var2Cluster[j],maxcdepth)) {
	  fprintf(file, "2 %d %d ", num[i], num[j]);
	  FPRINTCOST(file, BOTTOM);
	  fprintf(file, " %d\n", List_n_of_elem(&feasible_values[i])*List_n_of_elem(&feasible_values[j]));
	  idxa=0;
	  List_create(&feasible_values2[i]);
	  sort_val_lex(i);
	  List_reset_traversal(&feasible_values2[i]);
	  while(!List_end_traversal(&feasible_values2[i])) {
	    a = List_consult1(&feasible_values2[i]);
	    List_next(&feasible_values2[i]);
	    idxb=0;
	    List_create(&feasible_values2[j]);
	    sort_val_lex(j);
	    List_reset_traversal(&feasible_values2[j]);
	    while(!List_end_traversal(&feasible_values2[j])) {
	      b = List_consult1(&feasible_values2[j]);
	      List_next(&feasible_values2[j]);
	      fprintf(file, "%d %d ", idxa, idxb);
	      FPRINTCOST(file, CostB(i, a, j, b));
	      fprintf(file, "\n");
	      idxb++;
	    }
	    List_dispose_memory(&feasible_values2[j]);
	    idxa++;
	  }
	  List_dispose_memory(&feasible_values2[i]);  
	}
      }
    }
  }
  /* unary constraints */
  for (i=0; i<NVar; i++) {
    if (assignments[i]==-1 && ISMAXANCESTOR(cluster,Var2Cluster[i],maxcdepth)) {
      fprintf(file, "1 %d ", num[i]);
      FPRINTCOST(file, BOTTOM);
      fprintf(file, " %d\n", List_n_of_elem(&feasible_values[i]));
      idxa=0;
      List_create(&feasible_values2[i]);
      sort_val_lex(i);
      List_reset_traversal(&feasible_values2[i]);
      while(!List_end_traversal(&feasible_values2[i])) {
	a = List_consult1(&feasible_values2[i]);
	List_next(&feasible_values2[i]);
	fprintf(file, "%d ", idxa);
	FPRINTCOST(file, CostU(i, a));
	fprintf(file, "\n");
	idxa++;
      }
      List_dispose_memory(&feasible_values2[i]);
    }
  }
  if (subtreeBottom(cluster) > BOTTOM) {
    fprintf(file, "0 ");
    FPRINTCOST(file, subtreeBottom(cluster));
    fprintf(file, " 0\n");
  }
}
