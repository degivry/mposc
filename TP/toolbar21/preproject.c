/* ---------------------------------------------------------------------
  TOOLBAR - A constraint optimization toolbox

  File: preproject.c
  $Id: preproject.c,v 1.1.1.1 2007-11-21 09:26:17 degivry Exp $

  Authors: 
     Simon de Givry (1), Federico Heras,(2) 
     Javier Larrosa (2), Thomas Schiex (1)

     (1) INRA, Biometry and AI Lab. Toulouse, France
     (2) UPC, Language and Computer Sciences Dpt. Barcelona, Spain

  Copyright 2003 by the authors.
  This file is distributed under the Artistic License (See LICENSE file)
  --------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "wcsp.h"

#define UNKNOWN -1

cost_t ****BinProj;
#define BINPROJ(i,j) BinProj[i][j-i-1]
cost_t **CostProj;
int Projected;
int Created;
/* ------------------------------------------------------------- 
// Next tuple given that vi1 and vi2 are locked, returns FALSE if
// there is no next tuple (last is reached).
//  -----------------------------------------------------------*/
int NextTupleLock(int constr, int *tuple, int vi1,int vi2) 
{
  int idx = ARITY(constr)-1;

  while (idx >=0) {
    /* do not change locked variables */
    while (idx == vi1 || idx == vi2) idx--;
    if (idx < 0) return FALSE;
    tuple[idx]++;

    if (tuple[idx] < DOMSIZE(SCOPEONE(constr,idx))) return TRUE;
    else {
      tuple[idx] = 0;
      idx --;
    }
  }
  return FALSE;
}

/* -------------------------------------------------------------
// set the cost of an nary variable just from a tuple on the
// scope
// -----------------------------------------------------------*/
void setLocalCost(int c, int* tuple,cost_t howmuch)
{
  int i = 1, index = tuple[0];
  int fin = ARITY(c);

  while(i < fin) {
    index *= DOMSIZE(SCOPEONE(c,i));
    index += tuple[i];
    i++;
  }

  COSTS(c)[index] = howmuch;
}
/* -------------------------------------------------------------
// computes the cost of an nary variable just from a tuple on the
// scope
// -----------------------------------------------------------*/
cost_t localCost(int c, int* tuple)
{
  int i = 1, index = tuple[0];
  int fin = ARITY(c);
  
  while(i < fin) {
    index *= DOMSIZE(SCOPEONE(c,i));
    index += tuple[i];
    i++;
  }
  /*  printf("index %d [",index);
  for (i=0; i<ARITY(c); i++) printf(" %d",tuple[i]);
  printf(" ]\n");
  for (i=0; i<ARITY(c); i++) printf(" %d",DOMSIZE(SCOPEONE(c,i)));
  printf("\n");*/
  
  return COSTS(c)[index];
}
/* -------------------------------------------------------------
// projection of one nary constraint on one pair val1 val2 of a
// binary constraint cij
// -----------------------------------------------------------*/
cost_t ProjectToOnePair(int constr, int vi1,int vi2,int val1,int val2)
{
  int idx,jdx;
  cost_t Min = MAXCOST,Cost;
  //int tuple[ARITY(constr)];
  int *tuple;

  tuple=mymalloc(ARITY(constr),sizeof(int));

  /*printf("C%d, Arity %d, vi1 %d vi2 %d val1 %d val2 %d\n",
    constr,ARITY(constr),vi1,vi2,val1,val2); */

  for (idx=0; idx<ARITY(constr); idx++) tuple[idx] = 0; 
  tuple[vi1] = val1;
  tuple[vi2] = val2;

  /* Compute the minimum cost extension */
  while (1) {
    /*    printf("[ ");
    for(idx=0; idx<ARITY(constr); idx++) printf("%d ", tuple[idx]);
    printf("]\n");*/

    Cost = localCost(constr,tuple);
    Min = MIN(Min,Cost);
    if (Min == 0) break;
    if (!NextTupleLock(constr,tuple,vi1,vi2)) break;
  }

  if (Min) {
    if (CostProj[vi1][vi2-vi1-1] + Min >= Top) CostProj[vi1][vi2-vi1-1] = Top;
    else   CostProj[vi1][vi2-vi1-1] += Min;
    /* if min != 0, then substract */
    for (idx=0; idx<ARITY(constr); idx++) tuple[idx] = 0; 
    tuple[vi1] = val1;
    tuple[vi2] = val2;
    
    while (1) {
      /*      printf("[ ");
      for(idx=0; idx<ARITY(constr); idx++) printf("%d ", tuple[idx]);
      printf("]\n");*/

		if ((Cost = localCost(constr,tuple)) < Top) {
			setLocalCost(constr,tuple,Cost-Min);

			/* if the cost is 0, this creates a support for forthcoming pairs */
			if (Cost == Min)
				for (idx = vi1; idx < ARITY(constr) -1; idx++)
					for (jdx = ((idx == vi1) ? vi2+1 : idx+1); jdx < ARITY(constr); jdx++) 
						BINPROJ(idx,jdx)[tuple[idx]][tuple[jdx]] = 0;
		} 
      if (!NextTupleLock(constr,tuple,vi1,vi2)) break;
    }
  }
  else {
    /* Min = 0, we have found a support */
    for (idx = vi1+1; idx < ARITY(constr) -1; idx++)
      for (jdx = idx+1; jdx < ARITY(constr); jdx++)
	BINPROJ(idx,jdx)[tuple[idx]][tuple[jdx]] = 0;
  }

  free(tuple);
  return Min;
}
/* -------------------------------------------------------------
// projection of one nary constraint on one binary constraint cij
// we must have i<j
// -----------------------------------------------------------*/
void ComputeOneProj(int constr, int vi1, int vi2)
{
  int val1,val2;
  cost_t **Costs = BINPROJ(vi1,vi2);

  for (val1 = 0; val1 < DOMSIZE(SCOPEONE(constr,vi1)); val1++)
    for (val2 = 0; val2 < DOMSIZE(SCOPEONE(constr,vi2)); val2++) {
      assert(Costs[val1][val2] <0 || Costs[val1][val2] == 0);

		if (Costs[val1][val2] < 0){
			Costs[val1][val2] = ProjectToOnePair(constr, vi1,vi2,val1,val2);
		}
    }
}
/* -------------------------------------------------------------
// Merge back the results in the Wcsp
// -----------------------------------------------------------*/
void BinaryMergeInWCSP(int constr, int BN)
{
  int i,j,vi,vj,k,l;
  cost_t val;
	
  for (i=0; i < ARITY(constr)-1; i++) 
    for (j=(BN ? ARITY(constr)-1 :i+1); j< ARITY(constr); j++)
      if (CostProj[i][j-i-1]) {
	Projected++;
	/* we have something to say here */
	vi = SCOPEONE(constr,i);
	vj = SCOPEONE(constr,j);
	if (Verbose>=2) printf("nary constraint %d projected on binary constraint with variables %d,%d\n",constr,vi,vj);
	if (!ISBINARY(vi,vj)) {
		ISBINARY(vi,vj) = ISBINARY(vj,vi) = 
		(cost_t *)((double *)(mycalloc(DOMSIZE(vi)*DOMSIZE(vj)*sizeof(cost_t)+sizeof(double),1))+1);
	  Created++;
	}
	for (k = 0; k < DOMSIZE(vi); k++)	
		for (l=0; l < DOMSIZE(vj); l++) {
			val = binaryCost(vi,vj,k,l)+BINPROJ(i,j)[k][l]; 
			if (val > Top) val = Top;
			setBinaryCost(vi,vj,k,l,val);
		}
      }
}
/* -------------------------------------------------------------
// projection of one nary constraint to binary constraints
// -----------------------------------------------------------*/
void ProjectOneBinary(int constr, int BN)
{
  int i,j,k,l;
  
  /* allocation of bin. constraints and global costs */
  /* First: pointers to the binary constraints */
  BinProj = mymalloc(ARITY(constr)-1,sizeof(cost_t ***));
  /* Array of global cost projected */
  CostProj = mymalloc(ARITY(constr)-1,sizeof(cost_t *));

  for (i=0; i< ARITY(constr)-1; i++) {
    BinProj[i] = mymalloc(ARITY(constr)-i-1,sizeof(cost_t **));
    CostProj[i] = mycalloc(ARITY(constr)-i-1,sizeof(cost_t));
  }
  
  for (i=0; i < ARITY(constr)-1; i++)
    for (j=i+1; j < ARITY(constr); j++) {
      BINPROJ(i,j) = mymalloc(DOMSIZE(SCOPEONE(constr,i)),sizeof(cost_t *));
      for (k = 0; k < DOMSIZE(SCOPEONE(constr,i)); k++) {
	BINPROJ(i,j)[k] = 
	  mymalloc(DOMSIZE(SCOPEONE(constr,j)),sizeof(cost_t));
	for (l=0; l < DOMSIZE(SCOPEONE(constr,j)); l++)
	  BINPROJ(i,j)[k][l] = UNKNOWN;
      }
    }

  for (i=0; i < ARITY(constr)-1; i++)
    for (j=(BN ? ARITY(constr)-1 : i+1); j< ARITY(constr); j++) 
      ComputeOneProj(constr, i,j);

   // Merge with the WCSP
  BinaryMergeInWCSP(constr,BN);

   // deallocation
   for (i=0; i < ARITY(constr)-1; i++) {
     for (j=i+1; j< ARITY(constr); j++) { 
       for (k = 0; k < DOMSIZE(SCOPEONE(constr,i)); k++) 
	 free(BINPROJ(i,j)[k]);
       free(BINPROJ(i,j));
     }
     free(BinProj[i]);
     free(CostProj[i]);
   }
   free(BinProj);
   free(CostProj);
}
/* -------------------------------------------------------------
// projection of all nary constraints of arity below k to binary
// constraints
// for BayesNet dedicated projection of all nary constraints of arity
// below k to binary constraints: only the n binary constraints to the
// target variable are considered.
// -----------------------------------------------------------*/
 /* we first allocate all possible binary projections, fill them with
    -1 and start projecting. Each time a 0 is met, all the pairs that
    corresponds to this tuple are set to 0, meaning that it is useless
    to compute a projection for them.

    For each constraint, a global amount of cost is computed. If equal
    to 0 nothing is done.  Finally the constraints are merged with the
    CSP.
 */
void Project2Binary(int k, int BN) {

  int i;

  Projected = Created = 0;
  if (Verbose) {
    printf("Projecting on binary constraints...");
    fflush(stdout);
  }

  for (i=0; i<NConstr; i++) 
    if (ARITY(i) <= k) ProjectOneBinary(i,BN);
  

  if (Verbose)
    printf("%d constraints proj. (%d new).\n",Projected,Created);
}

