import choco.Problem;
import choco.Solver;
import choco.Solution;

// **************************************************
// *                   J-CHOCO                      *
// *   Copyright (C) F. Laburthe, 1999-2003         *
// **************************************************
// *  an open-source Constraint Programming Kernel  *
// *     for Research and Education                 *
// **************************************************

/* File chocosamples.Queen.java, last modified by flaburthe 16 janv. 2004 11:55:37 */

public class Queen {
  public static void main(String[] args) {
    System.out.println(new Queen().demo());
  }

  public String demo() {
    StringBuffer ret = new StringBuffer();
    int nbQueensSolution[] = {0,0,0,0,2,10,4,40,92,352,724,2680,14200,73712};
    int n = 4;

    Problem pb = new Problem();

    // create variables
    choco.integer.IntVar[] queens = new choco.integer.IntVar[n];
    for (int i = 0; i<n; i++) {
      queens[i] = pb.makeEnumIntVar("Q" + i,1,n);
    }
    // all different constraints
    for (int i=0; i<n; i++) {
      for (int j=i+1; j<n; j++) {
          int k = j - i;
          pb.post(pb.neq(queens[i], queens[j]));
          pb.post(pb.neq(queens[i], pb.plus(queens[j], k)));
          pb.post(pb.neq(queens[i], pb.minus(queens[j], k)));
      }
    }
    
    Solver s = pb.getSolver();
    pb.solve(true);

    System.out.println("feasible: " + pb.isFeasible());
    System.out.println("nbSol: " + s.getNbSolutions());

    // Display
    // -------
    ret.append("The queen's problem asks how to place n queens on an n x n chess board "+
        "so that none of them can hit any other in one move.\n");
    ret.append("Here n = " + n + "\n\n");
    ret.append("The " + s.getSearchSolver().solutions.size() + " last solutions (among " +
        s.getNbSolutions() + " solutions) are:\n");
    String line = "+";
    for(int i = 0; i < n; i++) line += "---+";
    line += "\n";
    for(int sol = 0; sol < s.getSearchSolver().solutions.size(); sol ++) {
      Solution solution = (Solution) s.getSearchSolver().solutions.get(sol);
      ret.append(line);
      for(int i = 0; i < n; i++) {
        ret.append("|");
        for(int j = 0; j < n; j++) {
          ret.append((solution.getValue(i) == j + 1)?" * |":"   |");
        }
        ret.append("\n" + line);
      }
      ret.append("\n\n\n");
    }
    return ret.toString();
  }
}
