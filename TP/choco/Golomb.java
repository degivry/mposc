import choco.Problem;
import choco.Solver;
import choco.Solution;
import choco.integer.IntVar;

// **************************************************
// *                   J-CHOCO                      *
// *   Copyright (C) F. Laburthe, 1999-2003         *
// **************************************************
// *  an open-source Constraint Programming Kernel  *
// *     for Research and Education                 *
// **************************************************

public class Golomb {
  public static void main(String[] args) {

    Problem pb = new Problem();

    int n = 6;

    // create the main variables
    IntVar[] X = new IntVar[n];

    for (int i = 0; i<n; i++) {
      X[i] = pb.makeEnumIntVar("X" + i, 0, 1 << n);
    }

    // create new (intermediate) distance variables
    IntVar[] D = new IntVar[n*(n-1)/2];
    int pos = 0;
    for (int i=0; i<n; i++) {
	for (int j=i+1; j<n; j++) {
	    D[pos] = pb.makeEnumIntVar("D" + i + "_" + j, 1, 1 << n);
	    pb.post(pb.eq(D[pos], pb.minus(X[j], X[i])));
	    pos++;
	}
    }

    // main constraint: all distances must be different
    pb.post(pb.allDifferent(D));

    // add a redundant constraint: lower bound on X
    for (int i=0; i<n; i++) {
	pb.post(pb.geq(X[i], i * (i+1) / 2));
    }

    // add a redundant constraint: upper bound on X
    for (int i=0; i<n-1; i++) {
	pb.post(pb.leq(X[i], pb.minus(X[n-1], (n - i - 1) * (n - i) / 2)));
    }

    // remove symetrical solutions by translation
    pb.post(pb.eq(X[0],0));

    // remove symetrical mirror solution by reflexion
    pb.post(pb.lt(D[0],D[pos-1]));
    
    // solve
    Solver s = pb.getSolver();
    pb.minimize(X[n-1],false);

    // Display
    // -------
    System.out.println("\nGolomb ruler.");
    System.out.println("Here n = " + n + "\n");
    System.out.println("The optimum solution (after " + s.getNbSolutions() + " solutions) is:");
    int sol = s.getSearchSolver().solutions.size() - 1;
    Solution solution = (Solution) s.getSearchSolver().solutions.get(sol);
    for(int i = 0; i < n; i++) {
        System.out.print(" " + solution.getValue(i));
    }
    System.out.print("\n");
  }
}
