import choco.Problem;
import choco.Solver;
import choco.Solution;
import choco.integer.IntVar;
import choco.ContradictionException;

// **************************************************
// *                   J-CHOCO                      *
// *   Copyright (C) F. Laburthe, 1999-2003         *
// **************************************************
// *  an open-source Constraint Programming Kernel  *
// *     for Research and Education                 *
// **************************************************

public class Sudoku {
  public static void main(String[] args) {

    Problem pb = new Problem();

    int n = 3;

    // create variables
    IntVar[] s = new IntVar[n*n];

    for (int i = 0; i<n*n; i++) {
      s[i] = pb.makeEnumIntVar("S" + i,1,9);
    }

    // all different constraints

    pb.post(pb.allDifferent(s));

//   for (int i = 0 ; i<n; i++) {
//  	IntVar[] diff_interior = new IntVar[n];
//  	IntVar[] diff_line = new IntVar[n];
//  	IntVar[] diff_column = new IntVar[n];
//  	for (int j = 0 ; j<n; j++) {
//  	    diff_interior[j] = s[i*n+j];
//  	    diff_line[j] = s[(i % 3)*3+(i / 3)*3*n+(j % 3)+(j / 3)*9];
//  	    diff_column[j] = s[(j % 3)*3+(j / 3)*3*n+(i % 3)+(i / 3)*9];
//  	}
//   }

    // known numbers. Easy grid
    pb.post(pb.eq(s[0],5));
    pb.post(pb.eq(s[1],3));
    pb.post(pb.eq(s[3],6));
    pb.post(pb.eq(s[7],9));
    pb.post(pb.eq(s[8],8));
//      pb.post(pb.eq(s[10],7));
//      pb.post(pb.eq(s[12],1));
//      pb.post(pb.eq(s[13],9));
//      pb.post(pb.eq(s[14],5));
//      pb.post(pb.eq(s[25],6));
//      pb.post(pb.eq(s[27],8));
//      pb.post(pb.eq(s[30],4));
//      pb.post(pb.eq(s[33],7));
//      pb.post(pb.eq(s[37],6));
//      pb.post(pb.eq(s[39],8));
//      pb.post(pb.eq(s[41],3));
//      pb.post(pb.eq(s[43],2));
//      pb.post(pb.eq(s[47],3));
//      pb.post(pb.eq(s[50],1));
//      pb.post(pb.eq(s[53],6));
//      pb.post(pb.eq(s[55],6));
//      pb.post(pb.eq(s[66],4));
//      pb.post(pb.eq(s[67],1));
//      pb.post(pb.eq(s[68],9));
//      pb.post(pb.eq(s[70],8));
//      pb.post(pb.eq(s[72],2));
//      pb.post(pb.eq(s[73],8));
//      pb.post(pb.eq(s[77],5));
//      pb.post(pb.eq(s[79],7));
//      pb.post(pb.eq(s[80],9));

    // known numbers. Difficult grid
//      pb.post(pb.eq(s[0],1));
//      pb.post(pb.eq(s[3],7));
//      pb.post(pb.eq(s[9],8));
//      pb.post(pb.eq(s[15],5));
//      pb.post(pb.eq(s[16],6));
//      pb.post(pb.eq(s[19],3));
//      pb.post(pb.eq(s[22],2));
//      pb.post(pb.eq(s[25],7));
//      pb.post(pb.eq(s[29],8));
//      pb.post(pb.eq(s[32],5));
//      pb.post(pb.eq(s[35],4));
//      pb.post(pb.eq(s[39],2));
//      pb.post(pb.eq(s[40],1));
//      pb.post(pb.eq(s[41],7));
//      pb.post(pb.eq(s[45],9));
//      pb.post(pb.eq(s[48],4));
//      pb.post(pb.eq(s[51],7));
//      pb.post(pb.eq(s[55],3));
//      pb.post(pb.eq(s[58],2));
//      pb.post(pb.eq(s[61],8));
//      pb.post(pb.eq(s[64],8));
//      pb.post(pb.eq(s[65],9));
//      pb.post(pb.eq(s[71],4));
//      pb.post(pb.eq(s[77],8));
//      pb.post(pb.eq(s[80],6));
    
    // show initial propagation effect
    try {
        pb.propagate();
    } catch (ContradictionException e) {
	assert(false);
    }
    System.out.println(pb.pretty());

    // solve
    Solver solver = pb.getSolver();
//  pb.solve();
    pb.solveAll();

    System.out.println("feasible: " + pb.isFeasible());
    System.out.println("nbSol: " + solver.getNbSolutions());

    // Display
    // -------
    System.out.println("Sudoku game.");
    System.out.println("Here n = " + n + "\n");
    System.out.println("The " + solver.getSearchSolver().solutions.size() + " last solutions (among " +
        solver.getNbSolutions() + " solutions) are:");
    String line = "+";
    for(int i = 0; i < n; i++) line += "---+";
    line += "\n";
    for(int sol = 0; sol < solver.getSearchSolver().solutions.size(); sol ++) {
      Solution solution = (Solution) solver.getSearchSolver().solutions.get(sol);
      System.out.print(line);
      for(int i = 0; i < n; i++) {
        System.out.print("|");
        for(int j = 0; j < n; j++) {
//          System.out.print(" " + solution.getValue((i % 3)*3+(i / 3)*3*n+(j % 3)+(j / 3)*9) + " |");
          System.out.print(" " + solution.getValue(i*3+j) + " |");
        }
        System.out.print("\n" + line);
      }
      System.out.print("\n\n\n");
    }
  }
}
