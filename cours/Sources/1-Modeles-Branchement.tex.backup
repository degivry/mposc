\documentclass[a4paper]{beamer}

\usepackage{graphicx}
\usepackage[french]{babel}
\usepackage[latin1]{inputenc}
\usepackage{latexsym}
\usepackage{ifthen}
\usepackage{fp}
\usepackage{tikz}
\usepackage{latexsym}
\usepackage{progressbarbeamertheme}
\usepackage{amssymb}
\usepackage[french,vlined]{algorithm2e}
%\input{treetex}

\let\phi\varphi
\nofiles

\let\cal\relax
\newcommand\cal[1]{\mathcal{#1}}
\newcommand\Desc{\nommath{Desc}}
\newcommand\limp{\to}
\newcommand\nommath[1]{\mathrm{#1}}
\newcommand\Obs{\nommath{Obs}}
\newcommand\ok{\pred{ok}}
\newcommand\pred[1]{\mathsf{#1}}
\newcommand\MRes{\nommath{Res}}\newcommand\Vrai{\mathsf{V}}
\newcommand\Faux{\mathsf{F}}

% Macros pour d�crire un arbre de Davis et Putnam
% pour TeXTree
\input{treetex}
\newcommand{\DPchoix}[2]{\relax
  \node{\ifx #1d\rightonly
        \else\ifx #1u\unary\else\leftonly\fi\fi
        \type{text}\cntr{\small #2}}}
\newcommand{\DPfeuille}[1]{\relax
  \node{\external\type{text}\cntr{#1}}}
\newcommand{\DPunit}[1]{\relax
  \node{\unary\type{text}\cntr{#1}}}
\newcommand\DPbin[1]{\relax
  \node{\type{text}\cntr{#1}}}

\title{M�thodes de preuves, Optimisation, Satisfaction de contraintes}
\subtitle{Probl�mes et repr�sentation}

\author{Master d'informatique}
\date{Ann�e 2008-2009}
\institute{Universit� Paul Sabatier}

\begin{document}

\begin{frame}

\titlepage

\end{frame}

\begin{frame}
  \frametitle{Chapitre 0 : Introduction}

Vous pouvez r�cup�rer les documents associ�s aux cours sur le web:
\begin{enumerate}
 \item Aller sur http://mulcyber.toulouse.inra.fr/gf/project/mposc
 \item Cliquer sur l'onglet ``CVS''
 \item dossier ``cours'': les PDF des transparents et des exercices
 \item dossier ``examens'': annales avec corrig�s
 \item cliquer sur le fichier et puis sur ``download''.
\end{enumerate}

\end{frame}

\begin{frame}
  \frametitle{Probl�mes combinatoires propositionnels}

Beaucoup de probl�mes pos�s par l'IA, la recherche op�rationnelle
(entre autres), se d�crivent par~:
\begin{enumerate}
\item un ensemble de variables � domaines fini
\item un ensemble de contraintes, �quations, fonctions sur les affectations
possibles qui d�finissent les (meilleures) solutions
du probl�me
\item une requ�te (question) sur cet ensemble de solutions, � laquelle
on demande au programme de r�pondre
\end{enumerate}

\begin{example}
\textbf{Diagnostic} : connaissant l'ensemble des propri�t�s de fonctionnement
d'un syst�me et observant un dysfonctionnement, en trouver la cause
\end{example}

\end{frame}

\begin{frame}
  \frametitle{Autres probl�mes}

\begin{example}
\begin{itemize}
\item \textbf{Conception}~: �tant donn� un ensemble de propri�t�s d�sir�es pour
  un objet et les contraintes qui peuvent exister dans sa fabrication,
  choisir les param�tres d�crivant l'objet.
\item \textbf{Raisonnement} : �tant donn�es des connaissances d�crites sur des
variables d'�tat, d�terminer quels sont les valeurs possibles de ces
variables; plus largement, d�duire des informations sur l'�tat du
syst�me d�crit.
\item \textbf{Ench�res combinatoires} : �tant donn�es des propositions d'achat
sur des ensembles d'objets, d�terminer � qui vendre chaque objet
de mani�re � maximiser son gain.
\end{itemize}
\end{example}
\end{frame}

\begin{frame}
  \frametitle{Probl�mes combinatoires propositionnels}

\begin{definition}[{Probl�mes combinatoires propositionnels}]
Probl�mes d�finis par : un ensemble de variables � domaines fini
+ propri�t�s (contraintes, �quations, co�ts) + \textbf{une requ�te} :

\begin{itemize}
\item \textbf{D�cision}~: existence d'une solution (un objet qui r�pond � toutes les propri�t�s).
\item \textbf{Recherche}~: trouver une solution.
\item \textbf{Optimisation}~: trouver la meilleure solution (crit�re).
\item \textbf{D�nombrement}~: combien de solutions possibles ?
\end{itemize}
\end{definition}

Tous sont des probl�mes ``Combinatoires''~: potentiellement, si $n$
variables, il y a $d_1 \times d_2 \times \cdots\times d_n$, disons $d^n$ ``affectations'' � explorer pour r�pondre � la requ�te.

\end{frame}

\begin{frame}
\frametitle{Exemple}

\begin{block}{Voyageur de commerce}
Un ensemble de villes $V$ et de routes connectant ces villes (distances $d_{ij}$).
\end{block}

\begin{block}{Probl�mes de\ldots}
\begin{itemize}
\item \textbf{D�cision}~: existe-t-il une route passant une fois 
  par chaque ville  et dont la longueur totale est inf�rieure � $k$.
\item \textbf{Recherche}~: chercher un tour de longueur inf�rieure � $k$.
\item \textbf{Optimisation}: chercher un tour de longueur minimum (il n'existe
  aucun tour strictement plus court)
\item \textbf{D�nombrement}~: combien de tours de longueur inf�rieure ou �gale �
  $k$ existe-t-il ?
\end{itemize}
\end{block}

Solution: facile � v�rifier ou pas\ldots

\end{frame}

\begin{frame}
\frametitle{Objectifs du cours}

Intelligence artificielle, recherche op�rationelle,\ldots proposent de
nombreux formalismes de repr�sentation mais s'int�ressent souvent
aux m�mes types de probl�mes.

Les principes des algorithmes sont souvent similaires.

\begin{block}{Objectifs}
\begin{itemize}
\item une bo�te � outils pour la r�solution de probl�mes combinatoires
en IA : pr�senter les principaux algorithmes et heuristiques
utilis�s.
\item une introduction � la th�orie de la complexit� des algorithmes
et des probl�mes
\end{itemize}
\end{block}

\end{frame}

\begin{frame}
\frametitle{Plan du cours}

\begin{block}{Plan}
\begin{enumerate}
\item Formalismes de repr�sentation et algorithmes de branchement 
\item Algorithmes de production de cons�quences complets
\item Introduction � la th�orie de la complexit�
\item Algorithmes de production de cons�quences incomplets (simplification, propagation)
\item M�thodes hybrides (branchement/propagation)
\item Recherche incompl�te et algorithmes ``m�ta-heuristiques''
\end{enumerate}
\end{block}

\end{frame}

\begin{frame}
  \frametitle{Chapitre 1 : Formalismes de repr�sentation, algorithmes de branchement}
\end{frame}


\begin{frame}
 
\frametitle{Logique des propositions}

\begin{block}{D�finitions}
\begin{itemize}
\item \textbf{Symboles de propositions} : un ensemble fini $X$
\item \textbf{Litt�raux} : $X^\pm = X \cup \{\lnot x \mid x \in X\}$
  (litt. positifs/n�gatifs)\\
  Si $l \in X^\pm $, $ \overline{l} = $ le compl�mentaire de $l$:
  $\overline{x} = \lnot x $, $\overline{\lnot x} = x $.
\item \textbf{Formules logiques} : obtenues en combinant les
  symboles avec des connecteurs ($\land$, $\lor$, $\lnot$,
  $\limp$, $\leftrightarrow$, $\top$, $\bot$)
\vskip -3mm
\[a \limp b \equiv (\lnot a \lor b)\]
\[a \leftrightarrow b \equiv (a \limp b) \land (b \limp a)\]

\item \textbf{Cube}  une conjonction de litt�raux ($a \land b \land \neg c$)
\item \textbf{Clause} : une disjonction de litt�raux ($a\lor b\lor \neg c$)
\item \textbf{FNC/CNF} : Forme Normale Conjonctive (conjonction de clauses).
\end{itemize}
\end{block}

\end{frame}

\begin{frame}
\frametitle{Logique des propositions}

\begin{block}{Affectation/mod�le}
\textbf{Affectation (ou interpr�tation)} : une fonction
  $A : X \limp \{\Vrai, \Faux\} $.
\begin{itemize}
\item $A$ est �tendue (alg�bre de Boole) aux formules (symboles pour lesquels $A$ est d�finie)
\item  Pour une formule $\phi$, on note $A \models \phi$ si $A(\phi)=\Vrai$. On dit que $A$ est un mod�le de $\phi$.
\item Mod�le d'un ensemble de formules $\Psi$ :  une affectation $A$ telle que $A \models \bigwedge_{\phi \in \Psi} \phi $.   On note aussi $A \models \Psi $.
\end{itemize}
\end{block}

\begin{definition}
 Une formule (resp. un ensemble de formules) est coh�rente
(ou ``consistante``) ssi elle admet un mod�le.
\end{definition}

\end{frame}


\begin{frame}
\frametitle{Diagnostic d'un syst�me}

\centerline{\includegraphics[width=9cm]{dia}}

\begin{example}{Test du circuit}
  $a_1=1$, $a_2=0$, $a_3=1$, on observe $c_1=1$, $c_2=0$

Y a t-il une panne et si oui, quels composants peuvent �tre
responsables de la panne ?
\end{example}

\end{frame}

\begin{frame}
\frametitle{Diagnostic d'un syst�me}

\includegraphics[width=5cm]{dia}\raisebox{1.16cm}{$\begin{array}{c}
 S =  \left\{\begin{array}{l}
    \ok(e_1) \limp (b_2 \Leftrightarrow (a_1 \land a_2)), \\
    \ok(e_2) \limp (b_3 \Leftrightarrow (b_1 \land a_3)), \\
    \ok(x_1) \limp (b_1 \Leftrightarrow (a_1 \Leftrightarrow \lnot a_2)),\\
    \ok(x_2) \limp (c_1 \Leftrightarrow (b_1 \Leftrightarrow \lnot a_3)),\\
    \ok(o) \limp (c_2 \Leftrightarrow (b_2 \lor b_3))
  \end{array}\right. 
\end{array} $}

o\`u $\ok(p)=$ la porte $p$ fonctionne bien.

\begin{example}
 
Observations : $a_1=1$, $a_2=0$, $a_3=1$,$c_1=1$, $c_2=0$ \\
\centerline{$O=\{a_1,\lnot a_2,a_3,c_1,\lnot c_2\}$}
\end{example}

Si $S\cup O\cup\{\ok(e_1), \ok(e_2),\ok(x_1), \ok(x_2), \ok(o)\}$ a un mod�le , le comportement est normal.

Ici, $S \cup O \cup \{\ok(e_1), \ldots\}$ n'est pas coh�rent $\Rightarrow $ Panne.

\end{frame}

\begin{frame}
\frametitle{Diagnostic d'un syst�me}

\includegraphics[width=5cm]{dia}\raisebox{1.16cm}{$\begin{array}{c}
 S =  \left\{\begin{array}{l}
    \ok(e_1) \limp (b_2 \Leftrightarrow (a_1 \land a_2)), \\
    \ok(e_2) \limp (b_3 \Leftrightarrow (b_1 \land a_3)), \\
    \ok(x_1) \limp (b_1 \Leftrightarrow (a_1 \Leftrightarrow \lnot a_2)),\\
    \ok(x_2) \limp (c_1 \Leftrightarrow (b_1 \Leftrightarrow \lnot a_3)),\\
    \ok(o) \limp (c_2 \Leftrightarrow (b_2 \lor b_3))
  \end{array}\right. 
\end{array} $}

\begin{block}{Diagnostic}
En situation de panne, trouver un diagnostic = trouver un mod�le de $S\cup O$.
\begin{itemize}
\item avec un litt�ral $\neg ok()$ (panne unique).
\item avec le minimum de litt�raux $\neg ok()$ (pannes rares, parsimonie).
\end{itemize}
\end{block}

\end{frame}

\begin{frame}
\frametitle{Cons�quence logique, �quivalence}

\begin{definition}[Cons�quence logique]
 $\phi$ est une cons�quence logique de $\psi$ ssi tout mod�le de $\psi$ est un mod�le de
 $\phi$~:\quad $\displaystyle(\psi \models \phi) \Leftrightarrow (\forall A, A \models \psi \Rightarrow A\models \phi)$
\end{definition}

\begin{definition}[Equivalence]
 Deux formules $\phi$ et $\psi$ sont logiquement �quivalentes si elles admettent
les m�mes mod�les (i.e. $\phi\models\psi$ et $\psi\models\phi$).
\end{definition}

\begin{example}
\begin{itemize}
\item $(A \leftrightarrow B) \equiv (A \to B) \land (B \to A)$.
\item $(A \to B) \equiv (\lnot A \lor B)$.
\item $\lnot(A \land B) \equiv (\lnot A \lor \lnot B)$ et $\lnot(A \lor B) \equiv (\lnot A \land \lnot B)$
\item $\lnot\lnot A \equiv A$ et $A \lor (B \land C) \equiv (A \lor B) \land (A \lor C)$.
\end{itemize}
\end{example}

\end{frame}

\begin{frame}
\frametitle{Examples}

\begin{example}[Conception de circuit]
 Deux circuits logiques diff�rents
impl�mentent il la m�me fonction ?

$\leadsto$ montrer l'�quivalence de deux formules.
\end{example}

\begin{example}[Raisonnement]
 Peut on d�duire la connaissance $\phi$ d'une
base de connaissance $K$ ?

$\leadsto$  montrer que $K \models \phi$
\end{example}

\end{frame}

\begin{frame}
\frametitle{Mod�le (partiel) d'une formule}

\begin{definition}
 Un \textbf{cube} $c=l_1\land\ldots\land l_i$ est un mod�le (partiel) de $\phi$ ssi  $c\models \phi$. On dit que c'est un \textbf{impliquant} de $\phi$.

A toute affectation $A$ correspond un cube $\bigwedge_{l\in X^\pm, A(l) = \Vrai} l$.
\end{definition}

\begin{example}
$a\land b \land \neg c$ pour $A(a) = \Vrai, A(b) = \Vrai$ et $A(c) = \Faux$.
\end{example}

\begin{definition}
Un impliquant premier de $\phi$ est un impliquant minimal de $\phi$ au sens de $\models$ (il n'existe pas d'autre impliquant $c'$ de $\phi$ tel que $c\models c'$).
\end{definition}

\end{frame}

\begin{frame}
\frametitle{Diagnostic}

\begin{itemize}
 \item Le syst�me ne fonctionne pas ssi $S \cup O \cup OK$ n'a pas de mod�le.
 \item Quand il y a panne, trouver un diagnostic = trouver un mod�le de $S \cup O$
(avec un ou plusieurs litt�raux $\neg ok()$).
\item Cad trouver un cube minimal $D = \neg ok(a) \land \neg ok(b) \land\ldots$ tel que
$D \cup O \models S$
\item I.e. trouver un impliquant (minimal) de $S$ (modulo $O$)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Autres probl�mes}

De nombreux autres probl�mes d'int�r�t pratique se ram�nent �
la recherche de mod�les (ou � la preuve de leur absence) :

\begin{block}{par exemple}
\begin{itemize}
\item Une base de connaissances est-elle coh�rente ?\\
  $\Rightarrow$ l'ensemble de formules correspondant a-t-il un mod�le?
  
\item Tous les mod�les de $\Phi$ satisfont ils une propri�t� $\psi$ ?\\
  Oui si et seulement si $\Phi \cup \{\lnot \psi\} $ n'a pas de mod�le.
  
\item diagnostic ,v�rification de circuit (model checking),planification \ldots
\end{itemize}
\end{block}

\vfill
\centerline{\textbf{Algorithmes pour la recherche de mod�les}}
\vfill
\end{frame}


\begin{frame}
\frametitle{SAT: recherche na�ve de mod�les}

\begin{block}{Table de v�rit�}
 Construction d'une table donnant la valeur de $\phi$ pour chaque affectation possible.
Si $\phi$ contient $n$ variables, il y a $2^n$ affectations possible des
variables.
\end{block}

\begin{example}[$\phi=p \to (q \lor (r \leftrightarrow (r \to \lnot p)))$]\small
  \begin{tabular}{c|c|c||c|c|c|c|c}
    $p$ & $q$ & $r$ & $\lnot p$ & $r \to \lnot p$ & $r
    \leftrightarrow (r \to  \lnot p)$ & $q \lor (r
    \leftrightarrow (r \to  \lnot p))$ & $\phi$ \\ \hline
    $t$ & $t$ & $t$ & $f$ & $f$ & $f$ & $t$ & $t$ \\
    $t$ & $t$ & $f$ & $f$ & $t$ & $f$ & $t$ & $t$ \\
    $t$ & $f$ & $t$ & $f$ & $f$ & $f$ & $f$ & $f$ \\
    $t$ & $f$ & $f$ & $f$ & $t$ & $f$ & $f$ & $f$ \\
    $f$ & $t$ & $t$ & $t$ & $t$ & $t$ & $t$ & $t$ \\
    $f$ & $t$ & $f$ & $t$ & $t$ & $f$ & $t$ & $t$ \\
    $f$ & $f$ & $t$ & $t$ & $t$ & $t$ & $t$ & $t$ \\
    $f$ & $f$ & $f$ & $t$ & $t$ & $f$ & $f$ & $t$
  \end{tabular}
\end{example}

\end{frame}
\begin{frame}
\frametitle{SAT: arbres s�mantiques}

\begin{definition}
\begin{itemize}
 \item Un arbre binaire o� chaque arc issu d'un sommet est
�tiquet� par un litteral $p$ ou sa n�gation $\lnot p$ et correspondant �
l'affectation de $p$ � $\Vrai$ ou $\Faux$ respectivement.
\item Chaque branche contient une occurrence et une seule de chaque
variable.  
\item \'Evaluation de la valeur de v�rit� de la formule aux
feuilles. 
\end{itemize}

\end{definition}

\begin{example}
 Construction d'une partie de l'arbre pour $\phi = p \to (q \lor (r \leftrightarrow (r \to \lnot p)))$
\end{example}


Toujours $2^n$ feuilles \`a �valuer.
\end{frame}

\begin{frame}
\frametitle{Forme Normale Conjonctive}

Pour toute formule $\Phi$, il existe une FNC logiquement �quivalente � $\Phi$.

\begin{block}{R�gles de r��criture}
\begin{itemize}
\item $(A \leftrightarrow B) \leadsto (A \to B) \land (B \to A)$.
\item $(A \to B) \leadsto (\lnot A \lor B)$.
\item $\lnot(A \land B) \leadsto (\lnot A \lor \lnot B)$ et $\lnot(A \lor B) \leadsto (\lnot A \land \lnot B)$
\item $\lnot\lnot A \leadsto A$.
\item $A \lor (B \land C) \leadsto (A \lor B) \land (A \lor C)$.
\end{itemize}
\end{block}

\begin{example}
  $\begin{array}{rcl} (\lnot p \lor (q \land s))\limp \lnot r
    & \equiv & \lnot r \lor (p \land \lnot q) \lor (p \land \lnot s)
  \end{array}$
\end{example}
 
\end{frame}


\begin{frame}
\frametitle{SAT: algorithme de Quine et FNC}

Simplification des clauses quand un litt�ral $l$ est affact� � $\Vrai$:
\begin{definition}
Pour une formule
$\Psi$ et un litt�ral $l$ affect� \`a $\Vrai$, on note $\Psi_l$ la formule
obtenue \`a partir de $\Psi$ en~:
  \begin{enumerate}
  \item supprimant toutes les clauses contenant $l$;
  \item supprimant toutes les occurrences de $\overline{l}$.
  \end{enumerate}
\end{definition}

\begin{example}[$\{\neg p \lor \neg r \lor q,\neg q \lor p, q \lor p\}$]
\begin{itemize}
 \item Sur la branche $p : \Vrai$ : on raccourcit $\neg p \lor \neg r \lor q$, on supprime
 $\neg q \lor p$ et $q \lor p$. On obtient $\{\neg r \lor q\}$.
\item Sur la branche $p : \Faux$:  on raccourcit $\neg q \lor p$ et $q \lor p$, on supprime
$\neg p \lor \neg r \lor q$. On obtient $\{\neg q, q\}$
\end{itemize}
\end{example}

\end{frame}


\begin{frame}
\frametitle{Conditions d'arr�t}


\begin{block}{Arr�ts}
 \begin{itemize}
  \item Si la formule obtenue par simplification est vide elle est trivialement vraie. On peut 
s'arr�ter  car toutes les clauses sont satisfaites.
\item Si elle contient
une clause vide elle est trivialement fausse (car aucun litt�ral de la clause n'est vrai). On peut s'arr�ter.
 \end{itemize}
\end{block}

\begin{example}
 \begin{itemize}
  \item Pour $\Psi = \{\neg r \lor q\}$, sur la branche $q : \Vrai$ on obtient $\{\}$. La
formule a un mod�le.
\item pour $\Psi= \{\neg q, q\}$ : sur la branche $q : \Vrai$ on obtient $\{\bot\}$, sur la
branche $q : \Faux$ on obtient $\{\bot\}$. La formule ne peut avoir aucun mod�le.
 \end{itemize}
\end{example}



\end{frame}



\begin{frame}
\frametitle{Exemple}

\Treestyle{\vdist{5.5ex} \minsep{1em} \addsep{0pt}\nodesize{3.5ex}}
\begin{Tree}
               \DPfeuille{$p,r,q$}
             \DPunit{$\{\}$}
           \DPchoix u{$q:\Vrai$}
        \DPunit{$\{q\}$}
      \DPchoix g{$r:\Vrai$}
          \DPfeuille{$p,\lnot r$}
        \DPunit{$\{\}$}
      \DPchoix d{$r:\Faux$}
    \DPbin{$\{\lnot r\lor q\}$}
  \DPchoix g{$p:\Vrai$}
        \DPfeuille{$\{\bot\}$}
      \DPchoix u{$q:\Faux$}
    \DPunit{$\{\lnot q,q\}$}
  \DPchoix d{$p:\Faux$}
\DPbin{$\{\lnot p\lor\lnot r\lor q,\lnot q\lor p, q \lor p\}$}
\end{Tree}

\centerline{\usebox{\TeXTree}}
\end{frame}

\begin{frame}
\frametitle{En r�sume}

Construction d'un arbre binaire o� chaque arc issu
d'un sommet est �tiquet� par un litt�ral $p$ ou sa n�gation $\neg p$
et correspondant � l'affectation de $p$ � $\Vrai$ ou $\Faux$ respectivement.
Simplification de la base � chaque affectation.

\begin{block}{Cas d'un ensemble de clauses $\Psi $}
\begin{enumerate}
\item Si $\Psi = \{\} $ alors $\Psi$ est satisfait.
\item Si $\bot \in \Psi $ alors $\Psi$ est insatisfiable.
\item Sinon, on choisit $x$ dans $\Psi$, les mod�les de $\Psi $ sont alors :
  \begin{enumerate}
  \item Les mod�les contenant $x$ et un mod�le de $\Psi_x$;
  \item et les mod�les contenant $\lnot x$ et un mod�le de $\Psi_{\overline x}$
  \end{enumerate}
\end{enumerate}
\end{block}

On peut parcourir l'arbre en largeur, en profondeur\ldots La profondeur
d'abord a un avantage en espace. Toutes ces m�thodes sont, dans le
pire des cas exponentielle en temps (en $O(2^n)$).

\end{frame}


\begin{frame}
\frametitle{Algorithme na�f pour la recherche de mod�le}

\SetKwFor{Fonction}{Fonction}{}{}
\SetKwFunction{BT}{BT}
\begin{algorithm}[H]
\Fonction{\rm\BT{\rm$\Psi$: \textsf{ensemble de clauses}, $l$: \textsf{litt�ral}} : \textsf{bool{�}en}}{
   Retirer de $\Psi$ les clauses satisfaites par $l$\;
   Raccourcir dans $\Psi$ les clauses portant $\bar{l}$\;
  \lSi{$(\Psi = \varnothing)$}{\Retour{vrai}\;}
  \lSi{$(\Psi = \{\bot\}$}{\Retour{faux}\;}

  \KwSty{soit} $x$ une variable de $\Psi$\;
  \PourCh{$l \in \{x,\neg x\}$}
  {
      \lSi{\rm\BT{$\Psi,l$}}{\Retour{vrai}\;}
  }
\Retour{faux}\;
}
\end{algorithm}


\end{frame}


\begin{frame}
\frametitle{Parenth�se sur la notation de Landau}


\begin{block}{Notation de Landau}
Soit $g$ une fonction positive et $f$ une fonction quelconque.  On dit
que $g=O(f)$ ($g$ est en $O$ de $f$) ssi $\exists n_0, c$ tels que $\forall n >n_0
|g(n)| \leq c.f(n)$.
\end{block}

Utilis� pour majorer $g$ par $f$ asymptotiquement.  $g$
peut-\^etre le temps ce calcul (nombre d'op�rations �l�mentaires) ou
espace (nombre de bits) n�cessaire pour ex�cuter un
algorithme.\\[2mm]

Eg. temps utilis� pour simplifier $\Psi$ par $l$.

Eg. espace utilis� par l'algorithme de recherche (strat�gie en largeur ou en profondeur).

\end{frame}

\begin{frame}
\frametitle{Implicants premiers}

\begin{block}{Rappel}
\begin{itemize}
 \item Impliquant de $\phi$ : cube $c = l_1 \land\ldots\land l_i$ t.q. $c \models \phi$.
 \item  Impliquant premier = impliquant minimal de $\phi$.
\end{itemize}
\end{block}

\begin{example}
$$\begin{array}[t]{rcl}
\varphi & = & (\lnot p \lor r) \land
(p \lor \lnot q) \\
& \equiv & (\lnot p \land p)
  \lor(\lnot p \land \lnot q)
  \lor(r \land p)
  \lor(r \land \lnot q)
\end{array}$$
$\Rightarrow $3 impliquants premiers:\\
$\lnot p \land \lnot q$, $r \land p$ et $r \land \lnot q$.\\
$p$ n'est pas impliquant, $r\land p \land q$ n'est pas premier.
\end{example}

\end{frame}

\begin{frame}
\frametitle{Recherche d'implicants premiers}

On d�veloppe (encore) un arbre d'exploration : l'exploration des implicants.

$\Rightarrow$ Choix d'un litt�ral qu'on rejette (''Out``, pas de simplification) ou que l'on prend (''In``, on simplifie).

\begin{block}{Strat�gie}
 \begin{itemize}
  \item $l : Out$ avant $ l : In$
\item tests d'inclusion par rapport aux impliquants d�j� trouv�s pour
�viter les non-premiers
 \end{itemize}
\end{block}


\end{frame}

\begin{frame}
\frametitle{Exemple}

\Treestyle{\vdist{5ex} \minsep{1em} \addsep{0pt}\nodesize{3ex}}

\begin{Tree}
            \DPfeuille{Sol: $\lnot q \land \lnot p$}
          \DPchoix g{$\lnot p: In$}
                \DPfeuille{Sol: $\lnot q \land r$}
              \DPchoix u{$r:In$}
            \DPunit{$\{r\}$}
          \DPchoix d{$\lnot p: Out$}
        \DPbin{$\{\lnot p \lor r\}$}
      \DPchoix u{$\lnot q:In$}
    \DPunit{$\{\lnot p \lor r,\lnot q\}$}
  \DPchoix g{$p:Out$}
        \DPfeuille{Sol: $p \land r$}
      \DPchoix u{$r:In$}
    \DPunit{$\{r\}$}
  \DPchoix d{$p:In$}
\DPbin{$\{\lnot p \lor r, p \lor \lnot q\}$}
\end{Tree}
\hbox{\hspace{\leftdist}\usebox{\TeXTree}\hspace{\rightdist}}

\end{frame}

% \begin{frame}
% \frametitle{Impliquants restreints}
% 
% On ne s'int�resse qu'aux variables d'un sous-ensemble $L$
% \begin{definition}
%  \begin{itemize}
%   \item \textbf{$L$-impliquant (premier)} :
%   un impliquant (premier) dont tous les litt�raux sont
%   dans $L$.\\
%   Calcul: on �limine les litt�raux qui ne sont pas dans $L$ avant le
%   parcours d'arbre.
% \item \textbf{Impliquant $L$-restreint (premier)} :
%   un cube $C$ (minimal) tel qu'il existe un cube $D$ tel que $C\land D$
%   soit un impliquant.\\
%   Calcul: choix sur les litt�raux de $L$ avant les autres.
%  \end{itemize}
% \end{definition}



\begin{frame}
\frametitle{Contraintes et logique : exemple}

\begin{example} Une carte contenant $R$ r�gions \`a colorier:
  \begin{itemize}
  \item $C$ couleurs disponibles
  \item Deux r�gions adjacentes ne doivent pas avoir la m\^eme couleur
  \end{itemize}
  $\Rightarrow $Est-ce possible? Quelle couleur pour quelle r�gion~?
\end{example}
\begin{block}{Repr�sentation en logique propositionnelle}
  \begin{itemize}
  \item $C $ variables par r�gion:\\
    $I(coul_{r,c}) = \Vrai $ signifie que la r�gion
    $r$ a la couleur $c$
  \item[$+$] Chaque r�gion doit avoir une et une seule
    couleur\ldots
  \item[$+$] Deux r�gions adjacentes ne peuvent pas avoir
    la m\^eme couleur\ldots
  \item[$=$] Combien de clauses?
    %  $4R +CF $clauses, o\`u $F $est le nombre de fronti�res
  \end{itemize}
\end{block}

\end{frame}

\begin{frame}
\frametitle{Contraintes}

On pourrait n'avoir qu'une variable par r�gion, qui peut prendre $C$
valeurs: les couleurs possibles.
\begin{block}{Repr�sentation}
\begin{enumerate}
\item Une variable par r�gion, $r$
\item On associe un domaine \`a chaque variable contenant les
  couleurs, ici $\vert D_r \vert = C$.
\item On donne des contraintes liant ces variables
  \begin{itemize}
  \item pas besoin de contrainte pour repr�senter le fait que
    chaque r�gion a une et une seule couleur (c'est implicite)\\
  \item pour les fronti�res~:
    $r_i \neq r_j$ si $r_i$ et $r_j$ sont adjacentes.
  \end{itemize}
\item On cherche s'il y a une \emph{affectation}
  de valeurs aux variables qui satisfait les contraintes
\end{enumerate}
\end{block}

\end{frame}

\begin{frame}
\frametitle{D�finitions}

\begin{block}{R�seau de contraintes : un triplet $\langle X,D,C\rangle$}
\begin{itemize}
\item $X = \{x_1,\ldots,x_n\}$ est une s�quence finie de $n$ variables,
\item $D = \langle D_1,\ldots,D_n\rangle$ est la s�quence des $n$ \emph{domaines}
  associ�s aux variables de $X$ ($D_i$ est le domaine de $x_i$)
\item $C$ est un ensemble de $e$ \emph{contraintes}.  Une
  contrainte $R_S\in C$ (ou $\langle R,S\rangle$) est une relation $R$ sur les variables
 $S\subseteq X$ sp�cifiant les affectations autoris�es pour les  variables de $S$.
\end{itemize}
\end{block}

\begin{block}{Notations}
\begin{itemize}
\item On note $d$ la taille du plus grand domaine.

\item Une relation $R_S$ sur $S=\langle x_1,\ldots,x_k\rangle$ est
un sous-ensemble de $D_1\times\cdots\times D_k$ (arit� $k$, port�e $S$). Repr�sentations.

\item Un �l�ment de $R_S$ est un tuple $t_S$, ou \emph{affectation} de $S$.
\end{itemize}
\end{block}

\end{frame}


\begin{frame}
 \frametitle{Un exemple}

\begin{example}
  La firme Peunault va sortir un nouveau mod�le de voiture fabriqu�
  dans toute l'Europe~:
\begin{itemize}
\item les {\rm porti�res} et le {\rm capot} sont faits \`a Lille, o\`u le
  constructeur ne dispose que de peinture {\rm rose}, {\rm rouge} et
  {\rm noire}~;
\item la {\rm carrosserie} est faite \`a Hambourg, o\`u l'on a de la
  peinture {\rm blanche}, {\rm rose}, {\rm rouge} et {\rm noire}~;
\item les {\rm pare-chocs}, faits \`a Palerme, sont toujours {\rm
    blancs}~;
\item la b\^ache du {\rm toit-ouvrant}, qui est faite \`a Madrid, ne peut
  \^etre que {\rm rouge}~;
\item les {\rm enjoliveurs} sont faits \`a Ath�nes, o\`u l'on a de la
  peinture {\rm rose} et de la peinture {\rm rouge}.
\end{itemize}
\end{example}

\end{frame}

\begin{frame}
 \frametitle{Un exemple}

\begin{example}[Suite]
Le concepteur de la voiture impose quelques-uns de ses d�sirs quant \`a
l'agencement des couleurs pour cette voiture~:
\begin{itemize}
\item la {\rm carrosserie} doit \^etre de la m\^eme couleur que les {\rm
    porti�res}, les {\em porti�res} de la m\^eme couleur que le {\rm
    capot}, le {\em capot} de la m\^eme couleur que la {\em
    carrosserie}~;
\item les {\rm enjoliveurs}, les {\rm pare-chocs} et le {\rm
    toit-ouvrant} doivent \^etre plus clairs que la {\rm carrosserie}.
\end{itemize}
\end{example}
\end{frame}

\begin{frame}
 \frametitle{Mod�le CSP et graphe du probl�me}

\begin{itemize}
\item $X=\{x_1,x_2,x_3,x_4,x_5,x_6\}$
\item Valeurs $a,b,c,d$ pour {\em blanc, rose, rouge, noir}.
\end{itemize}

 \centerline{\includegraphics[width=6cm]{CSP}}

Le graphe (ou hyper-graphe) de sommets $X$ et (hyper)ar\^etes d�finies par les port�es des relations. Autre exemple: coloriage de carte/graphe.

\end{frame}

\begin{frame}
 \frametitle{Notions importantes}

Tuple $t_S$: affectation des variables de $S\subseteq X$.

\begin{block}{D�finitions}
 \begin{itemize}
\item La \textbf{projection} d'un tuple $t_S$ sur une s�quence de variables $S'$, $S'
\subseteq S$, est le tuple form� par les valeurs de $t_S$ correspondant \`a des
variables de $S'$, not�e $t_S[S']$.

\item \'Etant donn� une affectation $t_S$ et une contrainte $c=R_T$, on dit que
$c$ est \textbf{totalement affect�e} par $t_S$ quand $T \subseteq S$. Si de plus
$t_S[T] \notin R_T$, alors $t_S$ \textbf{viole} $c$ (sinon il satisfait $c$).

\item Une affectation $t_S$ est \textbf{coh�rente} si elle satisfait toutes
les contraintes qu'elle affecte compl�tement. Elle est compl�te si
$S=X$.

\item Une \textbf{solution} du r�seau de contraintes est une affectation
compl�te et coh�rente.
\end{itemize}
\end{block}

\end{frame}

\begin{frame}
 \frametitle{Le CSP (Constraint Satisfaction Problem)}

 \centerline{\includegraphics[width=4cm]{CSP}}
 
 
 \begin{example}[Solutions] $\{x_1\to a,x_2\to d,x_3\to d,x_4\to b,x_5\to
 d,x_6\to c\}$ et $\{x_1\to a,x_2\to d,x_3\to d,x_4\to c,x_5\to
 d,x_6\to c\}$.
\end{example}

Existence d'une solution : NP-complet.

Le probl�me CSP  : exhiber une solution s'il en existe
une ou prouver l'incoher�nce.

\end{frame}

\begin{frame}
 \frametitle{Logique propositionnelle et CSP}

\begin{block}{Th�or�me}
La logique propositionelle en FNC est un cas particulier de CSP.
\end{block}

Montrer que toute formule en FNC peut se repr�senter par un CSP en conservant l'ensemble des mod�les (solutions).

\end{frame}

% \begin{frame}
% \frametitle{R�seaux Bay�siens}
% \includegraphics[width=10cm]{RB}
% 
% \end{frame}
% 
% \begin{frame}
% \frametitle{R�seaux Bay�siens}
% 
% % Rappeler ce qu'est une dist. de probas ? 
% % une proba conditionnelle ?
% 
% \begin{definition}
%   Un r�seau bay�sien est un quadruplet $\langle X,D,E,P\rangle$~:
%   \begin{itemize}
%   \item $X = \langle x_1,\ldots,x_n\rangle$ est une s�quence finie de variables,
%   \item $D =\langle D_1,\ldots,D_n\rangle$ les domaines finis correspondants,
%   \item $E$ un ensemble d'arcs d�finissant un graphe orient� sans
%     circuit $(X,E)$ sur $X$. On note $pa(x_i)$ les parents de $x_i$.
%   \item $P$ contient pour chaque $x_i\in X$, une table de prob. 
%     sp�cifiant $P(x_i | pa(x_i))$.
%   \end{itemize}
% \end{definition}
% Un RB d�finit une distribution de probabilit�s sur $X$. Pour toute
% affectation $t$ de $X$~:
% \[P(t) = \prod_{i=1}^n P( x_i = t[x_i] | pa(x_i) = t[pa(x_i)])\]
% % montrer que ca d�finit une dist. de probas sur $X$ ?
% 
% \end{frame}
% 
% \begin{frame}
% \frametitle{Exemple}
% 
% \begin{example}
% \[
% \begin{array}{lrcl@{~~~}lrcl}
% A: & p(a)        & = & 0.01   & C & p(c|f)   & = & 0.1\\
%    &             &   &        &   & p(c|\lnot f) & = & 0.01\\\hline
% 
% B: & p(b|f)      & = & 0.6    & F & p(f)     & = & 0.5\\
%    & p(b|\lnot f)    & = & 0.3    &   &          &   & \\\hline
% 
% D: & p(d|b,o)    & = & 0.9    & S & p(s|a)   & = & 0.05\\
%    & p(d|\lnot b, e)  & = & 0.7    &   & p(s|\lnot a) & = & 0.01\\
%    & p(d|b, \lnot e)  & = & 0.8    &   &          &   & \\
%    & p(d|\lnot b, \lnot e) & = & 0.1    &   &          &   & \\\hline
% 
% O: & p(o|c,s)    & = & 1      & R & p(r|o)   & = & 0.98\\
%    & p(o|\lnot c, s)  & = & 1      &   & p(r|\lnot o)  & = & 0.05\\
%    & p(o|c, \lnot s)  & = & 1      &   &          &   & \\
%    & p(o|\lnot c, \lnot s) & = & 0      &   &          &   & 
%  \end{array}
% \]
% \end{example}
% 
% \end{frame}
% 
% \begin{frame}
% \frametitle{Exemple}
% 
% \includegraphics[width=10cm]{RB-scope}
% 
% Hyper-graphe sous-jacent. Notion de 2-section et de moralisation.
% \end{frame}
% 
% \begin{frame}
% \frametitle{Probl�me consid�r�}
% 
% \begin{block}{Maximum probability Explanation}
% \'Etant donn� un RB, le probl�me de d�cider de l'existence d'une
% affectation compl�te dont la probabilit� est sup�rieure \`a une seuil
% donne; est appel� probl�me MPE (maximum Probability Explanation).
% \end{block}
% 
% C'est un probl�me NP-difficile.
% 
% 
% \end{frame}


\begin{frame}
\frametitle{R�solution na\"\i ve de CSP}

Par exploration d'un arbre, comme pour les bases de clauses 
\begin{block}{Principe}
 \begin{itemize}
\item  \textbf{branchement par variable}~: les arcs issus d'un n{\oe}ud
  correspondent au choix d'affecter la variable choisie avec chacune de
  ses (au plus $d$) valeurs possibles. Chaque variable appara\^\i t une
  fois dans chaque branche.
\item \textbf{Simplifications}~: une relation quelconque n'a pas a priori les bonnes propri�t�s d'une clause. On attend que la contrainte soit totalement instanci�e. Si
on satisfait la contrainte, on peut l'oublier, si on la viole, on
s'arr\^ete.
\end{itemize}
 \end{block}

\end{frame}

\begin{frame}
\frametitle{R�solution na�ve de CSP}


\vfill
\hrule
\vfill

\SetKwFunction{BT}{BT}
\begin{algorithm}[H]
\For{\rm\BT{\rm$t$: \textsf{instanciation}} : \textsf{bool{�}en}}{
  \lSi{$(|t| = n)$}{\Retour{vrai}\;}
  \KwSty{soit} $x_i$ une variable non instanci{�}e\;
  \PourCh{$a\in D_i$}
  {
    $t' \gets t \cup \{x_i=a\}$\;
    \Si{$t'$ est coh{�}rent}
    {
      \lSi{\rm\BT{$t'$}}{\Retour{vrai}\;}
    }
  }
\Retour{faux}\;
}
\end{algorithm}

\end{frame}
%-------------------------------------------------------------------------------
\begin{frame}
\frametitle{Exemple}

 \centerline{\includegraphics[width=9.4cm]{BT}}

\end{frame}
%-------------------------------------------------------------------------------
\begin{frame}
\frametitle{Branchements}

Les types de branchements peuvent �tre un peu plus complexes :

\begin{block}{Branchements}
\begin{itemize}
\item \textbf{ par variable} \dots
\item \textbf{par r�futation}~: arbre binaire. Les arcs issus d'un
  n{\oe}ud correspondent au choix d'affecter une valeur \`a une variable
  ou de ne pas affecter cette valeur.
\item \textbf{par coupure d'intervalle}~: domaines ordonn�es, arbre
  binaire. Les arcs issus d'un n{\oe}ud correspondent au choix
  d'affecter une valeur inf�rieure ou �gale (resp. sup�rieure) \`a une
  valeur du domaine non maximum.
\end{itemize}
\end{block}

Le branchement par r�futation est le plus utilis� (bonnes propri�t�s th�oriques).
\end{frame}
%-------------------------------------------------------------------------------
\begin{frame}
\frametitle{Choix de variables/valeurs}

Dans tous les cas pr�c�dents, la taille de l'arbre explor� est
affect�e par l'ordre choisi pour instancier les variables et l'ordre
dans lequel les valeurs sont choisies.

\begin{block}{Principe g�n�ral}
\begin{itemize}
\item \textbf{Variable/Verticale}: la plus contrainte (pour rencontrer un �chec vite).
  Petits domaines, impliqu�es dans de nombreuses clauses/contraintes. 
\item \textbf{Valeur/Horizontale}: celle qui a le plus de raison de mener \`a une solution. Tr�s d�pendant de  l'application.
\end{itemize}
\end{block}

\end{frame}
%-------------------------------------------------------------------------------
\begin{frame}
\frametitle{Exemple}

\begin{example}
\centerline{\includegraphics[width=6cm]{Nadel}}
\end{example}

Des contraintes dures (peu de combinaisons autoris�es) et moins dures,
des domaines de taille variable.

\end{frame}
%--------------------------------------------------------------------------
\begin{frame}
\frametitle{Exemple}

\centerline{\includegraphics[width=10cm]{nadel1}}

Plus petits domaines d'abord $(x_1,x_2,x_3,x_4)$.
\end{frame}
%--------------------------------------------------------------------------
\begin{frame}
\frametitle{Exemple}

\centerline{\includegraphics[width=10cm]{nadel2}}

Plus grand nombre de contraintes d'abord  $(x_2,x_4,x_3,x_1)$.
\end{frame}
%--------------------------------------------------------------------------
\begin{frame}
\frametitle{Exemple}

\centerline{\includegraphics[width=10cm]{nadel3}}

Contrainte la plus dure d'abord $(x_4,x_3,x_2,x_1)$. 

\end{frame}


\begin{frame}
\frametitle{Probl�mes d'optimisation}


Les probl�mes SAT, CSP,\ldots sont des probl�mes de d�cision
(existe-t-il une solution, un mod�le, etc.).

Souvent, on cherche plut�t � r�soudre un probl�me d'optimisation:
\begin{block}{Optimisation}
 \begin{itemize}
 \item trouver une affectation qui minimise le nombre de clauses (MaxSAT) ou
de contraintes (MaxCSP) viol�es ;
\item ou la somme des poids des contraintes
viol�es, ou de fonctions de co�t, etc.
\end{itemize}
\end{block}

On peut �num�rer toutes les affectations compl�tes, en utilisant
un arbre d'exploration.
\end{frame}


\begin{frame}
\frametitle{Algorithme de ''Branch and Bound``}

Exploration arborescente avec des coupes (optimisation)~: algorithme de s�paration/�valuation.

\begin{block}{Principe}
\begin{enumerate}
 \item pour toute  affectation $t_S$ d'un ensemble de variables $S\subset X$ on peut calculer un minorant $lb(t_S)$ du co�t
 de chacune des affectations compl�tes �tendant
$t_S$.
\item Soit $ub$ le co�t de la meilleure affectation connue. Si $lb(t_S)
\geq ub$, il est inutile de chercher \`a �tendre $t_S$.
\end{enumerate}
\end{block}

\begin{example}[Majorant simple]
\begin{itemize}
\item $ub$: co\^ut de la meilleure affectation compl�te connue (pendant la recherche). Initialement $+\infty$ (ou recherche locale).
\item $lb(t)$: somme des poids des contraintes viol�es par $t$ (la
  somme est une fonction monotone).
\end{itemize}
\end{example}

\end{frame}

\begin{frame}
\frametitle{Branch and bound}

\SetKwFunction{BB}{DFBB}
\begin{algorithm}[H]
  \For{\rm\BB{\rm$t$: \textsf{affectation},  $ub$: \textsf{co�t} } : \textsf{co�t}}{
    $v \gets lb(t)$\;
    \Si{$v < ub$}
    {
      \lSi{$(|t| = n)$}{\Retour{$v$}\;}
      {
        \KwSty{soit} $x_i$ une variable non instanci{�}e\;
        \PourCh{$a\in D_i$}
        {
          $ub \gets \min(ub,$\BB{$t \cup \{x_i=a\},ub$}$)$\;
        } 
        \Retour{$ub$}\;
      }
    }
    \Retour{''Pas de solution''}\;
  }
\end{algorithm}
\end{frame}

%!! mettre un example (k col de k+1 clique ?)

% \begin{frame}
% \frametitle{Exemple}
% 
% \includegraphics[width=3cm]{RB-scope}$\small
% \begin{array}{lrcl@{~~~}lrcl}
% A: & p(a)        & = & 0.01   & C & p(c|f)   & = & 0.1\\
%    &             &   &        &   & p(c|\lnot f) & = & 0.01\\\hline
% B: & p(b|f)      & = & 0.6    & F & p(f)     & = & 0.5\\
%    & p(b|\lnot f)    & = & 0.3    &   &          &   & \\\hline
% D: & p(d|b,o)    & = & 0.9    & S & p(s|a)   & = & 0.05\\
%    & p(d|\lnot b, o)  & = & 0.7    &   & p(s|\lnot a) & = & 0.01\\
%    & p(d|b, \lnot o)  & = & 0.8    &   &          &   & \\
%    & p(d|\lnot b, \lnot o) & = & 0.1    &   &          &   & \\\hline
% O: & p(o|c,s)    & = & 1      & R & p(r|o)   & = & 0.98\\
%    & p(o|\lnot c, s)  & = & 1      &   & p(r|\lnot o)  & = & 0.05\\
%    & p(o|c, \lnot s)  & = & 1      &   &          &   & \\
%    & p(o|\lnot c, \lnot s) & = & 0      &   &          &   & 
%  \end{array}
% $
% 
% \end{frame}
% 
% \begin{frame}
% \frametitle{Exemple}
% 
% $e=\{\lnot a,f,\lnot b,d,r\}$
% 
% \centerline{\includegraphics[width=10cm]{RB-ex}}
% \end{frame}


\begin{frame}
\frametitle{Conclusion}

Dans les formalismes CSP, SAT, Max-CSP (et r�seaux bay�siens, champs markoviens, graphes de facteurs, PLNE\ldots)

\begin{itemize}
\item on d\'efinit implicitement un espace de recherche (l'ensemble des
  affectations possibles).
\item avec un ensemble de formules courtes, on d\'ecrit une distribution
  sur ces affectations (satisfaction ou non, degr\'e de violation, probabilit\'e\ldots).
\end{itemize}

On veut r�pondre � des requ�tes sur cette distribution d\'efinie implicitement ~: d\'ecision, recherche, optimisation, d\'enombrement.

On y r�pond par des \textbf{algorithmes tr�s similaires} : ici \textbf{branchement},
mais aussi recherche locale, production de cons�quences
\end{frame}

\end{document}
