\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{jmens}[2004/10/08
  Macros pour mat�riel p�dagogique v1.4]

\RequirePackage{ifthen}
\RequirePackage{color}
% La classe a trois options, suivant que la
% sortie est pr�vue pour des transparents,
% une video-projection ou sur papier.

\newif\ifjmnotes \jmnotesfalse
\newif\ifjmtransp \jmtranspfalse
\newif\ifjmvideo \jmvideofalse
\newif\ifjmpdftex
  \ifx\pdfoutput\undefined\jmpdftexfalse\else\jmpdftextrue\fi

\DeclareOption{notes}{\jmnotestrue}
\DeclareOption{video}{\jmvideotrue}
\DeclareOption{transparents}{\jmtransptrue}
\DeclareOption*{\ClassWarning{jmens}{Option inconnue: \CurrentOption}}
\ProcessOptions

\ifjmnotes \message{Notes de cours}
\else \ifjmtransp \message{Transparents}
 \else \ifjmvideo \message{Diapositives}
  \else \message{AUCUNE SORTIE SELECTIONNEE}
  \fi
 \fi
\fi


% Polices de caract�res
\ifjmnotes
  \input{size10.clo}
\else
 \input{size20.clo}
 \def\rmdefault{lcmss}        % no roman
 \def\sfdefault{lcmss}
 \def\ttdefault{lcmtt}
 \def\itdefault{sl}
 \def\sldefault{sl}
 \def\bfdefault{bx}
 % Pour l'environnement picture:
 \def\lnsize{ at 17pt }
 \font\tenln  =line10\lnsize
 \font\tenlnw  =linew10\lnsize
 \font\tencirc=lcircle10\lnsize
 \font\tencircw=lcirclew10\lnsize
 \thinlines
\fi
\normalsize
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}



% Sectionnement
% Un cours est divis� en chapitres, elles-m�mes
% divis�es en section puis en sous-section.
% Une sous-section correspond � un ou plusieurs
% transparents. Des sections peuvent regrouper
% un ou plusieurs chapitres.

\setcounter{secnumdepth}{3}
\newcounter{chapter}
\newcounter{part}
\newcounter{section}[chapter]
\newcounter{subsection}[section]
\renewcommand\thesubsection{\thesection.\arabic{subsection}}
\ifjmnotes
 \newcommand\part[1]{\stepcounter{part}
  {\Large\bfseries \noindent Partie \thepart
   \par\nobreak \interlinepenalty\@M \noindent #1}
  \par\nobreak\medskip}
 \newcommand\chapter[1]{\refstepcounter{chapter}\chaptermark{#1}
  \renewcommand\thesection{\thechapter.\arabic{section}}
  {\Large\bfseries \noindent Chapitre \thechapter
   \par\nobreak \interlinepenalty\@M\noindent#1}
  \par\nobreak\medskip}
 \newcommand\section{\@startsection{section}{1}{\z@}%
  {-3.5ex \@plus -1ex \@minus -.2ex}%
  {2.3ex \@plus.2ex}{\normalfont\large\bfseries}}
 \newcommand\subsection{\@startsection{subsection}{2}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\bfseries}}
 \newcommand\nosubsection{\relax}
 \let\nextslide\relax
\else
 \newcommand\part[1]{\relax
  \newpage
  \thispagestyle{empty}
  .\vfill\begin{center}
  {\bfseries #1}
  \end{center}\vfill.}
 \newcommand\chapter[1]{\part{#1}\chaptermark{#1}}
 \newcommand{\section}[1]{\newpage\par
  \gdef\titrecourant{#1}
  \stepcounter{section}
  \gdef\rightmark{#1}}
 \newcommand{\subsection}[1]{\newpage\par
  \stepcounter{subsection}
  \gdef\rightmark{\titrecourant\ (\arabic{subsection}): #1}
  \diapoinit  }
 \newcommand{\nosubsection}{\newpage\par
  \stepcounter{subsection}
  \gdef\rightmark{\titrecourant\ (\arabic{subsection})}
  \diapoinit}
 \let\nextslide\newpage
\fi
\let\diapoinit\relax

% Titre
\def\title#1{\gdef\@title{#1}}
\def\@title{\@latex@error{No \noexpand\title given}\@ehc}
\def\author#1{\gdef\@author{#1}}
\def\@author{\@latex@warning@no@line{No \noexpand\author given}}
\def\date#1{\gdef\@date{#1}}
\gdef\@date{\today}


\def\maketitle{%
  \newpage
  \null
  \vskip 2em%
  \begin{center}%
    {\LARGE \@title \par}%
    \vskip 1.5em%
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 1em%
    {\large \@date}%
  \end{center}%
  \par
  \vskip 1.5em}

% Mise en page
\newcommand\thepage{\arabic{page}}
%  En-tete et pied-de-page
\ifjmnotes
 \@twosidetrue
 \@twocolumntrue
 \def\ps@headings{%
  \def\@oddfoot{\small\hfil{\tt\jobname\ }- \today}
  \let\@evenfoot\@oddfoot
  \def\@evenhead{\thepage\hfil\slshape\leftmark}
  \def\@oddhead{{\slshape\rightmark}\hfil\thepage}
  \let\@mkboth\markboth
  \def\chaptermark##1{\markboth{\thechapter.\ ##1}{}}%
  \def\sectionmark##1{\markright{\thesection.\ ##1}}%
  \def\subsectionmark##1{}}
 \pagestyle{headings}
 \textheight25.7cm\headsep2em\headheight2ex
 \voffset-1in\advance\voffset1cm
 \textwidth19cm\oddsidemargin0pt\topmargin0pt
 \hoffset-1in\advance\hoffset1cm
 \columnsep2em
\else
 \def\ps@headings{%
  \def\@oddfoot{\small\@date\hfill\leftmark\ \thepage}
  \let\@evenfoot\oddfoot
  \def\@evenhead{\hfil\bfseries\rightmark\hfil}
  \let\@oddhead\@evenhead
  \let\@mkboth\markboth
  \def\chaptermark##1{\markboth{##1}{}}%
  \def\sectionmark##1{\markright{##1}}%
  \def\subsectionmark##1{\relax
    \gdef\rightmark{\titrecourant\ (\arabic{subsection}) : ##1}}}
  \pagestyle{headings}
\fi

% M�me mise en page pour transparents et video
\ifjmnotes \relax \else
  % Dimensions des en-tete et pied-de-page:
  \headsep2em\headheight2ex\topmargin0pt
  \footskip 1.5em
  % Dimension pour un �cran: 19x25cm
  % On laisse un offset de 0,5cm en haut et en bas
  % Il reste alors 18cm pour le document
  % (en-tete et pied-de-page compris)
  % POUR PDFTEX:
  \ifjmpdftex
    \pdfpageheight19cm
    \pdfpagewidth25cm
    \pdfvorigin0.5cm
    \textheight\pdfpageheight
    \advance\textheight-1cm
  % POUR DVI TEX:
  \else
    \textheight18cm
    \voffset-1in\advance\voffset0.5cm
  \fi
  % Il ne reste plus qu'� calculer la hauteur
  % restant pour le texte:
  \advance\textheight-\headheight
  \advance\textheight-\headsep
  \advance\textheight-\footskip
  % Pour la largeur, on laisse 1cm � gauche et � droite,
  % il n'y a pas de marge:
  \oddsidemargin0pt
  \ifjmpdftex
    \pdfhorigin1cm
    \textwidth\pdfpagewidth
    \advance\textwidth-2cm
  \else
    \textwidth23cm
    \hoffset-1in
    \advance\hoffset1cm
  \fi
\fi

% Paragraphes:
\parskip.5ex plus1ex minus.1ex
\parindent0pt
\renewcommand\newtheorem[3][]{%
  \newenvironment{#2}{\paragraph*{#3}#1}{\par\medskip}}
\newcommand\paragraph{\@startsection{paragraph}{4}{\z@}
   {.75ex \@plus1ex \@minus.2ex}{-1em}
   {\normalfont\normalsize\bfseries}}

% Les enumerations

\newenvironment{description}
  {\list{}{\labelwidth\z@\itemindent-\leftmargin
    \labelsep1ex\let\makelabel\descriptionlabel}
    \parsep0pt\partopsep0pt\itemsep0pt\topsep0pt}
  {\endlist}
\newcommand*{\descriptionlabel}[1]{\hspace\labelsep
  \normalfont\bfseries #1:}
\newenvironment{quote}
  {\list{}{\rightmargin\leftmargin}\item[]}
  {\endlist}
\setlength\leftmargini   {4ex}
\setlength\leftmarginii  {3ex}
\setlength\leftmarginiii {2ex}
\setlength\leftmarginiv  {1ex}
\setlength\leftmarginv   {1ex}
\setlength\leftmarginvi  {1ex}
\def\@listi{\leftmargin\leftmargini
            \ifjmpdftex\parsep0pt\else\parsep-\parskip\fi
            \topsep 0pt
            \itemsep0pt
            \partopsep 0pt}
\def\@listii{\leftmargin\leftmarginii
             \labelwidth\leftmarginii
             \advance\labelwidth-\labelsep}
\def\@listiii{\leftmargin\leftmarginiii
              \labelwidth\leftmarginiii
              \advance\labelwidth-\labelsep}
\def\@listiv{\leftmargin\leftmarginiv
             \labelwidth\leftmarginiv
             \advance\labelwidth-\labelsep}
\def\@listv{\leftmargin\leftmarginv
            \labelwidth\leftmarginv
            \advance\labelwidth-\labelsep}
\def\@listvi{\leftmargin\leftmarginvi
             \labelwidth\leftmarginvi
             \advance\labelwidth-\labelsep}
\leftmargin\leftmargini
\labelwidth\leftmargini\advance\labelwidth-\labelsep
\renewcommand\theenumi{\@arabic\c@enumi}
\renewcommand\theenumii{\@alph\c@enumii}
\renewcommand\theenumiii{\@roman\c@enumiii}
\renewcommand\theenumiv{\@Alph\c@enumiv}
\newcommand\labelenumi{\theenumi.}
\newcommand\labelenumii{(\theenumii)}
\newcommand\labelenumiii{\theenumiii.}
\newcommand\labelenumiv{\theenumiv.}
\renewcommand\p@enumii{\theenumi}
\renewcommand\p@enumiii{\theenumi(\theenumii)}
\renewcommand\p@enumiv{\p@enumiii\theenumiii}
\newcommand\labelitemi{$\m@th\bullet$ }
\newcommand\labelitemii{\normalfont\bfseries \textendash }
\newcommand\labelitemiii{$\m@th\ast$ }
\newcommand\labelitemiv{$\m@th\cdot$ }

% Tableaux:
\arrayrulewidth .5pt

 \@lowpenalty   51
 \@medpenalty  151
 \@highpenalty 301

%\RequirePackage[frenchb]{babel}
% Extraits de frenchb: les guillemets, les nombres et la date:
\DeclareTextCommand{\guillemotleft}{OT1}{\hbox{%
  \fontencoding{U}\fontfamily{lasy}\selectfont(\kern-0.20em(}}%
\DeclareTextCommand{\guillemotright}{OT1}{\hbox{%
  \fontencoding{U}\fontfamily{lasy}\selectfont)\kern-0.20em)}}
\DeclareRobustCommand*{\begin@guill}{\leavevmode
  \guillemotleft\penalty\@M\guill@spacing}
\DeclareRobustCommand*{\end@guill}{\ifdim\lastskip>\z@\unskip\fi
  \penalty\@M\guill@spacing\guillemotright\relax}
\def\guill@spacing{\penalty\@M\hskip.8\fontdimen2\font
  plus.3\fontdimen3\font minus.8\fontdimen4\font}
\newcommand{\og}{\begin@guill}
\newcommand{\fg}{\end@guill}
\def\today{\number\day
    \ifnum1=\day er\fi
    \space \ifcase\month
    \or janvier\or f\'evrier\or mars\or avril\or mai\or juin\or
    juillet\or ao\^ut\or septembre\or octobre\or novembre\or
    d\'ecembre\fi
    \space \number\year}
\let\nombre\relax % definition de frenchb trop compliqu�e...

% titres d'environnements flottants (article.cls)
\newlength\abovecaptionskip
\newlength\belowcaptionskip
\setlength\abovecaptionskip{10\p@}
\setlength\belowcaptionskip{0\p@}
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{#1: #2}%
  \ifdim \wd\@tempboxa >\hsize
    #1: #2\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}

\newtheorem{example}{Exemple}
\newtheorem{exercise}{Exercice}
\newtheorem{remark}{Remarque}
\newtheorem{propriete}{Propri\'et\'e}
\newtheorem{corollary}{Corollaire}
\newtheorem[\couleurrougeth]{proposition}{Proposition}
\newtheorem[\couleurrougeth]{theorem}{Th\'eor\`eme}
\newtheorem{definition}{D\'efinition}

\ifjmvideo
  \newcommand\couleurbleudefs{\color[rgb]{0,0,1}}
  \newcommand\couleurrougeth{\color[rgb]{1,0,0}}
  \pagecolor[rgb]{0.3,0.8,0.4}
\else
  \let\couleurbleudefs\relax
  \let\couleurrougeth\relax
\fi


\endinput
